# coding: utf-8
from sqlalchemy import CHAR, Column, DECIMAL, Date, DateTime, Enum, Float, ForeignKey, Index, String, TIMESTAMP, Table, Text, Time, text
from sqlalchemy.dialects.mysql import BIGINT, DECIMAL, INTEGER, LONGTEXT, MEDIUMTEXT, SMALLINT, TINYINT, TINYTEXT, VARCHAR
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
metadata = Base.metadata


class ACCAPIACCOUNT(Base):
    __tablename__ = 'ACCAPI_ACCOUNTS'

    accapi_id = Column(INTEGER(10), primary_key=True)
    accsw_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_SOFTWARE')
    accapi_title = Column(String(100), nullable=False, server_default=text("''"))
    accapi_account_id = Column(String(255), nullable=False, server_default=text("''"))
    accapi_account_title = Column(String(85), nullable=False, server_default=text("''"))
    accapi_username = Column(String(255), nullable=False, server_default=text("''"))
    accapi_password = Column(Text, nullable=False, comment='encrypt')
    accapi_environment = Column(Enum('live', 'staging'), nullable=False, server_default=text("'live'"))
    auth_request_token = Column(String(1023), nullable=False, server_default=text("''"))
    auth_request_secret = Column(String(85), nullable=False, server_default=text("''"))
    auth_token = Column(String(1023), nullable=False, server_default=text("''"))
    auth_verifier = Column(String(85), nullable=False, server_default=text("''"))
    auth_access_token = Column(String(1023), nullable=False, server_default=text("''"))
    auth_access_secret = Column(String(85), nullable=False, server_default=text("''"))
    auth_session_handle = Column(String(85), nullable=False, server_default=text("''"))
    auth_org = Column(String(85), nullable=False, server_default=text("''"))
    auth_refresh_token = Column(String(1023), nullable=False, server_default=text("''"))
    date_token_requested = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_token_authorised = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_token_expires = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_session_expires = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    accapi_active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    config_group_id = Column(TINYINT(1), server_default=text("'0'"))
    sortorder = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    date_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    date_rate_limit_retry = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_last_reconnect_attempted = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))


class ACCAPICALLLOG(Base):
    __tablename__ = 'ACCAPI_CALL_LOG'

    id = Column(INTEGER(11), primary_key=True)
    accapi_id = Column(INTEGER(11), nullable=False)
    url = Column(String(85), nullable=False)
    request_content = Column(MEDIUMTEXT, nullable=False)
    response_header = Column(MEDIUMTEXT)
    response_content = Column(MEDIUMTEXT)
    created_ts = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    updated_ts = Column(TIMESTAMP)


class ACCAPICONFIG(Base):
    __tablename__ = 'ACCAPI_CONFIGS'

    accapi_id = Column(INTEGER(10), primary_key=True, nullable=False, server_default=text("'0'"), comment='FK: ACCAPI_ACCOUNTS')
    acccfg_name = Column(String(50), primary_key=True, nullable=False, server_default=text("''"))
    acccfg_value = Column(MEDIUMTEXT, nullable=False)


class ACCAPIDEFAULTTASK(Base):
    __tablename__ = 'ACCAPI_DEFAULT_TASKS'

    accqt_id = Column(INTEGER(10), primary_key=True, server_default=text("'0'"))
    accsw_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    acctask_module = Column(String(30), nullable=False, server_default=text("''"))
    acctask_name = Column(String(100), nullable=False, server_default=text("''"))
    acctask_priority = Column(String(30), nullable=False, server_default=text("''"))
    schedule_text = Column(String(30), nullable=False, server_default=text("''"))


class ACCAPIEXPENSEMAPPING(Base):
    __tablename__ = 'ACCAPI_EXPENSE_MAPPING'

    financial_account_id = Column(INTEGER(11), primary_key=True, nullable=False)
    acc_account_id = Column(INTEGER(11), primary_key=True, nullable=False)


class ACCAPIEXPLINKINVOICE(Base):
    __tablename__ = 'ACCAPI_EXPLINK_INVOICES'
    __table_args__ = (
        Index('accapi_exported', 'accapi_id', 'date_exported'),
        Index('exported_stock_adjustments', 'accapi_id', 'stock_adjustment_id'),
        Index('exported_orders', 'accapi_id', 'order_record_id'),
        Index('exported_rmas', 'accapi_id', 'rma_id'),
        Index('exported_purchase_orders', 'accapi_id', 'purchase_order_id')
    )

    accexlnk_invoice_id = Column(INTEGER(10), primary_key=True)
    parent_accexlnk_invoice_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    accapi_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_ACCOUNTS')
    order_record_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ORDERS')
    rma_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: RMA')
    warehouse_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    purchase_order_id = Column(INTEGER(11), index=True)
    purchase_order_expense_id = Column(INTEGER(11), index=True)
    stock_adjustment_id = Column(INTEGER(11), index=True)
    group_by_type = Column(Enum('', 'invoice', 'date'), nullable=False, server_default=text("''"))
    saved_export_date = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"), comment='Save value of date_exported on FK table')
    date_exported = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    export_status = Column(Enum('', 'linked', 'ready', 'exported'), nullable=False, server_default=text("''"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class ACCAPIEXPLINKPAYMENT(Base):
    __tablename__ = 'ACCAPI_EXPLINK_PAYMENTS'

    accexlnk_payment_id = Column(INTEGER(10), primary_key=True)
    parent_accexlnk_payment_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    accapi_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_ACCOUNTS')
    payment_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ORDERPAYMENTS')
    warehouse_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    group_by_type = Column(Enum('', 'payment', 'date'), nullable=False, server_default=text("''"))
    saved_export_date = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"), comment='Save value of date_exported on FK table')
    date_exported = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class ACCAPIEXPORTADDRESS(Base):
    __tablename__ = 'ACCAPI_EXPORT_ADDRESSES'

    accex_addr_id = Column(INTEGER(10), primary_key=True)
    accex_contact_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_EXPORT_CONTACTS')
    addr_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ADDRESSBOOK')
    addr_code = Column(String(50), nullable=False, index=True, server_default=text("''"))
    addr_type = Column(String(50), nullable=False, server_default=text("''"))
    addr_street1 = Column(String(50))
    addr_street2 = Column(String(50))
    addr_street3 = Column(String(50))
    addr_street4 = Column(String(50))
    addr_city = Column(String(50))
    addr_state = Column(String(50))
    addr_zip = Column(String(15))
    addr_country = Column(CHAR(2))
    addr_phone = Column(String(50))
    addr_phone2 = Column(String(50))
    addr_phone3 = Column(String(50))
    addr_fax = Column(String(50))
    addr_email = Column(String(255))
    addr_website = Column(String(255))
    addr_contact_name = Column(String(50))
    addr_contact_title = Column(String(15))
    date_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class ACCAPIEXPORTCONTACT(Base):
    __tablename__ = 'ACCAPI_EXPORT_CONTACTS'

    accex_contact_id = Column(INTEGER(10), primary_key=True)
    accim_contact_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_IMPORT_CONTACTS')
    accapi_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_ACCOUNTS')
    user_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: USERS')
    contact_code = Column(String(50), nullable=False, index=True, server_default=text("''"))
    contact_company_name = Column(String(50))
    contact_first_name = Column(String(50))
    contact_last_name = Column(String(50))
    contact_email = Column(String(50))
    contact_bank_info = Column(String(50))
    contact_tax_number = Column(String(50))
    contact_tax_code = Column(String(50))
    contact_freight_tax_code = Column(String(50))
    contact_phone = Column(String(50))
    contact_fax = Column(String(50))
    contact_website = Column(String(50))
    contact_type = Column(String(50))
    contact_group = Column(String(50))
    contact_currency = Column(CHAR(3))
    contact_terms = Column(String(50))
    contact_discount = Column(DECIMAL(5, 2))
    contact_notes = Column(Text)
    contact_credit_onhold = Column(Enum('', 'n', 'y'), nullable=False, server_default=text("''"))
    contact_active = Column(Enum('n', 'd', 'y'), nullable=False, server_default=text("'n'"))
    contact_role = Column(Enum('', 'administrator', 'customer', 'supplier', 'warehouse'), nullable=False, server_default=text("''"))
    contact_misctext1 = Column(Text)
    contact_misctext2 = Column(Text)
    contact_misctext3 = Column(Text)
    remote_record_id = Column(String(50))
    date_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    last_prepared = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    last_exported = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    last_export_status = Column(Enum('', 'success', 'error'), nullable=False, server_default=text("''"))
    last_export_error = Column(Text)


class ACCAPIEXPORTCRALLOCATION(Base):
    __tablename__ = 'ACCAPI_EXPORT_CR_ALLOCATIONS'

    accex_allocation_id = Column(INTEGER(10), primary_key=True)
    accapi_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_ACCOUNTS')
    payment_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ORDERPAYMENTS')
    invoice_number = Column(String(50), nullable=False, index=True, server_default=text("''"))
    creditnote_record_id = Column(String(50), nullable=False, index=True, server_default=text("''"), comment='FK: ACCAPI_IMPORT_INVOICES.remote_record_id')
    creditnote_number = Column(String(50), nullable=False, index=True, server_default=text("''"))
    date_allocated = Column(Date)
    currency_code = Column(CHAR(3))
    currency_rate = Column(DECIMAL(10, 4))
    allocation_status = Column(String(50), nullable=False, server_default=text("''"))
    allocation_amount = Column(DECIMAL(14, 2))
    allocation_type = Column(String(50))
    allocation_role = Column(Enum('', 'over_payment', 'credit_note', 'credit_account'), nullable=False, index=True, server_default=text("''"))
    allocation_note = Column(Text)
    remote_record_id = Column(String(50))
    date_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    last_prepared = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    last_exported = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    last_reconciled = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    last_export_status = Column(Enum('', 'success', 'error'), nullable=False, server_default=text("''"))
    last_export_error = Column(Text)


class ACCAPIEXPORTINVOICELINE(Base):
    __tablename__ = 'ACCAPI_EXPORT_INVOICELINES'

    accex_invln_id = Column(INTEGER(10), primary_key=True)
    accex_invoice_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_EXPORT_INVOICES')
    accex_item_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_EXPORT_ITEMS')
    accex_invln_counter = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    invln_remote_id = Column(String(50))
    item_code = Column(String(50), nullable=False, index=True, server_default=text("''"))
    invln_description = Column(String(255))
    invln_quantity = Column(INTEGER(11))
    invln_unit_price = Column(DECIMAL(20, 8))
    invln_unit_price_extax = Column(DECIMAL(20, 8))
    invln_taxtotal = Column(DECIMAL(20, 8))
    invln_subtotal = Column(DECIMAL(20, 8))
    invln_cubic = Column(DECIMAL(15, 9))
    invln_weight = Column(DECIMAL(12, 3))
    invln_discount_rate = Column(DECIMAL(5, 2))
    invln_taxcode = Column(String(50), nullable=False, server_default=text("''"))
    invln_tax_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    invln_acc = Column(String(50), nullable=False, server_default=text("''"))
    invln_job = Column(String(50), nullable=False, server_default=text("''"))
    invln_location = Column(String(50), nullable=False, server_default=text("''"))
    date_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class ACCAPIEXPORTINVOICE(Base):
    __tablename__ = 'ACCAPI_EXPORT_INVOICES'

    accex_invoice_id = Column(INTEGER(10), primary_key=True)
    accim_invoice_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_IMPORT_INVOICES')
    accapi_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_ACCOUNTS')
    accexlnk_invoice_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_EXPLINK_INVOICES')
    accex_contact_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_EXPORT_CONTACTS')
    accex_salesperson_contact_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    invoice_number = Column(String(50), nullable=False, index=True, server_default=text("''"))
    original_invoice_number = Column(String(50), nullable=False, server_default=text("''"))
    invoice_po = Column(String(50))
    contact_code = Column(String(50), index=True)
    warehouse_code = Column(String(50), index=True)
    invoice_type = Column(String(50))
    invoice_tax_type = Column(Enum('', 'inc', 'exc'), nullable=False, server_default=text("''"))
    invoice_template = Column(String(50))
    bill_company_name = Column(String(50))
    bill_first_name = Column(String(50))
    bill_last_name = Column(String(50))
    bill_street1 = Column(String(50))
    bill_street2 = Column(String(50))
    bill_state = Column(String(50))
    bill_zip = Column(String(15))
    bill_city = Column(String(50))
    bill_country = Column(CHAR(2))
    ship_company_name = Column(String(50))
    ship_first_name = Column(String(50))
    ship_last_name = Column(String(50))
    ship_street1 = Column(String(50))
    ship_street2 = Column(String(50))
    ship_street3 = Column(String(50))
    ship_street4 = Column(String(50))
    ship_city = Column(String(50))
    ship_state = Column(String(50))
    ship_zip = Column(String(15))
    ship_country = Column(CHAR(2))
    date_invoiced = Column(Date)
    date_due = Column(Date)
    date_required = Column(DateTime)
    date_completed = Column(DateTime)
    currency_code = Column(CHAR(3))
    currency_rate = Column(DECIMAL(10, 4))
    invoice_status = Column(String(50))
    invoice_freight = Column(DECIMAL(14, 2))
    invoice_freight_extax = Column(DECIMAL(14, 2))
    invoice_subtotal = Column(DECIMAL(14, 2))
    invoice_subtotal_extax = Column(DECIMAL(14, 2))
    invoice_grandtotal = Column(DECIMAL(14, 2))
    invoice_taxtotal = Column(DECIMAL(14, 2))
    amount_paid = Column(DECIMAL(14, 2))
    overpaid_credit_used = Column(DECIMAL(14, 2))
    invoice_freight_tax_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    invoice_terms = Column(String(50))
    invoice_sent = Column(Enum('', 'n', 'y'))
    invoice_role = Column(Enum('', 'invoice', 'credit', 'adjustment', 'purchase', 'overpayment', 'stock journal', 'purchase order', 'stock adjustment', 'purchase order expense'), nullable=False, index=True)
    invoice_comment = Column(Text)
    invoice_note = Column(Text)
    invoice_misctext1 = Column(Text)
    invoice_misctext2 = Column(Text)
    invoice_misctext3 = Column(Text)
    remote_record_id = Column(String(50))
    date_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    last_prepared = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    last_exported = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    last_reconciled = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    last_export_status = Column(Enum('', 'success', 'error'), nullable=False, server_default=text("''"))
    last_export_error = Column(Text)
    track_cogs_bill = Column(TINYINT(1), nullable=False, server_default=text("'0'"))


class ACCAPIEXPORTITEM(Base):
    __tablename__ = 'ACCAPI_EXPORT_ITEMS'

    accex_item_id = Column(INTEGER(10), primary_key=True)
    accim_item_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_IMPORT_ITEMS')
    accapi_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_ACCOUNTS')
    inventory_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: INVENTORY')
    item_code = Column(String(50), nullable=False, index=True, server_default=text("''"))
    item_name = Column(String(255))
    item_base_name = Column(String(255), nullable=False, server_default=text("''"))
    item_purchase_cost = Column(DECIMAL(12, 2))
    item_default_cost = Column(DECIMAL(9, 2))
    item_purchase_taxcode = Column(String(50))
    item_purchase_acc = Column(String(50))
    item_price01 = Column(DECIMAL(12, 2))
    item_price02 = Column(DECIMAL(12, 2))
    item_price03 = Column(DECIMAL(12, 2))
    item_price04 = Column(DECIMAL(12, 2))
    item_price05 = Column(DECIMAL(12, 2))
    item_price06 = Column(DECIMAL(12, 2))
    item_price07 = Column(DECIMAL(12, 2))
    item_price08 = Column(DECIMAL(12, 2))
    item_price09 = Column(DECIMAL(12, 2))
    item_price10 = Column(DECIMAL(12, 2))
    item_sales_taxcode = Column(String(50))
    item_sales_acc = Column(String(50))
    item_asset_acc = Column(String(50))
    item_income_acc = Column(String(50))
    item_barcode01 = Column(String(50))
    item_barcode02 = Column(String(50))
    item_barcode03 = Column(String(50))
    item_notes = Column(Text)
    item_length = Column(DECIMAL(12, 3))
    item_width = Column(DECIMAL(12, 3))
    item_height = Column(DECIMAL(12, 3))
    item_cubic = Column(DECIMAL(15, 9))
    item_weight = Column(DECIMAL(12, 3))
    item_group = Column(String(50))
    item_unit = Column(String(50))
    item_pack_size = Column(DECIMAL(12, 3))
    item_reorder_qty = Column(INTEGER(10))
    item_stock_alert01 = Column(INTEGER(10))
    item_stock_alert02 = Column(INTEGER(10))
    item_supplier = Column(String(50))
    item_supplier_code = Column(String(50))
    item_pick_location = Column(String(50))
    item_is_component = Column(Enum('', 'n', 'y'), nullable=False, server_default=text("''"))
    item_is_kitting = Column(Enum('', 'n', 'y'), nullable=False, server_default=text("''"))
    item_is_variation = Column(Enum('', 'n', 'y'), nullable=False, server_default=text("''"))
    item_active = Column(Enum('n', 'd', 'y'), nullable=False, server_default=text("'n'"))
    item_is_inventoried = Column(Enum('n', 'y'))
    item_is_service = Column(Enum('n', 'y'), server_default=text("'n'"))
    item_is_bought = Column(Enum('n', 'y'))
    item_is_sold = Column(Enum('n', 'y'))
    item_misctext1 = Column(Text)
    item_misctext2 = Column(Text)
    item_misctext3 = Column(Text)
    item_misctext4 = Column(Text)
    item_misctext5 = Column(Text)
    item_misctext6 = Column(Text)
    item_misctext7 = Column(Text)
    item_misctext8 = Column(Text)
    item_misctext9 = Column(Text)
    item_miscbool1 = Column(Enum('n', 'y'))
    item_attributes = Column(MEDIUMTEXT)
    item_options = Column(MEDIUMTEXT)
    item_specifics = Column(MEDIUMTEXT)
    item_mlp = Column(MEDIUMTEXT)
    item_images = Column(MEDIUMTEXT)
    error_count = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    remote_record_id = Column(String(50))
    date_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    last_prepared = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    last_exported = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    last_export_status = Column(Enum('', 'success', 'error'), nullable=False, server_default=text("''"))
    last_export_error = Column(Text)


class ACCAPIEXPORTPAYMENT(Base):
    __tablename__ = 'ACCAPI_EXPORT_PAYMENTS'

    accex_payment_id = Column(INTEGER(10), primary_key=True)
    accim_payment_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_IMPORT_PAYMENTS')
    accapi_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_ACCOUNTS')
    accex_invoice_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_IMPORT_INVOICES')
    accex_contact_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_IMPORT_CONTACTS')
    accexlnk_payment_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_EXPLINK_PAYMENTS')
    invoice_number = Column(String(50), nullable=False, index=True, server_default=text("''"))
    invoice_po = Column(String(50))
    contact_code = Column(String(50))
    payment_reference = Column(String(50), nullable=False, index=True, server_default=text("''"))
    payment_description = Column(String(100))
    date_paid = Column(Date)
    currency_code = Column(CHAR(3))
    currency_rate = Column(DECIMAL(10, 4))
    payment_status = Column(String(50))
    payment_amount = Column(DECIMAL(14, 2))
    overpaid_amount = Column(DECIMAL(14, 2))
    payment_acc = Column(String(50))
    payment_type = Column(String(50))
    payment_note = Column(Text)
    payment_role = Column(Enum('', 'payment', 'adjustment', 'refund', 'credit'), nullable=False, server_default=text("''"))
    payment_misctext1 = Column(Text)
    payment_misctext2 = Column(Text)
    payment_misctext3 = Column(Text)
    payment_misctext4 = Column(Text)
    payment_misctext5 = Column(Text)
    remote_record_id = Column(String(50))
    date_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    last_prepared = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    last_exported = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    last_reconciled = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    last_export_status = Column(Enum('', 'success', 'error'), nullable=False, server_default=text("''"))
    last_export_error = Column(Text)


class ACCAPIIMPORTACCOUNT(Base):
    __tablename__ = 'ACCAPI_IMPORT_ACCOUNTS'

    acc_account_id = Column(INTEGER(10), primary_key=True)
    accapi_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_ACCOUNTS')
    acc_account_ref = Column(String(50), nullable=False, index=True, server_default=text("''"))
    acc_account_parent_ref = Column(String(50))
    acc_account_code = Column(String(50), index=True)
    acc_account_name = Column(String(50))
    acc_account_type = Column(String(50))
    acc_account_class = Column(String(50))
    is_header_acc = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    is_asset_acc = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    is_expense_acc = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    is_income_acc = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    is_costofsales_acc = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    is_bank_acc = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    acc_account_note = Column(String(255))
    acc_account_active = Column(Enum('n', 'd', 'y'), nullable=False, server_default=text("'n'"))
    acc_account_is_detail = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))


class ACCAPIIMPORTADDRESS(Base):
    __tablename__ = 'ACCAPI_IMPORT_ADDRESSES'

    accim_addr_id = Column(INTEGER(10), primary_key=True)
    accim_contact_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_IMPORT_CONTACTS')
    addr_code = Column(String(50), nullable=False, index=True, server_default=text("''"))
    addr_type = Column(String(50), nullable=False, server_default=text("''"))
    addr_first_name = Column(String(255))
    addr_last_name = Column(String(255))
    addr_company = Column(String(255))
    addr_street1 = Column(String(50))
    addr_street2 = Column(String(50))
    addr_street3 = Column(String(50))
    addr_street4 = Column(String(50))
    addr_city = Column(String(50))
    addr_state = Column(String(50))
    addr_zip = Column(String(15))
    addr_country = Column(CHAR(2))
    addr_phone = Column(String(50))
    addr_phone2 = Column(String(50))
    addr_phone3 = Column(String(50))
    addr_fax = Column(String(50))
    addr_email = Column(String(255))
    addr_website = Column(String(255))
    addr_contact_name = Column(String(50))
    addr_contact_title = Column(String(15))
    last_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class ACCAPIIMPORTCONTACT(Base):
    __tablename__ = 'ACCAPI_IMPORT_CONTACTS'

    accim_contact_id = Column(INTEGER(10), primary_key=True)
    accapi_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_ACCOUNTS')
    contact_code = Column(String(50), nullable=False, index=True, server_default=text("''"))
    contact_company_name = Column(String(50))
    contact_first_name = Column(String(50))
    contact_last_name = Column(String(50))
    contact_email = Column(String(50))
    contact_currency_code = Column(String(3), server_default=text("''"))
    contact_bank_info = Column(String(50))
    contact_tax_number = Column(String(50))
    contact_tax_code = Column(String(50))
    contact_freight_tax_code = Column(String(50))
    contact_phone = Column(String(50))
    contact_fax = Column(String(50))
    contact_website = Column(String(50))
    contact_type = Column(String(50))
    contact_group = Column(String(50))
    contact_currency = Column(CHAR(3))
    contact_terms = Column(String(50))
    contact_discount = Column(DECIMAL(5, 2))
    contact_notes = Column(Text)
    contact_balance_outstanding = Column(DECIMAL(14, 2))
    contact_balance_overdue = Column(DECIMAL(14, 2))
    contact_credit_onhold = Column(Enum('', 'n', 'y'), nullable=False, server_default=text("''"))
    contact_active = Column(Enum('n', 'd', 'y'), nullable=False, server_default=text("'n'"))
    contact_role = Column(Enum('', 'administrator', 'customer', 'supplier', 'warehouse'), nullable=False, server_default=text("''"))
    internal_id = Column(INTEGER(11))
    internal_record_id = Column(String(50), nullable=False, server_default=text("''"), comment='Deprecated by PROJ-518 for 6.6.0')
    contact_misctext1 = Column(Text)
    contact_misctext2 = Column(Text)
    contact_misctext3 = Column(Text)
    remote_record_id = Column(String(50), nullable=False, index=True, server_default=text("''"))
    last_update_id = Column(String(50), nullable=False, server_default=text("''"))
    last_update_time = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    last_saved_time = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    last_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class ACCAPIIMPORTCONTENT(Base):
    __tablename__ = 'ACCAPI_IMPORT_CONTENTS'

    accim_content_id = Column(INTEGER(10), primary_key=True)
    accapi_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_ACCOUNTS')
    accim_content_code = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    accim_content_type = Column(String(20))
    content_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    accim_content_name = Column(String(255))
    accim_parent_content_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    accim_content_description = Column(MEDIUMTEXT)
    accim_content_sortorder = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    accim_content_pagetitle = Column(String(255))
    accim_content_metakeywords = Column(String(255))
    accim_content_metadescription = Column(String(255))
    accim_content_image = Column(String(255))
    accim_content_active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    content_custom_url = Column(String(255))
    accim_content_path = Column(String(255))
    last_update_id = Column(String(50), nullable=False, server_default=text("''"))
    last_update_time = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    last_saved_time = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    last_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class ACCAPIIMPORTINVOICELINE(Base):
    __tablename__ = 'ACCAPI_IMPORT_INVOICELINES'

    accim_invln_id = Column(INTEGER(10), primary_key=True)
    accim_invoice_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_IMPORT_INVOICES')
    accim_item_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_IMPORT_INVOICES')
    accim_invln_counter = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    invln_remote_id = Column(INTEGER(10))
    item_code = Column(String(50), nullable=False, index=True, server_default=text("''"))
    invln_description = Column(String(255))
    invln_quantity = Column(INTEGER(11))
    invln_unit_price = Column(DECIMAL(14, 2))
    invln_unit_price_extax = Column(DECIMAL(14, 2))
    invln_taxtotal = Column(DECIMAL(14, 2))
    invln_subtotal = Column(DECIMAL(14, 2))
    invln_cubic = Column(DECIMAL(15, 9))
    invln_weight = Column(DECIMAL(12, 3))
    invln_discount_rate = Column(DECIMAL(5, 2))
    invln_taxcode = Column(String(50), nullable=False, server_default=text("''"))
    invln_acc = Column(String(50), nullable=False, server_default=text("''"))
    invln_job = Column(String(50), nullable=False, server_default=text("''"))
    invln_location = Column(String(50), nullable=False, server_default=text("''"))
    invln_specifics = Column(MEDIUMTEXT)
    invln_shipping_method = Column(String(50))
    invln_shipping_cost = Column(INTEGER(10))
    invln_quantity_shipped = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    invln_address_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    invln_discount = Column(DECIMAL(12, 2))
    invln_surcharge = Column(DECIMAL(12, 2))
    invln_surcharge_extax = Column(DECIMAL(12, 2))
    invln_shipping_cost_extax = Column(INTEGER(10))
    last_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class ACCAPIIMPORTINVOICE(Base):
    __tablename__ = 'ACCAPI_IMPORT_INVOICES'

    accim_invoice_id = Column(INTEGER(10), primary_key=True)
    accapi_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_ACCOUNTS')
    accim_contact_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_IMPORT_CONTACTS')
    invoice_number = Column(String(50), nullable=False, index=True, server_default=text("''"))
    order_record_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    invoice_po = Column(String(50))
    contact_code = Column(String(50), nullable=False, index=True, server_default=text("''"))
    warehouse_code = Column(String(50), index=True)
    invoice_type = Column(String(50))
    invoice_tax_type = Column(Enum('', 'inc', 'exc'), nullable=False, server_default=text("''"))
    invoice_template = Column(String(50))
    bill_company_name = Column(String(50))
    bill_first_name = Column(String(50))
    bill_last_name = Column(String(50))
    bill_phone = Column(String(50))
    ship_company_name = Column(String(50))
    ship_first_name = Column(String(50))
    ship_last_name = Column(String(50))
    ship_street1 = Column(String(50))
    ship_street2 = Column(String(50))
    ship_street3 = Column(String(50))
    ship_street4 = Column(String(50))
    ship_city = Column(String(50))
    ship_state = Column(String(50))
    ship_zip = Column(String(15))
    ship_country = Column(CHAR(2))
    date_invoiced = Column(Date)
    date_due = Column(Date)
    date_required = Column(DateTime)
    date_completed = Column(DateTime)
    currency_code = Column(CHAR(3))
    currency_rate = Column(DECIMAL(10, 4))
    invoice_status = Column(String(50))
    invoice_freight = Column(DECIMAL(14, 2))
    invoice_freight_extax = Column(DECIMAL(14, 2))
    invoice_subtotal = Column(DECIMAL(14, 2))
    invoice_subtotal_extax = Column(DECIMAL(14, 2))
    invoice_grandtotal = Column(DECIMAL(14, 2))
    invoice_taxtotal = Column(DECIMAL(14, 2))
    amount_paid = Column(DECIMAL(14, 2))
    invoice_terms = Column(String(50))
    invoice_sent = Column(Enum('', 'n', 'y'))
    invoice_role = Column(Enum('', 'invoice', 'credit', 'adjustment', 'purchase', 'overpayment'), nullable=False, server_default=text("''"))
    invoice_comment = Column(Text)
    invoice_note = Column(Text)
    invoice_misctext1 = Column(Text)
    invoice_misctext2 = Column(Text)
    invoice_misctext3 = Column(Text)
    invoice_active = Column(Enum('n', 'd', 'y'), nullable=False, server_default=text("'n'"))
    invoice_addrs = Column(MEDIUMTEXT)
    remote_record_id = Column(String(50), nullable=False, index=True, server_default=text("''"))
    last_update_id = Column(String(50), nullable=False, server_default=text("''"))
    last_update_time = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    last_saved_time = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    last_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class ACCAPIIMPORTITEMCOMPONENT(Base):
    __tablename__ = 'ACCAPI_IMPORT_ITEMCOMPONENTS'

    accim_itmcom_id = Column(INTEGER(10), primary_key=True)
    accim_item_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_IMPORT_ITEMS')
    bundle_accim_item_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_IMPORT_ITEMS')
    accapi_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_ACCOUNTS')
    accim_itmcom_counter = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    accim_itmcom_qty01 = Column(DECIMAL(12, 2))
    accim_itmcom_qty02 = Column(DECIMAL(12, 2))
    accim_itmcom_qty03 = Column(DECIMAL(12, 2))
    accim_itmcom_price = Column(DECIMAL(12, 2))
    item_code = Column(String(50), nullable=False, index=True, server_default=text("''"))
    bundle_item_code = Column(String(50), nullable=False, index=True, server_default=text("''"))
    remote_record_id = Column(String(50), nullable=False, index=True, server_default=text("''"))
    last_update_id = Column(String(50), nullable=False, server_default=text("''"))
    last_update_time = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    last_saved_time = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    last_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class ACCAPIIMPORTITEM(Base):
    __tablename__ = 'ACCAPI_IMPORT_ITEMS'

    accim_item_id = Column(INTEGER(10), primary_key=True)
    accapi_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_ACCOUNTS')
    item_code = Column(String(50), nullable=False, index=True, server_default=text("''"))
    inventory_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    item_name = Column(String(255))
    item_base_name = Column(String(255), nullable=False, server_default=text("''"))
    item_purchase_cost = Column(DECIMAL(12, 2))
    item_default_cost = Column(DECIMAL(9, 2))
    item_default_price = Column(DECIMAL(12, 2))
    item_purchase_taxcode = Column(String(50))
    item_purchase_acc = Column(String(50))
    item_price01 = Column(DECIMAL(12, 2))
    item_price02 = Column(DECIMAL(12, 2))
    item_price03 = Column(DECIMAL(12, 2))
    item_price04 = Column(DECIMAL(12, 2))
    item_price05 = Column(DECIMAL(12, 2))
    item_price06 = Column(DECIMAL(12, 2))
    item_price07 = Column(DECIMAL(12, 2))
    item_price08 = Column(DECIMAL(12, 2))
    item_price09 = Column(DECIMAL(12, 2))
    item_price10 = Column(DECIMAL(12, 2))
    item_sales_taxcode = Column(String(50))
    item_sales_acc = Column(String(50))
    item_asset_acc = Column(String(50))
    item_income_acc = Column(String(50))
    item_barcode01 = Column(String(50))
    item_barcode02 = Column(String(50))
    item_barcode03 = Column(String(50))
    item_notes = Column(Text)
    item_length = Column(DECIMAL(12, 3))
    item_width = Column(DECIMAL(12, 3))
    item_height = Column(DECIMAL(12, 3))
    item_cubic = Column(DECIMAL(15, 9))
    item_weight = Column(DECIMAL(12, 3))
    item_group = Column(String(50))
    item_unit = Column(String(50))
    item_pack_size = Column(DECIMAL(12, 3))
    item_reorder_qty = Column(INTEGER(10))
    item_stock_alert01 = Column(INTEGER(10))
    item_stock_alert02 = Column(INTEGER(10))
    item_supplier = Column(String(50))
    item_supplier_code = Column(String(50))
    item_pick_location = Column(String(50))
    item_allocated_qty = Column(INTEGER(10))
    item_available_qty = Column(INTEGER(10))
    item_onhand_qty = Column(INTEGER(10))
    item_onpurchase_qty = Column(INTEGER(10))
    item_is_component = Column(Enum('', 'n', 'y'), nullable=False, server_default=text("''"))
    item_is_kitting = Column(Enum('', 'n', 'y'), nullable=False, server_default=text("''"))
    item_is_variation = Column(Enum('', 'n', 'y'), nullable=False, server_default=text("''"))
    item_active = Column(Enum('n', 'd', 'y'), nullable=False, server_default=text("'n'"))
    item_is_inventoried = Column(Enum('n', 'y'))
    item_is_service = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    item_is_bought = Column(Enum('n', 'y'))
    item_is_sold = Column(Enum('n', 'y'))
    item_misctext1 = Column(Text)
    item_misctext2 = Column(Text)
    item_misctext3 = Column(Text)
    item_misctext4 = Column(Text)
    item_misctext5 = Column(Text)
    item_misctext6 = Column(Text)
    item_misctext7 = Column(Text)
    item_misctext8 = Column(Text)
    item_misctext9 = Column(Text)
    item_miscbool1 = Column(Enum('n', 'y'))
    remote_record_id = Column(String(50), nullable=False, index=True, server_default=text("''"))
    item_description = Column(MEDIUMTEXT)
    item_price_retail = Column(DECIMAL(12, 2))
    item_price_promo = Column(DECIMAL(12, 2))
    item_approval = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    item_is_virtual = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    item_sortorder = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    item_upc = Column(String(100), nullable=False, server_default=text("''"))
    item_keywords = Column(String(255), nullable=False, server_default=text("''"))
    item_meta_keywords = Column(String(255), nullable=False, server_default=text("''"))
    item_page_title = Column(String(255), nullable=False, server_default=text("''"))
    item_meta_description = Column(MEDIUMTEXT)
    item_custom_url = Column(String(255))
    item_min_qty = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    item_max_qty = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    item_attributes = Column(MEDIUMTEXT)
    item_options = Column(MEDIUMTEXT)
    item_specifics = Column(MEDIUMTEXT)
    item_mlp = Column(MEDIUMTEXT)
    item_images = Column(MEDIUMTEXT)
    last_update_id = Column(String(50), nullable=False, server_default=text("''"))
    last_update_time = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    last_saved_time = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    last_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class ACCAPIIMPORTMISCDATUM(Base):
    __tablename__ = 'ACCAPI_IMPORT_MISCDATA'

    acc_misc_id = Column(INTEGER(10), primary_key=True)
    accapi_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_ACCOUNTS')
    acc_misc_type = Column(String(50), index=True)
    acc_misc_code = Column(String(50), nullable=False, index=True, server_default=text("''"))
    acc_misc_name = Column(String(50))
    acc_misc_field01 = Column(Text)
    acc_misc_field02 = Column(Text)
    acc_misc_field03 = Column(Text)
    acc_misc_field04 = Column(Text)
    acc_misc_field05 = Column(Text)
    acc_misc_active = Column(Enum('n', 'd', 'y'), nullable=False, server_default=text("'n'"))
    remote_record_id = Column(String(50), nullable=False, index=True, server_default=text("''"))
    last_update_id = Column(String(50), nullable=False, server_default=text("''"))
    last_update_time = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    last_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    last_saved_time = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))


class ACCAPIIMPORTPAYMENT(Base):
    __tablename__ = 'ACCAPI_IMPORT_PAYMENTS'

    accim_payment_id = Column(INTEGER(10), primary_key=True)
    accapi_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_ACCOUNTS')
    accim_invoice_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_IMPORT_INVOICES')
    accim_contact_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_IMPORT_CONTACTS')
    invoice_number = Column(String(50), nullable=False, index=True, server_default=text("''"))
    invoice_po = Column(String(50))
    payment_reference = Column(String(50), nullable=False, index=True, server_default=text("''"))
    payment_description = Column(String(100))
    date_paid = Column(Date)
    currency_code = Column(CHAR(3))
    currency_rate = Column(DECIMAL(10, 4))
    payment_status = Column(String(50))
    payment_amount = Column(DECIMAL(14, 2))
    payment_acc = Column(String(50))
    payment_type = Column(String(50))
    payment_note = Column(Text)
    payment_role = Column(Enum('', 'payment', 'adjustment', 'refund', 'credit'), nullable=False, server_default=text("''"))
    payment_misctext1 = Column(Text)
    payment_misctext2 = Column(Text)
    payment_misctext3 = Column(Text)
    payment_misctext4 = Column(Text)
    payment_misctext5 = Column(Text)
    payment_active = Column(Enum('n', 'd', 'y'), nullable=False, server_default=text("'n'"))
    remote_record_id = Column(String(50), nullable=False, index=True, server_default=text("''"))
    last_update_id = Column(String(50), nullable=False, server_default=text("''"))
    last_update_time = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    last_saved_time = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    last_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class ACCAPIIMPORTSTOCK(Base):
    __tablename__ = 'ACCAPI_IMPORT_STOCKS'

    accim_stock_id = Column(INTEGER(10), primary_key=True)
    accim_item_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_IMPORT_ITEMS')
    accim_contact_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_IMPORT_CONTACTS')
    accapi_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_ACCOUNTS')
    accim_stock_counter = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    item_code = Column(String(50), nullable=False, index=True, server_default=text("''"))
    warehouse_code = Column(String(50), nullable=False, index=True, server_default=text("''"))
    item_available_qty = Column(DECIMAL(12, 3))
    item_onhand_qty = Column(DECIMAL(12, 3))
    item_onpurchase_qty = Column(DECIMAL(12, 3))
    item_oncommit_qty = Column(DECIMAL(12, 3))
    item_onkit_qty = Column(DECIMAL(12, 3))
    item_avg_cost = Column(DECIMAL(12, 2))
    remote_record_id = Column(String(50), nullable=False, index=True, server_default=text("''"))
    last_update_id = Column(String(50), nullable=False, server_default=text("''"))
    last_update_time = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    last_saved_time = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    last_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class ACCAPIIMPORTTAXCODE(Base):
    __tablename__ = 'ACCAPI_IMPORT_TAXCODES'

    acc_taxcode_id = Column(INTEGER(10), primary_key=True)
    accapi_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_ACCOUNTS')
    acc_taxcode_ref = Column(String(50), nullable=False, index=True, server_default=text("''"))
    acc_taxcode_name = Column(String(50))
    acc_taxcode_type = Column(String(50))
    acc_taxcode_rate = Column(DECIMAL(8, 4))
    acc_taxcode_note = Column(String(255))
    acc_taxcode_active = Column(Enum('n', 'd', 'y'), nullable=False, server_default=text("'n'"))
    remote_record_id = Column(String(50), nullable=False, index=True, server_default=text("''"))


class ACCAPIPROCLOG(Base):
    __tablename__ = 'ACCAPI_PROC_LOG'

    accplog_id = Column(INTEGER(10), primary_key=True)
    accapi_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_ACCOUNTS')
    accsw_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_SOFTWARE')
    acctask_module = Column(String(30), nullable=False, index=True, server_default=text("''"))
    admin_username = Column(String(25), nullable=False, server_default=text("''"))
    log_description = Column(String(100), nullable=False, server_default=text("''"))
    log_notes = Column(String(255), nullable=False, server_default=text("''"))
    rtn_status = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    rtn_error = Column(Text, nullable=False)
    rtn_msg = Column(Text, nullable=False)
    rtn_warn = Column(Text, nullable=False)
    date_start = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    date_end = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))


class ACCAPIQUICKTASK(Base):
    __tablename__ = 'ACCAPI_QUICKTASKS'

    accqt_id = Column(INTEGER(10), primary_key=True, server_default=text("'0'"))
    accsw_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    accqt_type_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    acctask_module = Column(String(30), nullable=False, server_default=text("''"))
    accqt_sortorder = Column(INTEGER(10), nullable=False, server_default=text("'0'"))


class ACCAPIQUICKTASKTYPE(Base):
    __tablename__ = 'ACCAPI_QUICKTASK_TYPES'

    accqt_type_id = Column(INTEGER(10), primary_key=True, server_default=text("'0'"))
    accqt_type_name = Column(String(100), nullable=False, server_default=text("''"))
    accqt_type_direction = Column(Enum('', 'import', 'export'), nullable=False, server_default=text("''"))
    accqt_type_sortorder = Column(INTEGER(10), nullable=False, server_default=text("'0'"))


class ACCAPIREASONMAPPING(Base):
    __tablename__ = 'ACCAPI_REASON_MAPPING'

    reason_id = Column(INTEGER(11), primary_key=True, nullable=False)
    acc_account_id = Column(INTEGER(11), primary_key=True, nullable=False)


class ACCAPIREPORT(Base):
    __tablename__ = 'ACCAPI_REPORTS'

    accapi_id = Column(INTEGER(10), primary_key=True, nullable=False)
    accapi_report_type = Column(String(45), primary_key=True, nullable=False)
    date_pending = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    last_start = Column(DateTime, nullable=False)
    last_end = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))


class ACCAPISOFTWARE(Base):
    __tablename__ = 'ACCAPI_SOFTWARE'

    accsw_id = Column(INTEGER(10), primary_key=True, server_default=text("'0'"))
    accsw_title = Column(String(45), nullable=False, server_default=text("''"))
    accsw_library = Column(String(45), nullable=False, index=True, server_default=text("''"))
    accsw_description = Column(Text, nullable=False)
    accsw_helpurl = Column(String(255), nullable=False, server_default=text("''"))
    accsw_marketingurl = Column(String(255), nullable=False, server_default=text("''"))
    accsw_configureurl = Column(String(255), nullable=False, server_default=text("''"))
    accsw_imageurl = Column(String(255), nullable=False, server_default=text("''"))
    accsw_videourl = Column(String(255), nullable=False, server_default=text("''"))
    accsw_invoice_url = Column(String(255))
    accsw_auth_method = Column(Enum('', 'password', 'shared_key', 'oauth', 'oauth2'), nullable=False, server_default=text("''"))
    accsw_refreshable = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    accsw_sortorder = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    accsw_active = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    accsw_config_groups = Column(Text, nullable=False)
    accsw_allow_payment_mapping = Column(TINYINT(1), nullable=False, server_default=text("'1'"))
    is_third_party = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    type = Column(TINYTEXT, nullable=False)


class ACCAPISOFTWARECONFIG(Base):
    __tablename__ = 'ACCAPI_SOFTWARE_CONFIGS'

    accsw_id = Column(INTEGER(10), primary_key=True, nullable=False, server_default=text("'0'"), comment='FK: ACCAPI_ACCOUNTS')
    acccfg_name = Column(String(50), primary_key=True, nullable=False, server_default=text("''"))
    acccfg_value = Column(MEDIUMTEXT, nullable=False)
    acccfg_group_id = Column(INTEGER(10), primary_key=True, nullable=False, server_default=text("'0'"))


class ACCAPITASK(Base):
    __tablename__ = 'ACCAPI_TASKS'

    acctask_id = Column(INTEGER(10), primary_key=True)
    accapi_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: ACCAPI_ACCOUNTS')
    acctask_module = Column(String(30), nullable=False, index=True, server_default=text("''"))
    acctask_name = Column(String(100), nullable=False, server_default=text("''"))
    acctask_description = Column(String(255), nullable=False, server_default=text("''"))
    schedule_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK: TASKSCHEDULES')
    acctask_priority = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    acctask_active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class ACCAPITAXCODEMAPPING(Base):
    __tablename__ = 'ACCAPI_TAX_CODE_MAPPING'

    tax_code_id = Column(INTEGER(11), primary_key=True, nullable=False)
    acc_taxcode_id = Column(INTEGER(11), primary_key=True, nullable=False)


class ACCAPIWHMAPPING(Base):
    __tablename__ = 'ACCAPI_WH_MAPPING'

    accapi_id = Column(INTEGER(10), primary_key=True, nullable=False, server_default=text("'0'"))
    accim_contact_id = Column(INTEGER(10), primary_key=True, nullable=False, server_default=text("'0'"))
    warehouse_id = Column(INTEGER(10), primary_key=True, nullable=False, server_default=text("'0'"))


class ACCWSLOG(Base):
    __tablename__ = 'ACC_WS_LOG'

    id = Column(INTEGER(10), primary_key=True)
    module = Column(String(25), nullable=False, index=True)
    taskid = Column(INTEGER(10), nullable=False, index=True)
    transid = Column(INTEGER(10), nullable=False, index=True)
    action = Column(String(50), nullable=False)
    msg_type = Column(Enum('info', 'warn', 'error'), nullable=False, server_default=text("'info'"))
    msg = Column(Text, nullable=False)
    remote_ip = Column(String(50), nullable=False)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class ADDONSBLACKLISTSKU(Base):
    __tablename__ = 'ADDONS_BLACKLIST_SKU'

    id = Column(INTEGER(11), primary_key=True)
    addon_id = Column(INTEGER(11), nullable=False)
    addon_blacklist_id = Column(INTEGER(11), nullable=False)
    addon_blacklist_sku = Column(String(45), nullable=False)


class ADDONSDIRECTORY(Base):
    __tablename__ = 'ADDONS_DIRECTORY'

    addon_id = Column(INTEGER(10), primary_key=True, server_default=text("'0'"))
    addon_title = Column(String(45), nullable=False, server_default=text("''"))
    addon_description = Column(Text, nullable=False)
    addon_instruction = Column(Text)
    addon_post_install_instruction = Column(Text)
    addon_pre_uninstall_instruction = Column(Text)
    addon_post_uninstall_instruction = Column(Text)
    addon_helpurl = Column(String(255), nullable=False, server_default=text("''"))
    addon_marketingurl = Column(String(255), nullable=False, server_default=text("''"))
    addon_imageurl = Column(String(255), nullable=False, server_default=text("''"))
    addon_install_url = Column(String(255), nullable=False, server_default=text("''"))
    addon_configure_url = Column(String(255), nullable=False, server_default=text("''"))
    addon_videourl = Column(String(255), nullable=False, server_default=text("''"))
    addon_price = Column(String(45), nullable=False, server_default=text("''"))
    addon_type = Column(String(45), nullable=False, index=True, server_default=text("''"))
    addon_module = Column(String(85), nullable=False, server_default=text("''"))
    addon_sortorder = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    addon_active = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    addon_multiple = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    addon_category = Column(String(45))
    addon_switch = Column(String(45))
    addon_config = Column(Text)
    addon_SKU = Column(String(45))
    addon_developer = Column(String(45))
    addon_parent_app_id = Column(INTEGER(10), server_default=text("'0'"))
    addon_subscription = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    addon_show_self = Column(TINYINT(1), nullable=False, server_default=text("'1'"))
    addon_show_child = Column(TINYINT(1), nullable=False, server_default=text("'1'"))
    addon_plan_skus = Column(String(45))
    addon_trial_days = Column(TINYINT(3), server_default=text("'0'"))
    trial_expiry_date = Column(DateTime, server_default=text("'0000-00-00 00:00:00'"))
    addon_filter = Column(String(10), nullable=False)
    add_to_standing_order = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    addon_slug = Column(String(128), unique=True)


class ADDRESSBOOK(Base):
    __tablename__ = 'ADDRESSBOOK'

    id = Column(INTEGER(11), primary_key=True)
    username = Column(String(25), nullable=False, index=True, server_default=text("''"))
    ship_title = Column(String(255))
    ship_first_name = Column(String(50))
    ship_last_name = Column(String(50))
    ship_company = Column(String(50))
    ship_street1 = Column(String(50))
    ship_street2 = Column(String(50))
    ship_city = Column(String(50))
    ship_state = Column(String(50))
    ship_zip = Column(String(15))
    ship_country = Column(String(50))
    ship_phone = Column(String(30))
    is_residential = Column(TINYINT(4), nullable=False, server_default=text("'0'"))
    modification_date = Column(DateTime)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    ship_fax = Column(String(30), nullable=False, server_default=text("''"))
    wishlist = Column(TINYINT(4), nullable=False, server_default=text("'0'"))


class ADMINISTRATOR(Base):
    __tablename__ = 'ADMINISTRATORS'

    administrator_id = Column(INTEGER(10), primary_key=True)
    username = Column(String(25), nullable=False, unique=True)
    password = Column(String(100), nullable=False)
    pin = Column(String(100), nullable=False, server_default=text("''"))
    resetcode = Column(String(50), nullable=False)
    api_key = Column(String(50), nullable=False, server_default=text("''"))
    cpperm_gp_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    warehouse_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    daily_target = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    daily_call_target = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    last_login = Column(DateTime, nullable=False)
    active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    first_name = Column(String(50), nullable=False)
    last_name = Column(String(50), nullable=False)
    email = Column(String(255), nullable=False)
    mpanel_session_id = Column(String(32), nullable=False, server_default=text("''"))
    password_last_changed = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    failed_password_attempts = Column(TINYINT(4), nullable=False, server_default=text("'0'"))
    openid_uuid = Column(String(50), nullable=False, server_default=text("''"))
    photo_url = Column(String(128), nullable=False, server_default=text("''"))
    settings = Column(Text, nullable=False)
    failed_mfa_attempts = Column(INTEGER(1), nullable=False, server_default=text("'0'"))


t_ADMINISTRATORS_PASSWORD_HISTORY = Table(
    'ADMINISTRATORS_PASSWORD_HISTORY', metadata,
    Column('username', String(25), index=True),
    Column('password', String(100), nullable=False),
    Column('changed_date', DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
)


class ADMINISTRATORSSSO(Base):
    __tablename__ = 'ADMINISTRATORS_SSO'

    sso_id = Column(CHAR(48), primary_key=True)
    administrator_id = Column(INTEGER(10), nullable=False)
    sso_expiry = Column(DateTime, nullable=False)
    sso_used = Column(DateTime)
    sso_password_changed = Column(DateTime)


class ADMINISTRATORZONE(Base):
    __tablename__ = 'ADMINISTRATOR_ZONES'

    username = Column(String(25), primary_key=True, nullable=False)
    pick_zone = Column(String(25), primary_key=True, nullable=False, index=True)


class ADWAD(Base):
    __tablename__ = 'ADW_ADS'

    ad_id = Column(INTEGER(10), primary_key=True)
    parent_ad_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    plan_id = Column(INTEGER(10), nullable=False)
    adw_group_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    headline = Column(String(50), nullable=False, index=True)
    description = Column(String(5000), nullable=False)
    linktext = Column(String(50), nullable=False, index=True)
    ad_inventory_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    ad_stloc_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    ad_content_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    url = Column(String(255), nullable=False)
    img_width = Column(INTEGER(10), nullable=False)
    img_height = Column(INTEGER(10), nullable=False)
    adw_start = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    adw_end = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    ad_status = Column(Enum('Pending', 'Active', 'Ended'), server_default=text("'Pending'"))
    status_last_update_time = Column(DateTime)
    active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    sort_order = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    date_created = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class ADWADSCONTENT(Base):
    __tablename__ = 'ADW_ADS_CONTENTS'

    ad_id = Column(INTEGER(10), primary_key=True, nullable=False)
    content_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True)


class ADWGROUP(Base):
    __tablename__ = 'ADW_GROUPS'

    adw_group_id = Column(INTEGER(10), primary_key=True)
    adw_group_name = Column(String(50), nullable=False, server_default=text("''"))
    adw_group_description = Column(String(255), nullable=False, server_default=text("''"))


class ADWITEM(Base):
    __tablename__ = 'ADW_ITEMS'

    ad_id = Column(INTEGER(10), primary_key=True, nullable=False)
    inventory_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True)
    sort_order = Column(INTEGER(10), nullable=False, server_default=text("'0'"))


class ADWPLAN(Base):
    __tablename__ = 'ADW_PLANS'

    plan_id = Column(INTEGER(10), primary_key=True)
    plan_name = Column(String(50), nullable=False, index=True)
    active = Column(Enum('n', 'y'), nullable=False, index=True, server_default=text("'n'"))
    layout = Column(String(10), nullable=False)
    template = Column(String(15), nullable=False)
    description = Column(MEDIUMTEXT)


class APIKEY(Base):
    __tablename__ = 'API_KEYS'

    id = Column(INTEGER(11), primary_key=True)
    name = Column(String(255), nullable=False)
    access_key = Column(String(255), nullable=False, unique=True)
    secret_key = Column(String(255), nullable=False)
    cpperm_gp_id = Column(INTEGER(11))
    active = Column(TINYINT(1), nullable=False)
    date_created = Column(DateTime, nullable=False)
    date_updated = Column(DateTime)


class APILOG(Base):
    __tablename__ = 'API_LOG'

    apilog_id = Column(BIGINT(20), primary_key=True)
    api_action = Column(String(45), nullable=False)
    pid = Column(INTEGER(10), nullable=False)
    localip = Column(String(45), nullable=False)
    remoteip = Column(String(45), nullable=False)
    api_user = Column(String(45), nullable=False)
    api_postdata = Column(MEDIUMTEXT)
    api_response = Column(MEDIUMTEXT)
    api_request_ts = Column(DateTime, server_default=text("CURRENT_TIMESTAMP"))
    api_response_ts = Column(DateTime)
    access_key = Column(String(255))


class ASSETGROUP(Base):
    __tablename__ = 'ASSETGROUPS'

    asset_group_id = Column(INTEGER(10), primary_key=True)
    asset_group_name = Column(String(50), nullable=False, server_default=text("''"))
    asset_group_description = Column(String(255), nullable=False, server_default=text("''"))


class ASSETLOCATION(Base):
    __tablename__ = 'ASSETLOCATIONS'

    asset_id = Column(INTEGER(10), primary_key=True)
    dropshipper = Column(String(25), nullable=False, index=True, server_default=text("''"))
    warehouse_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    location_id = Column(String(15), nullable=False, index=True, server_default=text("''"))
    username = Column(String(25), nullable=False, index=True, server_default=text("''"))
    order_id = Column(String(15), nullable=False, index=True)
    loc_zip = Column(String(15), nullable=False, server_default=text("''"))
    loc_state = Column(String(50), nullable=False, server_default=text("''"))
    loc_country = Column(String(50), nullable=False, server_default=text("''"))
    date_moved = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class ASSETMOVE(Base):
    __tablename__ = 'ASSETMOVES'

    move_id = Column(INTEGER(10), primary_key=True)
    asset_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    move_type_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    move_status_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    freason_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    to_asset_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    dropshipper = Column(String(25), nullable=False, index=True, server_default=text("''"))
    warehouse_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    admin = Column(String(25), nullable=False, index=True, server_default=text("''"))
    order_id = Column(String(15), nullable=False, index=True, server_default=text("''"))
    fm_dropshipper = Column(String(25), nullable=False, index=True, server_default=text("''"))
    fm_warehouse_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    fm_location_id = Column(String(15), nullable=False, index=True, server_default=text("''"))
    fm_username = Column(String(25), nullable=False, index=True, server_default=text("''"))
    to_dropshipper = Column(String(25), nullable=False, index=True, server_default=text("''"))
    to_warehouse_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    to_location_id = Column(String(15), nullable=False, index=True, server_default=text("''"))
    to_username = Column(String(25), nullable=False, index=True, server_default=text("''"))
    ship_first_name = Column(String(50), nullable=False, server_default=text("''"))
    ship_last_name = Column(String(50), nullable=False, server_default=text("''"))
    ship_company = Column(String(50), nullable=False, server_default=text("''"))
    ship_street1 = Column(String(50), nullable=False, server_default=text("''"))
    ship_street2 = Column(String(50), nullable=False, server_default=text("''"))
    ship_city = Column(String(50), nullable=False, server_default=text("''"))
    ship_state = Column(String(50), nullable=False, server_default=text("''"))
    ship_zip = Column(String(50), nullable=False, server_default=text("''"))
    ship_country = Column(String(50), nullable=False, server_default=text("''"))
    ship_phone = Column(String(50), nullable=False, server_default=text("''"))
    ship_fax = Column(String(50), nullable=False, server_default=text("''"))
    email = Column(String(50), nullable=False, server_default=text("''"))
    ship_comment = Column(MEDIUMTEXT, nullable=False)
    jobcreator = Column(String(25), nullable=False, index=True)
    internal_note = Column(MEDIUMTEXT, nullable=False)
    date_required = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_created = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_completed = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class ASSETMOVESTATU(Base):
    __tablename__ = 'ASSETMOVESTATUS'

    move_status_id = Column(INTEGER(10), primary_key=True)
    move_status_name = Column(String(50), nullable=False, index=True)
    orderby = Column(TINYINT(3), nullable=False, server_default=text("'0'"))


class ASSETMOVETYPE(Base):
    __tablename__ = 'ASSETMOVE_TYPES'

    move_type_id = Column(INTEGER(10), primary_key=True)
    move_type_name = Column(String(50), nullable=False, index=True)
    replace_action = Column(Enum('place', 'swap', 'return'), nullable=False, server_default=text("'place'"))
    orderby = Column(TINYINT(3), nullable=False, server_default=text("'0'"))


class ASSETOWNER(Base):
    __tablename__ = 'ASSETOWNERS'

    asset_id = Column(INTEGER(10), primary_key=True)
    dropshipper = Column(String(25), nullable=False, index=True, server_default=text("''"))
    warehouse_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    username = Column(String(25), nullable=False, index=True, server_default=text("''"))
    date_owned = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class ASSET(Base):
    __tablename__ = 'ASSETS'

    asset_id = Column(INTEGER(10), primary_key=True)
    parent_asset_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    asset_group_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    SKU = Column(String(25), nullable=False, index=True)
    asset_serial = Column(String(50), nullable=False, index=True, server_default=text("''"))
    asset_status_id = Column(INTEGER(10), nullable=False, index=True)
    asset_active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    description = Column(String(255), nullable=False, server_default=text("''"))
    purchase_cost = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    service_cost = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    dispose_value = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    dispose_reason = Column(String(25), nullable=False, server_default=text("''"))
    mid = Column(String(25), nullable=False, index=True, server_default=text("''"), comment='Warranty Provider')
    po_number = Column(String(25), nullable=False, index=True, server_default=text("''"))
    warranty_start = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    warranty_end = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    date_next_service = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    asset_label_height = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    internal_notes = Column(String(255), nullable=False, server_default=text("''"))
    date_acquired = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    date_disposed = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    date_serviced = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class ASSETSALESCACH(Base):
    __tablename__ = 'ASSETSALES_CACHES'

    username = Column(String(25), primary_key=True, nullable=False)
    asset_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True)
    SKU = Column(String(25), nullable=False, index=True)
    monthly_spent = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    loc_zip = Column(String(15), nullable=False, server_default=text("''"))
    loc_state = Column(String(50), nullable=False, server_default=text("''"))
    loc_country = Column(String(50), nullable=False, server_default=text("''"))
    loc_phone = Column(String(30), nullable=False, server_default=text("''"))
    loc_first_name = Column(String(50), nullable=False, server_default=text("''"))
    loc_last_name = Column(String(50), nullable=False, server_default=text("''"))
    moved_since = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    is_owned = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class ASSETSERVICELINE(Base):
    __tablename__ = 'ASSETSERVICELINES'

    service_id = Column(INTEGER(10), primary_key=True, nullable=False)
    counter = Column(TINYINT(4), primary_key=True, nullable=False, server_default=text("'0'"))
    SKU = Column(String(25), index=True)
    quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    iskitting = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    description = Column(String(255))
    itm_serial = Column(String(50), nullable=False, server_default=text("''"))
    extra = Column(MEDIUMTEXT, nullable=False)
    unit_cost = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    notax = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    date_added = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class ASSETSERVICELINESPLIT(Base):
    __tablename__ = 'ASSETSERVICELINESPLITS'

    service_id = Column(INTEGER(10), primary_key=True, nullable=False)
    counter = Column(TINYINT(4), primary_key=True, nullable=False)
    SKU = Column(String(25), primary_key=True, nullable=False, index=True)
    description = Column(String(255), nullable=False, server_default=text("''"))
    quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    assemble_quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    assemble_notax = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    itm_price = Column(DECIMAL(12, 2))


class ASSETSERVICE(Base):
    __tablename__ = 'ASSETSERVICES'

    service_id = Column(INTEGER(10), primary_key=True)
    asset_id = Column(INTEGER(10), nullable=False, index=True)
    schedule_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    service_type_id = Column(INTEGER(10), nullable=False, index=True)
    service_name = Column(String(100), nullable=False, server_default=text("''"))
    service_status_id = Column(INTEGER(10), nullable=False, index=True)
    order_id = Column(String(15), nullable=False, index=True, server_default=text("''"))
    email = Column(String(50), nullable=False)
    ship_first_name = Column(String(50), nullable=False)
    ship_last_name = Column(String(50), nullable=False)
    ship_company = Column(String(50), nullable=False)
    ship_street1 = Column(String(50), nullable=False)
    ship_street2 = Column(String(50), nullable=False)
    ship_city = Column(String(50), nullable=False)
    ship_state = Column(String(50), nullable=False)
    ship_zip = Column(String(50), nullable=False)
    ship_country = Column(String(50), nullable=False)
    ship_phone = Column(String(50), nullable=False)
    ship_fax = Column(String(50), nullable=False)
    material_cost = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    labour_cost = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    cost_total = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    hour_spent = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    hour_required = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    dropshipper = Column(String(25), nullable=False, index=True, server_default=text("''"), comment='Operator - Dropshipper')
    mid = Column(String(25), nullable=False, index=True, server_default=text("''"), comment='Operator - Supplier')
    warehouse_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='Operator - Warehouse')
    admin = Column(String(25), nullable=False, index=True, server_default=text("''"), comment='Operator - Warehouse Administrator')
    service_instruction = Column(MEDIUMTEXT, nullable=False)
    service_counter1 = Column(INTEGER(10), nullable=False)
    service_counter2 = Column(INTEGER(10), nullable=False)
    service_field1 = Column(String(150), nullable=False)
    service_field2 = Column(String(150), nullable=False)
    service_note = Column(MEDIUMTEXT, nullable=False)
    service_admin = Column(String(25), nullable=False, index=True, server_default=text("''"))
    service_dsadmin = Column(String(25), nullable=False, index=True, server_default=text("''"))
    date_due = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_created = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_started = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_completed = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_invoiced = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class ASSETSERVICESTATU(Base):
    __tablename__ = 'ASSETSERVICESTATUS'

    service_status_id = Column(INTEGER(10), primary_key=True)
    service_status_name = Column(String(50), nullable=False, index=True)
    template = Column(String(20), nullable=False, server_default=text("''"))
    orderby = Column(TINYINT(3), nullable=False, server_default=text("'0'"))


class ASSETSERVICECOLUMN(Base):
    __tablename__ = 'ASSETSERVICE_COLUMNS'

    col_id = Column(String(25), primary_key=True)
    col_name = Column(String(50), nullable=False)
    col_type = Column(Enum('TEXT', 'DATE', 'INT', 'DECIMAL', 'BOOLEAN', 'SHORTTEXT', 'SELECTIONBOX'), nullable=False)
    col_comment = Column(String(255), nullable=False)
    col_order = Column(TINYINT(3), nullable=False)


class ASSETSERVICEDOC(Base):
    __tablename__ = 'ASSETSERVICE_DOCS'

    sdoc_id = Column(INTEGER(10), primary_key=True)
    service_id = Column(INTEGER(10), nullable=False, index=True)
    sdoc_def_to = Column(Enum('', 'A', 'C', 'P', 'O'), nullable=False, comment='A= Service Administrator; C= Customer; P= Service Provider; O= Service Operator')
    sdoc_email = Column(String(50), nullable=False, server_default=text("''"))
    sdoc_title = Column(String(255), nullable=False, server_default=text("''"))
    sdoc_body = Column(MEDIUMTEXT, nullable=False)
    sdoc_files = Column(MEDIUMTEXT, nullable=False)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class ASSETSERVICEGROUP(Base):
    __tablename__ = 'ASSETSERVICE_GROUPS'

    service_gp_id = Column(INTEGER(10), primary_key=True)
    service_gp_name = Column(String(50), nullable=False, server_default=text("''"))
    service_gp_description = Column(String(255), nullable=False, server_default=text("''"))


class ASSETSERVICENOTE(Base):
    __tablename__ = 'ASSETSERVICE_NOTES'

    servnote_id = Column(INTEGER(10), primary_key=True)
    asset_id = Column(INTEGER(10), nullable=False, index=True)
    note_id = Column(INTEGER(10), nullable=False, index=True)
    service_id = Column(INTEGER(10), nullable=False, index=True)
    note_group_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class ASSETSERVICESCHEDULE(Base):
    __tablename__ = 'ASSETSERVICE_SCHEDULES'

    schedule_id = Column(INTEGER(10), primary_key=True)
    asset_id = Column(INTEGER(10), nullable=False, index=True)
    service_type_id = Column(INTEGER(10), nullable=False, index=True)
    min_hour = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    service_frequence = Column(INTEGER(10), nullable=False, server_default=text("'0'"), comment='Days')
    schedule_active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    next_date_due = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    last_scheduled = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    date_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class ASSETSERVICETYPEDOC(Base):
    __tablename__ = 'ASSETSERVICE_TYPEDOCS'

    service_type_id = Column(INTEGER(10), primary_key=True, nullable=False)
    cr_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True)
    sdoc_def_to = Column(Enum('', 'A', 'C', 'P', 'O'), nullable=False, comment='A= Service Administrator; C= Customer; P= Service Provider; O= Service Operator')


class ASSETSERVICETYPE(Base):
    __tablename__ = 'ASSETSERVICE_TYPES'

    service_type_id = Column(INTEGER(10), primary_key=True)
    service_gp_id = Column(INTEGER(10), nullable=False, index=True)
    service_type_name = Column(String(100), nullable=False, server_default=text("''"))
    service_type_description = Column(MEDIUMTEXT, nullable=False)
    base_cost = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    hourly_rate = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    min_hour = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    service_frequence = Column(INTEGER(10), nullable=False, server_default=text("'0'"), comment='Days')
    service_active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class ASSETSTATU(Base):
    __tablename__ = 'ASSETSTATUS'

    asset_status_id = Column(INTEGER(10), primary_key=True)
    asset_status_name = Column(String(50), nullable=False, index=True)
    orderby = Column(TINYINT(3), nullable=False, server_default=text("'0'"))


class ASSETMOVEMENT(Base):
    __tablename__ = 'ASSET_MOVEMENTS'

    trans_id = Column(INTEGER(10), primary_key=True)
    trans_type = Column(Enum('Add', 'Move', 'Dispose', 'Assign'), nullable=False, server_default=text("'Move'"))
    asset_id = Column(INTEGER(10), nullable=False, index=True)
    dropshipper = Column(String(25), nullable=False, index=True, server_default=text("''"))
    warehouse_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    location_id = Column(String(15), nullable=False, index=True, server_default=text("''"))
    username = Column(String(25), nullable=False, index=True, server_default=text("''"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class ASSETTRANSACTION(Base):
    __tablename__ = 'ASSET_TRANSACTIONS'

    trans_id = Column(INTEGER(10), primary_key=True)
    trans_type = Column(Enum('Add', 'Dispose', 'Sell'), nullable=False, server_default=text("'Add'"))
    asset_id = Column(INTEGER(10), nullable=False, index=True)
    trans_date = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class AUTOMATIONEVENT(Base):
    __tablename__ = 'AUTOMATION_EVENTS'

    auto_event_id = Column(INTEGER(10), primary_key=True)
    auto_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    auto_event_type_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    auto_event_column = Column(String(100), nullable=False, server_default=text("''"))
    auto_event_values = Column(Text, nullable=False)
    auto_event_sortorder = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))


class AUTOMATIONEVENTTYPE(Base):
    __tablename__ = 'AUTOMATION_EVENT_TYPES'

    auto_event_type_id = Column(INTEGER(10), primary_key=True)
    auto_event_name = Column(String(100), nullable=False, server_default=text("''"))
    auto_module = Column(Enum('', 'Product', 'Content'), nullable=False, server_default=text("''"))
    auto_event_module = Column(String(30), nullable=False, server_default=text("''"))
    auto_event_type_active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class AUTOMATIONRULE(Base):
    __tablename__ = 'AUTOMATION_RULES'

    auto_id = Column(INTEGER(10), primary_key=True)
    auto_module = Column(Enum('', 'Product', 'Content'), nullable=False, server_default=text("''"))
    auto_name = Column(String(150), nullable=False, server_default=text("''"))
    auto_description = Column(String(255), nullable=False, server_default=text("''"))
    auto_filters = Column(Text, nullable=False)
    auto_settings = Column(Text, nullable=False)
    date_start = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_end = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    schedule_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    schedule_priority = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    auto_active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    date_processed = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_updated = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))


class BULKEMAIL(Base):
    __tablename__ = 'BULK_EMAIL'

    task_id = Column(INTEGER(10), primary_key=True)
    recipient = Column(String(255), nullable=False, server_default=text("''"))
    username = Column(String(25), nullable=False, server_default=text("''"))
    email = Column(String(255), nullable=False, server_default=text("''"))
    Cc = Column(String(255), nullable=False, server_default=text("''"))
    subject = Column(Text, nullable=False)
    body = Column(Text, nullable=False)
    param = Column(Text, nullable=False)
    error = Column(String(255), nullable=False)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class CANNEDRESPONSE(Base):
    __tablename__ = 'CANNEDRESPONSES'

    cr_id = Column(INTEGER(10), primary_key=True)
    cr_name = Column(String(150), nullable=False)
    cr_description = Column(String(255), nullable=False)
    cr_type_id = Column(INTEGER(10), nullable=False, index=True)
    cr_body = Column(MEDIUMTEXT, nullable=False)
    cr_files = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    cr_noatt = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    cr_hidden = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    date_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class CANNEDRESPONSETYPE(Base):
    __tablename__ = 'CANNEDRESPONSE_TYPES'

    cr_type_id = Column(INTEGER(10), primary_key=True)
    cr_type_name = Column(String(50), nullable=False)
    cr_type_description = Column(String(255), nullable=False)


class CARDCLIENTIDCOUNTER(Base):
    __tablename__ = 'CARDCLIENTIDCOUNTER'

    counter = Column(INTEGER(11), primary_key=True)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class CHECKOUTFAILEDORDER(Base):
    __tablename__ = 'CHECKOUT_FAILEDORDERS'

    order_id = Column(String(15), primary_key=True)
    username = Column(String(25), nullable=False)
    email = Column(String(80), nullable=False)
    failed_reason = Column(String(80), nullable=False)
    failed_description = Column(Text, nullable=False)
    cart_data = Column(Text, nullable=False)
    transaction_data = Column(Text, nullable=False)
    date_log = Column(DateTime, nullable=False)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class CONTACTFORM(Base):
    __tablename__ = 'CONTACT_FORMS'

    form_id = Column(INTEGER(10), primary_key=True)
    form_index = Column(String(15), nullable=False, index=True, server_default=text("''"))
    form_name = Column(String(50), nullable=False, server_default=text("''"))
    subject = Column(String(255), nullable=False, server_default=text("''"))
    recipient = Column(String(50), nullable=False, server_default=text("''"))
    form_template = Column(String(25), nullable=False, server_default=text("''"))
    email_template = Column(String(25), nullable=False, server_default=text("''"))
    require_captcha = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class CONTENTNOTIFICATION(Base):
    __tablename__ = 'CONTENTNOTIFICATIONS'

    notification_id = Column(INTEGER(10), primary_key=True)
    username = Column(String(25), nullable=False)
    full_name = Column(String(75), nullable=False)
    email = Column(String(50), nullable=False)
    content_id = Column(INTEGER(10), nullable=False)
    date_added = Column(Date, nullable=False)
    date_sent = Column(DateTime, nullable=False)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class CONTENT(Base):
    __tablename__ = 'CONTENTS'

    content_id = Column(INTEGER(10), primary_key=True)
    content_type_id = Column(INTEGER(10), nullable=False, index=True)
    parent_content_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    content_level = Column(INTEGER(10), nullable=False, index=True, server_default=text("'1'"))
    content_leaf = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    content_name = Column(String(100), nullable=False, server_default=text("''"))
    content_ref = Column(String(50), nullable=False, index=True, server_default=text("''"))
    content_module = Column(String(20), nullable=False, server_default=text("''"))
    content_module_settings = Column(Text, nullable=False)
    content_external_src = Column(String(50), nullable=False, server_default=text("''"))
    content_external_ref1 = Column(String(255), nullable=False, server_default=text("''"))
    content_external_ref2 = Column(String(255), nullable=False, server_default=text("''"))
    content_external_ref3 = Column(String(255), nullable=False, server_default=text("''"))
    content_author = Column(String(50), nullable=False, server_default=text("''"))
    content_short_description1 = Column(String(255), nullable=False, server_default=text("''"))
    content_short_description2 = Column(String(255), nullable=False, server_default=text("''"))
    content_short_description3 = Column(String(255), nullable=False, server_default=text("''"))
    content_description1 = Column(MEDIUMTEXT, nullable=False)
    content_description2 = Column(MEDIUMTEXT, nullable=False)
    content_description3 = Column(MEDIUMTEXT, nullable=False)
    content_wufoo_form = Column(Text, nullable=False)
    content_label1 = Column(String(50), nullable=False, server_default=text("''"))
    content_label2 = Column(String(50), nullable=False, server_default=text("''"))
    content_label3 = Column(String(50), nullable=False, server_default=text("''"))
    content_fullpath = Column(Text, nullable=False)
    content_fullpath_with_separator = Column(Text, nullable=False)
    content_nodes = Column(Text, nullable=False)
    content_keywords = Column(Text, nullable=False)
    product_count = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    template = Column(String(50), nullable=False, server_default=text("''"))
    templatehead = Column(String(50), nullable=False, server_default=text("''"))
    templatebody = Column(String(50), nullable=False, server_default=text("''"))
    templatefoot = Column(String(50), nullable=False, server_default=text("''"))
    templatesearch = Column(String(50), nullable=False, server_default=text("''"))
    thumb_content_type_id = Column(INTEGER(10), nullable=False)
    access_control = Column(Enum('', 'private'), nullable=False, server_default=text("''"))
    gp_restriction = Column(BIGINT(20), nullable=False, index=True, server_default=text("'0'"))
    active = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    sortorder = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    on_sitemap = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    on_menu = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    auto_url_update = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"), comment='+Automatically update the SITE_URL table')
    regen_url = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"), comment='+Regenerate URL in  SITE_URL table on next batch')
    search_description = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"), comment='+Index description fields')
    content_allow_reviews = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    date_posted = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_moved = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class CONTENTSCONTENT(Base):
    __tablename__ = 'CONTENTS_CONTENTS'

    content_id = Column(INTEGER(10), primary_key=True, nullable=False)
    child_content_id = Column(INTEGER(10), primary_key=True, nullable=False)
    display_priority = Column(INTEGER(10), nullable=False, server_default=text("'0'"))


class CONTENTSSUBSCRIBER(Base):
    __tablename__ = 'CONTENTS_SUBSCRIBERS'

    content_id = Column(INTEGER(10), primary_key=True, nullable=False)
    user_id = Column(INTEGER(10), primary_key=True, nullable=False)
    date_added = Column(TIMESTAMP, server_default=text("CURRENT_TIMESTAMP"))


class CONTENTTYPE(Base):
    __tablename__ = 'CONTENTTYPES'

    content_type_id = Column(INTEGER(10), primary_key=True)
    content_type_code = Column(String(20), nullable=False, unique=True, server_default=text("''"))
    content_type_name = Column(String(150), nullable=False, server_default=text("''"))
    content_type_description = Column(String(255), nullable=False, server_default=text("''"))
    url_format = Column(VARCHAR(255), nullable=False, server_default=text("'{lc_type_code}/{lc_path}/{lc_name}/'"))
    root_content_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    cp_visible_fields = Column(VARCHAR(1000), nullable=False, server_default=text("'*'"))
    template = Column(String(50), nullable=False, server_default=text("''"))
    templatehead = Column(String(50), nullable=False, server_default=text("''"))
    templatebody = Column(String(50), nullable=False, server_default=text("'default'"))
    templatefoot = Column(String(50), nullable=False, server_default=text("''"))
    templatesearch = Column(String(50), nullable=False, server_default=text("''"))
    sortorder = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    content_type_system = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    content_compatibility_code = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    default_approve_review = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    default_allow_reviews = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    date_built = Column(TIMESTAMP, nullable=False, index=True, server_default=text("'0000-00-00 00:00:00'"))
    show_in_cpanel = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))


class CONTENTEBAYCATEGORY(Base):
    __tablename__ = 'CONTENT_EBAY_CATEGORIES'

    ebstore_id = Column(INTEGER(10), primary_key=True, nullable=False)
    ebsite_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True)
    content_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True)
    content_type_id = Column(INTEGER(10), nullable=False, index=True)
    ebcat_id1 = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    ebcat_id2 = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    ebstcat_id1 = Column(BIGINT(20), nullable=False, index=True, server_default=text("'0'"))
    ebstcat_id2 = Column(BIGINT(20), nullable=False, index=True, server_default=text("'0'"))


class CONTENTLEVEL(Base):
    __tablename__ = 'CONTENT_LEVELS'

    content_id = Column(INTEGER(10), primary_key=True, nullable=False, server_default=text("'0'"))
    related_level = Column(INTEGER(10), primary_key=True, nullable=False, index=True, server_default=text("'0'"))
    related_content_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))


class CONTENTZONE(Base):
    __tablename__ = 'CONTENT_ZONES'

    content_id = Column(INTEGER(10), primary_key=True)
    content_name = Column(String(25), nullable=False, index=True)
    content_ref = Column(String(100), nullable=False, index=True, server_default=text("''"))
    content = Column(MEDIUMTEXT, nullable=False)
    description = Column(String(255), nullable=False)
    system = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class COUNTRY(Base):
    __tablename__ = 'COUNTRY'

    country_code = Column(CHAR(2), primary_key=True, server_default=text("''"))
    country_code3 = Column(CHAR(3), nullable=False, index=True)
    country_code_num = Column(CHAR(3), nullable=False)
    country_name = Column(String(50), nullable=False, server_default=text("''"))
    currency_code = Column(CHAR(3), nullable=False)
    has_postcode = Column(Enum('n', 'y'), server_default=text("'y'"))
    has_state = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    managed_postcode = Column(Enum('', 'n', 'y'), nullable=False, server_default=text("''"))
    country_active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    sort_order = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    continent = Column(Enum('Africa', 'Antarctica', 'Asia', 'Europe', 'North America', 'South America', 'Oceania'), nullable=False, server_default=text("'Africa'"))


class COUNTRYSTATE(Base):
    __tablename__ = 'COUNTRY_STATE'

    country_code = Column(CHAR(2), primary_key=True, nullable=False, server_default=text("''"))
    state_code = Column(String(80), primary_key=True, nullable=False, server_default=text("''"))
    state_name = Column(String(100), nullable=False, server_default=text("''"))
    iso3166_2_code = Column(String(4))


class CPADDONSLOG(Base):
    __tablename__ = 'CPADDONSLOGS'

    cpaddons_log_id = Column(INTEGER(10), primary_key=True)
    admin_username = Column(String(25), nullable=False, index=True, server_default=text("''"))
    addon_SKU = Column(String(25), nullable=False, index=True, server_default=text("''"))
    addon_fn = Column(String(25), nullable=False, index=True, server_default=text("''"))
    cpaddons_log_text = Column(String(1000), nullable=False, server_default=text("''"))
    ip_address = Column(String(50), nullable=False, server_default=text("''"))
    remote_admin = Column(String(32), nullable=False, server_default=text("''"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class CPADMINFILTER(Base):
    __tablename__ = 'CPADMINFILTERS'

    cpfilter_id = Column(INTEGER(10), primary_key=True)
    adminusername = Column(String(25), nullable=False, index=True, server_default=text("''"))
    cpfilter_tkn = Column(String(30), nullable=False, index=True, server_default=text("''"))
    cpfilter_name = Column(String(150), nullable=False, server_default=text("''"))
    cpfilter_search = Column(Text, nullable=False)
    cpfilter_sortorder = Column(TINYINT(3), nullable=False, index=True, server_default=text("'0'"))
    cpfilter_default = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class CPADMINLOG(Base):
    __tablename__ = 'CPADMINLOGS'

    cpadmin_log_id = Column(INTEGER(10), primary_key=True)
    admin_username = Column(String(25), nullable=False, index=True, server_default=text("''"))
    cpadmin_log_type = Column(String(50), nullable=False, index=True, server_default=text("''"))
    panel_type = Column(Enum('', 'mobile'), nullable=False, server_default=text("''"))
    page_tkn = Column(String(50), nullable=False, index=True, server_default=text("''"))
    page_fn = Column(String(50), nullable=False, index=True, server_default=text("''"))
    page_proc = Column(String(50), nullable=False, server_default=text("''"))
    ip_address = Column(String(50), nullable=False, server_default=text("''"))
    cpadmin_log_text = Column(String(1000), nullable=False, server_default=text("''"))
    pid = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    remote_admin = Column(String(50), nullable=False, server_default=text("''"))


class CPADMINPOPUP(Base):
    __tablename__ = 'CPADMINPOPUPS'

    admin_username = Column(String(25), primary_key=True, nullable=False, server_default=text("''"))
    cppopup_tkn = Column(String(25), primary_key=True, nullable=False, index=True, server_default=text("''"))
    cppopup_date = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    cppopup_off = Column(Enum('n', 'y'), nullable=False, index=True, server_default=text("'n'"))
    cppopup_lastid = Column(INTEGER(10), nullable=False, server_default=text("'0'"))


class CPADMINWIDGET(Base):
    __tablename__ = 'CPADMINWIDGETS'

    cpwidget_id = Column(INTEGER(10), primary_key=True, nullable=False, server_default=text("'0'"))
    admin_username = Column(String(25), primary_key=True, nullable=False, index=True, server_default=text("''"))
    cpwidget_position = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    cpwidget_sortorder = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    cpwidget_settings = Column(Text)


class CPERRORREPORT(Base):
    __tablename__ = 'CPERRORREPORTS'

    cperror_id = Column(BIGINT(20), primary_key=True)
    cperror_host = Column(String(50), nullable=False, server_default=text("''"))
    cperror_code = Column(String(100), nullable=False)
    cperror_agent = Column(String(150), nullable=False)
    cperror_url = Column(Text, nullable=False)
    cperror_msg = Column(Text, nullable=False)
    cperror_head = Column(Text, nullable=False)
    cperror_body = Column(Text, nullable=False)
    cperror_status = Column(Enum('New', 'Email', 'Fixed'), nullable=False, server_default=text("'New'"))
    cperror_created = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class CPGROUPPERMISSION(Base):
    __tablename__ = 'CPGROUPPERMISSIONS'

    cpperm_id = Column(INTEGER(10), primary_key=True, nullable=False, server_default=text("'0'"))
    cpperm_gp_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True, server_default=text("'0'"))
    security_level = Column(Enum('', 'Deny', 'Allow'), nullable=False, server_default=text("''"))


class CPGROUPPERMISSIONSCONTENT(Base):
    __tablename__ = 'CPGROUPPERMISSIONS_CONTENTS'

    content_type_id = Column(INTEGER(10), primary_key=True, nullable=False)
    cpperm_gp_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True)
    security_level = Column(Enum('', 'Deny', 'Allow'))


class CPHELPMANUAL(Base):
    __tablename__ = 'CPHELPMANUALS'

    cpmanual_id = Column(INTEGER(10), primary_key=True)
    cpmanual_tkn = Column(String(30), nullable=False, index=True, server_default=text("''"))
    cpmanual_fn = Column(String(10), nullable=False, index=True, server_default=text("''"))
    cpmanual_name = Column(String(150), nullable=False, server_default=text("''"))
    cpmanual_type = Column(Enum('', 'url', 'video'), nullable=False, server_default=text("''"))
    cpmanual_url = Column(Text, nullable=False)
    cpmanual_urlparam = Column(String(255), nullable=False, server_default=text("''"))
    cpmanual_sortorder = Column(TINYINT(3), nullable=False, server_default=text("'0'"))


class CPPERMISSIONGROUPROLE(Base):
    __tablename__ = 'CPPERMISSIONGROUPROLES'

    cpperm_gp_role_id = Column(INTEGER(10), primary_key=True)
    cpperm_gp_role_code = Column(String(45), nullable=False)
    cpperm_gp_role_name = Column(String(45), nullable=False)


class CPPERMISSIONGROUP(Base):
    __tablename__ = 'CPPERMISSIONGROUPS'

    cpperm_gp_id = Column(INTEGER(10), primary_key=True)
    cpperm_gp_title = Column(String(100), nullable=False, server_default=text("''"))
    cpperm_gp_description = Column(String(300), nullable=False, server_default=text("''"))
    cpperm_gp_role_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    cpperm_gp_hidden = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class CPPERMISSION(Base):
    __tablename__ = 'CPPERMISSIONS'

    cpperm_id = Column(INTEGER(10), primary_key=True)
    cpperm_type_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    cpperm_code = Column(String(50), index=True)
    cpperm_heading = Column(String(150), nullable=False, server_default=text("''"))
    cpperm_title = Column(String(150), nullable=False, server_default=text("''"))
    cpperm_description = Column(Text, nullable=False)
    cpperm_notes = Column(Text, nullable=False)
    cpperm_default = Column(Enum('Deny', 'Allow'), nullable=False, server_default=text("'Deny'"))
    cpperm_sortorder = Column(INTEGER(10), nullable=False, index=True)
    cpperm_visible = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class CPPERMISSIONTYPE(Base):
    __tablename__ = 'CPPERMISSIONTYPES'

    cpperm_type_id = Column(INTEGER(10), primary_key=True)
    cpperm_type_code = Column(String(30), nullable=False, index=True, server_default=text("''"))
    cpperm_type_title = Column(String(150), nullable=False, server_default=text("''"))
    cpperm_type_description = Column(String(300), nullable=False, server_default=text("''"))
    cpperm_type_sortorder = Column(INTEGER(10), nullable=False, index=True)


class CPREPORT(Base):
    __tablename__ = 'CPREPORTS'

    cpreport_id = Column(INTEGER(10), primary_key=True)
    cpreport_type_id = Column(INTEGER(10), nullable=False, index=True)
    cpreport_tkn = Column(String(25), nullable=False, index=True, server_default=text("''"))
    cpreport_fn = Column(String(25), nullable=False, server_default=text("''"))
    cpreport_title = Column(String(150), nullable=False, server_default=text("''"))
    cpreport_description = Column(String(255), nullable=False, server_default=text("''"))
    cpreport_qs = Column(Text, nullable=False)
    cpreport_system = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    cpreport_reqmodule = Column(String(10), nullable=False, server_default=text("''"))
    cpreport_sortorder = Column(TINYINT(3), nullable=False, server_default=text("'0'"))
    cpreport_visible = Column(Enum('n', 'y'), nullable=False, index=True, server_default=text("'n'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class CPREPORTTYPE(Base):
    __tablename__ = 'CPREPORTTYPES'

    cpreport_type_id = Column(INTEGER(10), primary_key=True)
    cpreport_type_name = Column(String(150), nullable=False, server_default=text("''"))
    cpreport_type_sortorder = Column(TINYINT(3), nullable=False, server_default=text("'0'"))


class CPSETUPWIZARD(Base):
    __tablename__ = 'CPSETUPWIZARD'

    setup_id = Column(INTEGER(10), primary_key=True)
    parent_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    setup_ref = Column(String(25), nullable=False, index=True, server_default=text("''"))
    setup_name = Column(String(255), nullable=False, server_default=text("''"))
    setup_description = Column(String(255), nullable=False, server_default=text("''"))
    admin_username = Column(String(25), nullable=False, index=True, server_default=text("''"))
    setup_hastmpl = Column(Enum('n', 'y', 'h', 's'), nullable=False, index=True, server_default=text("'n'"))
    date_completed = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class CPTOOLTIP(Base):
    __tablename__ = 'CPTOOLTIPS'

    tooltip_id = Column(INTEGER(10), primary_key=True)
    tooltip_type_id = Column(String(25), nullable=False, index=True, server_default=text("''"))
    tooltip_code = Column(String(25), nullable=False, index=True, server_default=text("''"))
    tooltip_title = Column(String(150), nullable=False, server_default=text("''"))
    tooltip_text = Column(String(500), nullable=False, server_default=text("''"))
    tooltip_url = Column(String(150), nullable=False, server_default=text("''"))
    tooltip_status = Column(Enum('', 'normal', 'popup', 'onload'), nullable=False, server_default=text("''"))


class CPTOOLTIPTYPE(Base):
    __tablename__ = 'CPTOOLTIPTYPES'

    tooltip_type_id = Column(INTEGER(10), primary_key=True)
    tooltip_type_code = Column(String(25), nullable=False, index=True, server_default=text("''"))
    tooltip_type_title = Column(String(150), nullable=False, server_default=text("''"))


class CPWIDGET(Base):
    __tablename__ = 'CPWIDGETS'

    cpwidget_id = Column(INTEGER(10), primary_key=True, server_default=text("'0'"))
    cpwidget_module = Column(String(30), nullable=False, index=True, server_default=text("''"))
    cpwidget_name = Column(String(150), nullable=False, server_default=text("''"))
    cpwidget_description = Column(String(1000), nullable=False, server_default=text("''"))
    cpwidget_type = Column(String(25), nullable=False)
    cpwidget_closable = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    cpwidget_active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class CRESCOMANAGEDSALESCHANNEL(Base):
    __tablename__ = 'CRESCO_MANAGED_SALES_CHANNEL'
    __table_args__ = (
        Index('managed_sales_channel_id_idx', 'managed_sales_channel_id', 'destination_id', 'environment', 'api_version', unique=True),
    )

    id = Column(INTEGER(11), primary_key=True)
    managed_sales_channel_id = Column(INTEGER(11), nullable=False)
    destination_id = Column(String(50), nullable=False)
    api_version = Column(Enum('2', '4'), nullable=False, server_default=text("'2'"))
    environment = Column(Enum('production', 'sandbox'), nullable=False, server_default=text("'production'"))


class CURRENCY(Base):
    __tablename__ = 'CURRENCY'

    currency_code = Column(String(3), primary_key=True)
    currency_name = Column(String(50), nullable=False, server_default=text("''"))
    currency_symbol = Column(String(25), nullable=False, server_default=text("''"))
    currency_rate = Column(DECIMAL(18, 8), nullable=False, server_default=text("'0.00000000'"))
    date_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class DATACACH(Base):
    __tablename__ = 'DATACACHES'

    cache_id = Column(INTEGER(10), primary_key=True)
    parent_cache_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    cache_module = Column(String(25), nullable=False, index=True)
    cache_name = Column(String(25), nullable=False, index=True)
    cache_type = Column(Enum('', 'store', 'cpanel', 'spanel'), nullable=False)
    cache_data = Column(MEDIUMTEXT, nullable=False)
    cache_parts = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    cache_ready = Column(Enum('n', 'y'), nullable=False)
    proc_pid = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    cache_expired = Column(DateTime, nullable=False)
    cache_created = Column(TIMESTAMP, nullable=False, index=True, server_default=text("CURRENT_TIMESTAMP"))


class DISCOUNTBYCATEGORY(Base):
    __tablename__ = 'DISCOUNTBY_CATEGORIES'

    trigger_id = Column(INTEGER(10), primary_key=True, nullable=False)
    category = Column(String(25), primary_key=True, nullable=False, index=True, server_default=text("''"))


class DISCOUNTBYCODE(Base):
    __tablename__ = 'DISCOUNTBY_CODE'

    trigger_id = Column(INTEGER(10), primary_key=True, nullable=False)
    discount_code = Column(String(50), primary_key=True, nullable=False, index=True)
    username = Column(String(25), nullable=False, index=True, server_default=text("''"))


class DISCOUNTBYDROPSHIPPER(Base):
    __tablename__ = 'DISCOUNTBY_DROPSHIPPERS'

    trigger_id = Column(INTEGER(10), primary_key=True, nullable=False)
    dropshipper = Column(String(25), primary_key=True, nullable=False, index=True, server_default=text("''"))


class DISCOUNTBYINVENTORY(Base):
    __tablename__ = 'DISCOUNTBY_INVENTORY'

    trigger_id = Column(INTEGER(10), primary_key=True, nullable=False)
    SKU = Column(String(25), primary_key=True, nullable=False, index=True, server_default=text("''"))


class DISCOUNTBYINVENTORYGROUP(Base):
    __tablename__ = 'DISCOUNTBY_INVENTORY_GROUPS'

    trigger_id = Column(INTEGER(10), primary_key=True, nullable=False)
    itm_gp_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True)


class DISCOUNTBYKITQTY(Base):
    __tablename__ = 'DISCOUNTBY_KITQTY'

    id = Column(INTEGER(11), primary_key=True)
    trigger_id = Column(INTEGER(10), nullable=False, unique=True)
    quantity_fm = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    quantity_to = Column(INTEGER(10), nullable=False, server_default=text("'0'"))


class DISCOUNTBYQTY(Base):
    __tablename__ = 'DISCOUNTBY_QTY'

    id = Column(INTEGER(11), primary_key=True)
    trigger_id = Column(INTEGER(10), nullable=False, unique=True)
    quantity_fm = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    quantity_to = Column(INTEGER(10), nullable=False, server_default=text("'0'"))


class DISCOUNTBYSHIPPINGGROUP(Base):
    __tablename__ = 'DISCOUNTBY_SHIPPING_GROUPS'

    trigger_id = Column(INTEGER(10), primary_key=True, nullable=False)
    sh_group_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True)


class DISCOUNTBYSHIPPINGID(Base):
    __tablename__ = 'DISCOUNTBY_SHIPPING_ID'

    trigger_id = Column(INTEGER(10), primary_key=True, nullable=False)
    sh_type_id = Column(TINYINT(3), primary_key=True, nullable=False, index=True)


class DISCOUNTBYTOTAL(Base):
    __tablename__ = 'DISCOUNTBY_TOTAL'

    trigger_id = Column(INTEGER(10), primary_key=True, nullable=False)
    target_type = Column(Enum('item', 'ship'), primary_key=True, nullable=False, index=True, server_default=text("'item'"))
    above_spent = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class DISCOUNTBYUSERGROUP(Base):
    __tablename__ = 'DISCOUNTBY_USERGROUPS'

    trigger_id = Column(INTEGER(10), primary_key=True, nullable=False)
    group_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True)


class DISCOUNTBYUSER(Base):
    __tablename__ = 'DISCOUNTBY_USERS'

    trigger_id = Column(INTEGER(10), primary_key=True, nullable=False)
    username = Column(String(25), primary_key=True, nullable=False, index=True, server_default=text("''"))


class DISCOUNTEVENT(Base):
    __tablename__ = 'DISCOUNTEVENTS'

    event_id = Column(INTEGER(10), primary_key=True)
    discount_id = Column(INTEGER(10), nullable=False, index=True)
    event_type_id = Column(INTEGER(10), nullable=False, index=True)
    event_apply_from = Column(Enum('', 'asc', 'desc', 'all'), nullable=False, server_default=text("''"))
    event_apply_type = Column(Enum('', 'lower_price'), nullable=False, server_default=text("''"))
    event_max_amount = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    event_max_repeat = Column(TINYINT(3), nullable=False, server_default=text("'0'"))
    event_apply_qty = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    event_active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    event_sortby = Column(TINYINT(3), nullable=False, server_default=text("'0'"))


class DISCOUNTEVENTTYPE(Base):
    __tablename__ = 'DISCOUNTEVENT_TYPES'

    event_type_id = Column(INTEGER(10), primary_key=True)
    event_type_name = Column(String(50), nullable=False, server_default=text("''"))
    event_type_code = Column(String(15), nullable=False, server_default=text("''"))


class DISCOUNT(Base):
    __tablename__ = 'DISCOUNTS'

    discount_id = Column(INTEGER(10), primary_key=True)
    discount_title = Column(String(50), nullable=False, server_default=text("''"))
    discount_subtitle = Column(String(100), nullable=False, server_default=text("''"))
    discount_description = Column(MEDIUMTEXT, nullable=False)
    discount_group_id = Column(INTEGER(11), index=True)
    default_code = Column(String(50), nullable=False, index=True)
    default_code_auto_gen = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    date_visible = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    discount_start = Column(DateTime, nullable=False, index=True, server_default=text("'0000-00-00 00:00:00'"))
    discount_end = Column(DateTime, nullable=False, index=True, server_default=text("'0000-00-00 00:00:00'"))
    discount_max = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    discount_once = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    discount_multiaddr = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    discount_exclusive = Column(Enum('n', 'y', 'a'), nullable=False, server_default=text("'n'"))
    discount_exc_promo = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    discount_auto = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    discount_dialog = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    discount_showapply = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    discount_private = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    discount_active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    discount_sortby = Column(TINYINT(3), nullable=False, index=True, server_default=text("'0'"))
    date_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class DISCOUNTTOCAP(Base):
    __tablename__ = 'DISCOUNTTO_CAP'
    __table_args__ = (
        Index('event_id', 'event_id', 'cap_type', unique=True),
    )

    id = Column(INTEGER(11), primary_key=True)
    event_id = Column(INTEGER(10), nullable=False)
    cap_type = Column(Enum('ship', 'item'), nullable=False, index=True, server_default=text("'ship'"))
    cap_amount = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    apply_all = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    repeat_apply = Column(TINYINT(1), nullable=False, server_default=text("'0'"))


class DISCOUNTTOCATEGORY(Base):
    __tablename__ = 'DISCOUNTTO_CATEGORY'

    event_id = Column(INTEGER(10), primary_key=True, nullable=False)
    content_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True, server_default=text("'0'"))
    reduce_percent = Column(DECIMAL(5, 2), nullable=False, server_default=text("'0.00'"))
    reduce_amount = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))


class DISCOUNTTOFREEITEM(Base):
    __tablename__ = 'DISCOUNTTO_FREEITEM'

    event_id = Column(INTEGER(10), primary_key=True, nullable=False)
    SKU = Column(String(25), primary_key=True, nullable=False, index=True, server_default=text("''"))


class DISCOUNTTOINVENTORY(Base):
    __tablename__ = 'DISCOUNTTO_INVENTORY'

    event_id = Column(INTEGER(10), primary_key=True, nullable=False)
    SKU = Column(String(25), primary_key=True, nullable=False, index=True, server_default=text("''"))
    reduce_percent = Column(DECIMAL(5, 2), nullable=False, server_default=text("'0.00'"))
    reduce_amount = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))


class DISCOUNTTOTOTAL(Base):
    __tablename__ = 'DISCOUNTTO_TOTAL'

    event_id = Column(INTEGER(10), primary_key=True, nullable=False)
    reduce_type = Column(Enum('item', 'ship'), primary_key=True, nullable=False, index=True, server_default=text("'item'"))
    reduce_percent = Column(DECIMAL(5, 2), nullable=False, server_default=text("'0.00'"))
    reduce_amount = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    apply_all = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class DISCOUNTTRIGGER(Base):
    __tablename__ = 'DISCOUNTTRIGGERS'

    trigger_id = Column(INTEGER(10), primary_key=True)
    discount_id = Column(INTEGER(10), nullable=False, index=True)
    trigger_type_id = Column(INTEGER(10), nullable=False, index=True)
    trigger_min_spend = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    trigger_single_use = Column(Enum('', 'c', 'u'), nullable=False, server_default=text("''"))
    trigger_exclude = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    trigger_active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    trigger_sortby = Column(TINYINT(3), nullable=False, server_default=text("'0'"))


class DISCOUNTTRIGGERTYPE(Base):
    __tablename__ = 'DISCOUNTTRIGGER_TYPES'

    trigger_type_id = Column(INTEGER(10), primary_key=True)
    trigger_type_name = Column(String(50), nullable=False, server_default=text("''"))
    trigger_type_code = Column(String(15), nullable=False, server_default=text("''"))


class DISCOUNTGROUP(Base):
    __tablename__ = 'DISCOUNT_GROUPS'

    id = Column(INTEGER(11), primary_key=True)
    discount_group_name = Column(String(255), nullable=False)


class DISCOUNTORDER(Base):
    __tablename__ = 'DISCOUNT_ORDERS'

    discount_id = Column(INTEGER(10), primary_key=True, nullable=False)
    order_id = Column(String(15), primary_key=True, nullable=False, index=True, server_default=text("''"))
    discount_code = Column(String(50), nullable=False, index=True, server_default=text("''"))
    username = Column(String(25), nullable=False, index=True, server_default=text("''"))
    email = Column(String(50), nullable=False, index=True, server_default=text("''"))
    discount_amount = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    date_used = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class DISPUTELINE(Base):
    __tablename__ = 'DISPUTELINES'

    dispute_id = Column(INTEGER(10), primary_key=True, nullable=False)
    counter = Column(TINYINT(4), primary_key=True, nullable=False, server_default=text("'0'"))
    SKU = Column(String(25), index=True)
    quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    description = Column(String(255))
    extra = Column(MEDIUMTEXT, nullable=False)
    dropshipper = Column(String(25), nullable=False, index=True)
    warehouse_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    itemnotes = Column(String(255))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class DISPUTEMESSAGE(Base):
    __tablename__ = 'DISPUTEMESSAGES'

    msg_id = Column(INTEGER(10), primary_key=True)
    dispute_id = Column(INTEGER(10), nullable=False, index=True)
    send_to = Column(Enum('Admin', 'Customer'), index=True, server_default=text("'Admin'"))
    admin = Column(String(25), nullable=False, index=True, server_default=text("''"))
    message = Column(MEDIUMTEXT, nullable=False)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class DISPUTEREASON(Base):
    __tablename__ = 'DISPUTEREASON'

    dispute_reason_id = Column(INTEGER(10), primary_key=True)
    dispute_type_id = Column(INTEGER(10), nullable=False, index=True)
    dispute_reason = Column(String(150), nullable=False)
    dispute_reason_desc = Column(String(150), nullable=False)
    orderby = Column(TINYINT(3), nullable=False, server_default=text("'0'"))
    dispute_active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class DISPUTE(Base):
    __tablename__ = 'DISPUTES'

    dispute_id = Column(INTEGER(10), primary_key=True)
    order_id = Column(String(15), nullable=False, index=True, server_default=text("''"))
    username = Column(String(25), nullable=False, index=True, server_default=text("''"))
    bill_first_name = Column(String(100), nullable=False, server_default=text("''"))
    bill_last_name = Column(String(100), nullable=False, server_default=text("''"))
    email = Column(String(50), nullable=False, server_default=text("''"))
    phone = Column(String(30), nullable=False, server_default=text("''"))
    dispute_reason_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    dispute_status_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    internal_notes = Column(MEDIUMTEXT, nullable=False)
    company_reply = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    customer_reply = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_filed = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_disputed = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class DISPUTESTATU(Base):
    __tablename__ = 'DISPUTESTATUS'

    dispute_status_id = Column(INTEGER(10), primary_key=True)
    dispute_status_name = Column(String(50), nullable=False, index=True)
    orderby = Column(TINYINT(3), nullable=False, server_default=text("'0'"))


class DISPUTETYPE(Base):
    __tablename__ = 'DISPUTETYPE'

    dispute_type_id = Column(INTEGER(10), primary_key=True)
    dispute_type_name = Column(String(25), nullable=False)
    dispute_type_title = Column(String(100), nullable=False)
    dispute_type_desc = Column(MEDIUMTEXT, nullable=False)
    orderby = Column(TINYINT(3), nullable=False, server_default=text("'0'"))


class DOCTEMPLATE(Base):
    __tablename__ = 'DOCTEMPLATES'

    doctmpl_id = Column(INTEGER(10), primary_key=True)
    doctmpl_name = Column(String(50), nullable=False)
    doctmpl_ref = Column(String(50), nullable=False, index=True)
    doctmpl_email = Column(String(50), nullable=False, server_default=text("''"))
    doctmpl_company = Column(String(50), nullable=False, server_default=text("''"))
    doctmpl_description = Column(String(255), nullable=False)
    doctmpl_file = Column(String(25), nullable=False)
    active = Column(Enum('n', 'y'), nullable=False)


class DOCTEMPLATESCUSTOM(Base):
    __tablename__ = 'DOCTEMPLATES_CUSTOM'

    doctmpl_id = Column(INTEGER(10), primary_key=True)
    doctmpl_name = Column(String(50), nullable=False)
    doctmpl_ref = Column(String(50), nullable=False, index=True)
    doctmpl_company = Column(String(50), nullable=False, server_default=text("''"))
    doctmpl_description = Column(String(255), nullable=False)
    doctmpl_file = Column(String(25), nullable=False)
    doctmpl_params = Column(MEDIUMTEXT, nullable=False)
    active = Column(Enum('n', 'y'), nullable=False)
    doctmpl_type = Column(String(25), nullable=False)
    doctmpl_sort = Column(INTEGER(2), nullable=False)


class DROPSHIPPER(Base):
    __tablename__ = 'DROPSHIPPERS'

    dropshipper = Column(String(25), primary_key=True)
    dropshipper_ref = Column(String(10), nullable=False, index=True, server_default=text("''"))
    sales_agent = Column(String(50), nullable=False, index=True)
    company = Column(String(50), nullable=False)
    first_name = Column(String(50), nullable=False)
    last_name = Column(String(50), nullable=False)
    street1 = Column(String(50), nullable=False)
    street2 = Column(String(50), nullable=False)
    city = Column(String(50), nullable=False)
    state = Column(String(50), nullable=False)
    zip = Column(String(15), nullable=False)
    country = Column(String(50), nullable=False)
    phone = Column(String(25), nullable=False)
    fax = Column(String(25), nullable=False)
    url = Column(String(50), nullable=False)
    email = Column(String(50), nullable=False)
    abn = Column(String(50), nullable=False)
    itm_cost_acc = Column(String(10), nullable=False)
    itm_income_acc = Column(String(10), nullable=False)
    itm_prefix = Column(String(10), nullable=False)
    itm_asset_acc = Column(String(10), nullable=False)
    itm_acc_code = Column(String(50), nullable=False)
    sh_type_id = Column(TINYINT(3), nullable=False)
    max_users = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    msg_email = Column(String(50), nullable=False)
    cancel_order_email = Column(String(50), nullable=False)
    approve_order_email = Column(String(50), nullable=False)
    new_order_email = Column(String(50), nullable=False)
    notes = Column(String(50), nullable=False)
    allow_create_purchase = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    allow_email_purchase = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    active = Column(Enum('N', 'Y'), nullable=False, server_default=text("'N'"))
    insert_date = Column(DateTime, nullable=False)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class DROPSHIPPERCONTACTLOG(Base):
    __tablename__ = 'DROPSHIPPER_CONTACT_LOG'

    log_id = Column(INTEGER(10), primary_key=True)
    username = Column(String(25), nullable=False, index=True)
    recontact_username = Column(String(25), index=True)
    dropshipper = Column(String(25), nullable=False, index=True)
    dropshipper_name = Column(String(100), nullable=False)
    date_contact = Column(DateTime, nullable=False)
    date_token = Column(DateTime, nullable=False)
    date_recontact = Column(Date)
    status = Column(Enum('Require Recontact', 'Recontacting', 'Completed'), nullable=False, server_default=text("'Require Recontact'"))
    notes = Column(MEDIUMTEXT)


class DROPSHIPPEREXPORT(Base):
    __tablename__ = 'DROPSHIPPER_EXPORTS'

    export_id = Column(INTEGER(10), primary_key=True)
    dropshipper = Column(String(25), nullable=False, index=True)
    export_group_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    export_name = Column(String(25), nullable=False)
    export_description = Column(String(255), nullable=False, server_default=text("''"))
    export_module = Column(String(25), nullable=False)
    export_method = Column(Enum('Email', 'FTP', 'URL', 'Local'), nullable=False, server_default=text("'Email'"))
    export_location = Column(String(2000), nullable=False, server_default=text("''"))
    export_loginusr = Column(String(50), nullable=False)
    export_loginpwd = Column(String(255), nullable=False)
    export_filename = Column(String(50), nullable=False)
    export_filetype = Column(String(15), nullable=False)
    export_params = Column(MEDIUMTEXT, nullable=False)
    export_file_header = Column(MEDIUMTEXT, nullable=False)
    export_file_footer = Column(MEDIUMTEXT, nullable=False)
    export_header = Column(MEDIUMTEXT, nullable=False)
    export_body = Column(MEDIUMTEXT, nullable=False)
    export_footer = Column(MEDIUMTEXT, nullable=False)
    export_setting = Column(MEDIUMTEXT, nullable=False)
    export_replace = Column(MEDIUMTEXT, nullable=False)
    export_max = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    export_priority = Column(TINYINT(3), nullable=False, server_default=text("'0'"))
    schedule_id = Column(INTEGER(10), nullable=False, index=True)
    active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    visible_menu = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    is_temporary = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    export_file_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    is_default = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    sample_url = Column(String(255), nullable=False, server_default=text("''"))
    export_ascii = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    export_encoding = Column(Enum('utf8', 'cp1252', 'ascii', 'htmlescaped', 'escaped'), nullable=False, server_default=text("'utf8'"))
    sub_type = Column(String(55), nullable=False, server_default=text("''"))


class DROPSHIPPEREXPORTGROUP(Base):
    __tablename__ = 'DROPSHIPPER_EXPORT_GROUPS'

    export_group_id = Column(INTEGER(10), primary_key=True)
    export_group_name = Column(String(50), nullable=False, server_default=text("''"))
    export_group_description = Column(String(255), nullable=False, server_default=text("''"))


class DROPSHIPPERIMPORT(Base):
    __tablename__ = 'DROPSHIPPER_IMPORTS'

    import_id = Column(INTEGER(10), primary_key=True)
    dropshipper = Column(String(25), nullable=False, index=True)
    import_group_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    import_name = Column(String(25), nullable=False)
    import_description = Column(String(255), nullable=False, server_default=text("''"))
    import_module = Column(String(25), nullable=False)
    import_soap_module = Column(String(25), nullable=False, server_default=text("''"))
    import_soap_setting = Column(Text, nullable=False)
    import_method = Column(Enum('File', 'URL', 'FTP'), nullable=False, server_default=text("'File'"))
    import_location = Column(String(255), nullable=False)
    import_loginusr = Column(String(50), nullable=False)
    import_loginpwd = Column(String(255), nullable=False, server_default=text("''"))
    import_filetype = Column(String(15), nullable=False)
    import_filezip = Column(String(5), nullable=False, server_default=text("''"))
    import_params = Column(MEDIUMTEXT, nullable=False)
    import_mapping = Column(MEDIUMTEXT, nullable=False)
    import_setting = Column(MEDIUMTEXT, nullable=False)
    import_replace = Column(MEDIUMTEXT, nullable=False)
    import_priority = Column(TINYINT(3), nullable=False, server_default=text("'0'"))
    schedule_id = Column(INTEGER(10), nullable=False, index=True)
    upd_id = Column(String(10), nullable=False)
    default_upd_id = Column(String(25), nullable=False)
    reset_upd_id = Column(Enum('n', 'y'), nullable=False)
    bkupftp_location = Column(String(255), nullable=False)
    bkupftp_loginusr = Column(String(50), nullable=False)
    bkupftp_loginpwd = Column(String(255), nullable=False, server_default=text("''"))
    active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    is_temporary = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    is_default = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    sample_url = Column(String(255), nullable=False, server_default=text("''"))
    file_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))


class DROPSHIPPERIMPORTGROUP(Base):
    __tablename__ = 'DROPSHIPPER_IMPORT_GROUPS'

    import_group_id = Column(INTEGER(10), primary_key=True)
    import_group_name = Column(String(50), nullable=False, server_default=text("''"))
    import_group_description = Column(String(255), nullable=False, server_default=text("''"))


class DROPSHIPPERIMPORTSOAP(Base):
    __tablename__ = 'DROPSHIPPER_IMPORT_SOAPS'

    import_soap_module = Column(String(25), primary_key=True, server_default=text("''"))
    import_module = Column(String(25), nullable=False, index=True, server_default=text("''"))
    module_name = Column(String(100), nullable=False, server_default=text("''"))
    module_status = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class DROPSHIPPERLOGINLOG(Base):
    __tablename__ = 'DROPSHIPPER_LOGINLOGS'

    log_id = Column(INTEGER(10), primary_key=True)
    dropshipper = Column(String(25), nullable=False, index=True)
    dsusername = Column(String(25), nullable=False, index=True)
    dssec_id = Column(INTEGER(10), nullable=False, index=True)
    ip = Column(String(25), nullable=False)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class DROPSHIPPERLOGIN(Base):
    __tablename__ = 'DROPSHIPPER_LOGINS'

    dssec_id = Column(INTEGER(10), primary_key=True)
    login_code = Column(String(25), nullable=False)
    login_ip = Column(String(25), nullable=False)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class DROPSHIPPERSECURITY(Base):
    __tablename__ = 'DROPSHIPPER_SECURITY'
    __table_args__ = (
        Index('dsusername', 'dropshipper', 'dsusername', unique=True),
    )

    dssec_id = Column(INTEGER(10), primary_key=True)
    dropshipper = Column(String(25), nullable=False)
    dsusername = Column(String(25), nullable=False)
    first_name = Column(String(50), nullable=False)
    last_name = Column(String(50), nullable=False)
    password = Column(String(255), nullable=False)
    enabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class DROPSHIPPERTOPSELLER(Base):
    __tablename__ = 'DROPSHIPPER_TOPSELLERS'

    SKU = Column(String(25), primary_key=True, nullable=False, server_default=text("''"))
    dropshipper = Column(String(25), primary_key=True, nullable=False, server_default=text("''"))
    totals = Column(INTEGER(11), nullable=False, server_default=text("'0'"))


class DROPSHIPPERUPDATE(Base):
    __tablename__ = 'DROPSHIPPER_UPDATES'

    dropshipper = Column(String(25), primary_key=True)
    last_checked = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    last_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class DROPSHIPPERZONE(Base):
    __tablename__ = 'DROPSHIPPER_ZONES'

    dropshipper = Column(String(25), primary_key=True, nullable=False)
    zone_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True)
    pirority = Column(TINYINT(3), nullable=False, server_default=text("'0'"))


class EBAYALLBIDDER(Base):
    __tablename__ = 'EBAY_ALL_BIDDERS'

    eblist_id = Column(INTEGER(10), primary_key=True, nullable=False)
    ebay_user_id = Column(String(50), primary_key=True, nullable=False, index=True, server_default=text("''"))
    maxbid = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    buyer_email = Column(String(50), nullable=False, server_default=text("''"))
    buyer_zip = Column(String(15), nullable=False, server_default=text("''"))
    buyer_country = Column(String(2), nullable=False, server_default=text("''"))
    buyer_goodstanding = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ebay_id = Column(String(38), nullable=False, index=True, server_default=text("''"))
    date_offered = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))


class EBAYAPICALLLOG(Base):
    __tablename__ = 'EBAY_API_CALL_LOG'

    eblog_id = Column(INTEGER(10), primary_key=True)
    ebstore_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    APIName = Column(String(20), nullable=False, index=True, server_default=text("''"))
    CallName = Column(String(50), nullable=False, index=True, server_default=text("''"))
    response_ack = Column(String(10), nullable=False, server_default=text("''"))
    response_code = Column(String(5), nullable=False, server_default=text("''"))
    error_message = Column(MEDIUMTEXT, nullable=False)
    date_called = Column(TIMESTAMP, nullable=False, index=True, server_default=text("CURRENT_TIMESTAMP"))


class EBAYBOPISINVENTORY(Base):
    __tablename__ = 'EBAY_BOPIS_INVENTORY'
    __table_args__ = (
        Index('bopis_location_id', 'bopis_location_id', 'SKU', unique=True),
    )

    id = Column(INTEGER(11), primary_key=True)
    bopis_location_id = Column(INTEGER(10), nullable=False)
    SKU = Column(String(25), nullable=False, server_default=text("''"))
    quantity = Column(INTEGER(11), nullable=False, server_default=text("'0'"))


class EBAYBOPISLOCATION(Base):
    __tablename__ = 'EBAY_BOPIS_LOCATIONS'
    __table_args__ = (
        Index('ebstore_id', 'ebstore_id', 'warehouse_id', unique=True),
    )

    id = Column(INTEGER(11), primary_key=True)
    ebstore_id = Column(INTEGER(11), nullable=False)
    warehouse_id = Column(INTEGER(11), nullable=False)
    ebay_location_id = Column(Text, nullable=False)
    is_active = Column(TINYINT(1), nullable=False, server_default=text("'1'"))
    date_created = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    date_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class EBAYCATEGORY(Base):
    __tablename__ = 'EBAY_CATEGORIES'

    ebcat_id = Column(INTEGER(10), primary_key=True, nullable=False)
    ebsite_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True)
    ebcatparent_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    ebcat_level = Column(TINYINT(3), nullable=False, server_default=text("'0'"))
    ebcat_name = Column(String(50))
    expired = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    intl_autofixed = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    leaf = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    no_lotsize = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    reserve_price = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    reduce_reserve = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    virtual = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    bestoffer = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    autopay = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    b2bvat = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class EBAYCATEGORIESCACHE(Base):
    __tablename__ = 'EBAY_CATEGORIES_CACHE'

    ebcat_id = Column(INTEGER(10), primary_key=True, nullable=False)
    ebsite_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True)
    ebcatparent_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    ebcat_level = Column(TINYINT(3), nullable=False, server_default=text("'0'"))
    ebcat_name = Column(String(50))
    expired = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    intl_autofixed = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    leaf = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    no_lotsize = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    reserve_price = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    reduce_reserve = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    virtual = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    bestoffer = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    autopay = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    b2bvat = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class EBAYCATEGORYFEATURE(Base):
    __tablename__ = 'EBAY_CATEGORY_FEATURES'

    ebcat_id = Column(INTEGER(10), primary_key=True, nullable=False)
    ebsite_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True)
    AdFormatEnabled = Column(Enum('n', 'y', 'classad', 'classadonly', 'local', 'only'), nullable=False, server_default=text("'n'"))
    BasicUpgradePackEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    BestOfferEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    BestOfferAutoAcceptEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    BestOfferAutoDeclineEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    BestOfferCounterEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    BuyerGuaranteeEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ClassifiedAdAutoAcceptEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ClassifiedAdAutoDeclineEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ClassifiedAdBestOfferEnabled = Column(Enum('n', 'y', 'required'), nullable=False, server_default=text("'n'"))
    ClassifiedAdCompanyNameEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ClassifiedAdContactByAddress = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ClassifiedAdContactByEmail = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ClassifiedAdContactByPhone = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ClassifiedAdCounterOffer = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ClassifiedAdPaymentMethod = Column(Enum('n', 'y', 'na'), nullable=False, server_default=text("'n'"))
    ClassifiedAdPayPerLead = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ClassifiedAdPhoneCount = Column(INTEGER(10), nullable=False)
    ClassifiedAdShippingMethod = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ClassifiedAdStreetCount = Column(INTEGER(10), nullable=False)
    CombinedFixedPriceTreatment = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ConditionEnabled = Column(Enum('n', 'y', 'required'), nullable=False, server_default=text("'n'"))
    ConditionValues = Column(Text, nullable=False)
    ConditionHelpURL = Column(String(255), nullable=False, server_default=text("''"))
    CrossBorderTradeAustralia = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    CrossBorderTradeGB = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    CrossBorderTradeNorthAmerica = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    DutchBINEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    eBayMotorsProAdFormat = Column(Enum('n', 'y', 'classad', 'classadonly', 'local', 'only'), nullable=False, server_default=text("'n'"))
    eBayMotorsProAutoAccept = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    eBayMotorsProAutoDecline = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    eBayMotorsProBestOffer = Column(Enum('n', 'y', 'required'), nullable=False, server_default=text("'n'"))
    eBayMotorsProCompanyName = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    eBayMotorsProContactByAddress = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    eBayMotorsProContactByEmail = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    eBayMotorsProContactByPhone = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    eBayMotorsProCounterOffer = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    eBayMotorsProPMCheckOut = Column(Enum('n', 'y', 'na'), nullable=False, server_default=text("'n'"))
    eBayMotorsProPhoneCount = Column(INTEGER(10), nullable=False)
    eBayMotorsProSeller = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    eBayMotorsProShippingMethod = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    eBayMotorsProStreetCount = Column(INTEGER(10), nullable=False)
    FreeGalleryPlusEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    FreePicturePackEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    GalleryFeaturedDurations = Column(Text, nullable=False)
    Group1MaxFlatShippingCost = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    Group2MaxFlatShippingCost = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    Group3MaxFlatShippingCost = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    HandlingTimeEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    HomePageFeaturedEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    INEscrowWorkflowTimeline = Column(Enum('default', 'workflowA', 'workflowB'), nullable=False, server_default=text("'default'"))
    ItemCompatibilityEnabled = Column(Enum('application', 'specification', 'n'), nullable=False, server_default=text("'n'"))
    ItemSpecificsEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ListingDuration = Column(Text, nullable=False)
    LocalListingDistancesNoSub = Column(String(50), nullable=False, server_default=text("''"))
    LocalListingDistancesRegular = Column(String(50), nullable=False, server_default=text("''"))
    LocalListingDistancesSpecialty = Column(String(50), nullable=False, server_default=text("''"))
    LocalMarketAdFormatEnabled = Column(Enum('n', 'y', 'classad', 'classadonly', 'local', 'only'), nullable=False, server_default=text("'n'"))
    LocalMarketAutoAcceptEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    LocalMarketAutoDeclineEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    LocalMarketBestOfferEnabled = Column(Enum('n', 'y', 'required'), nullable=False, server_default=text("'n'"))
    LocalMarketCompanyNameEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    LocalMarketContactByAddress = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    LocalMarketContactByEmail = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    LocalMarketContactByPhone = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    LocalMarketCounterOffer = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    LocalMarketNonSubscription = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    LocalMarketPMCheckOutEnabled = Column(Enum('n', 'y', 'na'), nullable=False, server_default=text("'n'"))
    LocalMarketPhoneCount = Column(INTEGER(10), nullable=False)
    LocalMarketPremiumSubscription = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    LocalMarketRegularSubscription = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    LocalMarketSeller = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    LocalMarketShippingMethod = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    LocalMarketSpecialitySub = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    LocalMarketStreetCount = Column(INTEGER(10), nullable=False)
    MaxFlatShippingCost = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    MaxFlatShippingCostCBTExempt = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    MaxItemCompatibility = Column(INTEGER(10), nullable=False)
    MinimumReservePrice = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    MinItemCompatibility = Column(INTEGER(10), nullable=False)
    NonSubscription = Column(Enum('local', 'localopt', 'national'), nullable=False, server_default=text("'local'"))
    PaisaPayFullEscrowEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    PaymentMethod = Column(Text, nullable=False)
    PayPalBuyerProtectionEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    PayPalRequired = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    PayPalRequiredForStoreOwner = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    PremiumSubscription = Column(Enum('local', 'localopt', 'national'), nullable=False, server_default=text("'local'"))
    ProPackEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ProPackPlusEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    RegularSubscription = Column(Enum('local', 'localopt', 'national'), nullable=False, server_default=text("'local'"))
    ReturnPolicyEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    RevisePriceAllowed = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ReviseQuantityAllowed = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    SafePaymentRequired = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    SellerContactDetailsEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ShippingTermsRequired = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    SkypeMeNonTransactionalEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    SkypeMeTransactionalEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    SpecialitySubscription = Column(Enum('local', 'localopt', 'national'), nullable=False, server_default=text("'local'"))
    StoreInventoryEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    StoreOwnerExtended = Column(Text, nullable=False)
    StoreOwnerExtendedEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    TransactionConfirmationRequest = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    UserConsentRequired = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ValueCategory = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ValuePackEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    VariationsEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    PickupDropOffEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class EBAYCATEGORYFEATURESCACHE(Base):
    __tablename__ = 'EBAY_CATEGORY_FEATURES_CACHE'

    ebcat_id = Column(INTEGER(10), primary_key=True, nullable=False)
    ebsite_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True)
    AdFormatEnabled = Column(Enum('n', 'y', 'classad', 'classadonly', 'local', 'only'), nullable=False, server_default=text("'n'"))
    BasicUpgradePackEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    BestOfferEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    BestOfferAutoAcceptEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    BestOfferAutoDeclineEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    BestOfferCounterEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    BuyerGuaranteeEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ClassifiedAdAutoAcceptEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ClassifiedAdAutoDeclineEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ClassifiedAdBestOfferEnabled = Column(Enum('n', 'y', 'required'), nullable=False, server_default=text("'n'"))
    ClassifiedAdCompanyNameEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ClassifiedAdContactByAddress = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ClassifiedAdContactByEmail = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ClassifiedAdContactByPhone = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ClassifiedAdCounterOffer = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ClassifiedAdPaymentMethod = Column(Enum('n', 'y', 'na'), nullable=False, server_default=text("'n'"))
    ClassifiedAdPayPerLead = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ClassifiedAdPhoneCount = Column(INTEGER(10), nullable=False)
    ClassifiedAdShippingMethod = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ClassifiedAdStreetCount = Column(INTEGER(10), nullable=False)
    CombinedFixedPriceTreatment = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ConditionEnabled = Column(Enum('n', 'y', 'required'), nullable=False, server_default=text("'n'"))
    ConditionValues = Column(Text, nullable=False)
    ConditionHelpURL = Column(String(255), nullable=False, server_default=text("''"))
    CrossBorderTradeAustralia = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    CrossBorderTradeGB = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    CrossBorderTradeNorthAmerica = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    DutchBINEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    eBayMotorsProAdFormat = Column(Enum('n', 'y', 'classad', 'classadonly', 'local', 'only'), nullable=False, server_default=text("'n'"))
    eBayMotorsProAutoAccept = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    eBayMotorsProAutoDecline = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    eBayMotorsProBestOffer = Column(Enum('n', 'y', 'required'), nullable=False, server_default=text("'n'"))
    eBayMotorsProCompanyName = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    eBayMotorsProContactByAddress = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    eBayMotorsProContactByEmail = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    eBayMotorsProContactByPhone = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    eBayMotorsProCounterOffer = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    eBayMotorsProPMCheckOut = Column(Enum('n', 'y', 'na'), nullable=False, server_default=text("'n'"))
    eBayMotorsProPhoneCount = Column(INTEGER(10), nullable=False)
    eBayMotorsProSeller = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    eBayMotorsProShippingMethod = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    eBayMotorsProStreetCount = Column(INTEGER(10), nullable=False)
    FreeGalleryPlusEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    FreePicturePackEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    GalleryFeaturedDurations = Column(Text, nullable=False)
    Group1MaxFlatShippingCost = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    Group2MaxFlatShippingCost = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    Group3MaxFlatShippingCost = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    HandlingTimeEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    HomePageFeaturedEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    INEscrowWorkflowTimeline = Column(Enum('default', 'workflowA', 'workflowB'), nullable=False, server_default=text("'default'"))
    ItemCompatibilityEnabled = Column(Enum('application', 'specification', 'n'), nullable=False, server_default=text("'n'"))
    ItemSpecificsEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ListingDuration = Column(Text, nullable=False)
    LocalListingDistancesNoSub = Column(String(50), nullable=False, server_default=text("''"))
    LocalListingDistancesRegular = Column(String(50), nullable=False, server_default=text("''"))
    LocalListingDistancesSpecialty = Column(String(50), nullable=False, server_default=text("''"))
    LocalMarketAdFormatEnabled = Column(Enum('n', 'y', 'classad', 'classadonly', 'local', 'only'), nullable=False, server_default=text("'n'"))
    LocalMarketAutoAcceptEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    LocalMarketAutoDeclineEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    LocalMarketBestOfferEnabled = Column(Enum('n', 'y', 'required'), nullable=False, server_default=text("'n'"))
    LocalMarketCompanyNameEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    LocalMarketContactByAddress = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    LocalMarketContactByEmail = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    LocalMarketContactByPhone = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    LocalMarketCounterOffer = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    LocalMarketNonSubscription = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    LocalMarketPMCheckOutEnabled = Column(Enum('n', 'y', 'na'), nullable=False, server_default=text("'n'"))
    LocalMarketPhoneCount = Column(INTEGER(10), nullable=False)
    LocalMarketPremiumSubscription = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    LocalMarketRegularSubscription = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    LocalMarketSeller = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    LocalMarketShippingMethod = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    LocalMarketSpecialitySub = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    LocalMarketStreetCount = Column(INTEGER(10), nullable=False)
    MaxFlatShippingCost = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    MaxFlatShippingCostCBTExempt = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    MaxItemCompatibility = Column(INTEGER(10), nullable=False)
    MinimumReservePrice = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    MinItemCompatibility = Column(INTEGER(10), nullable=False)
    NonSubscription = Column(Enum('local', 'localopt', 'national'), nullable=False, server_default=text("'local'"))
    PaisaPayFullEscrowEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    PaymentMethod = Column(Text, nullable=False)
    PayPalBuyerProtectionEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    PayPalRequired = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    PayPalRequiredForStoreOwner = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    PremiumSubscription = Column(Enum('local', 'localopt', 'national'), nullable=False, server_default=text("'local'"))
    ProPackEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ProPackPlusEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    RegularSubscription = Column(Enum('local', 'localopt', 'national'), nullable=False, server_default=text("'local'"))
    ReturnPolicyEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    RevisePriceAllowed = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ReviseQuantityAllowed = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    SafePaymentRequired = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    SellerContactDetailsEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ShippingTermsRequired = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    SkypeMeNonTransactionalEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    SkypeMeTransactionalEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    SpecialitySubscription = Column(Enum('local', 'localopt', 'national'), nullable=False, server_default=text("'local'"))
    StoreInventoryEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    StoreOwnerExtended = Column(Text, nullable=False)
    StoreOwnerExtendedEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    TransactionConfirmationRequest = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    UserConsentRequired = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ValueCategory = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ValuePackEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    VariationsEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    PickupDropOffEnabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class EBAYCATEGORYSPECIFIC(Base):
    __tablename__ = 'EBAY_CATEGORY_SPECIFICS'

    ebcat_id = Column(INTEGER(10), primary_key=True, nullable=False)
    ebsite_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True)
    ebspec_name = Column(String(65), primary_key=True, nullable=False, index=True, server_default=text("''"))
    ebspec_values = Column(Text, nullable=False)
    ebspec_variation = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    selection_mode = Column(Enum('FreeText', 'SelectionOnly', 'Prefilled'), nullable=False, server_default=text("'FreeText'"))
    minvalues = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    maxvalues = Column(INTEGER(10), nullable=False, server_default=text("'1'"))
    display_order = Column(INTEGER(10), nullable=False, server_default=text("'0'"))


class EBAYCATEGORYSPECIFICSCACHE(Base):
    __tablename__ = 'EBAY_CATEGORY_SPECIFICS_CACHE'

    ebcat_id = Column(INTEGER(10), primary_key=True, nullable=False)
    ebsite_id = Column(INTEGER(10), primary_key=True, nullable=False)
    ebspec_name = Column(String(65), primary_key=True, nullable=False, server_default=text("''"))
    ebspec_values = Column(Text, nullable=False)
    ebspec_variation = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    selection_mode = Column(Enum('FreeText', 'SelectionOnly', 'Prefilled'), nullable=False, server_default=text("'FreeText'"))
    minvalues = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    maxvalues = Column(INTEGER(10), nullable=False, server_default=text("'1'"))
    display_order = Column(INTEGER(10), nullable=False, server_default=text("'0'"))


class EBAYCOMPATBILITYNAME(Base):
    __tablename__ = 'EBAY_COMPATBILITY_NAMES'

    ebsite_id = Column(INTEGER(10), primary_key=True, nullable=False)
    ebcat_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True)
    ebcomp_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True)
    ebcompat_key = Column(String(50), nullable=False, index=True, server_default=text("''"))
    ebcompat_name = Column(String(50), nullable=False, server_default=text("''"))
    ebcompat_require = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class EBAYCOMPATBILITYVALUE(Base):
    __tablename__ = 'EBAY_COMPATBILITY_VALUES'
    __table_args__ = (
        Index('ebcomp', 'ebsite_id', 'ebcat_id', 'ebcomp_id'),
    )

    ebcompval_id = Column(INTEGER(10), primary_key=True)
    ebsite_id = Column(INTEGER(10), nullable=False)
    ebcat_id = Column(INTEGER(10), nullable=False, index=True)
    ebcomp_id = Column(INTEGER(10), nullable=False, index=True)
    parent_ebcompval_id = Column(INTEGER(10), nullable=False, index=True)
    ebcompval_name = Column(String(255), nullable=False, server_default=text("''"))
    ebcompval_type = Column(Enum('text', 'number', 'url'), nullable=False, server_default=text("'text'"))
    ebcompval_unit = Column(String(5), nullable=False, server_default=text("''"))


class EBAYCOMPATBILITYVERSION(Base):
    __tablename__ = 'EBAY_COMPATBILITY_VERSIONS'

    ebsite_id = Column(INTEGER(10), primary_key=True, nullable=False, server_default=text("'0'"))
    ebcat_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True)
    parent_ebcat_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    names_version = Column(String(50), nullable=False, server_default=text("''"))
    values_version = Column(String(50), nullable=False, server_default=text("''"))
    names_md5 = Column(String(50), nullable=False, server_default=text("''"))
    timesatmp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class EBAYCOUNTRY(Base):
    __tablename__ = 'EBAY_COUNTRY'

    ebcountry_id = Column(String(2), primary_key=True)
    ebcountry_name = Column(String(50), nullable=False, server_default=text("''"))


class EBAYCREATEDORDER(Base):
    __tablename__ = 'EBAY_CREATED_ORDERS'

    eborder_id = Column(INTEGER(10), primary_key=True, nullable=False, server_default=text("'0'"))
    order_id = Column(String(15), primary_key=True, nullable=False, index=True, server_default=text("''"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class EBAYCURRENCY(Base):
    __tablename__ = 'EBAY_CURRENCY'

    ebcurrency_id = Column(String(3), primary_key=True)
    ebcurrency_name = Column(String(50), nullable=False, server_default=text("''"))


class EBAYDESIGNTEMPLATE(Base):
    __tablename__ = 'EBAY_DESIGN_TEMPLATES'

    ebt_id = Column(INTEGER(10), primary_key=True)
    ebt_name = Column(String(150), nullable=False)
    ebt_template = Column(String(45), nullable=False)
    ebt_description = Column(Text)
    ebt_type_id = Column(INTEGER(10), nullable=False, index=True)
    ebt_body = Column(MEDIUMTEXT, nullable=False)
    ebt_hidden = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    date_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class EBAYDESIGNTEMPLATETYPE(Base):
    __tablename__ = 'EBAY_DESIGN_TEMPLATE_TYPES'

    ebt_type_id = Column(INTEGER(10), primary_key=True)
    ebt_type_name = Column(String(50), nullable=False)
    ebt_type_description = Column(String(255), nullable=False)


class EBAYDISPUTE(Base):
    __tablename__ = 'EBAY_DISPUTES'

    ebdispute_id = Column(INTEGER(10), primary_key=True)
    ebtrans_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    ebstore_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    ebay_id = Column(String(38), nullable=False, index=True, server_default=text("''"))
    ebay_trans_id = Column(String(38), nullable=False, index=True, server_default=text("''"))
    ebay_dispute_id = Column(String(50), nullable=False, index=True, server_default=text("''"))
    credit_eligibility = Column(Enum('', 'Eligible', 'InEligible'), nullable=False, server_default=text("''"))
    ebdispute_explanation_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    ebdispute_record_type = Column(Enum('', 'HalfDispute', 'ItemNotReceived', 'UnpaidItem'), nullable=False, server_default=text("''"))
    ebdispute_reason_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    ebdispute_state_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    ebdispute_status_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    date_modified = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_created = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_filed = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class EBAYDISPUTEEXPLANATION(Base):
    __tablename__ = 'EBAY_DISPUTE_EXPLANATIONS'

    ebdispute_explanation_id = Column(INTEGER(10), primary_key=True, server_default=text("'0'"))
    ebdispute_explanation_code = Column(String(50), nullable=False, index=True, server_default=text("''"))
    ebdispute_explanation_name = Column(String(50), nullable=False, server_default=text("''"))
    ebdispute_explanation_desc = Column(String(255), nullable=False, server_default=text("''"))


class EBAYDISPUTEREASONEXPLAN(Base):
    __tablename__ = 'EBAY_DISPUTE_REASONEXPLANS'

    ebdispute_reason_id = Column(INTEGER(10), primary_key=True, nullable=False, server_default=text("'0'"))
    ebdispute_explanation_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True, server_default=text("'0'"))


class EBAYDISPUTEREASON(Base):
    __tablename__ = 'EBAY_DISPUTE_REASONS'

    ebdispute_reason_id = Column(INTEGER(10), primary_key=True, server_default=text("'0'"))
    ebdispute_reason_code = Column(String(50), nullable=False, index=True, server_default=text("''"))
    ebdispute_reason_name = Column(String(50), nullable=False, server_default=text("''"))
    ebdispute_reason_desc = Column(String(255), nullable=False, server_default=text("''"))
    ebdispute_reason_type = Column(Enum('', 'i', 'o'), nullable=False, server_default=text("''"))


class EBAYDISPUTESTATE(Base):
    __tablename__ = 'EBAY_DISPUTE_STATES'

    ebdispute_state_id = Column(INTEGER(10), primary_key=True, server_default=text("'0'"))
    ebdispute_state_code = Column(String(50), nullable=False, index=True, server_default=text("''"))
    ebdispute_state_name = Column(String(50), nullable=False, server_default=text("''"))
    ebdispute_state_desc = Column(String(255), nullable=False, server_default=text("''"))


class EBAYDISPUTESTATU(Base):
    __tablename__ = 'EBAY_DISPUTE_STATUS'

    ebdispute_status_id = Column(INTEGER(10), primary_key=True, server_default=text("'0'"))
    ebdispute_status_code = Column(String(50), nullable=False, index=True, server_default=text("''"))
    ebdispute_status_name = Column(String(50), nullable=False, server_default=text("''"))
    ebdispute_status_desc = Column(String(350), nullable=False, server_default=text("''"))


class EBAYFEEDBACK(Base):
    __tablename__ = 'EBAY_FEEDBACKS'

    ebfeedback_id = Column(INTEGER(10), primary_key=True)
    ebfeedback_text = Column(String(80), nullable=False, server_default=text("''"))
    ebfeedback_active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class EBAYITEMIMAGE(Base):
    __tablename__ = 'EBAY_ITEM_IMAGES'

    ebimg_id = Column(INTEGER(10), primary_key=True)
    ebstore_id = Column(INTEGER(10), nullable=False, index=True)
    inventory_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    ebimg_type = Column(String(15), nullable=False, index=True, server_default=text("''"))
    ebimg_set = Column(Enum('', 'Supersize'), nullable=False, index=True, server_default=text("''"))
    ebimg_url = Column(String(255), nullable=False, server_default=text("''"))
    ebimg_filemd5 = Column(String(32), nullable=False, server_default=text("''"))
    ebimg_filesize = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    image_width = Column(INTEGER(10))
    image_height = Column(INTEGER(10))
    ebimg_msg = Column(String(255), nullable=False, server_default=text("''"))
    date_ping = Column(DateTime, nullable=False, index=True, server_default=text("'0000-00-00 00:00:00'"))
    date_check = Column(DateTime, nullable=False, index=True, server_default=text("'0000-00-00 00:00:00'"))
    date_upload = Column(DateTime, nullable=False, index=True, server_default=text("'0000-00-00 00:00:00'"))


class EBAYLISTING(Base):
    __tablename__ = 'EBAY_LISTINGS'
    __table_args__ = (
        Index('inventory_variations_status', 'inventory_id', 'list_variations', 'eblist_status', 'date_start'),
        Index('inventory_kitvariations_status', 'inventory_id', 'list_kit_variations', 'eblist_status', 'date_start'),
        Index('inventory_variations_kitvariations_status', 'inventory_id', 'list_variations', 'list_kit_variations'),
        Index('ebay_subtitle_etc_idx', 'ebay_subtitle', 'lot_size', 'start_price', 'list_type', 'list_variations', 'ebsite_id', 'ebstore_id')
    )

    eblist_id = Column(INTEGER(10), primary_key=True)
    ebtmpl_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    parent_eblist_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    relist_number = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    qty_control = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    offset_qty = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    ebstore_id = Column(INTEGER(10), nullable=False, index=True)
    ebsite_id = Column(INTEGER(10), nullable=False, index=True)
    ebcurrency_id = Column(String(3), nullable=False, index=True, server_default=text("''"))
    currency_rate = Column(DECIMAL(18, 8), nullable=False, server_default=text("'0.00000000'"))
    ebcat_id1 = Column(INTEGER(10), nullable=False, index=True)
    ebcat_id2 = Column(INTEGER(10), nullable=False, index=True)
    ebstcat_id1 = Column(BIGINT(20), nullable=False, index=True)
    ebstcat_id2 = Column(BIGINT(20), nullable=False, index=True)
    warehouse_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    list_type = Column(Enum('Chinese', 'FixedPriceItem', 'AdType', 'LeadGeneration', 'PersonalOffer'), nullable=False, index=True, server_default=text("'Chinese'"))
    list_subtype = Column(Enum('', 'ClassifiedAd', 'LocalMarketBestOfferOnly'), nullable=False, server_default=text("''"))
    list_variations = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    list_kit_variations = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    list_duration = Column(String(10), nullable=False, index=True, server_default=text("''"))
    condition_id = Column(INTEGER(10), nullable=False)
    ebay_id = Column(String(38), nullable=False, index=True, server_default=text("''"))
    SKU = Column(String(25), nullable=False, index=True, server_default=text("''"))
    ebay_itemcode = Column(String(50), nullable=False, server_default=text("''"))
    inventory_id = Column(INTEGER(10), nullable=False)
    ebay_title = Column(String(80), nullable=False, index=True, server_default=text("''"))
    ebay_subtitle = Column(String(55), nullable=False, server_default=text("''"))
    ebay_description = Column(MEDIUMTEXT, nullable=False)
    ebay_quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    available_quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    max_quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    sold_quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    lot_size = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    start_price = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    buy_now_price = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    current_price = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    reserve_price = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    auto_bestoffer_price = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    min_bestoffer_price = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    outlet_comparison_price = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    original_retail_price = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    min_advertised_price = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    min_advertised_price_type = Column(Enum('', 'None', 'DuringCheckout', 'PreCheckout'), nullable=False, server_default=text("''"))
    soldoff_ebay = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    use_template_data = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    use_template_ship_opt = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    use_template_pay_opt = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    private_listing = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    best_offer = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    best_offer_count = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    eblist_status = Column(Enum('', 'Failed', 'Scheduled', 'Active', 'Stopped', 'Ended', 'Sold', 'Completed'), nullable=False, index=True, server_default=text("''"))
    current_data = Column(Text, nullable=False)
    ebay_notes = Column(Text, nullable=False)
    private_notes = Column(Text, nullable=False)
    bid_count = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    bidder_count = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    reserve_met = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    lead_fee = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    lead_count = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    new_lead_count = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    question_count = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    watch_count = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    fee_currency_rate = Column(DECIMAL(18, 8), nullable=False, server_default=text("'0.00000000'"))
    fee_ebcurrency_id = Column(String(3), nullable=False, index=True, server_default=text("''"))
    ebay_fee = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    relist_option = Column(Enum('n', 'y', 'i', 'o'), nullable=False, server_default=text("'n'"))
    auto_free_update = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    return_message = Column(Text, nullable=False)
    ebay_url = Column(String(255), nullable=False, server_default=text("''"))
    date_start = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_end = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    end_reason = Column(Enum('', 'NotAvailable', 'Incorrect', 'LostOrBroken', 'OtherListingError', 'SellToHighBidder'), nullable=False, server_default=text("''"))
    admin_ended = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    date_dlbidder = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    chance2_accept_price = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    chance2_max_qty = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    chance2_duration = Column(Enum('', 'Days_1', 'Days_3', 'Days_5', 'Days_7'), server_default=text("''"))
    chance2_offered = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    has_promotion_discount = Column(TINYINT(1), nullable=False)
    relist_count = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    relist_fee = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    listed_by_neto = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    date_relist = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_updated = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_checked = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_sync = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_created = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    last_md5_revise_data = Column(String(32), nullable=False, server_default=text("''"))
    no_auto_revise = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    violations_count = Column(INTEGER(11), nullable=False, server_default=text("'0'"))


class EBAYLISTINGEMAIL(Base):
    __tablename__ = 'EBAY_LISTING_EMAIL'

    eblist_id = Column(INTEGER(10), primary_key=True)
    has_stock = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    date_sent = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class EBAYLISTINGVARIATION(Base):
    __tablename__ = 'EBAY_LISTING_VARIATIONS'

    eblist_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True)
    SKU = Column(String(25), primary_key=True, nullable=False)
    ebay_itemcode = Column(String(80), nullable=False, server_default=text("''"))
    inventory_id = Column(INTEGER(10), nullable=False, index=True)
    var_sold_qty = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    var_available_qty = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    var_start_price = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    var_specifics = Column(String(2000), nullable=False, server_default=text("''"))
    var_current_data = Column(String(1000), nullable=False, server_default=text("''"))


class EBAYLISTINGVIOLATION(Base):
    __tablename__ = 'EBAY_LISTING_VIOLATIONS'

    id = Column(INTEGER(11), primary_key=True)
    eblist_id = Column(INTEGER(11), nullable=False, index=True)
    ebstore_id = Column(INTEGER(11), nullable=False, index=True)
    ebsite_id = Column(INTEGER(11), nullable=False, index=True)
    ebay_id = Column(String(38), nullable=False, index=True)
    compliance_type = Column(String(50), nullable=False)
    violation_reason_code = Column(Enum('', 'OUTSIDE_EBAY_BUYING_AND_SELLING', 'HTTPS', 'PRODUCT_ADOPTION'), nullable=False, server_default=text("''"))
    item_code = Column(String(80), nullable=False)
    variation_data = Column(Text, nullable=False)
    message = Column(Text, nullable=False)
    date_downloaded = Column(DateTime, nullable=False)


class EBAYLISTTEMPLATE(Base):
    __tablename__ = 'EBAY_LISTTEMPLATES'

    ebtmpl_id = Column(INTEGER(10), primary_key=True)
    ebstore_id = Column(INTEGER(10), nullable=False, index=True)
    ebsite_id = Column(INTEGER(10), nullable=False, index=True)
    ebt_id = Column(INTEGER(10), nullable=False, index=True)
    active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ebtmpl_name = Column(String(50), nullable=False, server_default=text("''"))
    ebcurrency_id = Column(String(3), nullable=False, index=True)
    ebcat_id1 = Column(INTEGER(10), nullable=False, index=True)
    ebcat_id2 = Column(INTEGER(10), nullable=False, index=True)
    ebstcat_id1 = Column(BIGINT(20), nullable=False, index=True)
    ebstcat_id2 = Column(BIGINT(20), nullable=False, index=True)
    warehouse_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    condition_id = Column(INTEGER(10), nullable=False)
    compat_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    attr_prefill = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    attr_auto = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    category_auto = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    item_specifics = Column(Text, nullable=False)
    item_attributes = Column(Text, nullable=False)
    ebay_quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    lot_size = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    date_start = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_stop = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    schedule_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    qty_control = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    offset_qty = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    stock_lv1_qty = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    stock_lv2_qty = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    stock_lv1_prpercent = Column(DECIMAL(6, 1), nullable=False, server_default=text("'0.0'"))
    stock_lv2_prpercent = Column(DECIMAL(6, 1), nullable=False, server_default=text("'0.0'"))
    chance2_max_qty = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    chance2_duration = Column(Enum('', 'Days_1', 'Days_3', 'Days_5', 'Days_7'), server_default=text("''"))
    relist_option = Column(Enum('n', 'y', 'i', 'o'), nullable=False, server_default=text("'n'"))
    max_relist = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    auto_free_update = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ignore_exchange_rate = Column(Enum('n', 'y'), nullable=False)
    list_type = Column(Enum('Chinese', 'FixedPriceItem', 'AdType', 'LeadGeneration'), nullable=False, server_default=text("'Chinese'"))
    list_subtype = Column(Enum('', 'ClassifiedAd', 'LocalMarketBestOfferOnly'), nullable=False, server_default=text("''"))
    list_variations = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    list_kit_variations = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    list_duration = Column(String(10), nullable=False, server_default=text("''"))
    private_listing = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    hitcounter = Column(Enum('', 'BasicStyle', 'HiddenStyle', 'NoHitCounter', 'RetroStyle'), nullable=False, server_default=text("''"))
    list_enhancement = Column(Text, nullable=False)
    gift_icon = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    gift_services = Column(Text, nullable=False)
    pay_per_lead = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ad_contact_by_email = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    list_content = Column(Text, nullable=False)
    value_mapping = Column(Text, nullable=False)
    best_offer = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    min_advertised_price_type = Column(Enum('', 'None', 'DuringCheckout', 'PreCheckout'), nullable=False, server_default=text("''"))
    soldoff_ebay = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    prefill_info = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    inc_stock_photo = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    use_stock_gallery = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    gallery_type = Column(Enum('', 'None', 'Gallery', 'Plus', 'Featured'), nullable=False, server_default=text("''"))
    gallery_duration = Column(String(10), nullable=False, server_default=text("''"))
    photo_display = Column(Enum('', 'None', 'PicturePack', 'SuperSize'), nullable=False, server_default=text("''"))
    image_url = Column(Text, nullable=False)
    local_distance = Column(String(15), nullable=False)
    itm_country = Column(String(2), nullable=False, server_default=text("''"))
    itm_city = Column(String(45), nullable=False, server_default=text("''"))
    itm_zip = Column(String(15), nullable=False, server_default=text("''"))
    no_buyer_req = Column(Enum('', 'n', 'y'), nullable=False, server_default=text("''"))
    req_paypal_acc = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    max_bpv_count = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    max_bpv_period = Column(Enum('', 'Days_30', 'Days_180', 'Days_360'), nullable=False, server_default=text("''"))
    max_uis_count = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    max_uis_period = Column(Enum('', 'Days_30', 'Days_180', 'Days_360'), nullable=False, server_default=text("''"))
    cross_border_trade = Column(Text, nullable=False)
    ship_to_locations = Column(Text, nullable=False)
    reg_country_only = Column(Enum('', 'n', 'y'), nullable=False, server_default=text("''"))
    exclude_ship_loc = Column(Text, nullable=False)
    shipping_type = Column(Enum('n', 'f', 'c', 'e', 'd'), nullable=False, server_default=text("'n'"))
    shipping_int_type = Column(Enum('n', 'f', 'c', 'e'))
    sh_group_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    shipping_free = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    allow_pudo = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    shipping_options = Column(Text, nullable=False)
    shipping_int_options = Column(Text, nullable=False)
    dispatch_time_max = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    shterms_in_desc = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ship_disc = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ship_disc_code = Column(String(25), nullable=False, server_default=text("''"))
    ship_disc_int = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ship_disc_code_int = Column(String(25), nullable=False, server_default=text("''"))
    insurance_opt = Column(Enum('', 'NotOffered', 'IncludedInShippingHandling', 'Optional', 'Required'), nullable=False, server_default=text("''"))
    insurance_fee = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    insurance_cpercent = Column(DECIMAL(5, 2), nullable=False, server_default=text("'0.00'"))
    insurance_int_opt = Column(Enum('', 'NotOffered', 'IncludedInShippingHandling', 'Optional', 'Required'), nullable=False, server_default=text("''"))
    insurance_int_fee = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    insurance_int_cpercent = Column(DECIMAL(5, 2), nullable=False, server_default=text("'0.00'"))
    shcal_cost = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    shcal_int_cost = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    shcal_unit = Column(Enum('Metric', 'English'), nullable=False, server_default=text("'Metric'"))
    shcal_weight_major = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    shcal_weight_minor = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    shcal_weight_sys = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    shcal_length = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    shcal_width = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    shcal_height = Column(Float(asdecimal=True), nullable=False, server_default=text("'0'"))
    shcal_dim_sys = Column(Enum('n', 'y', 'a'), nullable=False, server_default=text("'n'"))
    shcal_from_zip = Column(String(15), nullable=False, server_default=text("''"))
    shcal_package = Column(String(30), nullable=False, server_default=text("''"))
    shcal_irregular = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    sales_tax_state = Column(String(50), nullable=False, server_default=text("''"))
    sales_tax = Column(Float, nullable=False, server_default=text("'0'"))
    shipping_inctax = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    bopis = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    payment_instructions = Column(Text, nullable=False)
    autopay = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    payment_methods = Column(Text, nullable=False)
    paypal_email = Column(String(75), nullable=False, server_default=text("''"))
    rtn_accept_opt = Column(String(25), nullable=False, server_default=text("''"))
    rtn_within_opt = Column(String(25), nullable=False, server_default=text("''"))
    rtn_refund_opt = Column(String(25), nullable=False, server_default=text("''"))
    rtn_paid_shipping = Column(String(25), nullable=False, server_default=text("''"))
    rtn_description = Column(Text, nullable=False)
    rtn_wty_offered = Column(String(25), nullable=False, server_default=text("''"))
    rtn_wty_type = Column(String(25), nullable=False, server_default=text("''"))
    rtn_wty_duration = Column(String(25), nullable=False, server_default=text("''"))
    contact_company = Column(String(50), nullable=False, server_default=text("''"))
    contact_street1 = Column(String(180), nullable=False, server_default=text("''"))
    contact_street2 = Column(String(180), nullable=False, server_default=text("''"))
    contact_county = Column(String(2), nullable=False, server_default=text("''"))
    contact_phone_c1 = Column(String(2), nullable=False, server_default=text("''"))
    contact_phone_a1 = Column(String(5), nullable=False, server_default=text("''"))
    contact_phone1 = Column(String(30), nullable=False, server_default=text("''"))
    contact_phone_c2 = Column(String(2), nullable=False, server_default=text("''"))
    contact_phone_a2 = Column(String(5), nullable=False, server_default=text("''"))
    contact_phone2 = Column(String(30), nullable=False, server_default=text("''"))
    hours_any1 = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    hours_days_1 = Column(Enum('None', 'EveryDay', 'Weekdays', 'Weekends'), nullable=False, server_default=text("'None'"))
    hours_from_1 = Column(Time, nullable=False, server_default=text("'00:00:00'"))
    hours_to_1 = Column(Time, nullable=False, server_default=text("'00:00:00'"))
    hours_any2 = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    hours_days_2 = Column(Enum('None', 'EveryDay', 'Weekdays', 'Weekends'), nullable=False, server_default=text("'None'"))
    hours_from_2 = Column(Time, nullable=False, server_default=text("'00:00:00'"))
    hours_to_2 = Column(Time, nullable=False, server_default=text("'00:00:00'"))
    ebtimezone_id = Column(String(25), nullable=False, index=True, server_default=text("''"))
    vat_bseller = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    vat_restricted = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    vat_percent = Column(Float, nullable=False, server_default=text("'0'"))
    payment_policy_id = Column(String(20), nullable=False)
    return_policy_id = Column(String(20), nullable=False)
    shipping_policy_id = Column(String(20), nullable=False)
    optimal_pic_size = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    eblslayout_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    eblsteme_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    date_updated = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_scheduled = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_created = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class EBAYLISTTEMPLATEITEM(Base):
    __tablename__ = 'EBAY_LISTTEMPLATE_ITEMS'

    ebtmpl_itm_id = Column(INTEGER(10), primary_key=True)
    ebtmpl_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    SKU = Column(String(25), nullable=False, index=True, server_default=text("''"))
    inventory_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    ebt_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    ebcat_id1 = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    ebcat_id2 = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    ebstcat_id1 = Column(BIGINT(20), nullable=False, index=True, server_default=text("'0'"))
    ebstcat_id2 = Column(BIGINT(20), nullable=False, index=True, server_default=text("'0'"))
    list_fee = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    date_verified = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_added = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_listed = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))


class EBAYORDER(Base):
    __tablename__ = 'EBAY_ORDERS'

    eborder_id = Column(INTEGER(10), primary_key=True)
    ebstore_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    ebay_user_id = Column(String(50), nullable=False, index=True, server_default=text("''"))
    ebay_order_id = Column(String(50), nullable=False, index=True, server_default=text("''"))
    buyer_email = Column(String(50), nullable=False, index=True, server_default=text("''"))
    ebcurrency_id = Column(String(3), nullable=False, index=True)
    currency_rate = Column(DECIMAL(18, 8), nullable=False, server_default=text("'0.00000000'"))
    ship_full_name = Column(String(50), nullable=False, index=True, server_default=text("''"))
    ship_street1 = Column(String(50), nullable=False, server_default=text("''"))
    ship_street2 = Column(String(50), nullable=False, server_default=text("''"))
    ship_city = Column(String(50), nullable=False, server_default=text("''"))
    ship_state = Column(String(50), nullable=False, server_default=text("''"))
    ship_zip = Column(String(15), nullable=False, server_default=text("''"))
    ship_country = Column(String(2), nullable=False, index=True, server_default=text("''"))
    ship_phone = Column(String(30), nullable=False, server_default=text("''"))
    shipping_expedited = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ebship_service_code = Column(String(50), nullable=False, index=True, server_default=text("''"))
    grand_total = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    subtotal = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    shipping_cost_add = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    shipping_cost_1st = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    is_bopis = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    is_pudo = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    pickup_status = Column(Enum('', 'Invalid', 'NotApplicable', 'PendingMerchantConfirmation', 'Pickedup', 'PickupCancelled', 'PickupCancelledBuyerNoShow', 'PickupCancelledBuyerRejected', 'PickupCancelledOutOfStock', 'ReadyToPickup'), nullable=False, server_default=text("''"))
    ebay_location_id = Column(INTEGER(11))
    total_shipping_insurance = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    total_adjust_amount = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    total_tax_amount = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    total_ebay_fvfee = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    amount_saved = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    amount_paid = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    shipping_time_max = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    ebpaytype_id = Column(String(30), nullable=False, index=True, server_default=text("''"))
    payment_reference_id = Column(String(50), nullable=False, server_default=text("''"))
    payment_refunded = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    buyer_comment = Column(Text, nullable=False)
    ebay_order_status = Column(Enum('Invalid', 'Authenticated', 'Active', 'Inactive', 'Default', 'InProcess', 'Completed', 'Cancelled', 'Shipped'), nullable=False, index=True, server_default=text("'Invalid'"))
    ebay_complete_status = Column(Enum('Incomplete', 'Pending', 'Complete'), nullable=False, index=True, server_default=text("'Incomplete'"))
    ebay_payment_status = Column(Enum('', 'PaymentInProcess', 'PayPalPaymentInProcess', 'BuyerCreditCardFailed', 'BuyerECheckBounced', 'BuyerFailedPaymentReportedBySeller', 'NoPaymentFailure'), nullable=False, index=True, server_default=text("''"))
    payment_applied = Column(Enum('n', 'y'), nullable=False, index=True, server_default=text("'n'"))
    date_modified = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_paid = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_created = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_shipped = Column(DateTime, nullable=False, index=True, server_default=text("'0000-00-00 00:00:00'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class EBAYPAYMENTTYPE(Base):
    __tablename__ = 'EBAY_PAYMENTTYPES'

    ebpaytype_id = Column(String(30), primary_key=True, nullable=False)
    ebsite_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True)
    ebpaytype_name = Column(String(100), nullable=False, server_default=text("''"))
    ebpaytype_safe = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class EBAYPAYMENTTYPEMAPPING(Base):
    __tablename__ = 'EBAY_PAYMENTTYPE_MAPPINGS'

    ebpaytype_id = Column(String(30), primary_key=True, server_default=text("''"))
    card_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))


class EBAYPOLICY(Base):
    __tablename__ = 'EBAY_POLICIES'
    __table_args__ = (
        Index('ebay_policy_id_2', 'ebay_policy_id', 'ebstore_id', 'policy_type', unique=True),
    )

    ep_id = Column(INTEGER(11), primary_key=True)
    ebay_policy_id = Column(String(20), nullable=False, index=True)
    ebstore_id = Column(INTEGER(11), nullable=False)
    ebsite_id = Column(INTEGER(10))
    policy_type = Column(Enum('', 'fulfillment', 'payment', 'return'), nullable=False, server_default=text("''"))
    policy_name = Column(String(64), nullable=False, index=True)
    policy_description = Column(String(250), nullable=False)


class EBAYPOLICIESSHIPSERVICESMAPPING(Base):
    __tablename__ = 'EBAY_POLICIES_SHIPSERVICES_MAPPING'

    ep_id = Column(INTEGER(11), primary_key=True, nullable=False)
    ebship_id = Column(INTEGER(11), primary_key=True, nullable=False)


class EBAYPROC(Base):
    __tablename__ = 'EBAY_PROCS'
    __table_args__ = (
        Index('status_approved', 'ebproc_status', 'ebproc_approved'),
        Index('status_priority_init', 'ebproc_status', 'ebproc_priority', 'date_init'),
        Index('status_module_priority_init', 'ebproc_status', 'ebay_module', 'ebproc_priority', 'date_init')
    )

    ebproc_id = Column(INTEGER(10), primary_key=True)
    parent_ebproc_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    ebay_module = Column(String(30), nullable=False, index=True, server_default=text("''"))
    ebay_fn = Column(String(40), nullable=False, server_default=text("''"))
    ebsite_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    ebstore_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    ebtmpl_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    eblist_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    eborder_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    ebtrans_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    SKU = Column(String(25), nullable=False, index=True, server_default=text("''"))
    log_description = Column(String(100), nullable=False, server_default=text("''"))
    input_param_md5 = Column(String(32), nullable=False, index=True, server_default=text("''"))
    input_param = Column(Text, nullable=False)
    admin_username = Column(String(25), nullable=False, index=True, server_default=text("''"))
    ebproc_status = Column(Enum('Pending', 'Running', 'Stopped', 'Success', 'Failed'), nullable=False, server_default=text("'Pending'"))
    ebproc_approved = Column(Enum('', 'n', 'y'), nullable=False, server_default=text("''"))
    ebproc_priority = Column(TINYINT(3), nullable=False, index=True, server_default=text("'0'"))
    date_init = Column(DateTime, nullable=False, index=True, server_default=text("'0000-00-00 00:00:00'"))
    date_scheduled = Column(DateTime, nullable=False, index=True, server_default=text("'0000-00-00 00:00:00'"))
    date_start = Column(DateTime, nullable=False, index=True, server_default=text("'0000-00-00 00:00:00'"))
    date_end = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_approved = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    heartbeat = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    retry_counter = Column(TINYINT(3), nullable=False, server_default=text("'0'"))


class EBAYPROCLOG(Base):
    __tablename__ = 'EBAY_PROC_LOG'

    ebplog_id = Column(INTEGER(10), primary_key=True)
    ebay_module = Column(String(30), nullable=False, index=True, server_default=text("''"))
    ebay_fn = Column(String(40), nullable=False, index=True, server_default=text("''"))
    ebproc_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    parent_ebproc_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    ebsite_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    ebstore_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    ebtmpl_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    eblist_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    eborder_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    ebtrans_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    SKU = Column(String(25), nullable=False, index=True, server_default=text("''"))
    log_description = Column(String(100), nullable=False, server_default=text("''"))
    admin_username = Column(String(25), nullable=False, index=True, server_default=text("''"))
    log_notes = Column(String(255), nullable=False, server_default=text("''"))
    rtn_status = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    rtn_errors = Column(Text, nullable=False)
    rtn_messages = Column(Text, nullable=False)
    rtn_warns = Column(Text, nullable=False)
    rtn_notes = Column(String(255), nullable=False, server_default=text("''"))
    date_start = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    date_end = Column(DateTime, nullable=False, index=True, server_default=text("'0000-00-00 00:00:00'"))


class EBAYPROCSTATISTIC(Base):
    __tablename__ = 'EBAY_PROC_STATISTICS'

    ebstore_id = Column(INTEGER(10), primary_key=True, nullable=False, server_default=text("'0'"))
    ebay_module = Column(String(30), primary_key=True, nullable=False, index=True, server_default=text("''"))
    ebay_fn = Column(String(40), primary_key=True, nullable=False, index=True, server_default=text("''"))
    run_time_avg = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    run_time_max = Column(INTEGER(10), nullable=False, server_default=text("'0'"))


class EBAYPRODUCTMAPPING(Base):
    __tablename__ = 'EBAY_PRODUCT_MAPPING'
    __table_args__ = (
        Index('inventory_id', 'inventory_id', 'ebsite_id', unique=True),
    )

    id = Column(INTEGER(11), primary_key=True)
    inventory_id = Column(INTEGER(10))
    ebsite_id = Column(INTEGER(10))
    epid = Column(String(255))


class EBAYPROMOTIONALSALE(Base):
    __tablename__ = 'EBAY_PROMOTIONAL_SALES'

    ebprom_id = Column(INTEGER(10), primary_key=True)
    ebsite_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    ebstore_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    promotionsale_id = Column(String(38), nullable=False, index=True, server_default=text("''"))
    discount_type = Column(Enum('Percentage', 'Price'), nullable=False, server_default=text("'Percentage'"))
    discount_value = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    promotionsale_start = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    promotionsale_end = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    promotionsale_type = Column(Enum('', 'PriceDiscountOnly', 'FreeShippingOnly', 'PriceDiscountAndFreeShipping'), nullable=False, server_default=text("''"))
    promotionsale_status = Column(Enum('Active', 'Deleted', 'Inactive', 'Processing', 'Scheduled'), nullable=False, server_default=text("'Active'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class EBAYPROMOTIONALSALESITEM(Base):
    __tablename__ = 'EBAY_PROMOTIONAL_SALES_ITEMS'

    ebprom_id = Column(INTEGER(10), primary_key=True, nullable=False, server_default=text("'0'"))
    eblist_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True, server_default=text("'0'"))
    ebprom_status = Column(Enum('Pending', 'Active', 'Delete'), nullable=False, server_default=text("'Pending'"))


class EBAYRETURNPOLICY(Base):
    __tablename__ = 'EBAY_RETURNPOLICY'

    ebreturn_id = Column(String(25), primary_key=True, nullable=False)
    ebsite_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True)
    ebreturn_type = Column(Enum('Refund', 'ReturnsAccepted', 'ReturnsWithin', 'ShippingCostPaidBy', 'WarrantyDuration', 'WarrantyOffered'), nullable=False, index=True, server_default=text("'Refund'"))
    ebreturn_name = Column(String(50), nullable=False, server_default=text("''"))


class EBAYSHIPPINGCARRIER(Base):
    __tablename__ = 'EBAY_SHIPPINGCARRIERS'
    __table_args__ = (
        Index('ebcarrier_id', 'ebcarrier_id', 'ebsite_id', unique=True),
    )

    id = Column(INTEGER(11), primary_key=True)
    ebcarrier_id = Column(INTEGER(11), nullable=False)
    ebsite_id = Column(INTEGER(11), nullable=False)
    ebcarrier_description = Column(String(50), nullable=False)
    ebcarrier_carrier = Column(String(50), nullable=False)


class EBAYSHIPPINGLOCATION(Base):
    __tablename__ = 'EBAY_SHIPPINGLOCATION'

    ebshloc_id = Column(String(50), primary_key=True, nullable=False, index=True, server_default=text("''"))
    ebsite_id = Column(INTEGER(10), primary_key=True, nullable=False)
    ebshloc_desc = Column(String(50), nullable=False)


class EBAYSHIPPINGLOCATIONEXC(Base):
    __tablename__ = 'EBAY_SHIPPINGLOCATION_EXC'

    ebshloc_id = Column(String(50), primary_key=True, nullable=False)
    ebsite_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True)
    ebshloc_region = Column(String(50), nullable=False, server_default=text("''"))
    ebshloc_desc = Column(String(50), nullable=False, server_default=text("''"))


class EBAYSHIPPINGPACKAGE(Base):
    __tablename__ = 'EBAY_SHIPPINGPACKAGE'

    ebshpkg_id = Column(INTEGER(10), primary_key=True, nullable=False)
    ebsite_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True)
    ebshpkg_code = Column(String(50), nullable=False, server_default=text("''"))
    ebshpkg_name = Column(String(100), nullable=False, server_default=text("''"))
    ebshpkg_dimensions = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ebshpkg_default = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class EBAYSHIPPINGSERVICE(Base):
    __tablename__ = 'EBAY_SHIPPINGSERVICES'

    ebship_id = Column(INTEGER(10), primary_key=True, nullable=False)
    ebsite_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True)
    ebship_description = Column(String(50), nullable=False, server_default=text("''"))
    ebship_service_code = Column(String(50), nullable=False, index=True, server_default=text("''"))
    ebship_dimensions = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ebship_expedited = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ebship_international = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ebship_servicetype = Column(Text, nullable=False)
    ebship_carrier = Column(Text, nullable=False)
    ebship_category = Column(String(50), nullable=False, server_default=text("''"))
    ebship_packages = Column(Text, nullable=False)
    ebship_timemax = Column(INTEGER(10), nullable=False)
    ebship_timemin = Column(INTEGER(10), nullable=False)
    ebship_surcharge = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ebship_valid = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ebship_weight_required = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class EBAYSHIPSERVICEMAPPING(Base):
    __tablename__ = 'EBAY_SHIPSERVICE_MAPPINGS'

    ebship_id = Column(INTEGER(10), primary_key=True, nullable=False, server_default=text("'0'"))
    ebsite_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True, server_default=text("'0'"))
    sh_group_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))


class EBAYSITE(Base):
    __tablename__ = 'EBAY_SITE'

    ebsite_id = Column(INTEGER(10), primary_key=True)
    ebsite_name = Column(String(50), nullable=False, server_default=text("''"))
    ebsite_full_name = Column(String(50), nullable=False)
    ebsite_global_id = Column(String(15), nullable=False)
    marketplace_id = Column(String(1023), nullable=False, server_default=text("''"))
    ebsite_freegallery = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ebsite_gifticon = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ebsite_featuredfirst = Column(Enum('n', 'y', 'p', 't'), nullable=False, server_default=text("'n'"))
    ebsite_rtndesc = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ebsite_rtnean = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    country_code = Column(String(2), nullable=False, index=True)
    ebsite_enabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ebsite_default = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class EBAYSITECURRENCY(Base):
    __tablename__ = 'EBAY_SITE_CURRENCY'

    ebsite_id = Column(INTEGER(10), primary_key=True)
    ebcurrency_id = Column(String(3), nullable=False)


class EBAYSTOREDETAIL(Base):
    __tablename__ = 'EBAY_STOREDETAILS'

    ebstore_id = Column(INTEGER(10), primary_key=True)
    ebstore_name = Column(String(35), nullable=False, server_default=text("''"))
    ebstore_desc = Column(Text, nullable=False)
    ebstore_owner = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ebstore_subscription = Column(Enum('', 'Basic', 'Anchor', 'Featured'), nullable=False, server_default=text("''"))
    ebstore_url = Column(String(255), nullable=False, server_default=text("''"))
    ebstore_logoname = Column(String(255), nullable=False, server_default=text("''"))
    ebstore_logourl = Column(String(255), nullable=False, server_default=text("''"))


class EBAYSTORE(Base):
    __tablename__ = 'EBAY_STORES'

    ebstore_id = Column(INTEGER(10), primary_key=True)
    doctmpl_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    ebay_username = Column(String(64), nullable=False, server_default=text("''"))
    ebay_notification_email = Column(String(50), nullable=False, server_default=text("''"))
    ebay_token = Column(Text, nullable=False)
    ebay_session_id = Column(String(255), nullable=False, server_default=text("''"))
    ebay_session_code = Column(String(20), nullable=False, server_default=text("''"))
    token_active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    token_expiration = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    ebay_notify_token = Column(Text, nullable=False)
    ebay_notify_session_id = Column(String(255), nullable=False, server_default=text("''"))
    ebay_notify_session_code = Column(String(20), nullable=False, server_default=text("''"))
    token_notify_active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    token_notify_ready = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    token_notify_expiration = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    auth_access_token = Column(Text, nullable=False)
    auth_refresh_token = Column(String(1023), nullable=False, server_default=text("''"))
    access_token_expires = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    refresh_token_expires = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    ebsite_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'15'"))
    date_getorders = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_gettrans = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_getdisputes = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_listnotified = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    allow_outofstock = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ebay_bopis_active = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    ebay_policy_active = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    ebstore_primary = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    date_storeadded = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    last_revised_inventory_timestamp = Column(TIMESTAMP, nullable=False)


class EBAYSTOREOAUTHSCOPE(Base):
    __tablename__ = 'EBAY_STORE_OAUTH_SCOPES'
    __table_args__ = (
        Index('ebstore_id', 'ebstore_id', 'oauth_scope', unique=True),
    )

    id = Column(INTEGER(11), primary_key=True)
    ebstore_id = Column(INTEGER(11), nullable=False)
    oauth_scope = Column(String(50), nullable=False)


class EBAYSTCATEGORY(Base):
    __tablename__ = 'EBAY_ST_CATEGORIES'

    ebstcat_id = Column(BIGINT(20), primary_key=True, nullable=False)
    ebstore_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True)
    ebstcatparent_id = Column(BIGINT(20), nullable=False, index=True, server_default=text("'0'"))
    ebstcat_name = Column(String(30), nullable=False, server_default=text("''"))
    ebstcat_order = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    ebstcat_level = Column(TINYINT(3), nullable=False, server_default=text("'0'"))
    leaf = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class EBAYSTCATEGORIESCACHE(Base):
    __tablename__ = 'EBAY_ST_CATEGORIES_CACHE'

    ebstcat_id = Column(BIGINT(20), primary_key=True, nullable=False)
    ebstore_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True)
    ebstcatparent_id = Column(BIGINT(20), nullable=False, index=True, server_default=text("'0'"))
    ebstcat_name = Column(String(30), nullable=False, server_default=text("''"))
    ebstcat_order = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    ebstcat_level = Column(TINYINT(3), nullable=False, server_default=text("'0'"))
    leaf = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class EBAYSTCHECKIN(Base):
    __tablename__ = 'EBAY_ST_CHECKINS'

    ebchkin_id = Column(INTEGER(10), primary_key=True, server_default=text("'0'"))
    CallName = Column(String(50), nullable=False, server_default=text("''"))
    date_created = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class EBAYSTEBAYACCESSRULE(Base):
    __tablename__ = 'EBAY_ST_EBAY_ACCESSRULES'
    __table_args__ = (
        Index('ebstore_id_CallName', 'ebstore_id', 'CallName', unique=True),
    )

    ebstrule_id = Column(INTEGER(10), primary_key=True)
    ebstore_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    CallName = Column(String(50), nullable=False, server_default=text("''"))
    CountsTowardAggregate = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    DailyHardLimit = Column(BIGINT(20), nullable=False, server_default=text("'0'"))
    DailySoftLimit = Column(BIGINT(20), nullable=False, server_default=text("'0'"))
    DailyUsage = Column(BIGINT(20), nullable=False, server_default=text("'0'"))
    HourlyHardLimit = Column(BIGINT(20), nullable=False, server_default=text("'0'"))
    HourlySoftLimit = Column(BIGINT(20), nullable=False, server_default=text("'0'"))
    HourlyUsage = Column(BIGINT(20), nullable=False, server_default=text("'0'"))
    Period = Column(INTEGER(11), nullable=False, server_default=text("'-1'"))
    PeriodicHardLimit = Column(BIGINT(20), nullable=False, server_default=text("'0'"))
    PeriodicSoftLimit = Column(BIGINT(20), nullable=False, server_default=text("'0'"))
    PeriodicUsage = Column(BIGINT(20), nullable=False, server_default=text("'0'"))
    PeriodicStartDate = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_sync = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class EBAYSTEBAYCALLCOUNTER(Base):
    __tablename__ = 'EBAY_ST_EBAY_CALLCOUNTER'

    ebstore_id = Column(INTEGER(10), primary_key=True, nullable=False, server_default=text("'0'"))
    date_called = Column(DateTime, primary_key=True, nullable=False, index=True, server_default=text("'0000-00-00 00:00:00'"))
    CallName = Column(String(50), primary_key=True, nullable=False, index=True, server_default=text("''"))
    count_aggregate = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    total_usage = Column(INTEGER(10), nullable=False, server_default=text("'0'"))


class EBAYTAXTYPE(Base):
    __tablename__ = 'EBAY_TAXTYPES'

    ebtaxtype_id = Column(String(30), primary_key=True, nullable=False)
    ebsite_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True)
    ebtaxtype_name = Column(String(100), nullable=False, server_default=text("''"))


class EBAYTIMEZONE(Base):
    __tablename__ = 'EBAY_TIMEZONE'

    ebtimezone_id = Column(String(25), primary_key=True, nullable=False)
    ebsite_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True)
    ebtimezone_name = Column(String(55), nullable=False, server_default=text("''"))
    ebtimezone_offset = Column(CHAR(6), nullable=False)
    ebtimezone_dsoffset = Column(CHAR(6), nullable=False)
    ebtimezone_dsname = Column(String(55), nullable=False)


class EBAYTRANSACTION(Base):
    __tablename__ = 'EBAY_TRANSACTIONS'

    ebtrans_id = Column(INTEGER(10), primary_key=True)
    eborder_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    ebstore_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    ebsite_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    eblist_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    ebtmpl_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    ebay_trans_id = Column(String(38), nullable=False, index=True, server_default=text("''"))
    ebay_user_id = Column(String(50), nullable=False, index=True, server_default=text("''"))
    ebay_order_id = Column(String(50), nullable=False, index=True, server_default=text("''"))
    ebay_sales_record_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    ebay_id = Column(String(38), nullable=False, index=True, server_default=text("''"))
    warehouse_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    SKU = Column(String(25), nullable=False, index=True, server_default=text("''"))
    ebay_kitcomponents = Column(String(80), nullable=False, server_default=text("''"))
    iskitting = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    list_type = Column(Enum('Chinese', 'FixedPriceItem', 'AdType', 'LeadGeneration', 'PersonalOffer'), nullable=False, server_default=text("'Chinese'"))
    ebay_title = Column(String(150), nullable=False, server_default=text("''"))
    lot_size = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    sold_qty = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    buyer_email = Column(String(50), nullable=False, index=True, server_default=text("''"))
    ebcurrency_id = Column(String(3), nullable=False, index=True)
    unit_price = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    buyer_name = Column(String(50), nullable=False, index=True, server_default=text("''"))
    buyer_street1 = Column(String(50), nullable=False, server_default=text("''"))
    buyer_street2 = Column(String(50), nullable=False, server_default=text("''"))
    buyer_city = Column(String(50), nullable=False, server_default=text("''"))
    buyer_state = Column(String(50), nullable=False, server_default=text("''"))
    buyer_zip = Column(String(15), nullable=False, server_default=text("''"))
    buyer_phone = Column(String(30), nullable=False, server_default=text("''"))
    buyer_country = Column(String(2), nullable=False, index=True, server_default=text("''"))
    buyer_status = Column(String(30), nullable=False, server_default=text("''"))
    buyer_feedbackscore = Column(INTEGER(10), nullable=False, server_default=text("'-1'"))
    buyer_feedbackpercent = Column(DECIMAL(5, 4), nullable=False, server_default=text("'0.0000'"))
    buyer_id_verified = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    buyer_goodstanding = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    buyer_vat_status = Column(Enum('n', 'e', 'y'), nullable=False, server_default=text("'n'"))
    best_offer = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    private_listing = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    shipping_expedited = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    deposit_type = Column(Enum('None', 'OtherMethod'), nullable=False, server_default=text("'None'"))
    buyer_protection = Column(Enum('NoCoverage', 'ItemEligible', 'ItemIneligible', 'ItemMarkedEligible', 'ItemMarkedIneligible'), nullable=False, server_default=text("'NoCoverage'"))
    guarantee_price = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    ebship_service_code = Column(String(50), nullable=False, index=True, server_default=text("''"))
    ebship_type = Column(Enum('', 'NotSpecified', 'Calculated', 'CalculatedDomesticFlatInternational', 'Flat', 'FlatDomesticCalculatedInternational', 'FreightFlat'), nullable=False, index=True, server_default=text("''"))
    shipping_cost_add = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    shipping_cost_1st = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    is_bopis = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    is_pudo = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    pickup_status = Column(Enum('', 'Invalid', 'NotApplicable', 'PendingMerchantConfirmation', 'Pickedup', 'PickupCancelled', 'PickupCancelledBuyerNoShow', 'PickupCancelledBuyerRejected', 'PickupCancelledOutOfStock', 'ReadyToPickup'), nullable=False, server_default=text("''"))
    ebay_location_id = Column(INTEGER(11))
    shipping_insurance = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    adjust_amount = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    tax_amount = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    ebay_fvfee = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    shipping_selected = Column(Enum('n', 'y'), nullable=False, index=True, server_default=text("'n'"))
    shipping_time_min = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    shipping_time_max = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    ebpaytype_id = Column(String(30), nullable=False, index=True, server_default=text("''"))
    buyer_comment = Column(Text, nullable=False)
    ebay_checkout_status = Column(Enum('CheckoutIncomplete', 'BuyerRequestsTotal', 'SellerResponded', 'CheckoutComplete'), nullable=False, index=True, server_default=text("'CheckoutIncomplete'"))
    ebay_complete_status = Column(Enum('Incomplete', 'Pending', 'Complete'), nullable=False, index=True, server_default=text("'Incomplete'"))
    ebay_payment_status = Column(Enum('', 'PaymentInProcess', 'PayPalPaymentInProcess', 'BuyerCreditCardFailed', 'BuyerECheckBounced', 'BuyerFailedPaymentReportedBySeller', 'NoPaymentFailure'), nullable=False, index=True, server_default=text("''"))
    ebay_hold_status = Column(Enum('', 'MerchantHold', 'NewSellerHold', 'PaymentReview', 'Released'), nullable=False, index=True, server_default=text("''"))
    has_order = Column(Enum('n', 'y'), nullable=False, index=True, server_default=text("'n'"))
    date_modified = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_paid = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_created = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_responsed = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_shipped = Column(DateTime, nullable=False, index=True, server_default=text("'0000-00-00 00:00:00'"))
    date_freestock = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_disputed = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    dispute_error = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class EBAYTRANSACTIONSPLIT(Base):
    __tablename__ = 'EBAY_TRANSACTIONSPLITS'

    ebtrans_id = Column(INTEGER(10), primary_key=True, nullable=False)
    SKU = Column(String(25), primary_key=True, nullable=False)
    assemble_quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"))


class EBAYTRANSACTIONINFO(Base):
    __tablename__ = 'EBAY_TRANSACTION_INFO'

    ebtrans_id = Column(INTEGER(10), primary_key=True, server_default=text("'0'"))
    ebay_id = Column(String(38), nullable=False, index=True, server_default=text("''"))
    ebay_trans_id = Column(String(38), nullable=False, index=True, server_default=text("''"))
    date_paid = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_feedback = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    ebfeedback_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    date_shipped = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    tracking_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    date_pickup_ready = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_pickup_cancelled = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_picked_up = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    pickup_reference = Column(String(25), nullable=False, server_default=text("''"))
    pickup_cancelled_reason = Column(Enum('', 'OUT_OF_STOCK', 'BUYER_NO_SHOW', 'BUYER_REFUSED'), nullable=False, server_default=text("''"))
    date_sync = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_bopis_sync = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    date_bopis_updated = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))


class EBAYVERSION(Base):
    __tablename__ = 'EBAY_VERSIONS'

    ebsite_id = Column(INTEGER(10), primary_key=True, nullable=False, server_default=text("'0'"))
    version_id = Column(String(30), primary_key=True, nullable=False, index=True)
    version = Column(String(50), nullable=False)
    timesatmp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class EBAYWSLOG(Base):
    __tablename__ = 'EBAY_WS_LOG'

    eblog_id = Column(INTEGER(10), primary_key=True)
    ebstore_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    eblog_soap_fn = Column(String(50), nullable=False, server_default=text("''"))
    eblog_correlation_id = Column(String(25), nullable=False, index=True, server_default=text("''"))
    eblog_status = Column(Enum('n', 'y'), nullable=False, index=True, server_default=text("'n'"))
    ip_address = Column(String(50), nullable=False, server_default=text("''"))
    eblog_content = Column(Text, nullable=False)
    eblog_return = Column(Text, nullable=False)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class EMAILTEMPLATEFILE(Base):
    __tablename__ = 'EMAILTEMPLATE_FILES'

    etmpl_id = Column(INTEGER(10), primary_key=True, nullable=False, server_default=text("'0'"))
    etmplgp_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True, server_default=text("'0'"))
    etmpl_name = Column(String(100), nullable=False, server_default=text("''"))
    etmpl_path = Column(String(255), nullable=False, server_default=text("''"))
    etmpl_description = Column(String(255), nullable=False, server_default=text("''"))


class EMAILTEMPLATEGROUP(Base):
    __tablename__ = 'EMAILTEMPLATE_GROUPS'

    etmplgp_id = Column(INTEGER(10), primary_key=True, server_default=text("'0'"))
    etmplgp_name = Column(String(100), nullable=False, server_default=text("''"))
    etmplgp_path = Column(String(255), nullable=False, server_default=text("''"))
    etmplgp_description = Column(String(100), nullable=False, server_default=text("''"))
    etmplgp_module = Column(CHAR(1), nullable=False, server_default=text("''"))
    dynamic_tmpl = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class EXONETIGLACC(Base):
    __tablename__ = 'EXONETI_GLACCS'

    ACCNO = Column(INTEGER(10), primary_key=True)
    ISACTIVE = Column(CHAR(1), nullable=False)
    NAME = Column(String(40), nullable=False)
    DRCR = Column(CHAR(1), nullable=False)
    USESUBCODES = Column(CHAR(1), nullable=False)
    ALLOWJOURNAL = Column(CHAR(1), nullable=False)
    LAST_UPDATED = Column(DateTime, nullable=False, index=True, server_default=text("'0000-00-00 00:00:00'"))


class EXONETIITEM(Base):
    __tablename__ = 'EXONETI_ITEMS'

    STOCKCODE = Column(String(23), primary_key=True)
    DESCRIPTION = Column(String(70), nullable=False, server_default=text("''"))
    STATUS = Column(CHAR(1), nullable=False, server_default=text("''"))
    ISACTIVE = Column(CHAR(1), nullable=False, server_default=text("''"))
    TOTALSTOCK = Column(Float, nullable=False, server_default=text("'0'"))
    STDCOST = Column(Float, nullable=False, server_default=text("'0'"))
    AVECOST = Column(Float, nullable=False, server_default=text("'0'"))
    SELLPRICE1 = Column(Float, nullable=False, server_default=text("'0'"))
    SELLPRICE2 = Column(Float, nullable=False, server_default=text("'0'"))
    SELLPRICE3 = Column(Float, nullable=False, server_default=text("'0'"))
    SELLPRICE4 = Column(Float, nullable=False, server_default=text("'0'"))
    SELLPRICE5 = Column(Float, nullable=False, server_default=text("'0'"))
    SELLPRICE6 = Column(Float, nullable=False, server_default=text("'0'"))
    SELLPRICE7 = Column(Float, nullable=False, server_default=text("'0'"))
    SELLPRICE8 = Column(Float, nullable=False, server_default=text("'0'"))
    SELLPRICE9 = Column(Float, nullable=False, server_default=text("'0'"))
    SALESTAXRATE = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    PURCHTAXRATE = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    SALES_GL_CODE = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    SALES_GLSUBCODE = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    PURCH_GL_CODE = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    PURCH_GLSUBCODE = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    COS_GL_CODE = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    COS_GLSUBCODE = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    WEIGHT = Column(Float, nullable=False, server_default=text("'0'"))
    CUBIC = Column(Float, nullable=False, server_default=text("'0'"))
    BARCODE1 = Column(String(30), nullable=False, server_default=text("''"))
    BARCODE2 = Column(String(30), nullable=False, server_default=text("''"))
    BARCODE3 = Column(String(30), nullable=False, server_default=text("''"))
    BINCODE = Column(String(12), nullable=False, server_default=text("''"))
    LABEL_QTY = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    X_LABEL_REQ = Column(String(1), nullable=False, server_default=text("''"))
    X_CUSTOMFIELD1 = Column(String(30), nullable=False, server_default=text("''"))
    X_CUSTOMFIELD2 = Column(String(30), nullable=False, server_default=text("''"))
    X_CUSTOMFIELD3 = Column(String(30), nullable=False, server_default=text("''"))
    X_DESC1 = Column(String(20), nullable=False, server_default=text("''"))
    X_DESC2 = Column(String(20), nullable=False, server_default=text("''"))
    X_DESC3 = Column(String(20), nullable=False, server_default=text("''"))
    X_DESC4 = Column(String(20), nullable=False, server_default=text("''"))
    NOTES = Column(String(4096), nullable=False, server_default=text("''"))
    LAST_UPDATED = Column(DateTime, nullable=False, index=True, server_default=text("'0000-00-00 00:00:00'"))
    UPDATEITEM_CODE = Column(String(23), nullable=False, index=True)
    UPDATEITEM_QTY = Column(Float, nullable=False, server_default=text("'0'"))


class EXONETITAXRATE(Base):
    __tablename__ = 'EXONETI_TAXRATES'

    SEQNO = Column(INTEGER(10), primary_key=True)
    SHORTNAME = Column(String(6), nullable=False, index=True, server_default=text("''"))
    NAME = Column(String(30), nullable=False, server_default=text("''"))
    RATE = Column(Float, nullable=False, server_default=text("'0'"))
    BASE = Column(Float, nullable=False, server_default=text("'0'"))
    GLACC = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    GLSUBACC = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    KEY_POINT = Column(String(5), nullable=False, server_default=text("''"))


class EXPORTBATCH(Base):
    __tablename__ = 'EXPORTBATCHES'

    exbatch_id = Column(INTEGER(10), primary_key=True)
    export_id = Column(INTEGER(10), nullable=False, index=True)
    dropshipper = Column(String(25), nullable=False, index=True)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class EXPORTRETRY(Base):
    __tablename__ = 'EXPORTRETRIES'

    retry_id = Column(INTEGER(10), primary_key=True)
    export_id = Column(INTEGER(10), nullable=False, index=True)
    exbatch_id = Column(INTEGER(10), nullable=False, index=True)
    file_name = Column(String(255), nullable=False, server_default=text("''"))
    tmp_file = Column(String(255), nullable=False, server_default=text("''"))
    last_error = Column(String(255), nullable=False, server_default=text("''"))
    last_tried = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class EXPORTWIZARDFILE(Base):
    __tablename__ = 'EXPORT_WIZARD_FILE'

    export_file_id = Column(INTEGER(10), primary_key=True)
    export_module = Column(String(25), nullable=False)
    export_method = Column(Enum('Email', 'FTP', 'URL', 'Local'), nullable=False, server_default=text("'Email'"))
    export_location = Column(String(255), nullable=False)
    export_loginusr = Column(String(50), nullable=False)
    export_loginpwd = Column(String(255), nullable=False)
    export_filename = Column(String(50), nullable=False)
    export_filetype = Column(String(15), nullable=False)
    export_params = Column(MEDIUMTEXT, nullable=False)
    export_template = Column(Text, nullable=False)
    export_setting = Column(MEDIUMTEXT, nullable=False)
    schedule_id = Column(INTEGER(10), nullable=False, index=True)
    export_name = Column(String(25), nullable=False, index=True)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class FOLLOWUPREASON(Base):
    __tablename__ = 'FOLLOWUP_REASONS'

    freason_id = Column(INTEGER(10), primary_key=True)
    freason_name = Column(String(50), nullable=False, server_default=text("''"))
    freason_description = Column(String(255), nullable=False, server_default=text("''"))
    freason_ignored = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class FOLLOWUPTYPE(Base):
    __tablename__ = 'FOLLOWUP_TYPES'

    ftype_id = Column(INTEGER(10), primary_key=True)
    ftype_name = Column(String(50), nullable=False, server_default=text("''"))
    ftype_description = Column(String(255), nullable=False, server_default=text("''"))
    active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class IMAGE(Base):
    __tablename__ = 'IMAGES'

    image_id = Column(INTEGER(11), primary_key=True)
    image_url = Column(VARCHAR(255), nullable=False, unique=True)
    s3_image_url = Column(VARCHAR(255), nullable=False, index=True, server_default=text("''"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class IMPORTBATCH(Base):
    __tablename__ = 'IMPORTBATCHES'

    imbatch_id = Column(INTEGER(10), primary_key=True)
    import_id = Column(INTEGER(10), nullable=False, index=True)
    upd_id = Column(String(10), nullable=False, index=True)
    last_update_id = Column(String(25), nullable=False, index=True)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class IMPORTWIZARDFILE(Base):
    __tablename__ = 'IMPORT_WIZARD_FILE'

    file_id = Column(INTEGER(10), primary_key=True)
    file_name = Column(String(150), nullable=False)
    import_module = Column(String(25), nullable=False)
    import_method = Column(Enum('File', 'URL', 'FTP'), nullable=False, server_default=text("'File'"))
    import_location = Column(String(255), nullable=False)
    import_loginusr = Column(String(50), nullable=False)
    import_loginpwd = Column(String(255), nullable=False)
    import_filetype = Column(String(15), nullable=False)
    import_fileheaders = Column(Text, nullable=False)
    import_params = Column(Text, nullable=False)
    import_mapping = Column(Text, nullable=False)
    import_setting = Column(Text, nullable=False)
    import_name = Column(String(25), nullable=False)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class INFOMESSAGE(Base):
    __tablename__ = 'INFOMESSAGES'

    info_id = Column(INTEGER(10), primary_key=True)
    info_type_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    info_code = Column(String(30), nullable=False, index=True, server_default=text("''"))
    info_name = Column(String(150), nullable=False, server_default=text("''"))
    info_severity = Column(Enum('info', 'msg', 'warn', 'error'), nullable=False, server_default=text("'info'"))
    info_msg = Column(String(1000), nullable=False, server_default=text("''"))
    info_tags = Column(String(300), nullable=False, server_default=text("''"))


class INFOMESSAGETYPE(Base):
    __tablename__ = 'INFOMESSAGE_TYPE'

    info_type_id = Column(INTEGER(10), primary_key=True)
    info_type_name = Column(String(50), nullable=False, server_default=text("''"))
    info_type_code = Column(String(15), nullable=False, index=True, server_default=text("''"))


class INTEGRATIONAPILOG(Base):
    __tablename__ = 'INTEGRATION_API_LOG'

    log_id = Column(INTEGER(11), primary_key=True)
    integration_type = Column(String(25), nullable=False, index=True)
    processor_module = Column(String(25), nullable=False, index=True)
    request_uri = Column(String(255), nullable=False)
    request_content = Column(Text)
    response_content = Column(Text)
    user_agent = Column(String(255), nullable=False, server_default=text("''"))
    ip_address = Column(String(50), nullable=False, server_default=text("''"))
    date_requested = Column(TIMESTAMP, nullable=False, index=True, server_default=text("CURRENT_TIMESTAMP"))


class INVENTORY(Base):
    __tablename__ = 'INVENTORY'
    __table_args__ = (
        Index('is_neto_utility', 'is_neto_utility', 'visible', 'SKU'),
        Index('webshop_thumblist', 'approval', 'visible', 'parent_inventory_id')
    )

    inventory_id = Column(INTEGER(10), primary_key=True)
    SKU = Column(String(25), nullable=False, unique=True, server_default=text("''"), comment='+*SKU')
    parent_sku = Column(String(25), nullable=False, index=True, server_default=text("''"), comment='+Parent Product SKU')
    parent_inventory_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    supplier = Column(String(50), nullable=False, index=True, server_default=text("''"), comment='Merchant ID')
    dropshipper = Column(String(25), nullable=False, index=True, comment='+Primary Drop Ship Supplier')
    acc_code = Column(String(50), nullable=False, index=True, server_default=text("''"), comment='+SKU/Ref In Accounting Software')
    pick_zone = Column(String(25), nullable=False, index=True, server_default=text("''"))
    expense_acc = Column(String(75), nullable=False, server_default=text("''"))
    brand = Column(String(50), nullable=False, index=True, comment='+Brand')
    model = Column(String(150), nullable=False, server_default=text("''"), comment='+*Name')
    product_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    retail = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"), comment='+RRP')
    store_price = Column(DECIMAL(12, 2), nullable=False, index=True, server_default=text("'0.00'"), comment='+*Price')
    cost = Column(DECIMAL(20, 8), nullable=False, server_default=text("'0.00000000'"))
    avg_cost_price = Column(DECIMAL(20, 8), nullable=False, server_default=text("'0.00000000'"))
    default_purchase_price = Column(DECIMAL(20, 8))
    store_quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"), comment='+*Store Quantity')
    committed_quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"), comment='Committed Quantity')
    build_quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"), comment='Build Quantity')
    restock_warning_level = Column(INTEGER(10), nullable=False, server_default=text("'0'"), comment='+ Restock Warning Level')
    default_restock_quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"), comment='+ Default Restock Quantity')
    ebay_quantity = Column(INTEGER(11), nullable=False, server_default=text("'0'"), comment='Ebay quantity')
    preorder_quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"), comment='+Preorder Quantity')
    restock_quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"), comment='+Minimum Level Before Reorder')
    reorder_quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"), comment='+Default Reorder Quantity')
    incoming_quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"), comment='+Incoming quantity')
    shipping = Column(DECIMAL(10, 4), nullable=False, server_default=text("'0.0000'"), comment='+Shipping Weight')
    type = Column(Enum('store', 'pack', 'build', 'raw'), nullable=False, server_default=text("'store'"), comment='Product Type')
    unit = Column(String(10), nullable=False, comment='+Unit of Measure')
    base_unit = Column(String(10), nullable=False, server_default=text("''"), comment='+Base Unit of Measure')
    base_unit_qty = Column(DECIMAL(8, 4), nullable=False, server_default=text("'1.0000'"), comment='+Base Unit Per Quantity')
    pick_scan_qty = Column(INTEGER(10), nullable=False, server_default=text("'1'"), comment='+Pick Quantity Per Scan')
    buy_qty = Column(INTEGER(10), nullable=False, server_default=text("'1'"), comment='+Buy Unit Quantity')
    sell_qty = Column(INTEGER(10), nullable=False, server_default=text("'1'"), comment='+Sell Unit Quantity')
    pick_priority = Column(Enum('FIFO', 'FEFO', 'LIFO'), nullable=False, comment='+Pick Priority')
    approval = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"), comment='+Approved')
    approval_pos = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    approval_mobile = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    visible = Column(Enum('N', 'Y'), nullable=False, server_default=text("'N'"), comment='+Active Product')
    notax = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"), comment='+Tax Free Item')
    tax_inc = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"), comment='+GST Inclusive')
    tax_category = Column(String(255))
    is_au_gst_exempted = Column(TINYINT(3), nullable=False, server_default=text("'0'"))
    is_nz_gst_exempted = Column(TINYINT(3), nullable=False, server_default=text("'0'"))
    keywords = Column(String(255), nullable=False, server_default=text("''"), comment='+Search Keywords')
    short_description = Column(String(255), nullable=False, comment='+Short Description')
    description = Column(MEDIUMTEXT, comment='+*Description')
    tnc = Column(MEDIUMTEXT, nullable=False, comment='+Terms And Conditions')
    extra = Column(MEDIUMTEXT, comment='+Extra Attributes')
    swatch = Column(String(45), nullable=False, server_default=text("''"), comment='+Used for colour variations')
    date_created = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    created_at = Column(TIMESTAMP, nullable=False, index=True, server_default=text("CURRENT_TIMESTAMP"))
    start_date = Column(Date, nullable=False, server_default=text("'0000-00-00'"), comment='+Date Of Arrival')
    promo_id = Column(String(25), index=True, comment='+Promotion ID')
    promo_start = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"), comment='+Promotion Start Date')
    promo_end = Column(DateTime, nullable=False, index=True, server_default=text("'0000-00-00 00:00:00'"), comment='+Promotion Expiry Date')
    sales_commission_percent = Column(DECIMAL(5, 2), nullable=False, server_default=text("'0.00'"), comment='Commission Percentage')
    free_gifts = Column(INTEGER(10), nullable=False, server_default=text("'0'"), comment='# of free gifts')
    coupons = Column(INTEGER(10), nullable=False, server_default=text("'0'"), comment='# of coupons')
    featured = Column(Enum('N', 'Y'), nullable=False, server_default=text("'N'"), comment='+Is Featured Product')
    image_url = Column(String(2083), nullable=False, server_default=text("''"), comment='+*Image URL')
    image_url_check_days = Column(INTEGER(11), nullable=False, server_default=text("'15'"), comment='image_url will be re-checked every of these days. Set to 0 to disable periodic checking')
    brochure_url = Column(String(2083), nullable=False, server_default=text("''"), comment='+Brochure URL')
    product_url = Column(String(2083), nullable=False, server_default=text("''"), comment='+Product URL')
    actual_length = Column(DECIMAL(8, 3), nullable=False, server_default=text("'0.000'"), comment='+Actual Length')
    actual_width = Column(DECIMAL(8, 3), nullable=False, server_default=text("'0.000'"), comment='+Actual Width')
    actual_height = Column(DECIMAL(8, 3), nullable=False, server_default=text("'0.000'"), comment='+Actual Height')
    is_inventoried = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"), comment='+Item Is Inventoried')
    purchase_taxcode = Column(String(25), nullable=False, server_default=text("'n'"), comment='+Purchase Tax Code')
    is_bought = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"), comment='+Item Is Brought')
    is_sold = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"), comment='+Item Is Sold')
    itm_subtype = Column(String(50), nullable=False, index=True, server_default=text("''"))
    filter1 = Column(String(50), nullable=False, index=True, server_default=text("''"), comment='+++ misc14')
    filter2 = Column(String(50), nullable=False, index=True, server_default=text("''"), comment='+++ misc15')
    filter3 = Column(String(50), nullable=False, index=True, server_default=text("''"), comment='+++ misc17')
    filter4 = Column(String(50), nullable=False, index=True, server_default=text("''"), comment='+++ misc18')
    filter5 = Column(String(50), nullable=False, index=True, server_default=text("''"), comment='+++ misc48')
    filter6 = Column(String(50), nullable=False, index=True, server_default=text("''"), comment='+++ misc49')
    filter7 = Column(String(50), nullable=False, index=True, server_default=text("''"), comment='+++ misc50')
    filter8 = Column(String(50), nullable=False, index=True, server_default=text("''"), comment='+++ misc51')
    filter9 = Column(String(50), nullable=False, index=True, server_default=text("''"), comment='+++ misc52')
    misc1 = Column(MEDIUMTEXT, comment='+Features')
    misc2 = Column(MEDIUMTEXT, comment='+Specifications')
    misc3 = Column(MEDIUMTEXT, comment='+Warranty')
    misc4 = Column(MEDIUMTEXT, comment='+Artist/Author')
    misc5 = Column(MEDIUMTEXT, comment='+Format')
    misc6 = Column(MEDIUMTEXT, comment='+++ misc1')
    misc7 = Column(String(25), nullable=False, server_default=text("''"), comment='+Model #')
    misc8 = Column(String(25), nullable=False, server_default=text("''"), comment='+Warehouse Ref/Location')
    misc9 = Column(String(56), nullable=False, server_default=text("''"), comment='+Subtitle')
    misc10 = Column(MEDIUMTEXT, comment='+Availability Description')
    misc11 = Column(DECIMAL(8, 3), nullable=False, index=True, server_default=text("'0.000'"), comment='+Item Length')
    misc12 = Column(DECIMAL(8, 3), nullable=False, index=True, server_default=text("'0.000'"), comment='+Item Width')
    misc13 = Column(DECIMAL(8, 3), nullable=False, index=True, server_default=text("'0.000'"), comment='+Item Height')
    misc14 = Column(String(100), nullable=False, server_default=text("''"), comment='+UPC')
    misc15 = Column(String(100), nullable=False, server_default=text("''"), comment='+Type')
    misc16 = Column(MEDIUMTEXT, comment='+++ misc2')
    misc17 = Column(TINYINT(4), nullable=False, server_default=text("'1'"), comment='+Print Label')
    misc18 = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='+Ref #')
    misc19 = Column(MEDIUMTEXT, comment='+++ misc3')
    misc20 = Column(MEDIUMTEXT, comment='+++ misc4')
    misc21 = Column(MEDIUMTEXT, comment='+Internal Note')
    misc22 = Column(INTEGER(10), nullable=False, server_default=text("'0'"), comment='+Barcode Height')
    misc23 = Column(String(100), nullable=False, server_default=text("''"), comment='+UPC 1')
    misc24 = Column(String(100), nullable=False, server_default=text("''"), comment='+UPC 2')
    misc25 = Column(String(100), nullable=False, server_default=text("''"), comment='+UPC 3')
    misc26 = Column(String(75), nullable=False, server_default=text("''"))
    misc27 = Column(String(75), nullable=False, server_default=text("''"))
    misc28 = Column(String(75), nullable=False, server_default=text("''"))
    misc29 = Column(String(50), nullable=False, comment='+Supplier Item Code')
    misc30 = Column(Enum('n', 'y'), nullable=False, index=True, server_default=text("'y'"), comment='+Split for Warehouse Packing')
    misc31 = Column(MEDIUMTEXT, comment='+Ebay Description')
    misc32 = Column(MEDIUMTEXT, comment='+++ misc5')
    misc33 = Column(MEDIUMTEXT, comment='+++ misc6')
    misc34 = Column(String(25), index=True, comment='+Primary Supplier')
    misc35 = Column(MEDIUMTEXT, comment='+++ misc7')
    misc36 = Column(MEDIUMTEXT, comment='+++ misc8')
    misc37 = Column(MEDIUMTEXT, comment='+++ misc9')
    misc38 = Column(DECIMAL(15, 9), nullable=False, index=True, server_default=text("'0.000000000'"))
    misc39 = Column(MEDIUMTEXT, comment='+++ misc10')
    misc40 = Column(MEDIUMTEXT, comment='+++ misc11')
    misc41 = Column(MEDIUMTEXT, comment='+++ misc12')
    misc42 = Column(MEDIUMTEXT, comment='+++ misc13')
    misc43 = Column(String(255), nullable=False, server_default=text("''"), comment='+Flat Rate Shipping Charge: Standard')
    misc44 = Column(MEDIUMTEXT, comment='+++ misc16')
    misc45 = Column(String(255), nullable=False, server_default=text("''"), comment='+Flat Rate Shipping Charge: Express ')
    misc46 = Column(MEDIUMTEXT, comment='+++ misc19')
    misc47 = Column(MEDIUMTEXT, comment='+++ misc20')
    misc48 = Column(MEDIUMTEXT, comment='+++ misc21')
    misc49 = Column(MEDIUMTEXT, comment='+++ misc22')
    misc50 = Column(MEDIUMTEXT, comment='+++ misc23')
    misc51 = Column(MEDIUMTEXT, comment='+++ misc24')
    misc52 = Column(MEDIUMTEXT, comment='+++ misc25')
    misc53 = Column(MEDIUMTEXT, comment='+++ misc26')
    misc54 = Column(MEDIUMTEXT, comment='+++ misc27')
    misc55 = Column(MEDIUMTEXT, comment='+++ misc28')
    misc56 = Column(MEDIUMTEXT, comment='+++ misc29')
    misc57 = Column(MEDIUMTEXT, comment='+++ misc30')
    misc58 = Column(MEDIUMTEXT, comment='+++ misc31')
    misc59 = Column(MEDIUMTEXT, comment='+++ misc32')
    misc60 = Column(MEDIUMTEXT, comment='+++ misc33')
    misc61 = Column(MEDIUMTEXT, comment='+++ misc34')
    misc62 = Column(MEDIUMTEXT, comment='+++ misc35')
    misc63 = Column(MEDIUMTEXT, comment='+++ misc36')
    misc64 = Column(MEDIUMTEXT, comment='+++ misc37')
    misc65 = Column(MEDIUMTEXT, comment='+++ misc38')
    misc66 = Column(MEDIUMTEXT, comment='+++ misc39')
    misc67 = Column(MEDIUMTEXT, comment='+++ misc40')
    misc68 = Column(MEDIUMTEXT, comment='+++ misc41')
    misc69 = Column(MEDIUMTEXT, comment='+++ misc42')
    misc70 = Column(MEDIUMTEXT, comment='+++ misc43')
    misc71 = Column(MEDIUMTEXT, comment='+++ misc44')
    misc72 = Column(MEDIUMTEXT, comment='+++ misc45')
    misc73 = Column(MEDIUMTEXT, comment='+++ misc46')
    misc74 = Column(MEDIUMTEXT, comment='+++ misc47')
    service_level = Column(DECIMAL(5, 2), nullable=False, server_default=text("'0.00'"))
    display_template = Column(String(50), nullable=False, server_default=text("''"), comment='+Display Template')
    editable_bundle = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"), comment='+Editable Bundle')
    has_components = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    update_bundle_price = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    req_pack = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"), comment='+Requires Packaging')
    rental_only = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"), comment='+Rental Only')
    allow_oversell = Column(Enum('n', 'y'), nullable=False, index=True, server_default=text("'n'"), comment='+Virtual')
    serial_tracking = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"), comment='+Serial Tracking')
    service_only = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    repeat_type = Column(Enum('', 'once', 'always'), nullable=False, server_default=text("''"), comment='+When To Repeat On Standing Orders')
    acc_qty_multiplier = Column(DECIMAL(8, 3), nullable=False, comment='+Quantity Multiplier For Accounting Software (Hidden)')
    auto_url_update = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"), comment='+Automatically update the SITE_URL table')
    regen_url = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"), comment='+Regenerate URL in SITE_URL table on next batch')
    itm_gp_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='+Item Group')
    sh_type_id = Column(TINYINT(3), nullable=False, index=True, server_default=text("'0'"), comment='+Shipping Method Identifier')
    hs_tariff_number = Column(String(15), nullable=False)
    origin_country = Column(String(2), nullable=False)
    job_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='+Default Job')
    monthly_spent = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"), comment='+Monthly Spent Required')
    status = Column(TINYINT(4), nullable=False, server_default=text("'0'"), comment='-')
    active = Column(TINYINT(4), nullable=False, server_default=text("'0'"), comment='-')
    gp_restriction = Column(BIGINT(20), nullable=False, index=True, server_default=text("'0'"))
    itm_sortorder = Column(INTEGER(10), nullable=False, index=True)
    itm_sortorder2 = Column(INTEGER(10), nullable=False, index=True)
    imgupd_date = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"), comment='Image update date')
    stockpriceupd_date = Column(DateTime, nullable=False, index=True, server_default=text("CURRENT_TIMESTAMP"))
    timestamp = Column(TIMESTAMP, nullable=False, index=True, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    primary_warehouse_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    is_neto_utility = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    ebay_product_detail = Column(TINYINT(1), nullable=False, server_default=text("'1'"))
    supplier_product_name = Column(String(150))
    handling_time = Column(INTEGER(4))


class INVENTORYDELETECOUNTER(Base):
    __tablename__ = 'INVENTORYDELETECOUNTER'

    counter = Column(INTEGER(11), primary_key=True)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class INVENTORYNOTE(Base):
    __tablename__ = 'INVENTORYNOTES'

    SKU = Column(String(25), primary_key=True, nullable=False, index=True)
    note_id = Column(INTEGER(10), primary_key=True, nullable=False)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class INVENTORYBUNDLE(Base):
    __tablename__ = 'INVENTORY_BUNDLES'

    inventory_id = Column(INTEGER(10), primary_key=True, nullable=False)
    bundle_SKU = Column(String(25), primary_key=True, nullable=False, index=True)
    assemble = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    min_assemble = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    max_assemble = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    assemble_group_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    itm_price = Column(DECIMAL(8, 2))
    sequence = Column(TINYINT(4), nullable=False, server_default=text("'0'"))


class INVENTORYBUNDLESGROUP(Base):
    __tablename__ = 'INVENTORY_BUNDLES_GROUP'

    assemble_group_id = Column(INTEGER(10), primary_key=True)
    assemble_group_name = Column(String(100), nullable=False, server_default=text("''"))
    assemble_group_fullname = Column(String(255), nullable=False, server_default=text("''"))
    assemble_group_paramsuffix = Column(String(45), nullable=False, server_default=text("''"))
    assemble_group_description = Column(Text, nullable=False)
    assemble_min_qty = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    assemble_max_qty = Column(INTEGER(10), nullable=False, server_default=text("'0'"))


class INVENTORYBUNDLESGROUPPRICE(Base):
    __tablename__ = 'INVENTORY_BUNDLES_GROUP_PRICES'

    assemble_group_id = Column(INTEGER(10), primary_key=True, nullable=False)
    group_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True)
    to_group_id = Column(INTEGER(10), nullable=False, index=True)


class INVENTORYCARTON(Base):
    __tablename__ = 'INVENTORY_CARTONS'

    carton_id = Column(INTEGER(10), primary_key=True)
    SKU = Column(String(25), nullable=False, index=True)
    quantity = Column(INTEGER(10), nullable=False, server_default=text("'1'"))
    weight = Column(DECIMAL(10, 4), nullable=False, server_default=text("'0.0000'"))
    cubic = Column(DECIMAL(15, 9), nullable=False, server_default=text("'0.000000000'"))
    length = Column(DECIMAL(8, 3), nullable=False, server_default=text("'0.000'"))
    width = Column(DECIMAL(8, 3), nullable=False, server_default=text("'0.000'"))
    height = Column(DECIMAL(8, 3), nullable=False, server_default=text("'0.000'"))
    req_pack = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    barcode = Column(String(25), nullable=False, server_default=text("''"))


class INVENTORYCOLUMN(Base):
    __tablename__ = 'INVENTORY_COLUMNS'

    col_id = Column(String(25), primary_key=True)
    col_name = Column(String(50), nullable=False)
    col_section = Column(String(45), nullable=False, server_default=text("''"), comment='Determines which section of the product manager this field will appear')
    col_type = Column(Enum('TEXT', 'DATE', 'INT', 'DECIMAL', 'BOOLEAN', 'SHORTTEXT', 'SELECTIONBOX'), nullable=False, server_default=text("'TEXT'"))
    col_max_length = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    col_display = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    col_thumb = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    col_filter = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    col_options = Column(MEDIUMTEXT, nullable=False)
    col_comment = Column(String(255), nullable=False)
    col_description = Column(String(100), nullable=False)
    col_order = Column(TINYINT(3), nullable=False)


class INVENTORYCONTENT(Base):
    __tablename__ = 'INVENTORY_CONTENTS'

    inventory_id = Column(INTEGER(10), primary_key=True, nullable=False)
    content_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True)
    display_priority = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    compatibility_code = Column(String(50), nullable=False, index=True, server_default=text("''"))
    compatibility_notes = Column(Text, nullable=False)


class INVENTORYDELETE(Base):
    __tablename__ = 'INVENTORY_DELETE'

    SKU = Column(String(25), primary_key=True, nullable=False)
    del_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True)
    del_required = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    del_approved = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class INVENTORYDROPSHIPPER(Base):
    __tablename__ = 'INVENTORY_DROPSHIPPERS'

    SKU = Column(String(25), primary_key=True, nullable=False)
    dropshipper = Column(String(25), primary_key=True, nullable=False, index=True)
    ds_code = Column(String(50), nullable=False)


class INVENTORYGROUP(Base):
    __tablename__ = 'INVENTORY_GROUPS'

    itm_gp_id = Column(INTEGER(10), primary_key=True)
    gp_name = Column(String(50), nullable=False, server_default=text("''"))
    gp_description = Column(String(255), nullable=False, server_default=text("''"))
    min_qty = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    min_spend = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    mod_qty = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    rental_plan = Column(Enum('n', 'y', 's'), nullable=False, server_default=text("'n'"))


class INVENTORYJOB(Base):
    __tablename__ = 'INVENTORY_JOBS'

    job_id = Column(INTEGER(10), primary_key=True)
    job_code = Column(String(15), nullable=False, index=True, server_default=text("''"))
    job_name = Column(String(25), nullable=False, server_default=text("''"))
    job_description = Column(String(255), nullable=False, server_default=text("''"))
    job_active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class INVENTORYMANAGEDSALESCHANNEL(Base):
    __tablename__ = 'INVENTORY_MANAGED_SALES_CHANNEL'
    __table_args__ = (
        Index('inventory_id_managed_sales_id_idx', 'inventory_id', 'managed_sales_channel_id', unique=True),
    )

    id = Column(INTEGER(11), primary_key=True)
    inventory_id = Column(INTEGER(10), nullable=False)
    managed_sales_channel_id = Column(INTEGER(11), nullable=False, index=True)
    date_added = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class INVENTORYMOQ(Base):
    __tablename__ = 'INVENTORY_MOQS'

    inventory_id = Column(INTEGER(10), primary_key=True, nullable=False)
    group_id = Column(INTEGER(10), primary_key=True, nullable=False, server_default=text("'0'"))
    min_qty = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    max_qty = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    mod_qty = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    mod_qty_start = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class INVENTORYMULTILEVELPRICE(Base):
    __tablename__ = 'INVENTORY_MULTILEVELPRICES'

    inventory_id = Column(INTEGER(10), primary_key=True, nullable=False)
    group_id = Column(INTEGER(11), primary_key=True, nullable=False, server_default=text("'0'"))
    minqty = Column(INTEGER(11), primary_key=True, nullable=False, server_default=text("'0'"))
    maxqty = Column(INTEGER(11), primary_key=True, nullable=False, server_default=text("'0'"))
    price = Column(DECIMAL(8, 2), nullable=False, server_default=text("'0.00'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class INVENTORYPACKAGE(Base):
    __tablename__ = 'INVENTORY_PACKAGES'

    SKU = Column(String(25), primary_key=True, nullable=False)
    package_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True)


class INVENTORYPART(Base):
    __tablename__ = 'INVENTORY_PARTS'

    SKU = Column(String(25), primary_key=True, nullable=False, server_default=text("''"))
    parts_SKU = Column(String(25), primary_key=True, nullable=False, index=True, server_default=text("''"))


class INVENTORYPRICE(Base):
    __tablename__ = 'INVENTORY_PRICES'

    inventory_id = Column(INTEGER(10), primary_key=True, nullable=False)
    group_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True, server_default=text("'0'"))
    store_price = Column(DECIMAL(12, 2), index=True)
    promo_price = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    priceupd_date = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class INVENTORYPROMOCHK(Base):
    __tablename__ = 'INVENTORY_PROMOCHK'

    SKU = Column(String(25), primary_key=True)
    promo_id = Column(String(25))
    promo_start = Column(DateTime, server_default=text("'0000-00-00 00:00:00'"))
    promo_end = Column(DateTime, server_default=text("'0000-00-00 00:00:00'"))
    promo_price = Column(DECIMAL(12, 2))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class INVENTORYPROMOTION(Base):
    __tablename__ = 'INVENTORY_PROMOTIONS'

    inventory_id = Column(INTEGER(10), primary_key=True, server_default=text("'0'"))
    promotion_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))


class INVENTORYQTYLOG(Base):
    __tablename__ = 'INVENTORY_QTY_LOGS'

    qtylog_id = Column(INTEGER(10), primary_key=True)
    SKU = Column(String(25), nullable=False, index=True, server_default=text("''"))
    warehouse_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    qtylog_opt = Column(Enum('adjust', 'assign', 'info'), nullable=False, server_default=text("'adjust'"))
    qtylog_type = Column(String(20), nullable=False, index=True, server_default=text("''"))
    qtylog_ref = Column(String(25), nullable=False, index=True, server_default=text("''"))
    qtylog_desc = Column(String(50), nullable=False, server_default=text("''"))
    store_quantity = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    committed_quantity = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    build_quantity = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    warehouse_quantity = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    component_quantity = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    timestamp = Column(TIMESTAMP, nullable=False, index=True, server_default=text("CURRENT_TIMESTAMP"))


class INVENTORYRECORD(Base):
    __tablename__ = 'INVENTORY_RECORDS'

    inventory_id = Column(INTEGER(10), primary_key=True)
    qty_sold = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    qty_sold_total = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    has_image_full = Column(Enum('', 'n', 'y'), nullable=False, server_default=text("''"))
    image_total = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    item_rank = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    rand_rank = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    num_components = Column(INTEGER(10), nullable=False, server_default=text("'0'"))


class INVENTORYRELATED(Base):
    __tablename__ = 'INVENTORY_RELATED'

    inventory_id = Column(INTEGER(10), primary_key=True, nullable=False)
    related_inventory_id = Column(INTEGER(10), primary_key=True, nullable=False)
    related_type = Column(Enum('crossell', 'upsell', 'freebies'), primary_key=True, nullable=False)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class INVENTORYSHIPPINGFLATRATE(Base):
    __tablename__ = 'INVENTORY_SHIPPING_FLATRATE'

    inventory_id = Column(INTEGER(10), primary_key=True, nullable=False)
    shipping_ref = Column(INTEGER(10), primary_key=True, nullable=False, index=True)
    price_1st_parcel = Column(DECIMAL(10, 2), nullable=False, server_default=text("'0.00'"))
    price_sub_parcel = Column(DECIMAL(10, 2), nullable=False, server_default=text("'0.00'"))
    price_max_parcel = Column(DECIMAL(10, 2), nullable=False, server_default=text("'0.00'"))


class INVENTORYSPECIFIC(Base):
    __tablename__ = 'INVENTORY_SPECIFICS'

    inventory_id = Column(INTEGER(10), primary_key=True, nullable=False)
    itmspec_id = Column(INTEGER(10), primary_key=True, nullable=False)
    itmspecval_id = Column(INTEGER(11), primary_key=True, nullable=False)
    itmspec_counter = Column(INTEGER(10), nullable=False)


class INVENTORYSPECIFICSCACHE(Base):
    __tablename__ = 'INVENTORY_SPECIFICS_CACHE'

    inventory_id = Column(INTEGER(10), primary_key=True)
    itmspec_id_list = Column(String(1000), nullable=False, server_default=text("''"))
    itmspec_id_instock = Column(String(1000), nullable=False, server_default=text("''"))


class INVENTORYVOUCHERPROGRAM(Base):
    __tablename__ = 'INVENTORY_VOUCHER_PROGRAMS'
    __table_args__ = (
        Index('inventory_id', 'inventory_id', 'vprogram_id', unique=True),
    )

    SKU = Column(String(25), primary_key=True, nullable=False)
    inventory_id = Column(INTEGER(11))
    vprogram_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True)
    earn_per_dollar = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    earn_amount = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    earn_override = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    earn_type = Column(Enum('reward', 'gift', 'thirdparty'), nullable=False, server_default=text("'reward'"))


class INVENTORYWAREHOUSE(Base):
    __tablename__ = 'INVENTORY_WAREHOUSES'
    __table_args__ = (
        Index('sku_warehouse', 'SKU', 'warehouse_id', unique=True),
    )

    inventory_warehouse_id = Column(INTEGER(11), primary_key=True)
    SKU = Column(String(25), nullable=False)
    warehouse_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    warehouse_quantity = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    committed_quantity = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    component_quantity = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    incoming_quantity = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    avg_cost_price = Column(DECIMAL(20, 8), nullable=False, server_default=text("'0.00000000'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class INVENTORYWAREHOUSELOCATION(Base):
    __tablename__ = 'INVENTORY_WAREHOUSE_LOCATIONS'

    SKU = Column(String(25), primary_key=True, nullable=False)
    warehouse_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True, server_default=text("'0'"))
    location_id = Column(String(50), primary_key=True, nullable=False, index=True, server_default=text("''"))
    location_barcode = Column(String(50), nullable=False, index=True, server_default=text("''"))
    count_quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    location_status = Column(Enum('', 'unknown', 'empty', 'low', 'medium', 'high', 'full'), nullable=False, server_default=text("''"))
    location_type = Column(Enum('pick', 'bulk', 'virtual', 'special'), nullable=False, server_default=text("'pick'"))
    location_priority = Column(INTEGER(10), nullable=False, server_default=text("'0'"))


class INVENTORYWARNINGEMAIL(Base):
    __tablename__ = 'INVENTORY_WARNING_EMAIL'

    inventory_id = Column(INTEGER(10), primary_key=True)
    restock_warning_level = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    restock_quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    available_quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    incoming_quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    date_checked = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_restock_notify = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_restock_email = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_warn_notify = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_warn_email = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))


class ITEMNOTE(Base):
    __tablename__ = 'ITEMNOTES'

    note_id = Column(INTEGER(10), primary_key=True)
    note_code = Column(String(15), nullable=False, index=True)
    note_name = Column(String(50), nullable=False, server_default=text("''"))
    note_type_id = Column(INTEGER(10), nullable=False, index=True)
    note_description = Column(MEDIUMTEXT, nullable=False)
    note_visible = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))


class ITEMNOTESMACRO(Base):
    __tablename__ = 'ITEMNOTES_MACRO'

    macro_id = Column(INTEGER(10), primary_key=True)
    macro_name = Column(String(50), nullable=False, server_default=text("''"))
    macro_description = Column(String(255), nullable=False, server_default=text("''"))
    macro_value = Column(MEDIUMTEXT, nullable=False)
    active = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))


class ITEMNOTETYPE(Base):
    __tablename__ = 'ITEMNOTE_TYPES'

    note_type_id = Column(INTEGER(10), primary_key=True)
    note_type_code = Column(String(15), nullable=False, index=True, server_default=text("''"))
    note_type_name = Column(String(50), nullable=False, server_default=text("''"))
    note_type_description = Column(String(255), nullable=False, server_default=text("''"))
    note_type_visible = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))


class ITEMPROMOTION(Base):
    __tablename__ = 'ITEM_PROMOTIONS'

    promotion_id = Column(INTEGER(10), primary_key=True)
    promo_code = Column(String(25), nullable=False, server_default=text("''"))
    promo_name = Column(String(50), nullable=False, server_default=text("''"))
    promo_start = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    promo_end = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    promo_status = Column(Enum('Pending', 'Active', 'Ended'), server_default=text("'Pending'"))
    status_last_update_time = Column(DateTime)
    date_created = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class ITEMSPECIFIC(Base):
    __tablename__ = 'ITEM_SPECIFICS'

    itmspec_id = Column(INTEGER(10), primary_key=True)
    itmspec_name = Column(String(50), nullable=False, index=True, server_default=text("''"))
    itmspec_display = Column(Enum('text', 'select', 'radio', 'image', 'textimage'), nullable=False, server_default=text("'select'"))
    itmspec_autohide = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    itmspec_ebay_visible = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    itmspec_filter = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    sortorder = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))


class ITEMSPECIFICSVALUE(Base):
    __tablename__ = 'ITEM_SPECIFICS_VALUES'

    itmspecval_id = Column(INTEGER(11), primary_key=True)
    itmspec_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    itmspecval_value = Column(String(150), nullable=False, index=True, server_default=text("''"))
    itmspecval_desc = Column(String(1024), nullable=False, server_default=text("''"))
    itmspecval_swatch = Column(String(45), nullable=False, server_default=text("''"))
    sortorder = Column(INTEGER(3), nullable=False, server_default=text("'0'"))


class LASTORDERPROC(Base):
    __tablename__ = 'LASTORDERPROC'

    proc_id = Column(INTEGER(10), primary_key=True)
    username = Column(String(25), nullable=False, index=True)
    dssec_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    task = Column(Enum('pick', 'pack', 'tracking'), nullable=False)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class LASTORDERPROCLINE(Base):
    __tablename__ = 'LASTORDERPROCLINES'

    proc_id = Column(INTEGER(10), primary_key=True, nullable=False)
    order_id = Column(String(15), primary_key=True, nullable=False, index=True)
    counter = Column(TINYINT(3), primary_key=True, nullable=False, index=True)


class LASTVIEW(Base):
    __tablename__ = 'LAST_VIEW'

    inventory_id = Column(INTEGER(10), primary_key=True, nullable=False, server_default=text("'0'"))
    viewed_inventory_id = Column(INTEGER(10), primary_key=True, nullable=False, server_default=text("'0'"))
    counter = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class LASTVIEWCACHE(Base):
    __tablename__ = 'LAST_VIEW_CACHE'

    inventory_id = Column(INTEGER(10), primary_key=True, nullable=False, server_default=text("'0'"))
    viewed_inventory_id = Column(INTEGER(10), primary_key=True, nullable=False, server_default=text("'0'"))
    counter = Column(INTEGER(10), nullable=False, server_default=text("'0'"))


class MANAGEDSALESCHANNEL(Base):
    __tablename__ = 'MANAGED_SALES_CHANNEL'

    id = Column(INTEGER(11), primary_key=True)
    name = Column(String(100), nullable=False, unique=True)
    is_active = Column(TINYINT(1), nullable=False, index=True, server_default=text("'0'"))
    internal_name = Column(String(100), nullable=False, unique=True)
    is_marketplace = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    sortorder = Column(INTEGER(11), nullable=False, server_default=text("'0'"))


class MANAGEDSALESCHANNELRECORDLISTINGDETAIL(Base):
    __tablename__ = 'MANAGED_SALES_CHANNEL_RECORD_LISTING_DETAILS'

    id = Column(INTEGER(11), primary_key=True)
    listing_id = Column(INTEGER(11), nullable=False, index=True)
    price = Column(DECIMAL(12, 2))
    stock = Column(INTEGER(11))
    external_record_id = Column(String(255), index=True)
    external_record_url = Column(String(255))
    external_config_url = Column(String(255))
    status = Column(String(128), index=True)


class MANAGEDSALESCHANNELRECORDMESSAGE(Base):
    __tablename__ = 'MANAGED_SALES_CHANNEL_RECORD_MESSAGES'

    id = Column(INTEGER(11), primary_key=True)
    record_id = Column(INTEGER(11), nullable=False, index=True)
    type = Column(String(32), nullable=False)
    code = Column(String(25))
    response_code = Column(String(30))
    message = Column(Text)
    timestamp = Column(TIMESTAMP, nullable=False)


class MANAGEDSALESCHANNELRECORDSTATU(Base):
    __tablename__ = 'MANAGED_SALES_CHANNEL_RECORD_STATUS'
    __table_args__ = (
        Index('record_type_2', 'record_type', 'record_id'),
    )

    id = Column(INTEGER(11), primary_key=True)
    managed_sales_channel_store_id = Column(INTEGER(11), nullable=False, index=True)
    record_type = Column(String(128), nullable=False, index=True)
    record_id = Column(INTEGER(11), index=True)
    mapping_value = Column(String(255))
    additional_information = Column(Text)
    date_first_successful_attempt = Column(TIMESTAMP)
    date_last_attempt = Column(TIMESTAMP)
    date_last_successful_attempt = Column(TIMESTAMP)
    date_created = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    date_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    date_status_updated = Column(TIMESTAMP)
    error_code = Column(String(255))
    sync_status = Column(String(128), index=True)


class MANAGEDSALESCHANNELRECORDSYNCSTATU(Base):
    __tablename__ = 'MANAGED_SALES_CHANNEL_RECORD_SYNC_STATUS'
    __table_args__ = (
        Index('record_id', 'record_id', 'code', unique=True),
    )

    id = Column(INTEGER(11), primary_key=True)
    record_id = Column(INTEGER(11), nullable=False)
    code = Column(String(255), nullable=False)
    status = Column(String(255), nullable=False)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class MANAGEDSALESCHANNELSHIPPINGSERVICE(Base):
    __tablename__ = 'MANAGED_SALES_CHANNEL_SHIPPING_SERVICE'
    __table_args__ = (
        Index('sales_channel_external_namex', 'managed_sales_channel_id', 'external_name', unique=True),
    )

    id = Column(INTEGER(11), primary_key=True)
    managed_sales_channel_id = Column(INTEGER(11), nullable=False)
    code = Column(String(255), nullable=False)
    external_name = Column(String(255), nullable=False)
    type = Column(String(255), nullable=False)


class MANAGEDSALESCHANNELSTORE(Base):
    __tablename__ = 'MANAGED_SALES_CHANNEL_STORE'

    id = Column(INTEGER(11), primary_key=True)
    managed_sales_channel_id = Column(INTEGER(11), nullable=False, index=True)
    name = Column(String(255))
    is_primary = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    is_active = Column(TINYINT(1), nullable=False, server_default=text("'0'"))


class MANAGEDSALESCHANNELSTORECONFIG(Base):
    __tablename__ = 'MANAGED_SALES_CHANNEL_STORE_CONFIG'

    id = Column(INTEGER(11), primary_key=True)
    managed_sales_channel_store_id = Column(INTEGER(11), nullable=False, unique=True)
    paytype_id = Column(INTEGER(11))
    user_group_id = Column(INTEGER(11), nullable=False)
    fallback_to_default_user_group = Column(TINYINT(1), nullable=False)
    managed_sales_channel_credentials_id = Column(INTEGER(11))
    max_stock_quantity = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    reserved_stock_quantity = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    default_handling_time = Column(INTEGER(11))
    sync_product = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    sync_stock = Column(TINYINT(1), nullable=False, server_default=text("'1'"))
    sync_price = Column(TINYINT(1), nullable=False, server_default=text("'1'"))
    use_promo_price = Column(TINYINT(1), nullable=False, server_default=text("'1'"))
    virtual_product_quantity = Column(INTEGER(11), nullable=False, server_default=text("'50'"))
    mapping_content_type_id = Column(INTEGER(11))
    rounding_enabled = Column(TINYINT(1), nullable=False, server_default=text("'1'"))
    ready_to_ship_order_status_id = Column(INTEGER(11))
    not_ready_to_ship_order_status_id = Column(INTEGER(11))
    inherit_parent_data = Column(TINYINT(1), nullable=False, server_default=text("'1'"))
    manual_product_id_mapping = Column(TINYINT(1), nullable=False, server_default=text("'0'"))


class MANAGEDSALESCHANNELSTORECONFIGSHIPPINGSERVICE(Base):
    __tablename__ = 'MANAGED_SALES_CHANNEL_STORE_CONFIG_SHIPPING_SERVICE'

    id = Column(INTEGER(11), primary_key=True)
    managed_sales_channel_store_config_id = Column(INTEGER(11), nullable=False)
    managed_sales_channel_shipping_service_id = Column(INTEGER(11), nullable=False)
    sh_group_id = Column(INTEGER(11), nullable=False)


class MANAGEDSALESCHANNELSTORECONTENTMAPPING(Base):
    __tablename__ = 'MANAGED_SALES_CHANNEL_STORE_CONTENT_MAPPING'
    __table_args__ = (
        Index('product_type_category_mapping_id', 'product_type_category_mapping_id', 'content_id', unique=True),
    )

    id = Column(INTEGER(11), primary_key=True)
    mapping_id = Column(INTEGER(11), nullable=False)
    content_id = Column(INTEGER(11), nullable=False)
    product_type_category_mapping_id = Column(INTEGER(11), nullable=False)


class MANAGEDSALESCHANNELSTORECREDENTIAL(Base):
    __tablename__ = 'MANAGED_SALES_CHANNEL_STORE_CREDENTIALS'

    id = Column(INTEGER(11), primary_key=True)
    managed_sales_channel_store_id = Column(INTEGER(11), nullable=False, unique=True)
    type = Column(String(255), nullable=False, index=True)
    credentials = Column(Text, nullable=False)


class MANAGEDSALESCHANNELSTOREFIELDMAPPING(Base):
    __tablename__ = 'MANAGED_SALES_CHANNEL_STORE_FIELD_MAPPING'
    __table_args__ = (
        Index('product_type_mapping_id', 'product_type_mapping_id', 'sc_field_name', unique=True),
    )

    id = Column(INTEGER(11), primary_key=True)
    sc_field_name = Column(String(255), nullable=False)
    mapped_to = Column(String(2000))
    type = Column(Enum('column', 'specific', 'static', 'image'), nullable=False)
    product_type_mapping_id = Column(INTEGER(11), nullable=False)


class MANAGEDSALESCHANNELSTOREINTEGRATIONMETADATUM(Base):
    __tablename__ = 'MANAGED_SALES_CHANNEL_STORE_INTEGRATION_METADATA'
    __table_args__ = (
        Index('store_id', 'store_id', 'integrator', 'group', 'key', unique=True),
    )

    id = Column(INTEGER(11), primary_key=True)
    store_id = Column(INTEGER(11), nullable=False)
    integrator = Column(String(20), nullable=False)
    group = Column(String(20), nullable=False)
    key = Column(String(50), nullable=False)
    value = Column(Text)


class MANAGEDSALESCHANNELSTOREPRODUCTTYPECATEGORYMAPPING(Base):
    __tablename__ = 'MANAGED_SALES_CHANNEL_STORE_PRODUCT_TYPE_CATEGORY_MAPPING'
    __table_args__ = (
        Index('sccat_id', 'sccat_id', 'product_type_mapping_id', unique=True),
    )

    id = Column(INTEGER(11), primary_key=True)
    product_type_mapping_id = Column(INTEGER(11), nullable=False)
    sccat_id = Column(String(255), nullable=False)
    sccat_fullpath = Column(Text)


class MANAGEDSALESCHANNELSTOREPRODUCTTYPEMAPPING(Base):
    __tablename__ = 'MANAGED_SALES_CHANNEL_STORE_PRODUCT_TYPE_MAPPING'

    id = Column(INTEGER(11), primary_key=True)
    scstore_id = Column(INTEGER(11), nullable=False)
    product_type_fullpath = Column(Text)


class MANUFACTURER(Base):
    __tablename__ = 'MANUFACTURERS'

    manufacturer_id = Column(INTEGER(10), primary_key=True)
    mid = Column(String(25), nullable=False, unique=True, server_default=text("''"))
    mref = Column(INTEGER(10))
    lead_time1 = Column(TINYINT(3), nullable=False, server_default=text("'0'"))
    lead_time2 = Column(TINYINT(3), nullable=False, server_default=text("'0'"))
    company = Column(String(50), nullable=False, server_default=text("''"))
    street1 = Column(String(50), nullable=False, server_default=text("''"))
    street2 = Column(String(50), nullable=False, server_default=text("''"))
    city = Column(String(50), nullable=False, server_default=text("''"))
    state = Column(String(50), nullable=False, server_default=text("''"))
    zip = Column(String(15), nullable=False, server_default=text("''"))
    country = Column(String(50), nullable=False, server_default=text("''"))
    phone = Column(String(25), nullable=False, server_default=text("''"))
    fax = Column(String(25), nullable=False, server_default=text("''"))
    url = Column(String(255), nullable=False, server_default=text("''"))
    email = Column(String(50), nullable=False, server_default=text("''"))
    email_notify = Column(Enum('n', 'y'), nullable=False, index=True)
    export_template = Column(String(25), nullable=False, server_default=text("'default'"))
    account_code = Column(String(25), nullable=False)
    factory_street1 = Column(String(50), nullable=False, server_default=text("''"))
    factory_street2 = Column(String(50), nullable=False, server_default=text("''"))
    factory_city = Column(String(50), nullable=False, server_default=text("''"))
    factory_state = Column(String(50), nullable=False, server_default=text("''"))
    factory_zip = Column(String(15), nullable=False)
    factory_country = Column(String(50), nullable=False, server_default=text("''"))
    note = Column(MEDIUMTEXT)
    currency_code = Column(String(3), server_default=text("''"))
    active = Column(TINYINT(1), nullable=False, server_default=text("'1'"))


class MANUFACTURERCONTACT(Base):
    __tablename__ = 'MANUFACTURER_CONTACTS'

    id = Column(INTEGER(10), primary_key=True)
    mid = Column(String(25), nullable=False, index=True, server_default=text("''"))
    email = Column(String(50), nullable=False, server_default=text("''"))
    department = Column(String(25), nullable=False, server_default=text("''"))
    first_name = Column(String(50), nullable=False, server_default=text("''"))
    last_name = Column(String(50), nullable=False, server_default=text("''"))
    phone1 = Column(String(25), nullable=False, server_default=text("''"))
    phone2 = Column(String(25), nullable=False, server_default=text("''"))
    fax = Column(String(25), nullable=False, server_default=text("''"))
    note = Column(MEDIUMTEXT, nullable=False)


class MANUFACTURERUPDATE(Base):
    __tablename__ = 'MANUFACTURER_UPDATES'

    mid = Column(String(25), primary_key=True)
    last_checked = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    last_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class MENUITEM(Base):
    __tablename__ = 'MENUITEMS'

    menuitem_id = Column(INTEGER(10), primary_key=True)
    menu_id = Column(INTEGER(10), nullable=False, index=True)
    name = Column(String(50), nullable=False, server_default=text("''"))
    hierarchy = Column(String(40), nullable=False, server_default=text("''"))
    token_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    doc_id = Column(String(25), nullable=False, index=True, server_default=text("''"))
    url = Column(String(255), nullable=False, server_default=text("''"))
    url_qs = Column(String(255), nullable=False, server_default=text("''"))
    visible = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    css_class = Column(String(30), nullable=False, server_default=text("''"))


class MENU(Base):
    __tablename__ = 'MENUS'

    menu_id = Column(INTEGER(10), primary_key=True)
    menu_ref = Column(String(15), nullable=False, unique=True, server_default=text("''"))
    menu_name = Column(String(25), nullable=False, server_default=text("''"))
    description = Column(String(255), nullable=False, server_default=text("''"))
    visible = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    system_menu = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class MESSAGE(Base):
    __tablename__ = 'MESSAGES'
    __table_args__ = (
        Index('msg_to_username', 'msg_to_username', 'msg_to_usertype'),
        Index('msg_from_username', 'msg_from_username', 'msg_from_usertype')
    )

    id = Column(INTEGER(11), primary_key=True)
    parent_id = Column(INTEGER(11), nullable=False, index=True, server_default=text("'0'"))
    msg_from_username = Column(String(25), nullable=False, server_default=text("''"))
    msg_from_usertype = Column(CHAR(1), nullable=False, server_default=text("''"))
    msg_to_username = Column(String(25), nullable=False, server_default=text("''"))
    msg_to_usertype = Column(CHAR(1), nullable=False, server_default=text("''"))
    subject = Column(String(255))
    body = Column(MEDIUMTEXT)
    urgent = Column(CHAR(1), server_default=text("'N'"))
    send_date = Column(DateTime)
    read_date = Column(DateTime)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    deleted = Column(CHAR(1), server_default=text("'N'"))
    deleted_outbox = Column(TINYINT(4), nullable=False, server_default=text("'0'"))
    deleted_trash_sender = Column(TINYINT(4), nullable=False, server_default=text("'0'"))
    deleted_trash_recipient = Column(TINYINT(4), nullable=False, server_default=text("'0'"))
    from_email = Column(String(255), nullable=False, server_default=text("''"))
    to_email = Column(String(255), nullable=False, server_default=text("''"))
    from_name = Column(String(100), nullable=False, server_default=text("''"))
    to_name = Column(String(100), nullable=False, server_default=text("''"))


class MYOBCUSTOMFIELD(Base):
    __tablename__ = 'MYOBCUSTOMFIELDS'

    field_id = Column(String(15), primary_key=True, nullable=False)
    field_type = Column(Enum('item', 'customer', 'supplier'), primary_key=True, nullable=False)
    field_name = Column(String(50), nullable=False, server_default=text("''"))
    insert_only = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    field_order = Column(TINYINT(3), nullable=False, server_default=text("'0'"))


class MYOBEXPORTED(Base):
    __tablename__ = 'MYOBEXPORTED'

    export_id = Column(String(10), primary_key=True, nullable=False)
    export_type = Column(String(10), primary_key=True, nullable=False)
    export_code = Column(Enum('success', 'warn', 'error'), nullable=False, server_default=text("'success'"))
    export_result = Column(String(255), nullable=False)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class MYOBECUSTOMERCARD(Base):
    __tablename__ = 'MYOBE_CUSTOMER_CARDS'

    username = Column(String(25), primary_key=True)
    CoLastName = Column(String(50))
    FirstName = Column(String(20))
    CardID = Column(String(15), index=True)
    CardStatus = Column(CHAR(1))
    CurrencyCode = Column(String(3))
    Address1AddressLine1 = Column(String(255))
    Address1AddressLine2 = Column(String(253))
    Address1AddressLine3 = Column(String(251))
    Address1AddressLine4 = Column(String(249))
    Address1City = Column(String(255))
    Address1State = Column(String(255))
    Address1PostCode = Column(String(10))
    Address1Country = Column(String(255))
    Address1Phone1 = Column(String(21))
    Address1Phone2 = Column(String(21))
    Address1Phone3 = Column(String(21))
    Address1Fax = Column(String(21))
    Address1Email = Column(String(255))
    Address1Website = Column(String(255))
    Address1ContactName = Column(String(25))
    Address1Salutation = Column(String(15))
    Address2AddressLine1 = Column(String(255))
    Address2AddressLine2 = Column(String(253))
    Address2AddressLine3 = Column(String(251))
    Address2AddressLine4 = Column(String(249))
    Address2City = Column(String(255))
    Address2State = Column(String(255))
    Address2PostCode = Column(String(10))
    Address2Country = Column(String(255))
    Address2Phone1 = Column(String(21))
    Address2Phone2 = Column(String(21))
    Address2Phone3 = Column(String(21))
    Address2Fax = Column(String(21))
    Address2Email = Column(String(255))
    Address2Website = Column(String(255))
    Address2ContactName = Column(String(25))
    Address2Salutation = Column(String(15))
    Address3AddressLine1 = Column(String(255))
    Address3AddressLine2 = Column(String(253))
    Address3AddressLine3 = Column(String(251))
    Address3AddressLine4 = Column(String(249))
    Address3City = Column(String(255))
    Address3State = Column(String(255))
    Address3PostCode = Column(String(10))
    Address3Country = Column(String(255))
    Address3Phone1 = Column(String(21))
    Address3Phone2 = Column(String(21))
    Address3Phone3 = Column(String(21))
    Address3Fax = Column(String(21))
    Address3Email = Column(String(255))
    Address3Website = Column(String(255))
    Address3ContactName = Column(String(25))
    Address3Salutation = Column(String(15))
    Address4AddressLine1 = Column(String(255))
    Address4AddressLine2 = Column(String(253))
    Address4AddressLine3 = Column(String(251))
    Address4AddressLine4 = Column(String(249))
    Address4City = Column(String(255))
    Address4State = Column(String(255))
    Address4PostCode = Column(String(10))
    Address4Country = Column(String(255))
    Address4Phone1 = Column(String(21))
    Address4Phone2 = Column(String(21))
    Address4Phone3 = Column(String(21))
    Address4Fax = Column(String(21))
    Address4Email = Column(String(255))
    Address4Website = Column(String(255))
    Address4ContactName = Column(String(25))
    Address4Salutation = Column(String(15))
    Address5AddressLine1 = Column(String(255))
    Address5AddressLine2 = Column(String(253))
    Address5AddressLine3 = Column(String(251))
    Address5AddressLine4 = Column(String(249))
    Address5City = Column(String(255))
    Address5State = Column(String(255))
    Address5PostCode = Column(String(10))
    Address5Country = Column(String(255))
    Address5Phone1 = Column(String(21))
    Address5Phone2 = Column(String(21))
    Address5Phone3 = Column(String(21))
    Address5Fax = Column(String(21))
    Address5Email = Column(String(255))
    Address5Website = Column(String(255))
    Address5ContactName = Column(String(25))
    Address5Salutation = Column(String(15))
    Picture = Column(String(255))
    Notes = Column(String(255))
    Identifiers = Column(String(10))
    CustomList1 = Column(String(30))
    CustomList2 = Column(String(30))
    CustomList3 = Column(String(30))
    CustomField1 = Column(String(255))
    CustomField2 = Column(String(255))
    CustomField3 = Column(String(255))
    BillingRate = Column(DECIMAL(14, 4))
    PaymentIsDue = Column(INTEGER(10))
    DiscountDays = Column(INTEGER(10))
    BalanceDueDays = Column(INTEGER(10))
    PercentDiscount = Column(DECIMAL(4, 2))
    PercentMonthlyCharge = Column(DECIMAL(4, 2))
    TaxCode = Column(CHAR(3))
    CreditLimit = Column(DECIMAL(9, 2))
    TaxIDNumber = Column(String(19))
    VolumeDiscount = Column(DECIMAL(4, 2))
    SaleLayout = Column(CHAR(1))
    ItemPriceLevel = Column(INTEGER(10))
    PaymentMethod = Column(String(20))
    ABN = Column(String(11))
    ABNBranch = Column(String(11))
    IncomeAccount = Column(INTEGER(5))
    Salesperson = Column(String(50))
    SalespersonCardID = Column(String(15))
    SaleComment = Column(String(255))
    ShippingMethod = Column(String(20))
    FreightTaxCode = Column(String(3))
    UseCustomersTaxCode = Column(CHAR(1))
    RecordID = Column(INTEGER(10))


class MYOBEINVENTORYADJUSTMENT(Base):
    __tablename__ = 'MYOBE_INVENTORY_ADJUSTMENTS'

    adjust_id = Column(INTEGER(10), primary_key=True)
    JournalNumber = Column(String(8))
    AdjustmentDate = Column(Date)
    Memo = Column(String(255))
    ItemNumber = Column(String(30))
    Location = Column(String(10))
    Quantity = Column(DECIMAL(14, 3))
    UnitCost = Column(DECIMAL(16, 4))
    Amount = Column(DECIMAL(15, 2))
    Account = Column(INTEGER(5))
    Job = Column(String(15))
    AllocationMemo = Column(String(255))
    Category = Column(String(15))


class MYOBEITEMPURCHASE(Base):
    __tablename__ = 'MYOBE_ITEM_PURCHASES'

    poline_id = Column(INTEGER(10), primary_key=True)
    po_id = Column(INTEGER(10), index=True)
    CoLastName = Column(String(50))
    FirstName = Column(String(20))
    AddressLine1 = Column(String(255))
    AddressLine2 = Column(String(253))
    AddressLine3 = Column(String(251))
    AddressLine4 = Column(String(249))
    Inclusive = Column(CHAR(1))
    PurchaseNumber = Column(String(8), index=True)
    PurchaseDate = Column(Date)
    SuppliersNumber = Column(String(20))
    ShipVia = Column(String(20))
    DeliveryStatus = Column(CHAR(1))
    ItemNumber = Column(String(30))
    Quantity = Column(DECIMAL(11, 3))
    Description = Column(String(255))
    ExTaxPrice = Column(DECIMAL(15, 2))
    IncTaxPrice = Column(DECIMAL(15, 2))
    Discount = Column(DECIMAL(5, 2))
    ExTaxTotal = Column(DECIMAL(15, 2))
    IncTaxTotal = Column(DECIMAL(15, 2))
    Job = Column(String(15))
    Comment = Column(String(255))
    Memo = Column(String(255))
    ShippingDate = Column(Date)
    TaxCode = Column(String(3))
    NonGSTImportAmount = Column(DECIMAL(15, 2))
    GSTAmount = Column(DECIMAL(15, 2))
    ImportDutyAmount = Column(DECIMAL(15, 2))
    FreightExTaxAmount = Column(DECIMAL(15, 2))
    FreighttIncTaxAmount = Column(DECIMAL(15, 2))
    FreightTaxCode = Column(DECIMAL(15, 2))
    FreightNonGSTImportAmount = Column(DECIMAL(15, 2))
    FreightGSTAmount = Column(String(3))
    FreightImportDutyAmount = Column(DECIMAL(15, 2))
    PurchaseStatus = Column(CHAR(1))
    CurrencyCode = Column(String(3))
    ExchangeRate = Column(DECIMAL(12, 6))
    PaymentIsDue = Column(CHAR(1))
    DiscountDays = Column(INTEGER(10))
    BalanceDueDays = Column(INTEGER(10))
    PercentDiscount = Column(DECIMAL(4, 2))
    AmountPaid = Column(DECIMAL(15, 2))
    Category = Column(String(15))
    Ordered = Column(DECIMAL(11, 3))
    Received = Column(DECIMAL(11, 3))
    Billed = Column(DECIMAL(11, 3))
    Location = Column(String(10))
    CardID = Column(String(15))
    RecordID = Column(INTEGER(10))


class MYOBEITEMREFUND(Base):
    __tablename__ = 'MYOBE_ITEM_REFUNDS'
    __table_args__ = (
        Index('order', 'invoice_id', 'counter'),
    )

    record_id = Column(INTEGER(10), primary_key=True)
    invoice_id = Column(String(10), nullable=False)
    counter = Column(TINYINT(3), nullable=False)
    CoLastName = Column(String(50))
    FirstName = Column(String(20))
    AddressLine1 = Column(String(255))
    AddressLine2 = Column(String(253))
    AddressLine3 = Column(String(251))
    AddressLine4 = Column(String(249))
    Inclusive = Column(CHAR(1))
    InvoiceNumber = Column(String(8))
    SaleDate = Column(Date)
    CustomersNumber = Column(String(20))
    ShipVia = Column(String(20))
    ItemNumber = Column(String(30))
    DeliveryStatus = Column(CHAR(1))
    Description = Column(String(255))
    AccountNumber = Column(INTEGER(5))
    ExTaxAmount = Column(DECIMAL(15, 2))
    IncTaxAmount = Column(DECIMAL(15, 2))
    Job = Column(String(15))
    Comment = Column(String(255))
    Memo = Column(String(255))
    SalespersonLastName = Column(String(50))
    SalespersonFirstName = Column(String(20))
    ShippingDate = Column(Date)
    ReferralSource = Column(String(20))
    TaxCode = Column(String(3))
    NonGSTLCTAmount = Column(DECIMAL(15, 2))
    GSTAmount = Column(DECIMAL(15, 2))
    LCTAmount = Column(DECIMAL(15, 2))
    FreightExTaxAmount = Column(DECIMAL(15, 2))
    FreightIncTaxAmount = Column(DECIMAL(15, 2))
    FreightTaxCode = Column(String(3))
    FreightNonGSTLCTAmount = Column(DECIMAL(15, 2))
    FreightLCTAmount = Column(DECIMAL(15, 2))
    FreightGSTAmount = Column(DECIMAL(15, 2))
    SaleStatus = Column(CHAR(1))
    CurrencyCode = Column(String(3))
    ExchangeRate = Column(DECIMAL(12, 6))
    PaymentIsDue = Column(INTEGER(1))
    DiscountDays = Column(INTEGER(10))
    BalanceDueDays = Column(INTEGER(10))
    PercentDiscount = Column(DECIMAL(4, 2))
    PercentMonthlyCharge = Column(DECIMAL(4, 2))
    AmountPaid = Column(DECIMAL(15, 2))
    PaymentMethod = Column(String(20))
    PaymentNotes = Column(String(255))
    NameOnCard = Column(String(50))
    CardNumber = Column(String(25))
    ExpiryDate = Column(String(5))
    AuthorisationCode = Column(String(255))
    DrawerBSB = Column(String(7))
    DrawerAccountNumber = Column(INTEGER(10))
    DrawerAccountName = Column(String(32))
    DrawerChequeNumber = Column(String(25))
    Category = Column(String(15))
    CardID = Column(String(15))
    RecordID = Column(INTEGER(10))


class MYOBEITEMRMA(Base):
    __tablename__ = 'MYOBE_ITEM_RMAS'
    __table_args__ = (
        Index('order', 'invoice_id', 'counter'),
    )

    record_id = Column(INTEGER(11), primary_key=True)
    invoice_id = Column(String(10), nullable=False, server_default=text("''"))
    counter = Column(TINYINT(3), nullable=False)
    CoLastName = Column(String(50))
    FirstName = Column(String(20))
    AddressLine1 = Column(String(255))
    AddressLine2 = Column(String(253))
    AddressLine3 = Column(String(251))
    AddressLine4 = Column(String(249))
    Inclusive = Column(CHAR(1))
    InvoiceNumber = Column(String(8))
    SaleDate = Column(Date)
    CustomersNumber = Column(String(20))
    ShipVia = Column(String(20))
    ItemNumber = Column(String(30))
    DeliveryStatus = Column(CHAR(1))
    Quantity = Column(DECIMAL(11, 3))
    Description = Column(String(255))
    ExTaxPrice = Column(DECIMAL(15, 2))
    IncTaxPrice = Column(DECIMAL(15, 2))
    Discount = Column(DECIMAL(5, 2))
    ExTaxTotal = Column(DECIMAL(15, 2))
    IncTaxTotal = Column(DECIMAL(15, 2))
    Job = Column(String(15))
    Comment = Column(String(255))
    Memo = Column(String(255))
    SalespersonLastName = Column(String(50))
    SalespersonFirstName = Column(String(20))
    ShippingDate = Column(Date)
    TaxCode = Column(String(3))
    NonGSTLCTAmount = Column(DECIMAL(15, 2))
    GSTAmount = Column(DECIMAL(15, 2))
    LCTAmount = Column(DECIMAL(15, 2))
    FreightExTaxAmount = Column(DECIMAL(15, 2))
    FreightIncTaxAmount = Column(DECIMAL(15, 2))
    FreightTaxCode = Column(String(3))
    FreightNonGSTLCTAmount = Column(DECIMAL(15, 2))
    FreightGSTAmount = Column(DECIMAL(15, 2))
    FreightLCTAmount = Column(DECIMAL(15, 2))
    SaleStatus = Column(CHAR(1))
    CurrencyCode = Column(String(3))
    ExchangeRate = Column(DECIMAL(12, 6))
    PaymentIsDue = Column(INTEGER(10))
    DiscountDays = Column(INTEGER(10))
    BalanceDueDays = Column(INTEGER(10))
    PercentDiscount = Column(DECIMAL(4, 2))
    PercentMonthlyCharge = Column(DECIMAL(4, 2))
    ReferralSource = Column(String(20))
    AmountPaid = Column(DECIMAL(15, 2))
    PaymentMethod = Column(String(20))
    PaymentNotes = Column(String(255))
    NameOnCard = Column(String(50))
    CardNumber = Column(String(25))
    ExpiryDate = Column(String(5))
    AuthorisationCode = Column(String(255))
    DrawerBSB = Column(String(7))
    DrawerAccountNumber = Column(String(9))
    DrawerAccountName = Column(String(32))
    DrawerChequeNumber = Column(String(25))
    Category = Column(String(15))
    Location = Column(String(10))
    CardID = Column(String(15))
    RecordID = Column(INTEGER(10))


class MYOBEITEMSALE(Base):
    __tablename__ = 'MYOBE_ITEM_SALES'
    __table_args__ = (
        Index('order', 'order_id', 'counter'),
    )

    record_id = Column(INTEGER(10), primary_key=True)
    order_id = Column(String(15), nullable=False)
    counter = Column(TINYINT(3), nullable=False)
    CoLastName = Column(String(50))
    FirstName = Column(String(20))
    AddressLine1 = Column(String(255))
    AddressLine2 = Column(String(253))
    AddressLine3 = Column(String(251))
    AddressLine4 = Column(String(249))
    Inclusive = Column(CHAR(1))
    InvoiceNumber = Column(String(8))
    SaleDate = Column(Date)
    CustomersNumber = Column(String(20))
    ShipVia = Column(String(20))
    ItemNumber = Column(String(30))
    DeliveryStatus = Column(CHAR(1))
    Quantity = Column(DECIMAL(11, 3))
    Description = Column(String(255))
    ExTaxPrice = Column(DECIMAL(15, 2))
    IncTaxPrice = Column(DECIMAL(15, 2))
    Discount = Column(DECIMAL(5, 2))
    ExTaxTotal = Column(DECIMAL(15, 2))
    IncTaxTotal = Column(DECIMAL(15, 2))
    Job = Column(String(15))
    Comment = Column(String(255))
    Memo = Column(String(255))
    SalespersonLastName = Column(String(50))
    SalespersonFirstName = Column(String(20))
    ShippingDate = Column(Date)
    TaxCode = Column(String(3))
    NonGSTLCTAmount = Column(DECIMAL(15, 2))
    GSTAmount = Column(DECIMAL(15, 2))
    LCTAmount = Column(DECIMAL(15, 2))
    FreightExTaxAmount = Column(DECIMAL(15, 2))
    FreightIncTaxAmount = Column(DECIMAL(15, 2))
    FreightTaxCode = Column(String(3))
    FreightNonGSTLCTAmount = Column(DECIMAL(15, 2))
    FreightGSTAmount = Column(DECIMAL(15, 2))
    FreightLCTAmount = Column(DECIMAL(15, 2))
    SaleStatus = Column(CHAR(1))
    CurrencyCode = Column(String(3))
    ExchangeRate = Column(DECIMAL(12, 6))
    PaymentIsDue = Column(INTEGER(10))
    DiscountDays = Column(INTEGER(10))
    BalanceDueDays = Column(INTEGER(10))
    PercentDiscount = Column(DECIMAL(4, 2))
    PercentMonthlyCharge = Column(DECIMAL(4, 2))
    ReferralSource = Column(String(20))
    AmountPaid = Column(DECIMAL(15, 2))
    PaymentMethod = Column(String(20))
    PaymentNotes = Column(String(255))
    NameOnCard = Column(String(50))
    CardNumber = Column(String(25))
    ExpiryDate = Column(String(5))
    AuthorisationCode = Column(String(255))
    DrawerBSB = Column(String(7))
    DrawerAccountNumber = Column(String(9))
    DrawerAccountName = Column(String(32))
    DrawerChequeNumber = Column(String(25))
    Category = Column(String(15))
    Location = Column(String(10))
    CardID = Column(String(15))
    RecordID = Column(INTEGER(10))
    AccountNumber = Column(INTEGER(5))
    ExTaxAmount = Column(DECIMAL(15, 2))
    IncTaxAmount = Column(DECIMAL(15, 2))


class MYOBERECEIVEPAYMENT(Base):
    __tablename__ = 'MYOBE_RECEIVE_PAYMENTS'

    payment_id = Column(INTEGER(10), primary_key=True)
    CoLastName = Column(String(50))
    FirstName = Column(String(20))
    DepositAccount = Column(String(6))
    TransactionID = Column(String(8))
    ReceiptDate = Column(Date)
    InvoiceNumber = Column(String(8))
    CustomersNumber = Column(String(20))
    InvoiceDate = Column(String(11))
    AmountApplied = Column(DECIMAL(15, 2))
    Memo = Column(String(255))
    CurrencyCode = Column(CHAR(3))
    ExchangeRate = Column(DECIMAL(12, 6))
    PaymentMethod = Column(String(20))
    PaymentNotes = Column(String(255))
    NameOnCard = Column(String(50))
    CardNumber = Column(String(25))
    ExpiryDate = Column(String(5))
    AuthorisationCode = Column(String(255))
    DrawerBSB = Column(String(7))
    DrawerAccountNumber = Column(INTEGER(10))
    DrawerAccountName = Column(String(32))
    DrawerChequeNumber = Column(String(25))
    CardID = Column(String(15))
    RecordID = Column(INTEGER(10))


class MYOBIMPORTED(Base):
    __tablename__ = 'MYOBIMPORTED'

    myob_id = Column(INTEGER(10), primary_key=True, nullable=False)
    myob_tbl = Column(String(25), primary_key=True, nullable=False, index=True)
    check_string = Column(String(255), nullable=False)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class MYOBIACCOUNT(Base):
    __tablename__ = 'MYOBI_ACCOUNTS'

    AccountID = Column(INTEGER(10), primary_key=True)
    ParentAccountID = Column(INTEGER(10), nullable=False, index=True)
    IsInactive = Column(CHAR(1), nullable=False)
    AccountName = Column(String(30), nullable=False)
    AccountNumber = Column(String(10), nullable=False, index=True)
    TaxCodeID = Column(INTEGER(10), nullable=False)
    AccountClassificationID = Column(String(4), nullable=False)
    SubAccountClassificationID = Column(String(4), nullable=False)


class MYOBIADDRES(Base):
    __tablename__ = 'MYOBI_ADDRESS'

    AddressID = Column(INTEGER(10), primary_key=True)
    CardRecordID = Column(String(50), nullable=False, index=True)
    Location = Column(INTEGER(10), nullable=False)
    Street = Column(String(255), nullable=False)
    StreetLine1 = Column(String(255), nullable=False)
    StreetLine2 = Column(String(255), nullable=False)
    StreetLine3 = Column(String(255), nullable=False)
    StreetLine4 = Column(String(255), nullable=False)
    City = Column(String(255), nullable=False)
    State = Column(String(255), nullable=False)
    Postcode = Column(String(10), nullable=False)
    Country = Column(String(255), nullable=False)
    Phone1 = Column(String(21), nullable=False)
    Phone2 = Column(String(21), nullable=False)
    Phone3 = Column(String(21), nullable=False)
    Fax = Column(String(21), nullable=False)
    Email = Column(String(255), nullable=False)
    Salutation = Column(String(15), nullable=False)
    ContactName = Column(String(25), nullable=False)
    WWW = Column(String(255), nullable=False, server_default=text("''"))
    ChangeControl = Column(String(5), nullable=False)


class MYOBIBUILDCOMPONENT(Base):
    __tablename__ = 'MYOBI_BUILDCOMPONENTS'

    BuildComponentID = Column(INTEGER(10), primary_key=True)
    BuiltItemID = Column(INTEGER(10), nullable=False, index=True)
    SequenceNumber = Column(INTEGER(10), nullable=False)
    ComponentID = Column(String(50), nullable=False, index=True)
    QuantityNeeded = Column(DECIMAL(14, 3), nullable=False)


class MYOBIBUILTITEM(Base):
    __tablename__ = 'MYOBI_BUILTITEMS'

    BuiltItemID = Column(INTEGER(10), primary_key=True)
    ItemID = Column(INTEGER(10), nullable=False, index=True)
    QuantityBuilt = Column(DECIMAL(11, 3), nullable=False)


class MYOBICUSTOMERPAYMENTLINE(Base):
    __tablename__ = 'MYOBI_CUSTOMERPAYMENTLINES'

    CustomerPaymentLineID = Column(INTEGER(10), primary_key=True)
    CustomerPaymentID = Column(INTEGER(10), nullable=False, index=True)
    LineNumber = Column(INTEGER(10), nullable=False)
    SaleID = Column(INTEGER(10), nullable=False, index=True)
    AmountApplied = Column(DECIMAL(15, 2), nullable=False)


class MYOBICUSTOMERPAYMENT(Base):
    __tablename__ = 'MYOBI_CUSTOMERPAYMENTS'

    CustomerPaymentID = Column(INTEGER(10), primary_key=True)
    CustomerPaymentNumber = Column(String(10), nullable=False, index=True)
    TransactionDate = Column(Date, nullable=False)
    MethodOfPaymentID = Column(INTEGER(10), nullable=False, index=True)
    TotalCustomerPayment = Column(DECIMAL(13, 2), nullable=False)


class MYOBICUSTOMER(Base):
    __tablename__ = 'MYOBI_CUSTOMERS'

    CustomerID = Column(INTEGER(10), primary_key=True)
    CardRecordID = Column(INTEGER(10), nullable=False, index=True)
    CardIdentification = Column(String(20), nullable=False, index=True)
    Name = Column(String(52), nullable=False)
    LastName = Column(String(52), nullable=False)
    FirstName = Column(String(20), nullable=False)
    IsInactive = Column(String(1), nullable=False)
    ABN = Column(String(14), nullable=False)
    ABNBranch = Column(String(11), nullable=False)
    PriceLevelID = Column(String(3), nullable=False, index=True)
    TermsID = Column(INTEGER(10), nullable=False, index=True)
    TaxCodeID = Column(INTEGER(10), nullable=False, index=True)
    FreightTaxCodeID = Column(INTEGER(10), nullable=False, index=True)
    CreditLimit = Column(DECIMAL(9, 2), nullable=False)
    OnHold = Column(String(1), nullable=False)
    VolumeDiscount = Column(DECIMAL(5, 3), nullable=False)
    CurrentBalance = Column(DECIMAL(15, 2), nullable=False)
    MethodOfPaymentID = Column(INTEGER(10), nullable=False, index=True)
    IncomeAccountID = Column(INTEGER(10), nullable=False, index=True)
    SalespersonID = Column(INTEGER(10), nullable=False, index=True)
    ShippingMethodID = Column(INTEGER(10), nullable=False, index=True)
    CustomList1ID = Column(INTEGER(10), nullable=False, index=True)
    CustomField1 = Column(String(255), nullable=False)
    CustomList2ID = Column(INTEGER(10), nullable=False, index=True)
    CustomField2 = Column(String(255), nullable=False)
    CustomList3ID = Column(INTEGER(10), nullable=False, index=True)
    CustomField3 = Column(String(255), nullable=False)
    PrintedForm = Column(String(34), nullable=False)
    ChangeControl = Column(String(15), nullable=False)


class MYOBICUSTOMLIST(Base):
    __tablename__ = 'MYOBI_CUSTOMLISTS'

    CustomListID = Column(INTEGER(10), primary_key=True)
    CustomListText = Column(String(30), nullable=False)
    CustomListType = Column(String(10), nullable=False)
    ChangeControl = Column(String(5), nullable=False)


class MYOBIEMPLOYEE(Base):
    __tablename__ = 'MYOBI_EMPLOYEES'

    EmployeeID = Column(INTEGER(10), primary_key=True)
    CardRecordID = Column(INTEGER(10), nullable=False, index=True)
    CardIdentification = Column(String(20), nullable=False, index=True)
    Name = Column(String(52), nullable=False)
    LastName = Column(String(52), nullable=False)
    FirstName = Column(String(20), nullable=False)
    IsInactive = Column(String(1), nullable=False)
    CustomList1ID = Column(INTEGER(10), nullable=False, index=True)
    CustomList2ID = Column(INTEGER(10), nullable=False, index=True)
    CustomList3ID = Column(INTEGER(10), nullable=False, index=True)
    ChangeControl = Column(String(15), nullable=False)


class MYOBIITEMLOCATION(Base):
    __tablename__ = 'MYOBI_ITEMLOCATIONS'

    ItemLocationID = Column(INTEGER(10), primary_key=True)
    ItemID = Column(INTEGER(10), nullable=False, index=True)
    LocationID = Column(INTEGER(10), nullable=False, index=True)
    QuantityOnHand = Column(DECIMAL(15, 3), nullable=False)


class MYOBIITEMPRICE(Base):
    __tablename__ = 'MYOBI_ITEMPRICES'

    ItemPriceID = Column(INTEGER(10), primary_key=True)
    ItemID = Column(INTEGER(10), nullable=False, index=True)
    QuantityBreak = Column(INTEGER(10), nullable=False)
    QuantityBreakAmount = Column(DECIMAL(13, 3), nullable=False)
    PriceLevel = Column(CHAR(1), nullable=False)
    PriceLevelNameID = Column(CHAR(3), nullable=False, index=True)
    SellingPrice = Column(DECIMAL(15, 4), nullable=False)
    PriceIsInclusive = Column(CHAR(1), nullable=False)
    ChangeControl = Column(String(5), nullable=False)


class MYOBIITEMPURCHASELINE(Base):
    __tablename__ = 'MYOBI_ITEMPURCHASELINES'

    ItemPurchaseLineID = Column(INTEGER(10), nullable=False, index=True)
    PurchaseID = Column(INTEGER(10), primary_key=True, nullable=False)
    LineNumber = Column(INTEGER(10), primary_key=True, nullable=False)
    LineTypeID = Column(CHAR(1), nullable=False)
    ItemID = Column(INTEGER(10), nullable=False, index=True)
    Description = Column(String(255), nullable=False)
    Quantity = Column(DECIMAL(11, 3), nullable=False)
    TaxExclusiveUnitPrice = Column(DECIMAL(15, 4), nullable=False)
    TaxInclusiveUnitPrice = Column(DECIMAL(15, 4), nullable=False)
    TaxExclusiveTotal = Column(DECIMAL(15, 2), nullable=False)
    TaxInclusiveTotal = Column(DECIMAL(15, 2), nullable=False)
    Discount = Column(DECIMAL(11, 2), nullable=False)
    IsMultipleJob = Column(CHAR(1), nullable=False)
    JobID = Column(INTEGER(10), nullable=False)
    TaxBasisAmount = Column(DECIMAL(15, 2), nullable=False)
    TaxBasisAmountIsInclusive = Column(CHAR(1), nullable=False)
    TaxCodeID = Column(INTEGER(10), nullable=False, index=True)
    Received = Column(DECIMAL(11, 3), nullable=False)
    LocationID = Column(INTEGER(10), nullable=False, index=True)


class MYOBIITEM(Base):
    __tablename__ = 'MYOBI_ITEMS'

    ItemID = Column(INTEGER(10), primary_key=True)
    IsInactive = Column(CHAR(1), nullable=False)
    ItemName = Column(String(30), nullable=False)
    ItemNumber = Column(String(30), nullable=False, index=True)
    QuantityOnHand = Column(DECIMAL(15, 3), nullable=False)
    PositiveAverageCost = Column(DECIMAL(23, 2), nullable=False)
    PurchaseOnOrder = Column(DECIMAL(15, 3), nullable=False)
    NegativeQuantityOnHand = Column(DECIMAL(15, 3), nullable=False)
    NegativeAverageCost = Column(DECIMAL(23, 2), nullable=False)
    ItemIsInventoried = Column(CHAR(1), nullable=False)
    IncomeAccountID = Column(INTEGER(10), nullable=False, index=True)
    ExpenseAccountID = Column(INTEGER(10), nullable=False, index=True)
    InventoryAccountID = Column(INTEGER(10), nullable=False, index=True)
    ItemDescription = Column(String(255), nullable=False)
    CustomList1ID = Column(INTEGER(10), nullable=False, index=True)
    CustomList2ID = Column(INTEGER(10), nullable=False, index=True)
    CustomList3ID = Column(INTEGER(10), nullable=False, index=True)
    CustomField1 = Column(String(30), nullable=False)
    CustomField2 = Column(String(30), nullable=False)
    CustomField3 = Column(String(30), nullable=False)
    BaseSellingPrice = Column(DECIMAL(15, 4), nullable=False)
    PriceIsInclusive = Column(CHAR(1), nullable=False)
    SellTaxCodeID = Column(INTEGER(10), nullable=False, index=True)
    SellUnitMeasure = Column(String(5), nullable=False)
    SellUnitQuantity = Column(INTEGER(10), nullable=False)
    TaxInclusiveStandardCost = Column(DECIMAL(15, 4), nullable=False)
    TaxExclusiveStandardCost = Column(DECIMAL(15, 4), nullable=False)
    BuyTaxCodeID = Column(INTEGER(10), nullable=False, index=True)
    BuyUnitMeasure = Column(String(5), nullable=False)
    BuyUnitQuantity = Column(INTEGER(10), nullable=False)
    PrimarySupplierID = Column(INTEGER(10), nullable=False, index=True)
    SupplierItemNumber = Column(String(30), nullable=False)
    MinLevelBeforeReorder = Column(DECIMAL(10, 3), nullable=False)
    DefaultReorderQuantity = Column(DECIMAL(10, 3), nullable=False)
    DefaultSellLocationID = Column(INTEGER(10), nullable=False, index=True)
    DefaultReceiveLocationID = Column(INTEGER(10), nullable=False, index=True)
    ChangeControl = Column(String(10), nullable=False)


class MYOBILOCATION(Base):
    __tablename__ = 'MYOBI_LOCATIONS'

    LocationID = Column(INTEGER(10), primary_key=True)
    IsInactive = Column(CHAR(1), nullable=False)
    CanBeSold = Column(CHAR(1), nullable=False)
    LocationIdentification = Column(String(10), nullable=False)
    LocationName = Column(String(30), nullable=False)
    Street = Column(String(255), nullable=False)
    City = Column(String(255), nullable=False)
    State = Column(String(255), nullable=False)
    Postcode = Column(String(11), nullable=False)
    Country = Column(String(255), nullable=False)
    Contact = Column(String(255), nullable=False)
    ContactPhone = Column(String(21), nullable=False)
    Notes = Column(String(255), nullable=False)


class MYOBIPAYMENTMETHOD(Base):
    __tablename__ = 'MYOBI_PAYMENTMETHODS'

    PaymentMethodID = Column(INTEGER(10), primary_key=True)
    PaymentMethod = Column(String(255), nullable=False)
    MethodType = Column(String(11), nullable=False)


class MYOBIPRICELEVEL(Base):
    __tablename__ = 'MYOBI_PRICELEVELS'

    PriceLevelID = Column(CHAR(3), primary_key=True)
    Description = Column(String(30), nullable=False)
    ImportPriceLevel = Column(CHAR(1), nullable=False)


class MYOBIPURCHASE(Base):
    __tablename__ = 'MYOBI_PURCHASES'

    PurchaseID = Column(INTEGER(10), primary_key=True)
    CardRecordID = Column(INTEGER(10), nullable=False, index=True)
    PurchaseNumber = Column(String(8), nullable=False, index=True)
    SupplierInvoiceNumber = Column(String(20), nullable=False)
    BackorderPurchaseID = Column(INTEGER(10), nullable=False)
    PurchaseDate = Column(Date, nullable=False)
    ShipToAddress = Column(String(255), nullable=False)
    ShipToAddressLine1 = Column(String(255), nullable=False)
    ShipToAddressLine2 = Column(String(255), nullable=False)
    ShipToAddressLine3 = Column(String(255), nullable=False)
    ShipToAddressLine4 = Column(String(255), nullable=False)
    PurchaseTypeID = Column(CHAR(1), nullable=False)
    PurchaseStatusID = Column(CHAR(2), nullable=False)
    OrderStatusID = Column(CHAR(2), nullable=False)
    ReversalLinkID = Column(INTEGER(10), nullable=False)
    TermsID = Column(INTEGER(10), nullable=False, index=True)
    TotalLines = Column(DECIMAL(15, 2), nullable=False)
    TaxExclusiveFreight = Column(DECIMAL(15, 2), nullable=False)
    TaxInclusiveFreight = Column(DECIMAL(15, 2), nullable=False)
    FreightTaxCodeID = Column(INTEGER(10), nullable=False, index=True)
    TotalTax = Column(DECIMAL(15, 2), nullable=False)
    TotalPaid = Column(DECIMAL(15, 2), nullable=False)
    TotalDeposits = Column(DECIMAL(15, 2), nullable=False)
    TotalDebits = Column(DECIMAL(15, 2), nullable=False)
    TotalDiscounts = Column(DECIMAL(15, 2), nullable=False)
    OutstandingBalance = Column(DECIMAL(15, 2), nullable=False)
    Memo = Column(String(255), nullable=False)
    Comment = Column(String(255), nullable=False)
    ShippingMethodID = Column(INTEGER(10), nullable=False)
    PromisedDate = Column(Date, nullable=False)
    CurrencyID = Column(INTEGER(10), nullable=False)
    IsTaxInclusive = Column(CHAR(1), nullable=False)
    TransactionExchangeRate = Column(DECIMAL(12, 6), nullable=False)


class MYOBISALE(Base):
    __tablename__ = 'MYOBI_SALES'

    SaleID = Column(INTEGER(10), primary_key=True)
    InvoiceNumber = Column(String(8), nullable=False, index=True)
    InvoiceDate = Column(Date, nullable=False)
    TotalLines = Column(DECIMAL(15, 2), nullable=False)
    TaxExclusiveFreight = Column(DECIMAL(15, 2), nullable=False)
    TotalTax = Column(DECIMAL(15, 2), nullable=False)
    TotalDiscounts = Column(DECIMAL(15, 2), nullable=False)


class MYOBISHIPPINGMETHOD(Base):
    __tablename__ = 'MYOBI_SHIPPINGMETHODS'

    ShippingMethodID = Column(INTEGER(10), primary_key=True)
    ShippingMethod = Column(String(255), nullable=False)


class MYOBISUPPLIER(Base):
    __tablename__ = 'MYOBI_SUPPLIERS'

    SupplierID = Column(INTEGER(10), primary_key=True)
    CardRecordID = Column(INTEGER(10), nullable=False, index=True)
    CardIdentification = Column(String(20), nullable=False, index=True)
    Name = Column(String(52), nullable=False)
    LastName = Column(String(52), nullable=False)
    FirstName = Column(String(20), nullable=False)
    IsInactive = Column(CHAR(1), nullable=False)
    Notes = Column(String(255), nullable=False)
    CustomList1ID = Column(INTEGER(10), nullable=False, index=True)
    CustomList2ID = Column(INTEGER(10), nullable=False, index=True)
    CustomList3ID = Column(INTEGER(10), nullable=False, index=True)
    CustomField1 = Column(String(255), nullable=False)
    CustomField2 = Column(String(255), nullable=False)
    CustomField3 = Column(String(255), nullable=False)
    TermsID = Column(INTEGER(10), nullable=False, index=True)
    ABN = Column(String(14), nullable=False)
    ABNBranch = Column(String(11), nullable=False)
    TaxCodeID = Column(INTEGER(10), nullable=False, index=True)
    FreightTaxCodeID = Column(INTEGER(10), nullable=False, index=True)
    MethodOfPaymentID = Column(INTEGER(10), nullable=False, index=True)
    ExpenseAccountID = Column(INTEGER(10), nullable=False, index=True)
    ShippingMethodID = Column(INTEGER(10), nullable=False, index=True)
    ChangeControl = Column(String(15), nullable=False)


class MYOBITAXCODE(Base):
    __tablename__ = 'MYOBI_TAXCODES'

    TaxCodeID = Column(INTEGER(10), primary_key=True)
    TaxCode = Column(String(3), nullable=False)
    TaxCodeDescription = Column(String(30), nullable=False)
    TaxPercentageRate = Column(DECIMAL(8, 4), nullable=False)
    TaxThreshold = Column(DECIMAL(13, 2), nullable=False)
    TaxCodeTypeID = Column(String(3), nullable=False, index=True)
    TaxCollectedAccountID = Column(INTEGER(10), nullable=False, index=True)
    TaxPaidAccountID = Column(INTEGER(10), nullable=False, index=True)


class MYOBITERM(Base):
    __tablename__ = 'MYOBI_TERMS'

    TermsID = Column(INTEGER(10), primary_key=True)
    LatePaymentChargePercent = Column(DECIMAL(4, 2), nullable=False)
    EarlyPaymentDiscountPercent = Column(DECIMAL(4, 2), nullable=False)
    TermsOfPaymentID = Column(String(4), nullable=False, index=True)
    DiscountDays = Column(INTEGER(10), nullable=False)
    BalanceDueDays = Column(INTEGER(10), nullable=False)
    ImportPaymentIsDue = Column(INTEGER(10), nullable=False)
    DiscountDate = Column(String(3), nullable=False)
    BalanceDueDate = Column(String(3), nullable=False)


class MYOBQUEUE(Base):
    __tablename__ = 'MYOBQUEUE'

    queueid = Column(INTEGER(10), primary_key=True)
    taskid = Column(INTEGER(10), nullable=False, index=True)
    proc_data = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    date_init = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    start_time = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    sortby = Column(TINYINT(3), nullable=False, server_default=text("'0'"))


class MYOBSCHEDULE(Base):
    __tablename__ = 'MYOBSCHEDULE'

    schedule_id = Column(INTEGER(10), primary_key=True)
    taskid = Column(INTEGER(10), nullable=False, index=True)
    proc_data = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    schedule_dow = Column(String(5), nullable=False, server_default=text("''"))
    schedule_hr = Column(String(5), nullable=False, server_default=text("''"))
    schedule_min = Column(String(5), nullable=False, server_default=text("''"))
    date_scheduled = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))


class MYOBSCHEMA(Base):
    __tablename__ = 'MYOBSCHEMA'

    taskid = Column(INTEGER(10), primary_key=True)
    task_name = Column(String(25), nullable=False, server_default=text("''"))
    task_type = Column(Enum('Import', 'Export'), nullable=False, server_default=text("'Import'"))
    acc_software = Column(Enum('MYOB', 'EXONET'), nullable=False, index=True, server_default=text("'MYOB'"))
    upd_method = Column(Enum('x', 'a', 'u'), nullable=False, server_default=text("'x'"))
    site_tbl = Column(String(50), nullable=False, server_default=text("''"))
    site_cols = Column(Text, nullable=False)
    myob_sql = Column(Text, nullable=False)
    myob_countsql = Column(Text, nullable=False)
    check_tbl = Column(String(25), nullable=False)
    check_cols = Column(Text, nullable=False)
    export_precheck = Column(Text, nullable=False)
    export_postcheck = Column(Text, nullable=False)
    processor = Column(String(25), server_default=text("''"))
    description = Column(String(255), nullable=False, server_default=text("''"))
    active = Column(Enum('y', 'n'), nullable=False, server_default=text("'n'"))
    child_proc = Column(Enum('y', 'n'), nullable=False, server_default=text("'n'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class MYOBSCHEMACHILD(Base):
    __tablename__ = 'MYOBSCHEMACHILDS'

    taskid = Column(INTEGER(10), primary_key=True, nullable=False)
    childid = Column(INTEGER(10), primary_key=True, nullable=False)
    task_key = Column(String(30), primary_key=True, nullable=False)
    child_key = Column(String(30), nullable=False)
    always = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class MYOBTRAN(Base):
    __tablename__ = 'MYOBTRANS'

    transid = Column(INTEGER(10), primary_key=True)
    taskid = Column(INTEGER(10), nullable=False)
    proc_data = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    token = Column(CHAR(20), nullable=False)
    transtkn = Column(CHAR(5), nullable=False)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class NDESIGNTHEME(Base):
    __tablename__ = 'NDESIGN_THEME'

    theme_id = Column(String(80), primary_key=True)
    theme_type = Column(Enum('website', 'facebook', 'mobile', 'ebay'), nullable=False, server_default=text("'website'"))
    theme_name = Column(String(100), nullable=False, server_default=text("''"))
    theme_description = Column(Text, nullable=False)
    theme_install_description = Column(Text, nullable=False)
    theme_version = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    zip_mtime = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    date_updated = Column(TIMESTAMP, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_created = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class NETOREPORT(Base):
    __tablename__ = 'NETOREPORTS'

    report_module = Column(String(25), primary_key=True)
    report_name = Column(String(50), nullable=False, server_default=text("''"))
    report_description = Column(String(255), nullable=False, server_default=text("''"))
    report_data = Column(Text, nullable=False)
    report_start = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    report_end = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    last_start = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    last_end = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    last_time = Column(Time, nullable=False, server_default=text("'00:00:00'"))


class NETOTAG(Base):
    __tablename__ = 'NETOTAGS'
    __table_args__ = (
        Index('tag_link', 'tag_link', 'tag_type'),
    )

    name = Column(String(50), primary_key=True, server_default=text("''"))
    tag_type = Column(Enum('config', 'function'), nullable=False, server_default=text("'config'"))
    tag_link = Column(String(50), nullable=False)
    tag_active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    system_tag = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    preload = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class NETOWSLOG(Base):
    __tablename__ = 'NETO_WS_LOG'

    id = Column(INTEGER(10), primary_key=True)
    module = Column(String(25), nullable=False, index=True)
    action = Column(String(50), nullable=False)
    msg_type = Column(Enum('info', 'warn', 'error'), nullable=False, server_default=text("'info'"))
    msg = Column(Text, nullable=False)
    remote_ip = Column(String(50), nullable=False)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class ORDERADMIN(Base):
    __tablename__ = 'ORDERADMINS'

    order_id = Column(String(15), primary_key=True, nullable=False)
    function = Column(Enum('sales', 'warehouse'), primary_key=True, nullable=False)
    admin = Column(String(25), nullable=False, index=True)
    lock_code = Column(CHAR(8))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class ORDERCOUNTER(Base):
    __tablename__ = 'ORDERCOUNTER'

    counter = Column(INTEGER(11), primary_key=True)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class ORDERDSADMIN(Base):
    __tablename__ = 'ORDERDSADMINS'

    order_id = Column(String(15), primary_key=True, nullable=False)
    dropshipper = Column(String(25), primary_key=True, nullable=False, index=True)
    dssec_id = Column(INTEGER(10), nullable=False, index=True)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class ORDERIMPORTCACH(Base):
    __tablename__ = 'ORDERIMPORT_CACHES'

    cache_id = Column(INTEGER(10), primary_key=True)
    cache_key = Column(String(255), nullable=False, index=True)
    cache_order = Column(MEDIUMTEXT, nullable=False)
    cache_line = Column(MEDIUMTEXT, nullable=False)
    cache_payment = Column(MEDIUMTEXT, nullable=False)
    recordnum = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    priority = Column(TINYINT(3), nullable=False, index=True, server_default=text("'0'"))


class ORDERLINE(Base):
    __tablename__ = 'ORDERLINES'
    __table_args__ = (
        Index('counter', 'order_id', 'counter', unique=True),
    )

    orderline_id = Column(INTEGER(10), primary_key=True)
    order_id = Column(String(15), nullable=False)
    counter = Column(TINYINT(4), nullable=False, server_default=text("'0'"))
    SKU = Column(String(25), index=True)
    quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    pk_quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    bko_quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    ship_quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    iskitting = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    description = Column(String(255))
    itm_serial = Column(String(50), nullable=False, index=True, server_default=text("''"))
    picker = Column(String(25), nullable=False, index=True, server_default=text("''"))
    upd_status = Column(Enum('Picked', 'A', 'B', 'C', 'New'), nullable=False, server_default=text("'New'"))
    supplier = Column(String(50), index=True)
    dropshipper = Column(String(25), nullable=False, index=True)
    warehouse_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    extra = Column(MEDIUMTEXT, nullable=False)
    unit_price = Column(DECIMAL(20, 8), nullable=False, server_default=text("'0.00000000'"))
    unit_cost = Column(DECIMAL(20, 8), nullable=False, server_default=text("'0.00000000'"))
    discount_percent = Column(DECIMAL(5, 2), nullable=False, server_default=text("'0.00'"))
    product_discount = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    coupon_applied = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    shipping_insurance = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    notax = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    tax_category = Column(String(255))
    tax_rate = Column(DECIMAL(8, 5), server_default=text("'0.00000'"))
    shipping = Column(DECIMAL(10, 4), nullable=False, server_default=text("'0.0000'"))
    cubic = Column(DECIMAL(15, 9), nullable=False, server_default=text("'0.000000000'"))
    restock_date = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    date_delivery = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    aff_id = Column(String(25), nullable=False, index=True)
    aff_ref = Column(String(50), nullable=False)
    external_order_ref = Column(String(50), nullable=False, index=True)
    ebtrans_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    job_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    bin_loc = Column(String(10), nullable=False)
    tracking_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    proc_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    exbatch_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    wms_exported = Column(DateTime, nullable=False, index=True, server_default=text("'0000-00-00 00:00:00'"))
    itemnotes = Column(String(255))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class ORDERLINESPLIT(Base):
    __tablename__ = 'ORDERLINESPLITS'
    __table_args__ = (
        Index('order_id', 'order_id', 'SKU', 'counter', unique=True),
    )

    id = Column(INTEGER(11), primary_key=True)
    order_id = Column(String(15), nullable=False)
    counter = Column(TINYINT(4), nullable=False)
    SKU = Column(String(25), nullable=False, index=True)
    description = Column(String(255), nullable=False)
    quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    pk_quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    bko_quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    ship_quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    assemble_quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    assemble_subtotal = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    assemble_notax = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    total_cal = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    shipping = Column(DECIMAL(10, 4), nullable=False, server_default=text("'0.0000'"))
    cubic = Column(DECIMAL(15, 9), nullable=False, server_default=text("'0.000000000'"))
    itm_price = Column(DECIMAL(12, 2))
    unit_cost = Column(DECIMAL(20, 8))


class ORDERNOTE(Base):
    __tablename__ = 'ORDERNOTES'

    onote_id = Column(INTEGER(10), primary_key=True)
    order_id = Column(String(15), nullable=False, index=True)
    admin = Column(String(25), nullable=False, server_default=text("''"))
    dropshipper = Column(String(25), nullable=False, server_default=text("''"))
    onote_title = Column(String(150), nullable=False, server_default=text("''"))
    onote_comment = Column(MEDIUMTEXT, nullable=False)
    timestamp = Column(TIMESTAMP, nullable=False, index=True, server_default=text("CURRENT_TIMESTAMP"))


class ORDERPAYMENT(Base):
    __tablename__ = 'ORDERPAYMENTS'
    __table_args__ = (
        Index('dateexported_creditid_cardid_idx', 'date_exported', 'credit_to_id', 'card_id'),
    )

    payment_id = Column(INTEGER(10), primary_key=True)
    order_id = Column(String(15), nullable=False, index=True)
    rma_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    admin = Column(String(25), nullable=False, index=True)
    amount = Column(DECIMAL(12, 2), nullable=False)
    currency_code = Column(String(3), nullable=False, index=True, server_default=text("''"))
    date_paid = Column(DateTime, nullable=False)
    card_id = Column(INTEGER(10), nullable=False, index=True)
    notes = Column(MEDIUMTEXT, nullable=False)
    realtime_notes = Column(String(50), nullable=False)
    card_holder = Column(String(50))
    card_number = Column(String(30))
    card_exp_month = Column(TINYINT(3))
    card_exp_year = Column(INTEGER(10))
    card_authorisation = Column(String(100), nullable=False)
    acc_name = Column(String(50), nullable=False)
    acc_bsb = Column(String(10), nullable=False)
    acc_number = Column(String(20), nullable=False)
    cheque_number = Column(String(20), nullable=False)
    date_exported = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    parent_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    credit_to_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    rec_payment_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    exbatch_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    preauth_id = Column(INTEGER(10), nullable=False, index=True)


class ORDERPAYMENTSREC(Base):
    __tablename__ = 'ORDERPAYMENTS_REC'

    order_id = Column(String(15), primary_key=True)
    rec_payment_id = Column(INTEGER(10), nullable=False, index=True)
    admin = Column(String(25), nullable=False, index=True)
    upfont_paid = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    amount_paid = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    count_paid = Column(TINYINT(3), nullable=False, server_default=text("'0'"))
    last_paid = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_next = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    card_id = Column(INTEGER(10), nullable=False, index=True)
    cc_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    card_holder = Column(String(50))
    card_number = Column(String(30))
    card_exp_month = Column(TINYINT(3))
    card_exp_year = Column(INTEGER(10))
    card_client_id = Column(String(20))
    card_rec_type = Column(Enum('once', 'day', 'week', 'month', 'year'), nullable=False, server_default=text("'once'"))
    card_rec_every = Column(TINYINT(3), nullable=False, server_default=text("'1'"))
    card_rec_day = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    card_rec_div = Column(TINYINT(3), nullable=False, server_default=text("'1'"))
    date_created = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class ORDERPAYMENTSRECLOG(Base):
    __tablename__ = 'ORDERPAYMENTS_REC_LOGS'

    log_id = Column(INTEGER(10), primary_key=True)
    order_id = Column(String(15), nullable=False, index=True)
    amount = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    currency_code = Column(String(3), nullable=False, server_default=text("''"))
    card_id = Column(INTEGER(10), nullable=False, index=True)
    status = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    notes = Column(MEDIUMTEXT, nullable=False)
    realtime_notes = Column(String(50), nullable=False, server_default=text("''"))
    card_holder = Column(String(50), nullable=False, server_default=text("''"))
    card_number = Column(String(30), nullable=False, server_default=text("''"))
    card_exp_month = Column(TINYINT(3), nullable=False, server_default=text("'0'"))
    card_exp_year = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    card_authorisation = Column(String(100), nullable=False, server_default=text("''"))
    card_client_id = Column(String(20), nullable=False, server_default=text("''"))
    rec_payment_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class ORDERPAYMENTSESSION(Base):
    __tablename__ = 'ORDERPAYMENT_SESSIONS'

    session_id = Column(INTEGER(10), primary_key=True)
    order_id = Column(String(15), nullable=False, index=True)
    hosted_agent = Column(String(50), nullable=False)
    session_code = Column(String(20), nullable=False, index=True)
    payment_param = Column(String(255), nullable=False)
    amount = Column(DECIMAL(12, 2), nullable=False)
    date_created = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_paid = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_confirmed = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))


class ORDERPLAN(Base):
    __tablename__ = 'ORDERPLANS'

    order_plan_id = Column(INTEGER(10), primary_key=True)
    order_plan_type = Column(Enum('sales', 'rental'), nullable=False, index=True, server_default=text("'sales'"))
    plan_name = Column(String(50), nullable=False, server_default=text("''"))
    plan_desc = Column(String(255), nullable=False)
    active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    discount_percent = Column(DECIMAL(5, 2), nullable=False, server_default=text("'0.00'"))
    repeat_pay_only = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    item_editable = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    fix_shipping = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    next_type = Column(Enum('', 'day', 'week', 'month', 'year'), nullable=False, server_default=text("''"))
    next_day = Column(INTEGER(10))
    rec_type = Column(Enum('once', 'day', 'week', 'month', 'year'), nullable=False, server_default=text("'once'"))
    rec_every = Column(TINYINT(3), nullable=False, server_default=text("'1'"))
    rec_day = Column(INTEGER(10))
    ff_amount = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    ff_count = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    ff_per = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ff_date_type = Column(Enum('', 'day', 'week', 'month', 'year'), nullable=False, server_default=text("''"))
    ff_date_every = Column(TINYINT(3), nullable=False, server_default=text("'1'"))
    ff_date_day = Column(INTEGER(10))
    auto_complete = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    card_rec_type = Column(Enum('once', 'day', 'week', 'month', 'year'), nullable=False, server_default=text("'once'"))
    card_rec_every = Column(TINYINT(3), nullable=False, server_default=text("'1'"))
    card_rec_day = Column(INTEGER(10))
    card_rec_div = Column(TINYINT(3), nullable=False, server_default=text("'1'"))
    card_rec_upfont = Column(DECIMAL(5, 2), nullable=False, server_default=text("'0.00'"))
    sortorder = Column(TINYINT(3), nullable=False, index=True, server_default=text("'0'"))


class ORDERPROC(Base):
    __tablename__ = 'ORDERPROC'

    proc_id = Column(INTEGER(10), primary_key=True)
    username = Column(String(25), nullable=False, index=True)
    dssec_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    task = Column(Enum('pick', 'pack', 'tracking'), nullable=False)
    proc_deleted = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    proc_pnp = Column(Enum('n', 'y'), nullable=False)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class ORDERPROCLINE(Base):
    __tablename__ = 'ORDERPROCLINES'

    proc_id = Column(INTEGER(10), primary_key=True, nullable=False)
    order_id = Column(String(15), primary_key=True, nullable=False, index=True)
    counter = Column(TINYINT(3), primary_key=True, nullable=False, index=True)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class ORDER(Base):
    __tablename__ = 'ORDERS'

    order_record_id = Column(INTEGER(10), primary_key=True)
    order_id = Column(String(15), nullable=False, unique=True)
    parent_order_id = Column(String(15), nullable=False, index=True)
    customer_po = Column(String(50), nullable=False, server_default=text("''"))
    order_status_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    order_type = Column(Enum('sales', 'dropshipping', 'quote'), nullable=False, server_default=text("'sales'"))
    allow_pack = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    quote_id = Column(String(15), nullable=False, index=True, server_default=text("''"))
    rma_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    storder_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    proposal_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    usr_group = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    order_plan_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    doctmpl_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    date_updated = Column(DateTime, nullable=False)
    date_created = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    date_placed = Column(DateTime, nullable=False, index=True)
    date_required = Column(DateTime, nullable=False, index=True, server_default=text("'0000-00-00 00:00:00'"))
    date_paid = Column(DateTime, nullable=False, index=True, server_default=text("'0000-00-00 00:00:00'"))
    date_fully_paid = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    date_invoiced = Column(Date, nullable=False, index=True, server_default=text("'0000-00-00'"))
    date_exported = Column(DateTime, nullable=False, index=True, server_default=text("'0000-00-00 00:00:00'"))
    date_due = Column(DateTime, nullable=False, index=True, server_default=text("'0000-00-00 00:00:00'"))
    username = Column(String(25), nullable=False, index=True, server_default=text("''"))
    email = Column(String(50))
    bill_first_name = Column(String(50))
    bill_last_name = Column(String(50))
    bill_company = Column(String(50))
    bill_street1 = Column(String(50))
    bill_street2 = Column(String(50))
    bill_city = Column(String(50))
    bill_state = Column(String(50))
    bill_zip = Column(String(15))
    bill_country = Column(String(50))
    phone = Column(String(30), index=True)
    fax = Column(String(30))
    ship_email = Column(String(50), nullable=False, server_default=text("''"))
    ship_first_name = Column(String(50))
    ship_last_name = Column(String(50))
    ship_company = Column(String(50))
    ship_street1 = Column(String(50))
    ship_street2 = Column(String(50))
    ship_city = Column(String(50))
    ship_state = Column(String(50))
    ship_zip = Column(String(15))
    ship_country = Column(String(50))
    ship_phone = Column(String(30))
    ship_fax = Column(String(30), nullable=False, server_default=text("''"))
    card_type = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    cc_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    preauth_id = Column(INTEGER(10), nullable=False, index=True)
    bpay_crn = Column(String(20))
    realtime_confirmation = Column(String(50))
    terms = Column(String(25), nullable=False, server_default=text("''"))
    credit_log = Column(MEDIUMTEXT, nullable=False)
    status_log = Column(MEDIUMTEXT, nullable=False)
    sh_group_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    shipping_method = Column(String(50), nullable=False, server_default=text("''"))
    ship_signature = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    cost = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    subtotal = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    shipping_cost = Column(DECIMAL(8, 2), nullable=False, server_default=text("'0.00'"))
    shipping_notax = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    tax = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    tax_rate = Column(DECIMAL(8, 5), server_default=text("'0.00000'"))
    shipping_tax = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    shipping_tax_rate = Column(DECIMAL(8, 5), server_default=text("'0.00000'"))
    tax_inc = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    shipping_discount = Column(DECIMAL(8, 2), nullable=False, server_default=text("'0.00'"))
    coupondiscount = Column(DECIMAL(8, 2), nullable=False, server_default=text("'0.00'"))
    surcharge_total = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    surcharge_taxable_amt = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    surcharge_percent = Column(DECIMAL(5, 2), nullable=False, server_default=text("'0.00'"))
    grand_total = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    currency_code = Column(String(3), nullable=False, index=True, server_default=text("''"))
    amount_paid = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    couponcode = Column(MEDIUMTEXT)
    special = Column(MEDIUMTEXT)
    onhold_type = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    notes = Column(MEDIUMTEXT)
    internal_notes = Column(MEDIUMTEXT, nullable=False)
    operator = Column(String(25), nullable=False, index=True, server_default=text("''"))
    salesperson = Column(String(25), nullable=False, index=True, server_default=text("''"))
    approval_username = Column(String(25), nullable=False, index=True, server_default=text("''"))
    customer_ref1 = Column(String(50), nullable=False, server_default=text("''"))
    customer_ref2 = Column(String(50), nullable=False, server_default=text("''"))
    customer_ref3 = Column(String(1000), nullable=False, server_default=text("''"))
    customer_ref4 = Column(String(1000), nullable=False, server_default=text("''"))
    customer_ref5 = Column(String(1000), nullable=False, server_default=text("''"))
    customer_ref6 = Column(String(1000), nullable=False, server_default=text("''"))
    customer_ref7 = Column(String(1000), nullable=False, server_default=text("''"))
    customer_ref8 = Column(String(1000), nullable=False, server_default=text("''"))
    customer_ref9 = Column(String(1000), nullable=False, server_default=text("''"))
    customer_ref10 = Column(String(1000), nullable=False, server_default=text("''"))
    order_image = Column(String(25), nullable=False)
    trace = Column(MEDIUMTEXT, nullable=False)
    fraud_score = Column(DECIMAL(4, 2), nullable=False, server_default=text("'0.00'"))
    authorisation_status = Column(Enum('', 'authorised', 'unauthorised'), nullable=False, server_default=text("''"))
    ip_address = Column(String(50), nullable=False, server_default=text("''"))
    sales_channel = Column(String(15), nullable=False, index=True, server_default=text("''"))
    timestamp = Column(TIMESTAMP, nullable=False, index=True, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class ORDERSTATU(Base):
    __tablename__ = 'ORDERSTATUS'

    order_status_id = Column(INTEGER(10), primary_key=True)
    order_status_name = Column(String(50), nullable=False, index=True)
    order_status_type = Column(String(10), nullable=False, index=True)
    notification = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    template = Column(String(20), nullable=False)
    orderby = Column(TINYINT(3), nullable=False, server_default=text("'0'"))


class ORDERSTATUSD(Base):
    __tablename__ = 'ORDERSTATUS_DS'

    ds_status_id = Column(INTEGER(10), primary_key=True)
    ds_status_name = Column(String(50), nullable=False, index=True)
    ds_status_type = Column(String(10), nullable=False)
    orderby = Column(TINYINT(3), nullable=False, server_default=text("'0'"))


class ORDERTRACKING(Base):
    __tablename__ = 'ORDERTRACKINGS'

    tracking_id = Column(INTEGER(10), primary_key=True)
    order_id = Column(String(15), nullable=False, index=True)
    dropshipper = Column(String(25), nullable=False, index=True, server_default=text("''"))
    warehouse_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    shipping_ref = Column(INTEGER(10), nullable=False, index=True)
    shserv_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    shacc_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    ship_tracking = Column(String(255), nullable=False, server_default=text("''"))
    ship_method = Column(String(50), nullable=False, server_default=text("''"))
    cngmt_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    date_shipped = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_notified = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))


class ORDERCOLUMN(Base):
    __tablename__ = 'ORDER_COLUMNS'

    col_id = Column(String(25), primary_key=True)
    col_name = Column(String(50), nullable=False)
    col_type = Column(Enum('TEXT', 'DATE', 'INT', 'DECIMAL', 'BOOLEAN', 'SHORTTEXT', 'SELECTIONBOX'), nullable=False)
    required_field = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    restricted = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    col_comment = Column(String(255), nullable=False)
    col_order = Column(TINYINT(3), nullable=False)
    visible_on_checkout = Column(Enum('n', 'y'), nullable=False, index=True, server_default=text("'n'"))


class ORDEREMAILCOUNTER(Base):
    __tablename__ = 'ORDER_EMAILCOUNTER'

    counter = Column(INTEGER(11), primary_key=True)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class ORDERHOSTEDPAYMENT(Base):
    __tablename__ = 'ORDER_HOSTED_PAYMENTS'

    hostpay_id = Column(INTEGER(10), primary_key=True)
    hostpay_code = Column(String(10), nullable=False, server_default=text("''"))
    order_id = Column(String(15), nullable=False, index=True)
    card_id = Column(INTEGER(10), nullable=False, index=True)
    amount_agreed = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    amount_paid = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    currency_code = Column(String(3), nullable=False, server_default=text("''"))
    hostpay_status = Column(Enum('Pending', 'Processing', 'Success', 'Failed'), nullable=False, server_default=text("'Pending'"))
    request_url = Column(String(1000), nullable=False, server_default=text("''"))
    token_id = Column(String(255))
    template = Column(String(255))
    hostpay_obj = Column(String(1000))
    return_msg = Column(String(1000), nullable=False, server_default=text("''"))
    return_error = Column(String(1000), nullable=False, server_default=text("''"))
    date_proc = Column(DateTime, nullable=False, index=True)
    date_paid = Column(DateTime, nullable=False, index=True)
    date_init = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class ORDERPICKLOG(Base):
    __tablename__ = 'ORDER_PICK_LOG'

    pklog_id = Column(INTEGER(10), primary_key=True)
    admin_username = Column(String(25), nullable=False)
    order_id = Column(String(15), nullable=False, index=True)
    counter = Column(TINYINT(4), nullable=False)
    SKU = Column(String(25), nullable=False, index=True, server_default=text("''"))
    warehouse_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    pklog_type = Column(String(50), nullable=False, index=True, server_default=text("''"))
    pk_quantity = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    bko_quantity = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    ship_quantity = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class ORDERPREAUTHSESSION(Base):
    __tablename__ = 'ORDER_PREAUTH_SESSIONS'

    preauth_id = Column(INTEGER(10), primary_key=True)
    card_id = Column(INTEGER(10), nullable=False, index=True)
    order_id = Column(String(15), nullable=False, index=True)
    username = Column(String(25), nullable=False, index=True)
    currency_code = Column(String(3), server_default=text("''"))
    authorised_amount = Column(DECIMAL(12, 2), nullable=False)
    transaction_id = Column(String(255), nullable=False)
    realtime_notes = Column(String(50), nullable=False)
    card_authorisation = Column(String(100), nullable=False)
    fraud_score = Column(DECIMAL(4, 2), nullable=False)
    payment_notes = Column(MEDIUMTEXT)
    capture_url = Column(Text)
    preauth_status = Column(Enum('unauthorised', 'authorised', 'cancelled', 'completed'), nullable=False, index=True, server_default=text("'unauthorised'"))
    last_capture_status = Column(MEDIUMTEXT)
    admin = Column(String(25), nullable=False, index=True)
    date_created = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    date_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class ORDERREFERRALCOMMISSION(Base):
    __tablename__ = 'ORDER_REFERRAL_COMMISSIONS'

    order_id = Column(String(15), primary_key=True, nullable=False)
    username = Column(String(25), primary_key=True, nullable=False, index=True, server_default=text("''"))
    currency_code = Column(String(3), primary_key=True, nullable=False, index=True, server_default=text("''"))
    commission_amount = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    credited_amount = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    date_credited = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class ORDERSUEMAIL(Base):
    __tablename__ = 'ORDER_SUEMAILS'

    order_id = Column(String(15), primary_key=True)
    status = Column(Enum('Pending', 'Sending', 'Sent'), nullable=False, index=True, server_default=text("'Pending'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class ORDERSUPPLIEREMAIL(Base):
    __tablename__ = 'ORDER_SUPPLIEREMAILS'

    email_id = Column(INTEGER(10), primary_key=True)
    bulk_id = Column(INTEGER(10), nullable=False, index=True)
    order_id = Column(String(15), nullable=False, index=True)
    mid = Column(String(25), nullable=False, index=True)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class PAYMENTDESCTYPE(Base):
    __tablename__ = 'PAYMENT_DESC_TYPES'

    pay_desc_id = Column(INTEGER(10), primary_key=True, server_default=text("'0'"))
    charge_type = Column(Enum('', 'bank', 'cc', 'bpay', 'web', 'acc', 'reward', 'hosted', 'acccredit'), nullable=False, index=True, server_default=text("''"))
    payment_desc_type = Column(String(25), nullable=False, index=True, server_default=text("''"))
    charge_type_name = Column(String(150), nullable=False, server_default=text("''"))
    pay_desc_active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    configurable = Column(TINYINT(1), nullable=False, index=True, server_default=text("'1'"))


class PAYMENTPROCESSOR(Base):
    __tablename__ = 'PAYMENT_PROCESSORS'

    processor_id = Column(INTEGER(10), primary_key=True)
    pay_desc_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    payment_product_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    preferred_list_sort_order = Column(INTEGER(10), server_default=text("'0'"))
    processor_icon_type = Column(String(50))
    processor_name = Column(String(150), nullable=False, server_default=text("''"))
    processor_type = Column(Enum('', 'realtime', 'hosted'), nullable=False, server_default=text("''"))
    can_store_card = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    repeat_pay = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    multi_currency = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    default_currency = Column(String(3), nullable=False, server_default=text("''"))
    masterpass_enabled = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    require_cvv = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    stored_card_require_cvv = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    processor_module = Column(String(25), nullable=False, index=True, server_default=text("''"))
    pay_option_name1 = Column(String(50), nullable=False, server_default=text("''"))
    pay_option_values1 = Column(Text, nullable=False)
    requires_lightbox = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    allow_preauth = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    is_3ds_enabled = Column(TINYINT(3), nullable=False, server_default=text("'0'"))
    processor_active = Column(Enum('n', 'y'), nullable=False, index=True, server_default=text("'y'"))


class PAYMENTPROVIDER(Base):
    __tablename__ = 'PAYMENT_PROVIDERS'

    payment_provider_id = Column(INTEGER(10), primary_key=True, server_default=text("'0'"))
    payment_provider_name = Column(String(255), nullable=False, server_default=text("''"))
    provider_display_name = Column(String(50), server_default=text("''"))
    provider_description = Column(Text)
    provider_info_url = Column(String(255), server_default=text("''"))
    provider_signup_url = Column(String(255), server_default=text("''"))


class PAYMENTPROVIDERPRODUCT(Base):
    __tablename__ = 'PAYMENT_PROVIDER_PRODUCTS'

    payment_product_id = Column(INTEGER(10), primary_key=True, server_default=text("'0'"))
    payment_provider_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    payment_product_name = Column(String(255), nullable=False, server_default=text("''"))
    has_provision_integration = Column(Enum('n', 'y'), server_default=text("'n'"))
    controlled_checkout_only = Column(Enum('n', 'y'), server_default=text("'n'"))
    payment_product_active = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    sortorder = Column(INTEGER(10), nullable=False, server_default=text("'0'"))


class PAYMENTTYPE(Base):
    __tablename__ = 'PAYMENT_TYPES'

    payment_type_id = Column(INTEGER(10), primary_key=True, server_default=text("'0'"))
    payment_type_name = Column(String(255), nullable=False, server_default=text("''"))
    payment_icon = Column(String(50), server_default=text("''"))
    for_ecom = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    for_pos = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    help_url = Column(String(255))
    instantiable = Column(TINYINT(1), nullable=False, index=True, server_default=text("'1'"))


class PAYMENTTYPESPROCESSOR(Base):
    __tablename__ = 'PAYMENT_TYPES_PROCESSORS'

    payment_type_id = Column(INTEGER(10), primary_key=True, nullable=False, server_default=text("'0'"))
    processor_id = Column(INTEGER(10), primary_key=True, nullable=False, server_default=text("'0'"))
    pay_desc_id = Column(INTEGER(10), primary_key=True, nullable=False, server_default=text("'0'"))
    payment_default_description = Column(Text)
    is_description_editable = Column(TINYINT(1), nullable=False, server_default=text("'1'"))
    default_min_balance = Column(DECIMAL(12, 2))
    default_max_balance = Column(DECIMAL(12, 2))
    is_amount_limit_editable = Column(TINYINT(1), nullable=False, server_default=text("'1'"))
    is_allow_surcharge = Column(TINYINT(1), nullable=False, server_default=text("'1'"))


class PAYTYPE(Base):
    __tablename__ = 'PAYTYPES'

    id = Column(INTEGER(10), primary_key=True)
    name = Column(String(100), nullable=False, server_default=text("''"))
    realtime_agent = Column(String(255), nullable=False, server_default=text("''"))
    hosted_agent = Column(String(50), nullable=False)
    processor_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    charge_type = Column(Enum('', 'bank', 'cc', 'bpay', 'web', 'acc', 'reward', 'hosted', 'acccredit'), nullable=False, server_default=text("''"))
    payment_desc_type = Column(String(25), nullable=False, server_default=text("''"))
    pay_desc_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    currency_code = Column(String(3), nullable=False, index=True, server_default=text("''"))
    use_store_card = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    repeat_pay = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    min_balance = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    max_balance = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    surcharge = Column(DECIMAL(5, 2), nullable=False, server_default=text("'0.00'"))
    active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    visible = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    posvisible = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    acc_code = Column(String(10), nullable=False, server_default=text("''"))
    gp_restriction = Column(BIGINT(20), nullable=False, index=True, server_default=text("'0'"))
    pay_option1 = Column(String(50), nullable=False, server_default=text("''"))
    payment_description = Column(Text)
    payment_icons = Column(String(255), nullable=False, server_default=text("''"))
    sortorder = Column(TINYINT(3), nullable=False, server_default=text("'0'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    is_description_editable = Column(TINYINT(1), nullable=False, server_default=text("'1'"))
    is_preauth = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    stored_card_description = Column(Text, nullable=False)
    is_webpayment = Column(TINYINT(3), nullable=False, server_default=text("'0'"))


t_PAYTYPES_BACKUP_PREMIGRATION = Table(
    'PAYTYPES_BACKUP_PREMIGRATION', metadata,
    Column('id', INTEGER(10), nullable=False, server_default=text("'0'")),
    Column('name', String(100), nullable=False, server_default=text("''")),
    Column('realtime_agent', String(255), nullable=False, server_default=text("''")),
    Column('hosted_agent', String(50), nullable=False),
    Column('processor_id', INTEGER(10), nullable=False, server_default=text("'0'")),
    Column('charge_type', Enum('', 'bank', 'cc', 'bpay', 'web', 'acc', 'reward', 'hosted', 'acccredit'), nullable=False, server_default=text("''")),
    Column('payment_desc_type', String(25), nullable=False, server_default=text("''")),
    Column('pay_desc_id', INTEGER(10), nullable=False, server_default=text("'0'")),
    Column('currency_code', String(3), nullable=False, server_default=text("''")),
    Column('use_store_card', Enum('n', 'y'), nullable=False, server_default=text("'n'")),
    Column('repeat_pay', Enum('n', 'y'), nullable=False, server_default=text("'n'")),
    Column('min_balance', DECIMAL(12, 2), nullable=False, server_default=text("'0.00'")),
    Column('max_balance', DECIMAL(12, 2), nullable=False, server_default=text("'0.00'")),
    Column('surcharge', DECIMAL(5, 2), nullable=False, server_default=text("'0.00'")),
    Column('active', Enum('n', 'y'), nullable=False, server_default=text("'n'")),
    Column('visible', Enum('n', 'y'), nullable=False, server_default=text("'n'")),
    Column('posvisible', Enum('n', 'y'), nullable=False, server_default=text("'n'")),
    Column('acc_code', String(10), nullable=False, server_default=text("''")),
    Column('gp_restriction', BIGINT(20), nullable=False, server_default=text("'0'")),
    Column('pay_option1', String(50), nullable=False, server_default=text("''")),
    Column('payment_description', Text),
    Column('payment_icons', String(255), nullable=False, server_default=text("''")),
    Column('sortorder', TINYINT(3), nullable=False, server_default=text("'0'")),
    Column('timestamp', TIMESTAMP, nullable=False, server_default=text("'0000-00-00 00:00:00'")),
    Column('is_description_editable', TINYINT(1), nullable=False, server_default=text("'1'")),
    Column('is_preauth', TINYINT(1), nullable=False, server_default=text("'0'")),
    Column('stored_card_description', Text, nullable=False)
)


t_PAYTYPE_CONFIGS = Table(
    'PAYTYPE_CONFIGS', metadata,
    Column('pay_type_id', INTEGER(11), nullable=False),
    Column('name', String(50), nullable=False),
    Column('value', String(255), nullable=False),
    Index('pay_type_id_name', 'pay_type_id', 'name', unique=True)
)


class PICKBINSIZE(Base):
    __tablename__ = 'PICKBIN_SIZES'

    pbinsz_id = Column(INTEGER(10), primary_key=True)
    pbinsz_name = Column(String(80), nullable=False)
    pbinsz_description = Column(String(255), nullable=False, server_default=text("''"))
    pbinsz_length = Column(DECIMAL(8, 3), nullable=False)
    pbinsz_width = Column(DECIMAL(8, 3), nullable=False)
    pbinsz_height = Column(DECIMAL(8, 3), nullable=False)
    pbinsz_volume = Column(DECIMAL(12, 6), nullable=False)
    pbinsz_active = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    pbinsz_sortorder = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class PIIREMOVELOG(Base):
    __tablename__ = 'PII_REMOVE_LOG'
    __table_args__ = (
        Index('order_id', 'order_id', 'username', unique=True),
    )

    id = Column(INTEGER(11), primary_key=True)
    order_id = Column(String(15), nullable=False)
    username = Column(String(25), nullable=False)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class PLAN(Base):
    __tablename__ = 'PLANS'

    plan_id = Column(INTEGER(11), primary_key=True)
    addon_id = Column(INTEGER(11), nullable=False, unique=True)


class PLANFEATURE(Base):
    __tablename__ = 'PLAN_FEATURES'

    plan_feature_id = Column(INTEGER(11), primary_key=True)
    addon_id = Column(INTEGER(11), index=True)
    group_name = Column(String(85), nullable=False)
    feature_key = Column(String(85), nullable=False, unique=True)
    feature_name = Column(String(85), nullable=False)
    description = Column(Text, nullable=False)
    sortorder = Column(INTEGER(11), nullable=False)
    active = Column(TINYINT(1), nullable=False)
    system_feature = Column(TINYINT(1), nullable=False)


class PLANFEATUREVALUE(Base):
    __tablename__ = 'PLAN_FEATURE_VALUES'
    __table_args__ = (
        Index('plan_id_2', 'plan_id', 'plan_feature_id', unique=True),
    )

    plan_feature_value_id = Column(INTEGER(11), primary_key=True)
    plan_id = Column(INTEGER(11), nullable=False, index=True)
    plan_feature_id = Column(INTEGER(11), nullable=False, index=True)
    display_value = Column(String(85))
    system_value = Column(String(85))
    description = Column(Text, nullable=False)


class PRODUCTNOTIFICATION(Base):
    __tablename__ = 'PRODUCTNOTIFICATIONS'

    notification_id = Column(INTEGER(10), primary_key=True)
    username = Column(String(25), nullable=False, index=True, server_default=text("''"))
    full_name = Column(String(75), nullable=False, server_default=text("''"))
    email = Column(String(50), nullable=False, server_default=text("''"))
    SKU = Column(String(25), nullable=False, index=True, server_default=text("''"))
    date_added = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    date_sent = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))


class PROMOTIONPRICE(Base):
    __tablename__ = 'PROMOTION_PRICES'

    promotion_id = Column(INTEGER(10), primary_key=True, nullable=False, server_default=text("'0'"))
    group_id = Column(INTEGER(11), primary_key=True, nullable=False, server_default=text("'0'"))
    discount_amount = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    discount_percent = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))


class PROPOSALADMIN(Base):
    __tablename__ = 'PROPOSALADMINS'

    proposal_id = Column(INTEGER(10), primary_key=True)
    admin = Column(String(25), nullable=False, index=True)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class PROPOSALLINE(Base):
    __tablename__ = 'PROPOSALLINES'

    proposal_id = Column(INTEGER(10), primary_key=True, nullable=False)
    counter = Column(TINYINT(4), primary_key=True, nullable=False, server_default=text("'0'"))
    SKU = Column(String(25), index=True)
    quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    iskitting = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    description = Column(String(255))
    itm_serial = Column(String(50), nullable=False, index=True, server_default=text("''"))
    supplier = Column(String(50), index=True)
    dropshipper = Column(String(25), nullable=False, index=True)
    warehouse_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    extra = Column(MEDIUMTEXT, nullable=False)
    unit_price = Column(DECIMAL(13, 3), nullable=False, server_default=text("'0.000'"))
    discount_percent = Column(DECIMAL(5, 2), nullable=False, server_default=text("'0.00'"))
    product_discount = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    notax = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    aff_id = Column(String(25), nullable=False)
    aff_ref = Column(String(50), nullable=False)
    job_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    itemnotes = Column(String(255))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class PROPOSALLINESPLIT(Base):
    __tablename__ = 'PROPOSALLINESPLITS'

    proposal_id = Column(INTEGER(10), primary_key=True, nullable=False)
    counter = Column(TINYINT(4), primary_key=True, nullable=False)
    SKU = Column(String(25), primary_key=True, nullable=False, index=True)
    description = Column(String(255), nullable=False, server_default=text("''"))
    quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    assemble_quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    assemble_subtotal = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    assemble_notax = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    total_cal = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    itm_price = Column(DECIMAL(12, 2))


class PROPOSALMESSAGE(Base):
    __tablename__ = 'PROPOSALMESSAGES'

    pmsg_id = Column(INTEGER(10), primary_key=True)
    proposal_id = Column(INTEGER(10), nullable=False, index=True)
    admin = Column(String(25), nullable=False, index=True)
    pmsg_subject = Column(String(255), nullable=False)
    pmsg_body = Column(MEDIUMTEXT, nullable=False)
    pmsg_files = Column(MEDIUMTEXT, nullable=False)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class PROPOSAL(Base):
    __tablename__ = 'PROPOSALS'

    proposal_id = Column(INTEGER(10), primary_key=True)
    proposal_status_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    freason_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    usr_group = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    date_updated = Column(DateTime, nullable=False)
    date_placed = Column(DateTime, nullable=False, index=True)
    date_agreed = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    last_email_sent = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    last_order_created = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_followup = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    username = Column(String(25), nullable=False, index=True, server_default=text("''"))
    email = Column(String(50))
    bill_first_name = Column(String(50))
    bill_last_name = Column(String(50))
    bill_company = Column(String(50))
    bill_street1 = Column(String(50))
    bill_street2 = Column(String(50))
    bill_city = Column(String(50))
    bill_state = Column(String(50))
    bill_zip = Column(String(15))
    bill_country = Column(String(50))
    phone = Column(String(30))
    fax = Column(String(30))
    terms = Column(String(25), nullable=False, server_default=text("''"))
    status_log = Column(MEDIUMTEXT, nullable=False)
    sh_group_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    shipping_method = Column(String(50), nullable=False, server_default=text("''"))
    ship_signature = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    subtotal = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    shipping_cost = Column(DECIMAL(8, 2), nullable=False, server_default=text("'0.00'"))
    shipping_notax = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    tax = Column(DECIMAL(8, 2), nullable=False, server_default=text("'0.00'"))
    tax_inc = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    coupondiscount = Column(DECIMAL(8, 2), nullable=False, server_default=text("'0.00'"))
    grand_total = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    currency_code = Column(String(3), nullable=False, index=True, server_default=text("''"))
    internal_notes = Column(MEDIUMTEXT, nullable=False)
    operator = Column(String(25), nullable=False, index=True, server_default=text("''"))
    salesperson = Column(String(25), nullable=False, index=True, server_default=text("''"))
    followupperson = Column(String(25), nullable=False, index=True)
    ftype_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class PROPOSALSTATU(Base):
    __tablename__ = 'PROPOSALSTATUS'

    proposal_status_id = Column(INTEGER(10), primary_key=True)
    proposal_status_name = Column(String(50), nullable=False, index=True)
    notification = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    template = Column(String(20), nullable=False)
    orderby = Column(TINYINT(3), nullable=False, server_default=text("'0'"))


class PURCHASECOUNTER(Base):
    __tablename__ = 'PURCHASECOUNTER'

    counter = Column(INTEGER(11), primary_key=True)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class PURCHASEORDER(Base):
    __tablename__ = 'PURCHASEORDERS'

    poline_id = Column(INTEGER(10), primary_key=True)
    po_id = Column(String(25), nullable=False, index=True)
    accimport_id = Column(String(25), nullable=False, index=True)
    SKU = Column(String(25), nullable=False, index=True)
    order_qty = Column(DECIMAL(8, 2), nullable=False)
    arrived_qty = Column(DECIMAL(8, 2), nullable=False)
    unit_price = Column(DECIMAL(12, 4), nullable=False, server_default=text("'0.0000'"))
    notax = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    date_arrived = Column(Date, nullable=False, server_default=text("'0000-00-00'"))


class PURCHASE(Base):
    __tablename__ = 'PURCHASES'

    po_id = Column(String(25), primary_key=True)
    bill_id = Column(String(25), nullable=False, index=True, server_default=text("''"))
    dropshipper = Column(String(25), nullable=False, index=True)
    mid = Column(String(25), nullable=False, index=True, server_default=text("''"))
    supplier_invoice_id = Column(String(20), nullable=False, server_default=text("''"))
    postatus = Column(Enum('Open', 'Partially Receipted', 'Receipt Completed', 'Closed'), nullable=False, index=True)
    terms = Column(String(25), nullable=False)
    shipping_method = Column(String(50), nullable=False, server_default=text("''"))
    grand_total = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    subtotal = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    shipping_cost = Column(DECIMAL(8, 2), nullable=False, server_default=text("'0.00'"))
    shipping_notax = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    tax = Column(DECIMAL(8, 2), nullable=False, server_default=text("'0.00'"))
    tax_inc = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    date_placed = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    date_promised = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    ship_first_name = Column(String(50), nullable=False)
    ship_last_name = Column(String(50), nullable=False)
    ship_company = Column(String(50), nullable=False)
    ship_street1 = Column(String(50), nullable=False)
    ship_street2 = Column(String(50), nullable=False)
    ship_city = Column(String(50), nullable=False)
    ship_state = Column(String(50), nullable=False)
    ship_zip = Column(String(50), nullable=False)
    ship_country = Column(String(50), nullable=False)
    comment = Column(String(255), nullable=False)
    internal_notes = Column(MEDIUMTEXT, nullable=False)
    is_imported = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    date_closed = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_exported = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))


class RCACHEASSETLOCATION(Base):
    __tablename__ = 'RCACHE_ASSETLOCATIONS'

    date_record = Column(Date, primary_key=True, nullable=False, server_default=text("'0000-00-00'"))
    SKU = Column(String(25), primary_key=True, nullable=False, index=True)
    asset_status_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True, server_default=text("'0'"))
    loc_country = Column(String(50), primary_key=True, nullable=False, index=True, server_default=text("''"))
    loc_state = Column(String(50), primary_key=True, nullable=False, index=True, server_default=text("''"))
    total_qty = Column(INTEGER(10), nullable=False, server_default=text("'0'"))


class RCACHEASSETPURCHASE(Base):
    __tablename__ = 'RCACHE_ASSETPURCHASES'

    username = Column(String(25), primary_key=True, nullable=False)
    SKU = Column(String(25), primary_key=True, nullable=False, index=True)
    date_sales = Column(Date, primary_key=True, nullable=False, index=True)
    asset_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True, server_default=text("'0'"))
    loc_country = Column(String(50), primary_key=True, nullable=False, index=True, server_default=text("''"))
    loc_state = Column(String(50), primary_key=True, nullable=False, index=True, server_default=text("''"))
    purchase_qty = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    purchase_qty_all = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    total_sales = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    total_sales_all = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))


class RCACHEASSETSALESTOTAL(Base):
    __tablename__ = 'RCACHE_ASSETSALESTOTAL'

    asset_id = Column(INTEGER(10), primary_key=True)
    total_qty = Column(INTEGER(10), nullable=False)
    total_subtotal = Column(DECIMAL(10, 2), nullable=False)
    total_cost = Column(DECIMAL(10, 2), nullable=False)


class RCACHEASSETSERV(Base):
    __tablename__ = 'RCACHE_ASSETSERVS'

    asset_id = Column(INTEGER(10), primary_key=True)
    days_between_service = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    pods_used = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    total_services = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    total_labour_cost = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    total_material_cost = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    username = Column(String(25), nullable=False, index=True)


class RCACHEASSETSERVSPART(Base):
    __tablename__ = 'RCACHE_ASSETSERVS_PARTS'

    asset_id = Column(INTEGER(10), primary_key=True, nullable=False)
    SKU = Column(String(25), primary_key=True, nullable=False, index=True, server_default=text("''"))
    total_quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    total_cost = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))


class RCACHECATEGORYMONTHLY(Base):
    __tablename__ = 'RCACHE_CATEGORY_MONTHLY'

    content_id = Column(INTEGER(10), primary_key=True, nullable=False)
    date_invoiced = Column(Date, primary_key=True, nullable=False)
    sales_total = Column(DECIMAL(10, 2), nullable=False)
    qty_total = Column(INTEGER(10), nullable=False)


class RCACHECHANNELITEMSALE(Base):
    __tablename__ = 'RCACHE_CHANNEL_ITEMSALES'

    SKU = Column(String(25), primary_key=True, nullable=False)
    date_sales = Column(Date, primary_key=True, nullable=False, index=True)
    classification1 = Column(String(20), primary_key=True, nullable=False, index=True)
    total_sold = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    total_sales = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))


class RCACHECOMPATPRODUCT(Base):
    __tablename__ = 'RCACHE_COMPAT_PRODUCTS'

    content_id = Column(INTEGER(10), primary_key=True, nullable=False)
    inventory_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True)
    leaf_id = Column(INTEGER(10), nullable=False, index=True)


class RCACHEHISTORICALRECEIVABLE(Base):
    __tablename__ = 'RCACHE_HISTORICAL_RECEIVABLE'

    his_id = Column(INTEGER(10), primary_key=True)
    type = Column(String(15), nullable=False)
    date = Column(Date, index=True)
    date_due = Column(Date)
    order_record_id = Column(INTEGER(10))
    payment_id = Column(INTEGER(10))
    rma_id = Column(INTEGER(10))
    refund_id = Column(INTEGER(10))
    trans_id = Column(INTEGER(10))
    username = Column(String(45), nullable=False, server_default=text("''"))
    amount = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    balance = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    credit = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    user_id = Column(INTEGER(10), index=True)
    group_id = Column(INTEGER(10))


t_RCACHE_HISTORICAL_RECEIVABLE_REPORTS = Table(
    'RCACHE_HISTORICAL_RECEIVABLE_REPORTS', metadata,
    Column('user_id', INTEGER(11), nullable=False, index=True),
    Column('total_due', DECIMAL(12, 2), nullable=False, server_default=text("'0.00'")),
    Column('current', DECIMAL(12, 2), nullable=False, server_default=text("'0.00'")),
    Column('0_14', DECIMAL(12, 2), nullable=False, server_default=text("'0.00'")),
    Column('15_30', DECIMAL(12, 2), nullable=False, server_default=text("'0.00'")),
    Column('31_60', DECIMAL(12, 2), nullable=False, server_default=text("'0.00'")),
    Column('61_90', DECIMAL(12, 2), nullable=False, server_default=text("'0.00'")),
    Column('91_180', DECIMAL(12, 2), nullable=False, server_default=text("'0.00'")),
    Column('181_365', DECIMAL(12, 2), nullable=False, server_default=text("'0.00'")),
    Column('over_1year', DECIMAL(12, 2), nullable=False, server_default=text("'0.00'")),
    Column('balance', DECIMAL(12, 2), nullable=False, server_default=text("'0.00'")),
    Column('credit', DECIMAL(12, 2), nullable=False, server_default=text("'0.00'")),
    Column('total', DECIMAL(12, 2), nullable=False, index=True, server_default=text("'0.00'"))
)


class RCACHEITEMSALE(Base):
    __tablename__ = 'RCACHE_ITEMSALES'

    SKU = Column(String(25), primary_key=True, nullable=False)
    date_sales = Column(Date, primary_key=True, nullable=False, index=True)
    total_sold = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    total_sales = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))


class RCACHEITEMSALESANALYSI(Base):
    __tablename__ = 'RCACHE_ITEMSALES_ANALYSIS'

    SKU = Column(String(25), primary_key=True)
    total_sales = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    cost_of_sales = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    gross_profit = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    total_sold = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    total_return = Column(INTEGER(10), nullable=False, server_default=text("'0'"))


class RCACHEMONTHLYSALE(Base):
    __tablename__ = 'RCACHE_MONTHLY_SALES'

    order_id = Column(String(15), primary_key=True, nullable=False)
    date_sales = Column(Date, primary_key=True, nullable=False, index=True)
    classification1 = Column(String(20), primary_key=True, nullable=False, index=True)
    total_products = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    total_shipping = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    total_surcharge = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    total_sales = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    total_tax = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))


class RCACHEPROPOSALDAILY(Base):
    __tablename__ = 'RCACHE_PROPOSALDAILY'

    report_date = Column(Date, primary_key=True, nullable=False)
    SKU = Column(String(25), primary_key=True, nullable=False, index=True)
    total_proposals = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    total_trials = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    total_approved_trials = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    total_failed = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    total_swaps = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    total_swaps_done = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    total_pickups = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    total_pickups_done = Column(INTEGER(10), nullable=False, server_default=text("'0'"))


class RCACHEPROPOSALDAILYTOTAL(Base):
    __tablename__ = 'RCACHE_PROPOSALDAILYTOTAL'

    report_date = Column(Date, primary_key=True)
    date_recorded = Column(Date, nullable=False, server_default=text("'0000-00-00'"))


class RCACHEPROPOSAL(Base):
    __tablename__ = 'RCACHE_PROPOSALS'

    username = Column(String(25), primary_key=True)
    first_sales = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    last_sales = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    last_sales_since = Column(INTEGER(10), nullable=False, server_default=text("'-1'"))
    total_sales = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    total_assets = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    monthly_target = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    rolling_average = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    proposal_last_msg = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    proposal_followup = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    last_proposal_status_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    last_proposal_freason_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    proposal_created = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    proposal_agreed = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    proposal_failed = Column(INTEGER(10), nullable=False, server_default=text("'0'"))


class RCACHESALE(Base):
    __tablename__ = 'RCACHE_SALES'

    order_id = Column(String(15), primary_key=True, nullable=False)
    username = Column(String(25), primary_key=True, nullable=False, index=True)
    date_due = Column(Date, primary_key=True, nullable=False, index=True)
    total_due = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))


class RCACHESALESGEO(Base):
    __tablename__ = 'RCACHE_SALES_GEO'

    inventory_id = Column(INTEGER(10), primary_key=True, nullable=False)
    ship_city = Column(String(50), primary_key=True, nullable=False)
    ship_zip = Column(String(15), primary_key=True, nullable=False)
    ship_state = Column(String(50), primary_key=True, nullable=False)
    ship_country = Column(CHAR(2), primary_key=True, nullable=False)
    purchase_qty = Column(INTEGER(10), nullable=False)
    total_sales = Column(DECIMAL(10, 2), nullable=False)


class RCACHESTORDERPURCHASE(Base):
    __tablename__ = 'RCACHE_STORDERPURCHASES'

    username = Column(String(25), primary_key=True, nullable=False)
    asset_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True, server_default=text("'0'"))
    SKU = Column(String(25), primary_key=True, nullable=False, index=True)
    purchase_qty = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    purchase_qty_all = Column(INTEGER(10), nullable=False, server_default=text("'0'"))


class RCACHESTORDER(Base):
    __tablename__ = 'RCACHE_STORDERS'

    username = Column(String(25), primary_key=True)
    first_sales = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    last_sales = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    last_sales_since = Column(INTEGER(10), nullable=False, server_default=text("'-1'"))
    total_sales = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    total_assets = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    monthly_target = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    rolling_average = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    storder_active = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    storder_fulfilled = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    storder_onhold = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    storder_cancelled = Column(INTEGER(10), nullable=False, server_default=text("'0'"))


class RCACHEUSERREWARDPOINT(Base):
    __tablename__ = 'RCACHE_USER_REWARD_POINTS'

    rcache_id = Column(INTEGER(10), primary_key=True)
    voucher_id = Column(INTEGER(10), nullable=False, index=True)
    vprogram_id = Column(INTEGER(10), nullable=False)
    record_date = Column(Date, nullable=False, index=True, server_default=text("'0000-00-00'"))
    voucher_active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    amount_used = Column(DECIMAL(14, 2), nullable=False, server_default=text("'0.00'"))
    amount_earned = Column(DECIMAL(14, 2), nullable=False, server_default=text("'0.00'"))
    current_balance = Column(DECIMAL(14, 2), nullable=False, server_default=text("'0.00'"))


class RCACHEWAREHOUSESALE(Base):
    __tablename__ = 'RCACHE_WAREHOUSE_SALES'

    inventory_id = Column(INTEGER(10), primary_key=True, nullable=False)
    warehouse_id = Column(INTEGER(10), primary_key=True, nullable=False)
    content_id = Column(INTEGER(10), primary_key=True, nullable=False)
    sh_group_id = Column(INTEGER(10), primary_key=True, nullable=False)
    shserv_id = Column(INTEGER(10), primary_key=True, nullable=False)
    purchase_qty = Column(INTEGER(11), nullable=False)
    total_sales = Column(DECIMAL(10, 2), nullable=False)
    purchase_qty_all = Column(INTEGER(11), nullable=False)
    total_sales_all = Column(DECIMAL(10, 2), nullable=False)


class RCACHEWAREHOUSESALESSHSERV(Base):
    __tablename__ = 'RCACHE_WAREHOUSE_SALES_SHSERV'

    order_id = Column(String(15), primary_key=True, nullable=False)
    SKU = Column(String(25), primary_key=True, nullable=False)
    qty = Column(INTEGER(10), nullable=False)
    shserv_id = Column(INTEGER(10), nullable=False)


class RECPAYMENTCOUNTER(Base):
    __tablename__ = 'RECPAYMENTCOUNTER'

    counter = Column(INTEGER(11), primary_key=True)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class REFUND(Base):
    __tablename__ = 'REFUNDS'

    refund_id = Column(INTEGER(10), primary_key=True)
    username = Column(String(25), nullable=False, index=True, server_default=text("''"))
    order_id = Column(String(15), nullable=False, index=True)
    rma_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    bill_first_name = Column(String(50))
    bill_last_name = Column(String(50))
    bill_company = Column(String(50))
    bill_street1 = Column(String(50))
    bill_street2 = Column(String(50))
    bill_city = Column(String(50))
    bill_state = Column(String(50))
    bill_zip = Column(String(15))
    bill_country = Column(String(50))
    admin = Column(String(25), nullable=False, index=True)
    amount = Column(DECIMAL(12, 2), nullable=False)
    date_issued = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    date_paid = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    card_id = Column(INTEGER(10), nullable=False, index=True)
    notes = Column(MEDIUMTEXT, nullable=False)
    realtime_notes = Column(String(50), nullable=False)
    card_holder = Column(String(50))
    card_number = Column(String(30))
    card_exp_month = Column(TINYINT(3))
    card_exp_year = Column(INTEGER(10))
    card_authorisation = Column(String(100), nullable=False)
    acc_name = Column(String(50), nullable=False)
    acc_bsb = Column(String(10), nullable=False)
    acc_number = Column(String(20), nullable=False)
    cheque_number = Column(String(20), nullable=False)
    date_exported = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_placed = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class REGISTER(Base):
    __tablename__ = 'REGISTERS'

    register_id = Column(INTEGER(10), primary_key=True)
    warehouse_id = Column(INTEGER(10), nullable=False)
    register_code = Column(String(5), nullable=False, unique=True)
    name = Column(String(128), nullable=False)
    in_use = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    current_device_id = Column(String(128), nullable=False)
    active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    last_sync = Column(DateTime)
    logout_period = Column(INTEGER(10))
    last_order_id = Column(INTEGER(10), nullable=False)


class REGISTERINFO(Base):
    __tablename__ = 'REGISTER_INFO'

    register_id = Column(INTEGER(11), primary_key=True)
    operating_system = Column(String(45))
    operating_system_version = Column(String(45))
    printer_model = Column(String(45))
    printer_connected = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    device_model = Column(String(45))
    web_browser = Column(String(45))
    screen_resolution = Column(String(45))
    tyro_enabled = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    app_build_number = Column(String(45))
    app_version = Column(String(45))
    frontend_version = Column(String(45))


class REVIEW(Base):
    __tablename__ = 'REVIEWS'
    __table_args__ = (
        Index('token_doc_approved_idx', 'token_id', 'doc_id', 'approved'),
    )

    id = Column(INTEGER(11), primary_key=True)
    token_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    doc_id = Column(String(15), nullable=False, index=True, server_default=text("''"))
    username = Column(String(25))
    name = Column(String(45), nullable=False, server_default=text("''"))
    email = Column(String(50), nullable=False, server_default=text("''"))
    ipaddress = Column(String(50), nullable=False, server_default=text("''"))
    insert_date = Column(TIMESTAMP, nullable=False, index=True, server_default=text("CURRENT_TIMESTAMP"))
    score = Column(INTEGER(11))
    title = Column(String(50))
    review = Column(String(5000))
    reviewcustom1 = Column(String(255), nullable=False, server_default=text("''"))
    reviewcustom2 = Column(String(255), nullable=False, server_default=text("''"))
    reviewcustom3 = Column(String(255), nullable=False, server_default=text("''"))
    reviewcustom4 = Column(String(255), nullable=False, server_default=text("''"))
    reviewcustom5 = Column(String(255), nullable=False, server_default=text("''"))
    reviewcustom6 = Column(String(255), nullable=False, server_default=text("''"))
    reviewcustom7 = Column(String(255), nullable=False, server_default=text("''"))
    reviewcustom8 = Column(String(255), nullable=False, server_default=text("''"))
    reviewcustom9 = Column(String(255), nullable=False, server_default=text("''"))
    reviewcustom10 = Column(String(255), nullable=False, server_default=text("''"))
    reviewcustom11 = Column(String(255), nullable=False, server_default=text("''"))
    reviewcustom12 = Column(String(255), nullable=False, server_default=text("''"))
    reviewcustom13 = Column(String(255), nullable=False, server_default=text("''"))
    reviewcustom14 = Column(String(255), nullable=False, server_default=text("''"))
    reviewcustom15 = Column(String(255), nullable=False, server_default=text("''"))
    reviewcustom16 = Column(String(255), nullable=False, server_default=text("''"))
    reviewcustom17 = Column(String(255), nullable=False, server_default=text("''"))
    reviewcustom18 = Column(String(255), nullable=False, server_default=text("''"))
    reviewcustom19 = Column(String(255), nullable=False, server_default=text("''"))
    reviewcustom20 = Column(String(255), nullable=False, server_default=text("''"))
    review_response = Column(String(5000), nullable=False, server_default=text("''"))
    trace = Column(String(100))
    approved = Column(Enum('n', 'y', 'h', 'f'), nullable=False, index=True, server_default=text("'n'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class REVIEWCOLUMN(Base):
    __tablename__ = 'REVIEW_COLUMNS'

    col_id = Column(String(25), primary_key=True)
    col_type = Column(Enum('TEXT', 'DATE', 'INT', 'DECIMAL', 'BOOLEAN', 'SHORTTEXT', 'SELECTIONBOX'), server_default=text("'TEXT'"))
    col_name = Column(String(50))
    col_default = Column(String(1500))
    col_comment = Column(String(255))
    col_order = Column(TINYINT(3))


class RMA(Base):
    __tablename__ = 'RMA'

    rma_id = Column(INTEGER(10), primary_key=True)
    username = Column(String(25), nullable=False, index=True, server_default=text("''"), comment='FK USERS.username')
    rma_status = Column(Enum('Open', 'Closed'), nullable=False, server_default=text("'Open'"))
    creator = Column(String(25), nullable=False, index=True)
    order_id = Column(String(15), nullable=False, index=True)
    customer_po = Column(String(50), nullable=False, server_default=text("''"))
    invoice_id = Column(String(25), nullable=False, index=True)
    doctmpl_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    msg_id = Column(INTEGER(10), nullable=False, index=True)
    tax_inc = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    refund_shipping = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    refund_shipping_notax = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    refund_subtotal = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    refund_surcharge = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    refund_surcharge_taxable = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    refund_total = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    currency_code = Column(String(3), nullable=False, index=True, server_default=text("''"))
    refund_tax = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    internal_notes = Column(MEDIUMTEXT, nullable=False)
    date_issued = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_updated = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_approved = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_exported = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class RMAITEMSTATU(Base):
    __tablename__ = 'RMAITEMSTATUS'

    rma_item_status_id = Column(INTEGER(10), primary_key=True)
    rma_item_status = Column(String(100), nullable=False)
    active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    able_to_complete = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    sortorder = Column(TINYINT(3), nullable=False, server_default=text("'0'"))


class RMALINE(Base):
    __tablename__ = 'RMALINES'

    rma_id = Column(INTEGER(10), primary_key=True, nullable=False)
    counter = Column(TINYINT(4), primary_key=True, nullable=False)
    SKU = Column(String(25), nullable=False, index=True)
    description = Column(String(255), nullable=False)
    ItemNumber = Column(String(30), nullable=False, server_default=text("''"))
    warehouse_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='FK WAREHOUSES.warehouse_id')
    extra = Column(MEDIUMTEXT, nullable=False)
    quantity = Column(INTEGER(10), server_default=text("'0'"))
    iskitting = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    refund = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    original_quantity = Column(INTEGER(11))
    original_unit_price = Column(DECIMAL(12, 2))
    original_discount = Column(DECIMAL(12, 2))
    notax = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    reason = Column(String(100), nullable=False, server_default=text("''"))
    status = Column(String(100), nullable=False, server_default=text("''"))
    item_status = Column(String(100), nullable=False, server_default=text("''"))
    line_status_type = Column(String(100), nullable=False)
    manufaturer_claim = Column(String(100), nullable=False, server_default=text("''"))
    outcome = Column(String(100), nullable=False, server_default=text("''"))
    item_notes = Column(String(255), nullable=False, server_default=text("''"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class RMALINESPLIT(Base):
    __tablename__ = 'RMALINESPLITS'

    rma_id = Column(INTEGER(10), primary_key=True, nullable=False)
    counter = Column(TINYINT(4), primary_key=True, nullable=False)
    SKU = Column(String(25), primary_key=True, nullable=False, index=True)
    description = Column(String(255), nullable=False)
    quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    assemble_quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    assemble_subtotal = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    assemble_notax = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    total_cal = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    itm_price = Column(DECIMAL(12, 2))


class RMALINESTATU(Base):
    __tablename__ = 'RMALINESTATUS'

    rma_lnstatus_id = Column(INTEGER(10), primary_key=True)
    rma_lnstatus = Column(String(100), nullable=False)
    is_completed = Column(Enum('n', 'y'), server_default=text("'n'"))
    active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    able_to_complete = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    sortorder = Column(TINYINT(3), nullable=False, server_default=text("'0'"))


class RMALINESTATUSTYPE(Base):
    __tablename__ = 'RMALINESTATUSTYPES'

    rma_lnstatustype_id = Column(INTEGER(10), primary_key=True)
    name = Column(String(100))
    active = Column(Enum('n', 'y'))
    sortorder = Column(TINYINT(3))


class RMAMANUFACTURERCLAIM(Base):
    __tablename__ = 'RMAMANUFACTURERCLAIMS'

    rma_supclaims_id = Column(INTEGER(10), primary_key=True)
    rma_supclaims = Column(String(100), nullable=False)
    active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    sortorder = Column(TINYINT(3), nullable=False, server_default=text("'0'"))


class RMAOUTCOME(Base):
    __tablename__ = 'RMAOUTCOMES'

    rma_outcome_id = Column(INTEGER(10), primary_key=True)
    rma_outcome = Column(String(100), nullable=False)
    rma_outcome_description = Column(String(255), server_default=text("''"))
    invoice_type = Column(Enum('', 'refunds', 'returns'), server_default=text("''"))
    issue_restock = Column(Enum('n', 'y'), server_default=text("'n'"))
    return_parts = Column(Enum('n', 'y'), server_default=text("'n'"))
    active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    system_record = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    sortorder = Column(TINYINT(3), nullable=False, server_default=text("'0'"))


class RMAREASON(Base):
    __tablename__ = 'RMAREASON'

    rma_reason_id = Column(INTEGER(10), primary_key=True)
    rma_reason_gp_id = Column(INTEGER(10), nullable=False, index=True)
    rma_reason = Column(String(100), nullable=False)
    active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    sortorder = Column(TINYINT(3), nullable=False, server_default=text("'0'"))


class RMAREASONGROUP(Base):
    __tablename__ = 'RMAREASONGROUP'

    rma_reason_gp_id = Column(INTEGER(10), primary_key=True)
    rma_reason_gp_name = Column(String(50), nullable=False, server_default=text("''"))
    sortorder = Column(TINYINT(3), nullable=False, server_default=text("'0'"))


class SAASUACCOUNT(Base):
    __tablename__ = 'SAASU_ACCOUNTS'

    transactionCategoryUid = Column(INTEGER(10), primary_key=True, server_default=text("'0'"))
    lastUpdatedUid = Column(String(128), nullable=False, server_default=text("''"))
    type = Column(Enum('Other Income', 'Liability', 'Asset', 'Income', 'Expense', 'Equity', 'Other Expense', 'Cost of Sales'), nullable=False, server_default=text("'Other Income'"))
    name = Column(String(30), nullable=False, server_default=text("''"))
    isActive = Column(CHAR(1), nullable=False, server_default=text("'n'"))


class SAASUBANKACCOUNT(Base):
    __tablename__ = 'SAASU_BANKACCOUNTS'

    bankAccountUid = Column(INTEGER(10), primary_key=True, server_default=text("'0'"))
    lastUpdatedUid = Column(String(128), nullable=False, server_default=text("''"))
    type = Column(Enum('Other Income', 'Liability', 'Asset', 'Income', 'Expense', 'Equity', 'Other Expense', 'Cost of Sales'), nullable=False, server_default=text("'Other Income'"))
    bsb = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    accountNumber = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    displayName = Column(String(30), nullable=False, server_default=text("''"))
    isActive = Column(CHAR(1), nullable=False, server_default=text("'n'"))


class SAASUCOMBOITEM(Base):
    __tablename__ = 'SAASU_COMBOITEMS'

    uid = Column(INTEGER(10), primary_key=True, server_default=text("'0'"))
    lastUpdatedUid = Column(String(128), nullable=False, server_default=text("''"))
    code = Column(String(32), nullable=False, index=True, server_default=text("''"))
    description = Column(MEDIUMTEXT, nullable=False)
    isActive = Column(CHAR(1), nullable=False, server_default=text("'n'"))
    notes = Column(MEDIUMTEXT, nullable=False)
    isInventoried = Column(CHAR(1), nullable=False, server_default=text("'n'"))
    assetAccountUid = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    stockOnHand = Column(DECIMAL(10, 3), nullable=False, server_default=text("'0.000'"))
    currentValue = Column(DECIMAL(10, 3), nullable=False, server_default=text("'0.000'"))
    isBought = Column(CHAR(1), nullable=False, server_default=text("'n'"))
    purchaseExpenseAccountUid = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    purchaseTaxCode = Column(String(25), nullable=False, server_default=text("''"))
    primarySupplierContactUid = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    primarySupplierItemCode = Column(String(32), nullable=False, server_default=text("''"))
    buyingPrice = Column(DECIMAL(10, 3), nullable=False, server_default=text("'0.000'"))
    isBuyingPriceIncTax = Column(CHAR(1), nullable=False, server_default=text("'n'"))
    isSold = Column(CHAR(1), nullable=False, server_default=text("'n'"))
    saleIncomeAccountUid = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    saleTaxCode = Column(String(25), nullable=False, server_default=text("''"))
    saleCoSAccountUid = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    sellingPrice = Column(DECIMAL(10, 3), nullable=False, server_default=text("'0.000'"))
    isSellingPriceIncTax = Column(String(25), nullable=False, server_default=text("''"))
    isVisible = Column(CHAR(1), nullable=False, server_default=text("'n'"))
    items = Column(MEDIUMTEXT, nullable=False)


class SAASUCUSTOMER(Base):
    __tablename__ = 'SAASU_CUSTOMERS'

    uid = Column(INTEGER(10), primary_key=True, server_default=text("'0'"))
    lastUpdatedUid = Column(String(128), nullable=False, server_default=text("''"))
    givenname = Column(String(50), nullable=False, server_default=text("''"))
    familyname = Column(String(50), nullable=False, server_default=text("''"))
    email = Column(String(50), nullable=False, server_default=text("''"))
    dateofbirth = Column(DateTime, nullable=False)
    organisationname = Column(String(50), nullable=False, server_default=text("''"))
    abn = Column(String(50), nullable=False, server_default=text("''"))
    website = Column(MEDIUMTEXT, nullable=False)
    phone = Column(String(30), nullable=False, server_default=text("''"))
    fax = Column(String(30), nullable=False, server_default=text("''"))
    contactID = Column(String(25), nullable=False, index=True, server_default=text("''"))
    street = Column(String(50), nullable=False, server_default=text("''"))
    city = Column(String(50), nullable=False, server_default=text("''"))
    state = Column(String(50), nullable=False, server_default=text("''"))
    postcode = Column(String(15), nullable=False, server_default=text("''"))
    country = Column(String(50), nullable=False, server_default=text("''"))
    customField1 = Column(String(50), nullable=False, server_default=text("''"))
    customField2 = Column(String(50), nullable=False, server_default=text("''"))
    isCustomer = Column(CHAR(1), nullable=False, server_default=text("'n'"))
    isSupplier = Column(CHAR(1), nullable=False, server_default=text("'n'"))
    isActive = Column(CHAR(1), nullable=False, server_default=text("'n'"))
    last_updated = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))


class SAASUEXPORTED(Base):
    __tablename__ = 'SAASU_EXPORTED'

    export_id = Column(String(25), primary_key=True, nullable=False, server_default=text("''"))
    export_type = Column(String(10), primary_key=True, nullable=False, server_default=text("''"))
    export_code = Column(Enum('success', 'error'), nullable=False, server_default=text("'error'"))
    export_result = Column(String(255), nullable=False, server_default=text("''"))
    saasu_uid = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    last_updateid = Column(String(128), nullable=False, server_default=text("''"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class SAASUITEM(Base):
    __tablename__ = 'SAASU_ITEMS'

    uid = Column(INTEGER(10), primary_key=True, server_default=text("'0'"))
    lastUpdatedUid = Column(String(128), nullable=False, server_default=text("''"))
    code = Column(String(32), nullable=False, index=True, server_default=text("''"))
    description = Column(MEDIUMTEXT, nullable=False)
    isActive = Column(CHAR(1), nullable=False, server_default=text("'n'"))
    notes = Column(MEDIUMTEXT)
    isInventoried = Column(CHAR(1), nullable=False, server_default=text("'n'"))
    assetAccountUid = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    stockOnHand = Column(DECIMAL(10, 3), nullable=False, server_default=text("'0.000'"))
    currentValue = Column(DECIMAL(10, 3), nullable=False, server_default=text("'0.000'"))
    minimumStockLevel = Column(DECIMAL(10, 3), nullable=False, server_default=text("'0.000'"))
    defaultReOrderQuantity = Column(DECIMAL(10, 3), nullable=False, server_default=text("'0.000'"))
    isBought = Column(CHAR(1), nullable=False, server_default=text("'n'"))
    purchaseExpenseAccountUid = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    purchaseTaxCode = Column(String(25), nullable=False, server_default=text("''"))
    primarySupplierContactUid = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    primarySupplierItemCode = Column(String(32), nullable=False, server_default=text("''"))
    buyingPrice = Column(DECIMAL(10, 3), nullable=False, server_default=text("'0.000'"))
    isBuyingPriceIncTax = Column(CHAR(1), nullable=False, server_default=text("'n'"))
    isSold = Column(CHAR(1), nullable=False, server_default=text("'n'"))
    saleIncomeAccountUid = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    saleTaxCode = Column(String(25), nullable=False, server_default=text("''"))
    saleCoSAccountUid = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    sellingPrice = Column(DECIMAL(10, 3), nullable=False, server_default=text("'0.000'"))
    isSellingPriceIncTax = Column(String(25), nullable=False, server_default=text("''"))
    isVisible = Column(CHAR(1), nullable=False, server_default=text("'n'"))
    isDeleted = Column(CHAR(1), nullable=False, server_default=text("'n'"))
    last_updated = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))


class SAASUITEMSUPDATE(Base):
    __tablename__ = 'SAASU_ITEMS_UPDATES'

    SKU = Column(String(25), primary_key=True, server_default=text("''"))
    last_checked = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    last_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class SAASUORDER(Base):
    __tablename__ = 'SAASU_ORDERS'

    uid = Column(INTEGER(10), primary_key=True, server_default=text("'0'"))
    order_id = Column(String(128), nullable=False, server_default=text("''"))
    status = Column(String(128), nullable=False)
    amount_paid = Column(DECIMAL(10, 2), nullable=False)
    amount_due = Column(DECIMAL(10, 2), nullable=False)


class SAASUPAYMENT(Base):
    __tablename__ = 'SAASU_PAYMENTS'

    uid = Column(INTEGER(10), primary_key=True, server_default=text("'0'"))
    lastUpdatedUid = Column(String(128), nullable=False, server_default=text("''"))
    paymentDate = Column(DateTime, nullable=False)
    dateCleared = Column(DateTime, nullable=False)
    summary = Column(MEDIUMTEXT)
    reference = Column(MEDIUMTEXT)
    bankAccountUid = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    amount = Column(DECIMAL(8, 2), nullable=False, server_default=text("'0.00'"))
    last_updated = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))


class SAASUSCHEMA(Base):
    __tablename__ = 'SAASU_SCHEMA'

    task_id = Column(INTEGER(10), primary_key=True)
    task_name = Column(String(25), nullable=False, server_default=text("''"))
    task_type = Column(Enum('Import', 'Export'), nullable=False, server_default=text("'Import'"))
    site_tbl = Column(String(50), nullable=False)
    upd_method = Column(Enum('x', 'a', 'u'), nullable=False, server_default=text("'x'"))
    processor = Column(String(25), nullable=False)
    description = Column(String(255), nullable=False, server_default=text("''"))
    schedule_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    active = Column(Enum('y', 'n'), nullable=False, server_default=text("'n'"))
    child_proc = Column(Enum('y', 'n'), nullable=False, server_default=text("'n'"))
    last_updated = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class SAASUTAXCODE(Base):
    __tablename__ = 'SAASU_TAXCODES'

    taxCodeUid = Column(INTEGER(10), primary_key=True, server_default=text("'0'"))
    code = Column(String(20), nullable=False, server_default=text("''"))
    name = Column(String(30), nullable=False, server_default=text("''"))
    rate = Column(DECIMAL(8, 6), nullable=False, server_default=text("'0.000000'"))
    postingAccountUid = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    isSale = Column(CHAR(1), nullable=False, server_default=text("'n'"))
    isPurchase = Column(CHAR(1), nullable=False, server_default=text("'n'"))
    isShared = Column(CHAR(1), nullable=False, server_default=text("'n'"))
    isActive = Column(CHAR(1), nullable=False, server_default=text("'n'"))


class SALESCHANNEL(Base):
    __tablename__ = 'SALES_CHANNELS'

    sch_id = Column(INTEGER(11), primary_key=True)
    sch_title = Column(String(15), nullable=False, unique=True, server_default=text("''"))
    sch_orders_visible = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    sch_users_visible = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class SAVEDSHOPPINGCART(Base):
    __tablename__ = 'SAVED_SHOPPING_CART'

    cart_id = Column(INTEGER(11), primary_key=True)
    card_id = Column(INTEGER(11), nullable=False, index=True)
    username = Column(String(25), nullable=False, index=True)
    gateway_token = Column(String(255), nullable=False, index=True)
    order_id = Column(String(15), nullable=False)
    cart_data = Column(Text)
    cart_status = Column(Enum('open', 'closed'), nullable=False, server_default=text("'open'"))
    closed_reason = Column(Enum('', 'payment_declined', 'order_created'), nullable=False, server_default=text("''"))
    date_created = Column(TIMESTAMP, nullable=False, index=True, server_default=text("CURRENT_TIMESTAMP"))


class SCHEDULEDPROC(Base):
    __tablename__ = 'SCHEDULED_PROCS'

    proc_id = Column(INTEGER(10), primary_key=True)
    task_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    fk_batch_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    task_module = Column(String(30), nullable=False, index=True, server_default=text("''"))
    task_param = Column(String(1000), nullable=False, server_default=text("''"))
    task_param_md5 = Column(String(32), nullable=False, index=True, server_default=text("''"))
    task_priority = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    proc_status = Column(Enum('Pending', 'Queued', 'Running', 'Finished', 'Stopped'), nullable=False, index=True, server_default=text("'Pending'"))
    date_schedule = Column(DateTime, nullable=False, index=True, server_default=text("'0000-00-00 00:00:00'"))
    date_queued = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_checked = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_started = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_ended = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))


class SCHEDULEDTASK(Base):
    __tablename__ = 'SCHEDULED_TASKS'

    task_id = Column(INTEGER(10), primary_key=True)
    task_name = Column(String(50), nullable=False, server_default=text("''"))
    task_module = Column(String(30), nullable=False, index=True, server_default=text("''"))
    task_param = Column(String(1000), nullable=False, server_default=text("''"))
    task_priority = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    task_active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    last_scheduled = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    schedule_hour = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    schedule_month = Column(SMALLINT(5), nullable=False, server_default=text("'0'"))
    schedule_mday = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    schedule_wday = Column(TINYINT(3), nullable=False, index=True, server_default=text("'0'"))
    schedule_minute = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))


class SCHEDULEDTASKLOG(Base):
    __tablename__ = 'SCHEDULED_TASK_LOGS'

    log_id = Column(INTEGER(10), primary_key=True)
    proc_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    task_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    task_pid = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    task_module = Column(String(30), nullable=False, index=True, server_default=text("''"))
    log_type = Column(Enum('info', 'msg', 'error', 'warn', 'debug', 'init'), nullable=False, index=True, server_default=text("'info'"))
    log_msg = Column(String(3000), nullable=False, server_default=text("''"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class SCHEDULEDTASKSTATISTIC(Base):
    __tablename__ = 'SCHEDULED_TASK_STATISTICS'

    statistic_id = Column(INTEGER(10), primary_key=True)
    task_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    task_module = Column(String(30), nullable=False, index=True, server_default=text("''"))
    total_executed = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    run_time_avg = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    run_time_min = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    run_time_max = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    last_started = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    statistic_started = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    statistic_ended = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))


class SHIPPINGCARRIER(Base):
    __tablename__ = 'SHIPPING_CARRIERS'

    carrier_id = Column(INTEGER(10), primary_key=True)
    carrier_name = Column(String(50), nullable=False, server_default=text("''"))
    carrier_key = Column(String(25), nullable=False, index=True, server_default=text("''"))
    integration_type = Column(Enum('direct', 'easypost', 'sendle'), nullable=False, server_default=text("'direct'"))
    active = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    rate = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    label = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    min_weight = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    max_weight = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    region = Column(String(50), nullable=False, server_default=text("''"))
    currency = Column(String(50), nullable=False, server_default=text("''"))
    sortorder = Column(INTEGER(10), nullable=False, server_default=text("'0'"))


class SHIPPINGCARRIERSMODULE(Base):
    __tablename__ = 'SHIPPING_CARRIERS_MODULES'
    __table_args__ = (
        Index('carrier_module', 'carrier_id', 'shratemod_id', 'shmodule_id', unique=True),
    )

    carrier_module_id = Column(INTEGER(10), primary_key=True)
    carrier_id = Column(INTEGER(10), nullable=False, index=True)
    shratemod_id = Column(INTEGER(10), index=True)
    shmodule_id = Column(INTEGER(10), index=True)


class SHIPPINGCARRIERSSERVICE(Base):
    __tablename__ = 'SHIPPING_CARRIERS_SERVICES'

    carrier_id = Column(INTEGER(10), primary_key=True, nullable=False, server_default=text("'0'"))
    shrserv_code = Column(String(150), primary_key=True, nullable=False, server_default=text("''"))
    shrserv_name = Column(String(255), nullable=False, server_default=text("''"))


class SHIPPINGGROUP(Base):
    __tablename__ = 'SHIPPING_GROUPS'

    sh_group_id = Column(INTEGER(10), primary_key=True)
    sh_group_name = Column(String(50), nullable=False, index=True)
    sh_group_routing = Column(String(25), nullable=False, server_default=text("''"))
    sh_group_description = Column(MEDIUMTEXT, nullable=False)
    dispatch_notification = Column(MEDIUMTEXT, nullable=False)
    min_charge = Column(DECIMAL(10, 2), nullable=False, server_default=text("'0.00'"))
    max_charge = Column(DECIMAL(10, 2), nullable=False, server_default=text("'0.00'"))
    min_weight = Column(DECIMAL(10, 4), nullable=False, server_default=text("'0.0000'"))
    max_weight = Column(DECIMAL(10, 4), nullable=False, server_default=text("'0.0000'"))
    free_shipping_subtotal = Column(DECIMAL(10, 2), nullable=False, server_default=text("'0.00'"))
    free_shipping_charge = Column(DECIMAL(10, 2), nullable=False, server_default=text("'0.00'"))
    expedited = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    priority_dispatch = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    option_visible_days = Column(TINYINT(3), nullable=False, server_default=text("'0'"))
    exc_vacation_group_id = Column(TINYINT(3), nullable=False, server_default=text("'0'"))
    only_vacation_group_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    travel_vacation_group_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    delivery_days = Column(TINYINT(3), nullable=False, server_default=text("'0'"))
    manifest_days = Column(TINYINT(3), nullable=False, server_default=text("'0'"))
    travel_days = Column(TINYINT(3), nullable=False, server_default=text("'0'"))
    option_visible_hrs = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    daily_close_time = Column(Time, nullable=False, server_default=text("'00:00:00'"))
    pickup = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    service_only = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    check_type = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    split_default_shtype = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    visible = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    staff_visible = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    ebay_visible = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    sortorder = Column(TINYINT(3), nullable=False, index=True, server_default=text("'0'"))
    nopobox = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class SHIPPINGGROUPGROUP(Base):
    __tablename__ = 'SHIPPING_GROUP_GROUPS'

    sh_group_id = Column(INTEGER(10), primary_key=True, nullable=False, server_default=text("'0'"))
    group_id = Column(INTEGER(11), primary_key=True, nullable=False, index=True, server_default=text("'0'"))


class SHIPPINGGROUPMETHOD(Base):
    __tablename__ = 'SHIPPING_GROUP_METHODS'

    sh_group_id = Column(INTEGER(10), primary_key=True, nullable=False)
    sh_type_id = Column(TINYINT(3), primary_key=True, nullable=False, index=True, server_default=text("'0'"))
    shipping_ref = Column(INTEGER(10), nullable=False, index=True)


class SHIPPINGIDENTIFIER(Base):
    __tablename__ = 'SHIPPING_IDENTIFIER'

    sh_type_id = Column(TINYINT(3), primary_key=True)
    sh_type_name = Column(String(50), nullable=False, index=True)
    sh_type_description = Column(String(255), nullable=False)


class SHIPPINGSALESCHANNELCARRIERMAPPING(Base):
    __tablename__ = 'SHIPPING_SALES_CHANNEL_CARRIER_MAPPING'
    __table_args__ = (
        Index('shipping_ref_idx', 'shipping_ref', 'managed_sales_channel_id', unique=True),
    )

    id = Column(INTEGER(11), primary_key=True)
    shipping_ref = Column(INTEGER(11), nullable=False)
    managed_sales_channel_id = Column(INTEGER(11), nullable=False)
    sales_channel_carrier_code = Column(String(100), nullable=False, index=True)
    sales_channel_carrier_name = Column(String(255), nullable=False)


class SHIPPINGSETUPDEFAULT(Base):
    __tablename__ = 'SHIPPING_SETUP_DEFAULTS'

    ssm_id = Column(INTEGER(10), primary_key=True)
    shmodule_id = Column(INTEGER(10), nullable=False)
    ssm_carrierid = Column(String(45), nullable=False)
    ssm_shrate_module = Column(String(50), nullable=False, server_default=text("''"))
    ssm_shrate_code = Column(String(200), nullable=False, server_default=text("''"))
    ssm_section = Column(String(45), nullable=False)
    ssm_subsection = Column(String(45), nullable=False, server_default=text("''"))
    ssm_name = Column(String(190), nullable=False)
    ssm_default_name = Column(String(50), nullable=False, server_default=text("''"))
    ssm_default_checked = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ssm_description = Column(String(1000), nullable=False, server_default=text("'Delivery Australia Wide'"))
    ssm_service_name = Column(String(45), nullable=False)
    ssm_origin_type = Column(Enum('', 'business', 'residence'), nullable=False, server_default=text("''"))
    ssm_destination_type = Column(Enum('', 'business', 'residence'), nullable=False, server_default=text("''"))
    ssm_zone_code = Column(String(45), nullable=False, server_default=text("'AU'"))
    ssm_courier = Column(String(45), nullable=False, server_default=text("'Australia'"))
    ssm_min_charge = Column(DECIMAL(10, 2), nullable=False)
    ssm_tax_inclusive = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    ssm_pobox = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ssm_pobox_ask = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    ssm_data = Column(Text)
    ssm_notes = Column(Text)
    ssm_sortorder = Column(INTEGER(10), nullable=False)
    ssm_active = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))


class SHIPPINGTYPE(Base):
    __tablename__ = 'SHIPPING_TYPE'

    shtype = Column(CHAR(1), primary_key=True)
    type_name = Column(String(30), nullable=False)
    shmethod = Column(Enum('Flat', 'Fixed', 'Weight', 'Price', 'Qty'), nullable=False, server_default=text("'Flat'"))


class SHOPPINGCART(Base):
    __tablename__ = 'SHOPPING_CART'

    cart_id = Column(INTEGER(10), primary_key=True)
    cart_code = Column(CHAR(8), nullable=False, index=True)
    cart_data = Column(MEDIUMTEXT, nullable=False)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class SHOPPINGCARTREC(Base):
    __tablename__ = 'SHOPPING_CART_REC'

    id = Column(INTEGER(10), primary_key=True)
    username = Column(String(25), nullable=False)
    shopper_ip = Column(String(50), nullable=False)
    cart_data = Column(MEDIUMTEXT)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class SHOPPINGCARTTRACK(Base):
    __tablename__ = 'SHOPPING_CART_TRACK'
    __table_args__ = (
        Index('cart_id_code_idx', 'cart_id', 'cart_code'),
    )

    cart_track_id = Column(INTEGER(11), primary_key=True)
    cart_id = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    cart_code = Column(String(45), nullable=False, server_default=text("''"))
    username = Column(String(45), nullable=False, server_default=text("''"))
    cart_data = Column(MEDIUMTEXT, nullable=False)
    date_updated = Column(TIMESTAMP, index=True)
    processed = Column(Enum('y', 'n'), nullable=False, server_default=text("'n'"))
    notified_customer = Column(Enum('y', 'n'), nullable=False, server_default=text("'n'"))
    cart_email = Column(String(55), nullable=False, server_default=text("''"))
    date_created = Column(TIMESTAMP)
    checkout_status = Column(Enum('open', 'closed', 'abandoned'), nullable=False, server_default=text("'open'"))
    close_reason = Column(Enum('order_created', 'session_abandoned'), comment='Deprecated in DEVELOP-57, use checkout_status instead')
    order_id = Column(String(15))
    address_data = Column(Text)


class SHRATELOG(Base):
    __tablename__ = 'SHRATE_LOGS'

    log_id = Column(INTEGER(10), primary_key=True)
    module = Column(String(25), nullable=False, index=True, server_default=text("''"))
    msg_type = Column(Enum('info', 'error', 'return', 'request', 'response'), nullable=False, index=True, server_default=text("'info'"))
    return_msg = Column(Text, nullable=False)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class SHRATESERVICE(Base):
    __tablename__ = 'SHRATE_SERVICES'

    shrate_module = Column(String(20), primary_key=True, nullable=False, server_default=text("''"))
    shrserv_code = Column(String(150), primary_key=True, nullable=False, index=True, server_default=text("''"))
    shrserv_name = Column(String(255), nullable=False, server_default=text("''"))


class SHACCOUNT(Base):
    __tablename__ = 'SH_ACCOUNTS'

    shacc_id = Column(INTEGER(10), primary_key=True)
    warehouse_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    dropshipper = Column(String(25), nullable=False, index=True)
    shacc_name = Column(String(50), nullable=False, server_default=text("''"))
    shacc_module = Column(String(25), nullable=False)
    module_param = Column(MEDIUMTEXT, nullable=False)
    realtime = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    login_usr = Column(String(100), nullable=False, server_default=text("''"))
    login_pwd = Column(String(255), nullable=False)
    merchant_id = Column(String(25), nullable=False)
    merchant_acc = Column(String(25), nullable=False)
    chargeback_acc = Column(String(25), nullable=False)
    location_id = Column(String(50), nullable=False)
    lodgement_title = Column(String(50), nullable=False)
    lodgement_code = Column(String(25), nullable=False, server_default=text("''"))
    lodgement_location = Column(String(50), nullable=False, server_default=text("''"))
    lodgement_address = Column(String(255), nullable=False)
    manifest_prefix = Column(String(5), nullable=False)
    require_weight = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    print_lbheader = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    max_cngmt = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    max_arts = Column(TINYINT(4), nullable=False, server_default=text("'255'"))
    courier_id = Column(INTEGER(10), nullable=False, index=True)
    shrateacc_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    shmodule_id = Column(INTEGER(11), nullable=False, index=True)


class SHARTICLE(Base):
    __tablename__ = 'SH_ARTICLES'

    cngmt_id = Column(INTEGER(10), primary_key=True, nullable=False)
    art_id = Column(TINYINT(3), primary_key=True, nullable=False)
    article_number = Column(String(25), nullable=False)
    article_group = Column(String(100), nullable=False, server_default=text("''"))
    article_destref = Column(String(255), nullable=False, server_default=text("''"))
    article_excesslabels = Column(Text, nullable=False)
    barcode = Column(String(60), nullable=False)
    package_type = Column(String(15), nullable=False, server_default=text("''"))
    cubic = Column(DECIMAL(15, 9), nullable=False, server_default=text("'0.000000000'"))
    has_been_scanned = Column(Enum('y', 'n'), nullable=False, server_default=text("'n'"))
    shipping = Column(DECIMAL(10, 4), nullable=False, server_default=text("'0.0000'"))
    length = Column(DECIMAL(8, 3), nullable=False, server_default=text("'0.000'"))
    width = Column(DECIMAL(8, 3), nullable=False, server_default=text("'0.000'"))
    height = Column(DECIMAL(8, 3), nullable=False, server_default=text("'0.000'"))
    transit_cover = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    article_notes = Column(Text, nullable=False)
    article_items = Column(Text, nullable=False)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class SHCONSIGNMENT(Base):
    __tablename__ = 'SH_CONSIGNMENTS'

    cngmt_id = Column(INTEGER(10), primary_key=True)
    manifest_id = Column(INTEGER(10), nullable=False, index=True)
    shserv_id = Column(INTEGER(10), nullable=False, index=True)
    order_id = Column(String(15), nullable=False, index=True)
    dropshipper = Column(String(25), nullable=False, index=True)
    cngmt_number = Column(String(25), nullable=False)
    cngmt_record_id = Column(String(50), nullable=False)
    cngmt_atl = Column(String(25), nullable=False, server_default=text("''"))
    danger = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    signature = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    zone_id = Column(INTEGER(10), nullable=False, index=True)
    cubic_modifier = Column(DECIMAL(8, 3), nullable=False, server_default=text("'0.000'"))


class SHCONSIGNMENTPOOL(Base):
    __tablename__ = 'SH_CONSIGNMENT_POOLS'

    consignment_pool_id = Column(INTEGER(10), primary_key=True)
    consignment_pool_name = Column(String(255), nullable=False)


class SHCONSIGNMENTPOOLSACCOUNTMAPPING(Base):
    __tablename__ = 'SH_CONSIGNMENT_POOLS_ACCOUNT_MAPPING'

    consignment_pool_account_mapping_id = Column(INTEGER(10), primary_key=True)
    shacc_id = Column(INTEGER(10), nullable=False, unique=True)
    consignment_pool_id = Column(INTEGER(10), nullable=False)


class SHCONSIGNMENTPOOLSNUMBER(Base):
    __tablename__ = 'SH_CONSIGNMENT_POOLS_NUMBERS'

    consignment_pool_number_id = Column(INTEGER(10), primary_key=True)
    consignment_number = Column(String(255), nullable=False, unique=True)
    consignment_pool_id = Column(INTEGER(10))
    is_used = Column(TINYINT(1), nullable=False, server_default=text("'0'"))


class SHCOUNTER(Base):
    __tablename__ = 'SH_COUNTER'

    ctr_id = Column(String(15), primary_key=True)
    increment = Column(INTEGER(10), nullable=False, server_default=text("'0'"))


class SHCOURIERZONE(Base):
    __tablename__ = 'SH_COURIER_ZONES'

    courier_id = Column(INTEGER(10), primary_key=True)
    courier_name = Column(String(25), nullable=False)


class SHMANIFEST(Base):
    __tablename__ = 'SH_MANIFESTS'

    manifest_id = Column(INTEGER(10), primary_key=True)
    shacc_id = Column(INTEGER(10), nullable=False, index=True)
    manifest_number = Column(String(25), nullable=False)
    date_created = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_manifest = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    date_submit = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_lodge = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    submit_msg = Column(MEDIUMTEXT, nullable=False)


class SHMETHOD(Base):
    __tablename__ = 'SH_METHODS'

    shipping_ref = Column(INTEGER(10), primary_key=True, nullable=False)
    shserv_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True)


class SHMODULE(Base):
    __tablename__ = 'SH_MODULES'

    shmodule_id = Column(INTEGER(10), primary_key=True)
    shmodule_name = Column(String(50), nullable=False, server_default=text("''"))
    shmodule_lib = Column(String(25), nullable=False, index=True, server_default=text("''"), comment='Link to SH_ACCOUNTS.shacc_module')
    shmodule_api = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    shmodule_track_loc = Column(Enum('', 'consignment', 'article'), nullable=False, server_default=text("''"))
    shmodule_description = Column(String(255), nullable=False, server_default=text("''"))
    shmodule_system = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    shmodule_active = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    shmodule_use_api_label = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    shmodule_use_api_manifest = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    shmodule_sortorder = Column(INTEGER(10), nullable=False, server_default=text("'0'"))


class SHPACKAGE(Base):
    __tablename__ = 'SH_PACKAGES'

    package_id = Column(INTEGER(10), primary_key=True)
    package_type = Column(String(15), nullable=False, index=True, server_default=text("''"))
    package_name = Column(String(35), nullable=False, server_default=text("''"))
    package_barcode = Column(String(25), nullable=False, index=True, server_default=text("''"))
    cubic = Column(DECIMAL(15, 9), nullable=False, server_default=text("'0.000000000'"))
    shipping = Column(DECIMAL(10, 4), nullable=False, server_default=text("'0.0000'"))
    min_weight = Column(DECIMAL(10, 4), nullable=False, server_default=text("'0.0000'"))
    length = Column(DECIMAL(8, 3), nullable=False, server_default=text("'0.000'"))
    width = Column(DECIMAL(8, 3), nullable=False, server_default=text("'0.000'"))
    height = Column(DECIMAL(8, 3), nullable=False, server_default=text("'0.000'"))
    itm_quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    itm_quantity_all = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    use_weight = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    auto_package = Column(Enum('n', 'y'), server_default=text("'n'"))
    active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class SHPACKAGESHIPREF(Base):
    __tablename__ = 'SH_PACKAGE_SHIPREF'

    package_id = Column(INTEGER(10), primary_key=True, nullable=False)
    shipping_ref = Column(INTEGER(10), primary_key=True, nullable=False, index=True)


class SHPACKAGESHIPTYPE(Base):
    __tablename__ = 'SH_PACKAGE_SHIPTYPE'

    package_id = Column(INTEGER(10), primary_key=True, nullable=False)
    sh_type_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True)


class SHPACKAGETYPE(Base):
    __tablename__ = 'SH_PACKAGE_TYPES'

    package_type = Column(String(15), primary_key=True)
    package_type_ref = Column(String(50), nullable=False, server_default=text("''"))
    package_type_desc = Column(String(255), nullable=False)


class SHRATEACCOUNT(Base):
    __tablename__ = 'SH_RATE_ACCOUNTS'

    shrateacc_id = Column(INTEGER(10), primary_key=True)
    shratemod_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    shrateacc_name = Column(String(255), nullable=False, server_default=text("''"))
    shrateacc_username = Column(String(255), nullable=False, server_default=text("''"))
    shrateacc_password = Column(Text)
    shrateacc_api_id = Column(String(255), nullable=False, server_default=text("''"))
    shrateacc_api_key = Column(Text)
    shrateacc_multiple_regions = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    shrateacc_environment = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    shrateacc_postcode = Column(String(50), nullable=False, server_default=text("''"))
    shrateacc_city = Column(String(50), nullable=False, server_default=text("''"))
    shrateacc_state = Column(String(50), nullable=False, server_default=text("''"))
    shrateacc_country = Column(CHAR(2), nullable=False, server_default=text("''"))


class SHRATEMODULE(Base):
    __tablename__ = 'SH_RATE_MODULES'

    shratemod_id = Column(INTEGER(10), primary_key=True)
    shratemod_name = Column(String(50), nullable=False, server_default=text("''"))
    shratemod_lib = Column(String(25), nullable=False, index=True, server_default=text("''"))
    shratemod_description = Column(String(255), nullable=False, server_default=text("''"))
    shratemod_system = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    shratemod_active = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    shratemod_sortorder = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    max_allow_weight = Column(DECIMAL(10, 4), nullable=False, server_default=text("'0.0000'"), comment='Deprecated by TRG-1050 for 6.15.0')
    allow_mulit_articles = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"), comment='Deprecated by TRG-1050 for 6.15.0')
    ui_field_names = Column(Text, nullable=False)
    ui_help_info = Column(Text, nullable=False)
    ui_cfg_note = Column(Text, nullable=False)
    currency_code = Column(String(3), server_default=text("''"))


class SHSERVICE(Base):
    __tablename__ = 'SH_SERVICES'

    shserv_id = Column(INTEGER(10), primary_key=True)
    shacc_id = Column(INTEGER(10), nullable=False, index=True)
    shserv_code = Column(String(255))
    shserv_name = Column(String(50))
    cngmt_prefix = Column(String(10), nullable=False, server_default=text("''"))
    cngmt_barcode_prefix = Column(String(10), nullable=False, server_default=text("''"))
    shserv_service_code = Column(String(10), nullable=False, server_default=text("''"))


class SITECACHE(Base):
    __tablename__ = 'SITE_CACHE'

    doc_id = Column(INTEGER(10), primary_key=True, nullable=False, server_default=text("'0'"))
    cache_type = Column(String(45), primary_key=True, nullable=False, server_default=text("''"))
    cache_key = Column(String(255), primary_key=True, nullable=False, index=True, server_default=text("''"))
    page_cache_key = Column(String(10), nullable=False, server_default=text("''"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class SITEIMAGE(Base):
    __tablename__ = 'SITE_IMAGES'

    image_id = Column(INTEGER(10), primary_key=True)
    thumb_url = Column(VARCHAR(255), nullable=False)
    medium_url = Column(VARCHAR(255), nullable=False)
    full_url = Column(VARCHAR(255), nullable=False)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class SITEREDIRECT(Base):
    __tablename__ = 'SITE_REDIRECT'
    __table_args__ = (
        Index('request_url', 'request_url', 'request_qs', unique=True),
    )

    redirect_id = Column(INTEGER(10), primary_key=True)
    content_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    inventory_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    stloc_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    request_url = Column(String(255), nullable=False, server_default=text("''"))
    request_qs = Column(String(255), nullable=False, server_default=text("''"))
    redirect_qs = Column(String(255), nullable=False, server_default=text("''"))
    clear_cache = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    date_expiry = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class SITEREFERRALTRACKING(Base):
    __tablename__ = 'SITE_REFERRAL_TRACKING'

    referral_id = Column(INTEGER(10), primary_key=True)
    referral_name = Column(String(50), nullable=False, server_default=text("''"))
    referral_description = Column(Text, nullable=False)
    require_referral_key1 = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    referral_key1 = Column(String(50), nullable=False, server_default=text("''"))
    referral_session1 = Column(String(50), nullable=False, server_default=text("''"))
    referral_key2 = Column(String(50), nullable=False, server_default=text("''"))
    referral_session2 = Column(String(50), nullable=False, server_default=text("''"))
    referral_key3 = Column(String(50), nullable=False, server_default=text("''"))
    referral_session3 = Column(String(50), nullable=False, server_default=text("''"))
    referral_key4 = Column(String(50), nullable=False, server_default=text("''"))
    referral_session4 = Column(String(50), nullable=False, server_default=text("''"))
    referral_declaration = Column(Text, nullable=False)
    referral_purchase = Column(Text, nullable=False)
    referral_visitor = Column(Text, nullable=False)
    referral_register = Column(Text, nullable=False)
    referral_productdescription = Column(Text, nullable=False)
    referral_buyingoptions = Column(Text, nullable=False)
    referral_cartcta = Column(Text, nullable=False)
    referral_thumbnails = Column(Text, nullable=False)
    referral_system_code = Column(String(45), nullable=False, server_default=text("''"))
    referral_column_mapping = Column(String(255), nullable=False, server_default=text("''"))
    active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    hidden = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class SITEURL(Base):
    __tablename__ = 'SITE_URL'

    content_id = Column(INTEGER(10), primary_key=True, nullable=False, server_default=text("'0'"))
    inventory_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True, server_default=text("'0'"))
    stloc_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True, server_default=text("'0'"))
    request_url = Column(String(255), nullable=False, unique=True, server_default=text("''"))
    url_active = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    expiry = Column(INTEGER(11))
    clear_cache = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class STORDERADMIN(Base):
    __tablename__ = 'STORDERADMINS'

    storder_id = Column(INTEGER(10), primary_key=True)
    admin = Column(String(25), nullable=False, index=True)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class STORDERLINE(Base):
    __tablename__ = 'STORDERLINES'

    storder_id = Column(INTEGER(10), primary_key=True, nullable=False)
    counter = Column(TINYINT(4), primary_key=True, nullable=False, server_default=text("'0'"))
    SKU = Column(String(25), index=True)
    quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    iskitting = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    description = Column(String(255))
    itm_serial = Column(String(50), nullable=False, index=True, server_default=text("''"))
    supplier = Column(String(50), index=True)
    dropshipper = Column(String(25), nullable=False, index=True)
    warehouse_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    extra = Column(MEDIUMTEXT, nullable=False)
    repeat_type = Column(Enum('', 'once', 'always'), nullable=False, server_default=text("''"))
    editable = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    unit_price = Column(DECIMAL(13, 3), nullable=False, server_default=text("'0.000'"))
    discount_percent = Column(DECIMAL(5, 2), nullable=False, server_default=text("'0.00'"))
    product_discount = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    notax = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    aff_id = Column(String(25), nullable=False)
    aff_ref = Column(String(50), nullable=False)
    job_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    itemnotes = Column(String(255))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class STORDERLINESPLIT(Base):
    __tablename__ = 'STORDERLINESPLITS'

    storder_id = Column(INTEGER(10), primary_key=True, nullable=False)
    counter = Column(TINYINT(4), primary_key=True, nullable=False)
    SKU = Column(String(25), primary_key=True, nullable=False, index=True)
    description = Column(String(255), nullable=False, server_default=text("''"))
    quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    assemble_quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    assemble_subtotal = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    assemble_notax = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    total_cal = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    itm_price = Column(DECIMAL(12, 2))


class STORDER(Base):
    __tablename__ = 'STORDERS'

    storder_id = Column(INTEGER(10), primary_key=True)
    parent_storder_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    customer_po = Column(String(50), nullable=False, index=True)
    storder_status_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    req_inactive = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    usr_group = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    order_plan_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    date_updated = Column(DateTime, nullable=False)
    date_placed = Column(DateTime, nullable=False, index=True)
    date_agreed = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_stopped = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_completed = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_start = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    date_next = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    date_delay = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_delay_fm = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    rec_type = Column(Enum('day', 'week', 'month', 'year'), nullable=False, server_default=text("'month'"))
    rec_every = Column(TINYINT(3), nullable=False, server_default=text("'1'"))
    rec_day = Column(INTEGER(10), nullable=False, server_default=text("'1'"))
    order_total = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    order_count = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    ff_amount = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    ff_count = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    ff_date = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    auto_complete = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    username = Column(String(25), nullable=False, index=True, server_default=text("''"))
    email = Column(String(50))
    bill_first_name = Column(String(50))
    bill_last_name = Column(String(50))
    bill_company = Column(String(50))
    bill_street1 = Column(String(50))
    bill_street2 = Column(String(50))
    bill_city = Column(String(50))
    bill_state = Column(String(50))
    bill_zip = Column(String(15))
    bill_country = Column(String(50))
    phone = Column(String(30))
    fax = Column(String(30))
    ship_first_name = Column(String(50))
    ship_last_name = Column(String(50))
    ship_company = Column(String(50))
    ship_street1 = Column(String(50))
    ship_street2 = Column(String(50))
    ship_city = Column(String(50))
    ship_state = Column(String(50))
    ship_zip = Column(String(15))
    ship_country = Column(String(50))
    ship_phone = Column(String(30))
    ship_fax = Column(String(30), nullable=False, server_default=text("''"))
    card_holder = Column(String(75))
    card_type = Column(INTEGER(10), index=True)
    cc_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    card_number = Column(String(30))
    card_exp_month = Column(TINYINT(3))
    card_exp_year = Column(INTEGER(10))
    card_client_id = Column(String(20))
    card_rec_type = Column(Enum('once', 'day', 'week', 'month', 'year'), nullable=False, server_default=text("'once'"))
    card_rec_every = Column(TINYINT(3), nullable=False, server_default=text("'1'"))
    card_rec_day = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    card_rec_div = Column(TINYINT(3), nullable=False, server_default=text("'1'"))
    realtime_confirmation = Column(String(50))
    terms = Column(String(25), nullable=False, server_default=text("''"))
    status_log = Column(MEDIUMTEXT, nullable=False)
    sh_group_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    shipping_method = Column(String(50), nullable=False, server_default=text("''"))
    ship_signature = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    fix_shipping = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    subtotal = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    shipping_cost = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    shipping_notax = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    tax = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    tax_inc = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    coupondiscount = Column(DECIMAL(8, 2), nullable=False, server_default=text("'0.00'"))
    grand_total = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    currency_code = Column(String(3), nullable=False, index=True, server_default=text("''"))
    couponcode = Column(MEDIUMTEXT)
    special = Column(MEDIUMTEXT)
    notes = Column(MEDIUMTEXT)
    credit_status = Column(Enum('', 'Approved', 'Referred', 'Declined'), nullable=False)
    credit_status_notes = Column(MEDIUMTEXT, nullable=False)
    internal_notes = Column(MEDIUMTEXT, nullable=False)
    operator = Column(String(25), nullable=False, index=True, server_default=text("''"))
    salesperson = Column(String(25), nullable=False, index=True, server_default=text("''"))
    customer_ref1 = Column(String(50), nullable=False, server_default=text("''"))
    customer_ref2 = Column(String(50), nullable=False, server_default=text("''"))
    customer_ref3 = Column(String(1000), nullable=False, server_default=text("''"))
    customer_ref4 = Column(String(1000), nullable=False, server_default=text("''"))
    customer_ref5 = Column(String(1000), nullable=False, server_default=text("''"))
    customer_ref6 = Column(String(1000), nullable=False, server_default=text("''"))
    customer_ref7 = Column(String(1000), nullable=False, server_default=text("''"))
    customer_ref8 = Column(String(1000), nullable=False, server_default=text("''"))
    customer_ref9 = Column(String(1000), nullable=False, server_default=text("''"))
    customer_ref10 = Column(String(1000), nullable=False, server_default=text("''"))
    fraud_score = Column(DECIMAL(4, 2), nullable=False, server_default=text("'0.00'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class STORDERSTATU(Base):
    __tablename__ = 'STORDERSTATUS'

    storder_status_id = Column(INTEGER(10), primary_key=True)
    storder_status_name = Column(String(50), nullable=False, index=True)
    notification = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    template = Column(String(20), nullable=False)
    orderby = Column(TINYINT(3), nullable=False, server_default=text("'0'"))


class STORELOCATIONCOLUMN(Base):
    __tablename__ = 'STORELOCATION_COLUMNS'

    col_id = Column(String(25), primary_key=True)
    col_name = Column(String(50), nullable=False)
    col_type = Column(Enum('TEXT', 'DATE', 'INT', 'DECIMAL', 'BOOLEAN', 'SHORTTEXT', 'SELECTIONBOX'), nullable=False)
    col_default = Column(String(1500), nullable=False, server_default=text("''"))
    col_req = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    required_field = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    col_comment = Column(String(255), nullable=False)
    col_order = Column(TINYINT(3), nullable=False)


class STORELOCATIONCONTENT(Base):
    __tablename__ = 'STORELOCATION_CONTENTS'

    stloc_id = Column(INTEGER(10), primary_key=True, nullable=False)
    content_id = Column(INTEGER(10), primary_key=True, nullable=False)


class STREETTYPE(Base):
    __tablename__ = 'STREETTYPES'

    streettype = Column(String(10), primary_key=True)
    streettype_name = Column(String(20), nullable=False, server_default=text("''"))


class SUBSCRIBERSCOLUMNSPROGRAM(Base):
    __tablename__ = 'SUBSCRIBERS_COLUMNS_PROGRAM'

    program_id = Column(TINYINT(3), primary_key=True, nullable=False)
    column_id = Column(TINYINT(3), primary_key=True, nullable=False)
    remote_column_name = Column(String(80), nullable=False)
    remote_column_id = Column(String(45), nullable=False)


class SUBSCRIBERSCOLUMNSUSER(Base):
    __tablename__ = 'SUBSCRIBERS_COLUMNS_USER'

    column_id = Column(TINYINT(3), primary_key=True)
    column_title = Column(String(45), nullable=False)
    user_column_name = Column(String(45), nullable=False)
    column_type = Column(String(20), nullable=False)
    ts = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class SUBSCRIBERSLIST(Base):
    __tablename__ = 'SUBSCRIBERS_LISTS'

    list_id = Column(INTEGER(10), primary_key=True)
    program_id = Column(INTEGER(10), nullable=False)
    remote_list_id = Column(INTEGER(10))
    src = Column(Enum('neto', 'remote'), nullable=False, server_default=text("'neto'"))
    list_name = Column(String(100), nullable=False)
    list_description = Column(Text, nullable=False)
    list_email = Column(String(100), nullable=False)
    list_type = Column(String(100), nullable=False)
    subscribers_allow = Column(Enum('default', 'noncust', 'nonuser', 'user', 'manual'), nullable=False, server_default=text("'default'"))
    list_active = Column(Enum('n', 'y', 'd', 'r'), nullable=False, server_default=text("'y'"), comment='d=to delete; r=to re-enable')
    list_warning = Column(Enum('', 'inactive', 'missing'), nullable=False, server_default=text("''"), comment='set when list is deleted on remote site')
    schedule_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    schedule_type = Column(Enum('external', 'schedule', 'once'), nullable=False, server_default=text("'external'"))
    list_filter = Column(Text, nullable=False)
    list_param = Column(Text, nullable=False)
    api_param = Column(Text, nullable=False)
    date_created = Column(DateTime, nullable=False)
    date_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    date_sync = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))


class SUBSCRIBERSLOG(Base):
    __tablename__ = 'SUBSCRIBERS_LOG'

    log_id = Column(INTEGER(10), primary_key=True)
    list_id = Column(INTEGER(10), nullable=False)
    subscriber_id = Column(INTEGER(10), nullable=False)
    log_type = Column(String(45), nullable=False)
    log_msg = Column(Text, nullable=False)
    ts = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class SUBSCRIBERSPROGRAM(Base):
    __tablename__ = 'SUBSCRIBERS_PROGRAMS'

    program_id = Column(TINYINT(3), primary_key=True)
    program_module = Column(String(45), nullable=False)
    program_library = Column(String(45), nullable=False)
    program_name = Column(String(45), nullable=False)
    program_description = Column(String(255), nullable=False)
    program_external_sync = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    program_visible = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))


class SUBSCRIBERSSUBSCRIBER(Base):
    __tablename__ = 'SUBSCRIBERS_SUBSCRIBERS'

    subscriber_id = Column(INTEGER(10), primary_key=True)
    email = Column(String(255), nullable=False, unique=True)
    username = Column(String(25), nullable=False)
    opt_in_newsletter = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    opt_in_notification = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    active = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    unsub_check = Column(String(16), nullable=False, server_default=text("''"))
    date_created = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    date_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    date_sync = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    misc1 = Column(String(255), nullable=False, server_default=text("''"))
    misc2 = Column(String(255), nullable=False, server_default=text("''"))
    misc3 = Column(String(255), nullable=False, server_default=text("''"))
    misc4 = Column(String(255), nullable=False, server_default=text("''"))
    misc5 = Column(String(255), nullable=False, server_default=text("''"))
    misc6 = Column(String(255), nullable=False, server_default=text("''"))
    misc7 = Column(String(255), nullable=False, server_default=text("''"))
    misc8 = Column(String(255), nullable=False, server_default=text("''"))
    misc9 = Column(String(255), nullable=False, server_default=text("''"))
    misc10 = Column(String(255), nullable=False, server_default=text("''"))
    misc11 = Column(String(255), nullable=False, server_default=text("''"))
    misc12 = Column(String(255), nullable=False, server_default=text("''"))
    misc13 = Column(String(255), nullable=False, server_default=text("''"))
    misc14 = Column(String(255), nullable=False, server_default=text("''"))
    misc15 = Column(String(255), nullable=False, server_default=text("''"))
    misc16 = Column(String(255), nullable=False, server_default=text("''"))
    misc17 = Column(String(255), nullable=False, server_default=text("''"))
    misc18 = Column(String(255), nullable=False, server_default=text("''"))
    misc19 = Column(String(255), nullable=False, server_default=text("''"))
    misc20 = Column(String(255), nullable=False, server_default=text("''"))
    misc21 = Column(String(255), nullable=False, server_default=text("''"))
    misc22 = Column(String(255), nullable=False, server_default=text("''"))
    misc23 = Column(String(255), nullable=False, server_default=text("''"))
    misc24 = Column(String(255), nullable=False, server_default=text("''"))
    misc25 = Column(String(255), nullable=False, server_default=text("''"))
    misc26 = Column(String(255), nullable=False, server_default=text("''"))
    misc27 = Column(String(255), nullable=False, server_default=text("''"))
    misc28 = Column(String(255), nullable=False, server_default=text("''"))
    misc29 = Column(String(255), nullable=False, server_default=text("''"))
    misc30 = Column(String(255), nullable=False, server_default=text("''"))


class SUBSCRIBERSSUBSCRIBERLIST(Base):
    __tablename__ = 'SUBSCRIBERS_SUBSCRIBER_LIST'

    subscriber_id = Column(INTEGER(10), primary_key=True, nullable=False)
    list_id = Column(INTEGER(10), primary_key=True, nullable=False)
    remote_id = Column(INTEGER(11), nullable=False, index=True, server_default=text("'0'"))
    opt_in = Column(Enum('n', 'y', 'x'), nullable=False, server_default=text("'y'"))
    active = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    date_created = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    date_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    date_sync = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_checked = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))


class SUBSCRIBERSWSLOG(Base):
    __tablename__ = 'SUBSCRIBERS_WS_LOG'

    log_id = Column(INTEGER(10), primary_key=True)
    program_id = Column(TINYINT(3), nullable=False)
    ws_dir = Column(Enum('in', 'out'), nullable=False, server_default=text("'out'"))
    request_url = Column(String(255), nullable=False)
    request_header = Column(Text, nullable=False)
    request_content = Column(MEDIUMTEXT, nullable=False)
    response_header = Column(Text, nullable=False)
    response_content = Column(MEDIUMTEXT, nullable=False)
    error_content = Column(Text, nullable=False)
    success = Column(Enum('p', 'y', 'n'), nullable=False, server_default=text("'p'"))
    ping = Column(DECIMAL(8, 3), nullable=False)
    ts = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class SUPPLIERSBROCHUREUPDATE(Base):
    __tablename__ = 'SUPPLIERS_BROCHURE_UPDATES'

    SKU = Column(String(25), primary_key=True, server_default=text("''"))
    date_update = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_proc = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    brochure_url = Column(String(2083), nullable=False, server_default=text("''"))
    message = Column(MEDIUMTEXT, nullable=False)
    error = Column(MEDIUMTEXT, nullable=False)
    warning = Column(MEDIUMTEXT, nullable=False)


class SUPPLIERSIMAGEUPDATE(Base):
    __tablename__ = 'SUPPLIERS_IMAGE_UPDATES'

    SKU = Column(String(25), primary_key=True, server_default=text("''"))
    date_update = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_proc = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    image_url = Column(String(2083), nullable=False, server_default=text("''"))
    message = Column(MEDIUMTEXT, nullable=False)
    error = Column(MEDIUMTEXT, nullable=False)
    warning = Column(MEDIUMTEXT, nullable=False)


class SUPPLIERDEFAULTCATMAP(Base):
    __tablename__ = 'SUPPLIER_DEFAULT_CATMAP'

    name = Column(INTEGER(10), primary_key=True)
    mapping = Column(MEDIUMTEXT, nullable=False)


class SUPPLIERDEFAULTEXPORT(Base):
    __tablename__ = 'SUPPLIER_DEFAULT_EXPORT'

    id = Column(String(20), primary_key=True, server_default=text("''"))
    type = Column(String(50), nullable=False, server_default=text("''"))
    header = Column(MEDIUMTEXT, nullable=False)
    body = Column(MEDIUMTEXT, nullable=False)
    footer = Column(MEDIUMTEXT, nullable=False)
    filter = Column(MEDIUMTEXT, nullable=False)
    replaces = Column(MEDIUMTEXT, nullable=False)
    filename = Column(String(50), nullable=False, server_default=text("''"))
    fileformat = Column(String(10), nullable=False, server_default=text("''"))


class SUPPLIERDEFAULTFEED(Base):
    __tablename__ = 'SUPPLIER_DEFAULT_FEED'

    id = Column(INTEGER(10), primary_key=True)
    name = Column(String(45), nullable=False, server_default=text("''"))
    aliases = Column(MEDIUMTEXT)
    setting = Column(MEDIUMTEXT)
    catmap = Column(String(20))
    final = Column(MEDIUMTEXT)
    type = Column(String(20), nullable=False, server_default=text("''"))
    source = Column(String(4), nullable=False, server_default=text("''"))
    driver = Column(String(50), nullable=False, server_default=text("''"))


class SUPPLIERDEFAULTNAMEMAP(Base):
    __tablename__ = 'SUPPLIER_DEFAULT_NAMEMAP'

    name = Column(String(20), primary_key=True, server_default=text("''"))
    mapping = Column(MEDIUMTEXT, nullable=False)
    default = Column(MEDIUMTEXT, nullable=False)


class SUPPLIEREXPORT(Base):
    __tablename__ = 'SUPPLIER_EXPORT'

    id = Column(String(20), primary_key=True, nullable=False, server_default=text("''"))
    supplier = Column(String(50), primary_key=True, nullable=False, server_default=text("''"))
    header = Column(MEDIUMTEXT, nullable=False)
    body = Column(MEDIUMTEXT, nullable=False)
    footer = Column(MEDIUMTEXT, nullable=False)
    filter = Column(MEDIUMTEXT, nullable=False)
    fileformat = Column(String(10), nullable=False, server_default=text("''"))
    filename = Column(String(50), nullable=False, server_default=text("''"))
    setting = Column(MEDIUMTEXT, nullable=False)
    type = Column(String(50), primary_key=True, nullable=False, server_default=text("''"))
    url = Column(String(50), nullable=False, index=True, server_default=text("''"))
    replaces = Column(MEDIUMTEXT, nullable=False)


class SUPPLIERFEEDCATMAP(Base):
    __tablename__ = 'SUPPLIER_FEED_CATMAP'

    supplier = Column(String(50), primary_key=True, nullable=False, server_default=text("''"))
    mapping = Column(MEDIUMTEXT, nullable=False)
    name = Column(String(20), primary_key=True, nullable=False, server_default=text("''"))


class SUPPLIERFEEDNAMEMAP(Base):
    __tablename__ = 'SUPPLIER_FEED_NAMEMAP'

    supplier = Column(String(50), primary_key=True, nullable=False, server_default=text("''"))
    name = Column(String(20), primary_key=True, nullable=False, server_default=text("''"))
    mapping = Column(MEDIUMTEXT, nullable=False)
    default = Column(MEDIUMTEXT, nullable=False)


class SUPPLIERSHIPPINGMETHOD(Base):
    __tablename__ = 'SUPPLIER_SHIPPING_METHODS'

    shipping_ref = Column(INTEGER(10), primary_key=True)
    supplier = Column(String(50), nullable=False, index=True, server_default=text("''"))
    method_name = Column(String(50), nullable=False, index=True, server_default=text("''"))
    type = Column(CHAR(1), nullable=False, index=True, server_default=text("''"), comment='F=Flat Rate, W= Weight')
    description = Column(MEDIUMTEXT, nullable=False)
    tracking_url = Column(String(255), nullable=False, server_default=text("''"), comment='Tracking URL')
    shrate_module = Column(String(20), nullable=False)
    shrateacc_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    realtime_backup_shipping_ref = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    origin_type = Column(Enum('', 'business', 'residence'), nullable=False, server_default=text("''"))
    destination_type = Column(Enum('', 'business', 'residence'), nullable=False, server_default=text("''"))
    ship_package_type = Column(String(25), nullable=False, server_default=text("''"))
    ship_load_type = Column(String(25), nullable=False, server_default=text("''"))
    from_postcode = Column(String(15), nullable=False)
    from_city = Column(String(50), nullable=False)
    from_state = Column(String(50), nullable=False)
    from_country = Column(CHAR(2), nullable=False, server_default=text("''"))
    shrate_account = Column(String(50), nullable=False)
    shrate_code = Column(String(200), nullable=False)
    thirdparty_name = Column(String(45))
    min_cost = Column(DECIMAL(8, 2), nullable=False, server_default=text("'0.00'"))
    max_cost = Column(DECIMAL(8, 2))
    include_perkg = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    apply_all = Column(TINYINT(1), comment='Apply To All Products')
    cubic_modifier = Column(DECIMAL(8, 3), comment='Cubic Weight Modifier')
    package_allowance = Column(DECIMAL(6, 3), nullable=False, server_default=text("'0.000'"), comment='Package Allowance')
    package_allowance_fix = Column(DECIMAL(10, 4), nullable=False, server_default=text("'0.0000'"))
    shipping_levy = Column(DECIMAL(6, 3), nullable=False, server_default=text("'0.000'"))
    shipping_levy_fix = Column(DECIMAL(10, 2), nullable=False, server_default=text("'0.00'"))
    item_handling = Column(DECIMAL(8, 2), nullable=False, server_default=text("'0.00'"))
    min_insure_subtotal = Column(DECIMAL(10, 2), nullable=False, server_default=text("'0.00'"))
    max_insure_subtotal = Column(DECIMAL(10, 2), nullable=False, server_default=text("'0.00'"))
    insure_percent = Column(DECIMAL(4, 1), nullable=False, server_default=text("'0.0'"))
    cus_insure_percent = Column(DECIMAL(4, 1), nullable=False, server_default=text("'0.0'"))
    max_length = Column(DECIMAL(8, 3), comment='Maximum Length')
    round_up_weight = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    use_cubic_only = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    ship_pobox = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    tax_inc = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    min_charge_per_zone = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    api_breakdown_weight = Column(DECIMAL(10, 2))
    auto_dimension = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class SUPPLIERSHIPPINGRATE(Base):
    __tablename__ = 'SUPPLIER_SHIPPING_RATE'
    __table_args__ = (
        Index('shipping_ref', 'shipping_ref', 'zone_id', 'min_weight', unique=True),
    )

    id = Column(INTEGER(11), primary_key=True)
    shipping_ref = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    zone_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    min_weight = Column(DECIMAL(8, 3), nullable=False, server_default=text("'0.000'"), comment='kg')
    max_weight = Column(DECIMAL(8, 3), comment='kg')
    add_weight = Column(DECIMAL(8, 3), nullable=False, server_default=text("'0.000'"), comment='kg')
    price_1st_parcel = Column(DECIMAL(10, 2), nullable=False, server_default=text("'0.00'"))
    price_sub_parcel = Column(DECIMAL(10, 2))
    price_per_kg = Column(DECIMAL(10, 2), comment='kg')
    min_charge = Column(DECIMAL(10, 2), nullable=False, server_default=text("'0.00'"))
    delivery_time = Column(INTEGER(10))
    region_name = Column(String(50), nullable=False, server_default=text("''"))


class SUPPORTWSLOG(Base):
    __tablename__ = 'SUPPORT_WS_LOG'

    id = Column(INTEGER(10), primary_key=True)
    module = Column(String(25), nullable=False, index=True)
    msg_type = Column(Enum('info', 'warn', 'error'), nullable=False)
    msg = Column(MEDIUMTEXT, nullable=False)
    remote_ip = Column(String(50), nullable=False)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class SYSTEMINFO(Base):
    __tablename__ = 'SYSTEMINFO'

    syskey = Column(String(50), primary_key=True, server_default=text("''"))
    sysval = Column(String(255), nullable=False, server_default=text("''"))
    sysdesc = Column(String(255), nullable=False, server_default=text("''"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class SYSTEMNOTIFICATION(Base):
    __tablename__ = 'SYSTEM_NOTIFICATIONS'

    ntf_id = Column(INTEGER(10), primary_key=True)
    ntf_type = Column(Enum('info', 'msg', 'warn', 'error'), nullable=False, server_default=text("'info'"))
    ntf_msg = Column(String(255), nullable=False, server_default=text("''"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class TASKSCHEDULE(Base):
    __tablename__ = 'TASKSCHEDULES'

    schedule_id = Column(INTEGER(10), primary_key=True)
    schedule_name = Column(String(50), nullable=False, server_default=text("''"))
    schedule_month = Column(SMALLINT(5), nullable=False, server_default=text("'0'"))
    schedule_mday = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    schedule_wday = Column(TINYINT(3), nullable=False, server_default=text("'0'"))
    schedule_hour = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    schedule_minute = Column(INTEGER(10), nullable=False)


class TAXBREAKDOWN(Base):
    __tablename__ = 'TAX_BREAKDOWNS'
    __table_args__ = (
        Index('type_identifier', 'type', 'identifier', unique=True),
    )

    tax_breakdown_id = Column(INTEGER(11), primary_key=True)
    type = Column(String(15), nullable=False, comment='This can be order / orderline / shipping / etc')
    identifier = Column(INTEGER(11), nullable=False, comment='Links to the other tables id (i.e. order_record_id / orderline_id)')
    country_jurisdiction = Column(String(255))
    state_jurisdiction = Column(String(255))
    state_taxable_amount = Column(DECIMAL(12, 2))
    state_tax_rate = Column(DECIMAL(8, 5))
    state_tax_collectable = Column(DECIMAL(12, 2))
    county_jurisdiction = Column(String(255))
    county_taxable_amount = Column(DECIMAL(12, 2))
    county_tax_rate = Column(DECIMAL(8, 5))
    county_tax_collectable = Column(DECIMAL(12, 2))
    city_jurisdiction = Column(String(255))
    city_taxable_amount = Column(DECIMAL(12, 2))
    city_tax_rate = Column(DECIMAL(8, 5))
    city_tax_collectable = Column(DECIMAL(12, 2))
    district_jurisdiction = Column(String(255))
    district_taxable_amount = Column(DECIMAL(12, 2))
    district_tax_rate = Column(DECIMAL(8, 5))
    district_tax_collectable = Column(DECIMAL(12, 2))


class UNITMEASURE(Base):
    __tablename__ = 'UNITMEASURES'

    unit = Column(String(10), primary_key=True)
    unit_name = Column(String(25), nullable=False)
    unit_class = Column(String(15), nullable=False)


class UPCCOUNTER(Base):
    __tablename__ = 'UPCCOUNTER'

    upc = Column(INTEGER(10), primary_key=True)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class URLINFO(Base):
    __tablename__ = 'URL_INFO'

    token_id = Column(INTEGER(10), primary_key=True, nullable=False)
    doc_id = Column(String(25), primary_key=True, nullable=False)
    page_title = Column(String(100), nullable=False)
    meta_keywords = Column(String(255), nullable=False)
    meta_description = Column(String(320), nullable=False)
    page_heading = Column(String(100), nullable=False, server_default=text("''"))
    canonical_url = Column(String(2083), nullable=False, server_default=text("''"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class URLTOKEN(Base):
    __tablename__ = 'URL_TOKEN'

    token_id = Column(INTEGER(10), primary_key=True)
    token_index = Column(String(20), nullable=False, unique=True, server_default=text("''"))
    old_token_index = Column(String(20), nullable=False, index=True, server_default=text("''"))
    token_name = Column(String(20), nullable=False, unique=True, server_default=text("''"))
    template_dir = Column(String(20), nullable=False, server_default=text("''"))


class USERCOUNTER(Base):
    __tablename__ = 'USERCOUNTER'

    counter = Column(INTEGER(11), primary_key=True)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class USER(Base):
    __tablename__ = 'USERS'

    user_id = Column(INTEGER(10), primary_key=True)
    username = Column(String(25), nullable=False, unique=True, comment='Username')
    parent_username = Column(String(25), nullable=False, index=True)
    is_prospect = Column(Enum('n', 'y'), nullable=False, index=True, server_default=text("'n'"), comment='Prospect')
    password = Column(String(255), nullable=False, comment='Password')
    email = Column(String(255), index=True)
    secondary_email = Column(String(255), nullable=False, server_default=text("''"))
    opt_in_newsletter = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"), comment='Email Subscription')
    opt_in_notification = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    opt_in_live_chat = Column(Enum('y', 'n'), nullable=False, server_default=text("'y'"))
    phone = Column(String(30), nullable=False, comment='Billing Phone')
    fax = Column(String(30), nullable=False, comment='Billing Fax')
    bill_first_name = Column(String(50), nullable=False, comment='Billing First Name')
    bill_last_name = Column(String(50), nullable=False, comment='Billing Last Name')
    bill_company = Column(String(50), nullable=False, comment='Billing Company')
    bill_street1 = Column(String(50), nullable=False, comment='Billing Street Line 1')
    bill_street2 = Column(String(50), nullable=False, comment='Billing Street Line 2')
    bill_city = Column(String(50), nullable=False, comment='Billing Suburb')
    bill_state = Column(String(50), nullable=False, comment='Billing State')
    bill_zip = Column(String(15), nullable=False, comment='Billing Postal Code')
    bill_country = Column(CHAR(2), nullable=False, server_default=text("''"), comment='Billing Country')
    reside_middle_name = Column(String(50), nullable=False, server_default=text("''"), comment='Residential Middle Name')
    reside_unit_num = Column(String(15), nullable=False, server_default=text("''"), comment='Residential Unit Number')
    reside_property1 = Column(String(50), nullable=False, server_default=text("''"), comment='Residential Building/Property Name 1')
    reside_property2 = Column(String(50), nullable=False, server_default=text("''"), comment='Residential Building/Property Name 2')
    reside_street_num = Column(String(12), nullable=False, server_default=text("''"), comment='Residential Street Number')
    reside_street = Column(String(30), nullable=False, server_default=text("''"), comment='Residential Street')
    reside_streettype = Column(String(10), nullable=False, server_default=text("''"), comment='Residential Street Type')
    reside_city = Column(String(50), nullable=False, server_default=text("''"), comment='Residential Suburb')
    reside_state = Column(String(50), nullable=False, server_default=text("''"), comment='Residential State')
    reside_zip = Column(String(15), nullable=False, server_default=text("''"), comment='Residential Postal Code')
    reside_country = Column(CHAR(2), nullable=False, server_default=text("''"), comment='Residential Country')
    gender = Column(Enum('', 'M', 'F', 'U'), nullable=False, server_default=text("''"), comment='Gender')
    date_of_birth = Column(Date, nullable=False, server_default=text("'0000-00-00'"), comment='Date Of Birth')
    personal_id_type = Column(String(20), nullable=False, server_default=text("''"), comment='Personal Identification Type')
    personal_identification = Column(String(25), nullable=False, server_default=text("''"), comment='Personal Identification Numbers')
    default_discounts = Column(DECIMAL(5, 2), nullable=False, server_default=text("'0.00'"), comment='Default Customer Discounts')
    doctmpl_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    card_holder = Column(String(50), comment='CC Holder Name')
    card_type = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='Payment Method')
    cc_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='Default Stored Credit Card')
    card_number = Column(String(30), comment='CC Number')
    card_exp_month = Column(TINYINT(3), comment='CC Expire Month')
    card_exp_year = Column(INTEGER(10), comment='CC Expire Year')
    card_client_id = Column(String(20), comment='CC CCID')
    check_routing = Column(String(50), comment='Bank Account Name')
    check_account = Column(String(30), comment='Bank Account Number (BSB-ACC)')
    registration_date = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"), comment='Registration Date')
    notes = Column(MEDIUMTEXT, comment='Internal Notes')
    usercustom1 = Column(String(255), nullable=False, comment='++')
    usercustom2 = Column(String(255), nullable=False, comment='ABN/ACN')
    usercustom3 = Column(String(255), nullable=False, comment='URL')
    usercustom4 = Column(String(255), nullable=False, comment='++')
    usercustom5 = Column(String(255), nullable=False, comment='++')
    usercustom6 = Column(String(255), nullable=False, comment='++')
    usercustom7 = Column(String(255), nullable=False, comment='++')
    usercustom8 = Column(DECIMAL(8, 2), nullable=False, server_default=text("'0.00'"), comment='Credit Limit')
    usercustom9 = Column(String(25), nullable=False, index=True, comment='Payment Terms')
    usercustom10 = Column(String(255), nullable=False, server_default=text("''"))
    usercustom11 = Column(String(255), nullable=False, comment='++')
    usercustom12 = Column(String(255), nullable=False, comment='++')
    usercustom13 = Column(String(255), nullable=False, comment='++')
    usercustom14 = Column(String(255), nullable=False, comment='++')
    usercustom15 = Column(String(255), nullable=False, comment='++')
    usercustom16 = Column(String(255), nullable=False, comment='++')
    usercustom17 = Column(String(255), nullable=False, comment='++')
    usercustom18 = Column(String(255), nullable=False, comment='++')
    usercustom19 = Column(String(255), nullable=False, comment='++')
    usercustom20 = Column(String(255), nullable=False, comment='++')
    usercustom21 = Column(String(255), nullable=False, comment='++')
    usercustom22 = Column(String(255), nullable=False, comment='++')
    usercustom23 = Column(String(255), nullable=False, comment='++')
    usercustom24 = Column(String(255), nullable=False, comment='++')
    usercustom25 = Column(String(255), nullable=False, comment='++')
    usercustom26 = Column(String(255), nullable=False, server_default=text("''"))
    usercustom27 = Column(String(255), nullable=False, server_default=text("''"))
    usercustom28 = Column(String(255), nullable=False, server_default=text("''"))
    usercustom29 = Column(String(255), nullable=False, server_default=text("''"))
    usercustom30 = Column(String(255), nullable=False, server_default=text("''"))
    usercustom31 = Column(String(255), nullable=False, server_default=text("''"))
    usercustom32 = Column(String(255), nullable=False, server_default=text("''"))
    usercustom33 = Column(String(255), nullable=False, server_default=text("''"))
    usercustom34 = Column(String(255), nullable=False, server_default=text("''"))
    usercustom35 = Column(String(255), nullable=False, server_default=text("''"))
    usercustom36 = Column(String(255), nullable=False, server_default=text("''"))
    usercustom37 = Column(String(255), nullable=False, server_default=text("''"))
    usercustom38 = Column(String(255), nullable=False, server_default=text("''"))
    usercustom39 = Column(String(255), nullable=False, server_default=text("''"))
    usercustom40 = Column(String(255), nullable=False, server_default=text("''"))
    usercustom41 = Column(String(255), nullable=False, server_default=text("''"))
    usercustom42 = Column(String(255), nullable=False, server_default=text("''"))
    usercustom43 = Column(String(255), nullable=False, server_default=text("''"))
    usercustom44 = Column(String(255), nullable=False, server_default=text("''"))
    usercustom45 = Column(String(255), nullable=False, server_default=text("''"))
    usercustom46 = Column(String(255), nullable=False, server_default=text("''"))
    usercustom47 = Column(String(255), nullable=False, server_default=text("''"))
    usercustom48 = Column(String(255), nullable=False, server_default=text("''"))
    usercustom49 = Column(String(255), nullable=False, server_default=text("''"))
    usercustom50 = Column(String(255), nullable=False, server_default=text("''"))
    usercustom51 = Column(String(255), nullable=False, server_default=text("''"))
    usercustom52 = Column(String(255), nullable=False, server_default=text("''"))
    usercustom53 = Column(String(255), nullable=False, server_default=text("''"))
    usercustom54 = Column(String(255), nullable=False, server_default=text("''"))
    classification1 = Column(String(20), nullable=False, index=True, comment='User Classification 1')
    classification2 = Column(String(20), nullable=False, index=True, comment='User Classification 2')
    addr_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"), comment='Default Shipping Address')
    active = Column(Enum('N', 'Y'), nullable=False, server_default=text("'Y'"), comment='Active')
    credit_onhold = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"), comment='Credit Onhold')
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"), comment='-')
    group_id = Column(INTEGER(11), nullable=False, index=True, server_default=text("'0'"), comment='User Group')
    balance = Column(DECIMAL(14, 2), nullable=False, server_default=text("'0.00'"), comment='Outstanding Account Balance')
    credit = Column(DECIMAL(14, 2), nullable=False, server_default=text("'0.00'"), comment='Account Credit')
    sales_agent = Column(String(25), nullable=False, index=True, comment='Account Manager')
    approval_username = Column(String(25), nullable=False, index=True, server_default=text("''"))
    referral_username = Column(String(25), nullable=False, index=True, server_default=text("''"))
    sales_commission_percent = Column(DECIMAL(5, 2), nullable=False, server_default=text("'0.00'"))
    sales_channel = Column(String(15), nullable=False, index=True, server_default=text("''"))
    order_comment = Column(MEDIUMTEXT, nullable=False, comment='Order Comment')
    def_order_type = Column(Enum('sales', 'dropshipping'), nullable=False, server_default=text("'sales'"), comment='Default Order Type')
    order_limit = Column(Enum('', 'Quote', 'Admin', 'POS', 'Store'), nullable=False, server_default=text("''"))
    failed_password_attempt = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    auto_statement = Column(Enum('y', 'n'), nullable=False, server_default=text("'y'"))
    is_neto_utility = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    date_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    last_updated = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_exported = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_exported_mail = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    is_vip = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class USERSALESCACH(Base):
    __tablename__ = 'USERSALES_CACHES'

    username = Column(String(25), primary_key=True)
    first_sales = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    last_sales = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    last_sales_since = Column(INTEGER(10), nullable=False, server_default=text("'-1'"))
    first_order = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    last_order = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    last_order_since = Column(INTEGER(10), nullable=False, server_default=text("'-1'"))
    sales_ytd = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    sales_last_ytd = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    sales_last_12mon = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    monthly_target = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    rolling_average = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    total_sales = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    invoice_balance = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    total_assets = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    total_assets_own = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    total_assets_rtn = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    last_contacted = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    first_purchase = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    second_purchase = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    third_purchase = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    first_year_total_amount = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    second_year_total_amount = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    third_year_total_amount = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    first_year_total_order_num = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    second_year_total_order_num = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    third_year_total_order_num = Column(INTEGER(10), nullable=False, server_default=text("'0'"))


class USERSGROUP(Base):
    __tablename__ = 'USERS_GROUP'

    group_id = Column(INTEGER(11), primary_key=True)
    group_name = Column(String(50), nullable=False, index=True, server_default=text("''"))
    group_code = Column(String(10), nullable=False, server_default=text("''"))
    group_type = Column(TINYINT(3), nullable=False, server_default=text("'0'"))
    free_shipping = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    min_purchase = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    sales_requirement = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    currency_code = Column(String(3), nullable=False, server_default=text("''"))
    sales_category = Column(String(50), nullable=False, server_default=text("''"))


class USERSLOGIN(Base):
    __tablename__ = 'USERS_LOGIN'

    username = Column(String(25), primary_key=True)
    code = Column(String(25), nullable=False)
    cart_data = Column(MEDIUMTEXT)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class USERBALANCELOG(Base):
    __tablename__ = 'USER_BALANCE_LOG'
    __table_args__ = (
        Index('idx_username_timestamp', 'username', 'timestamp'),
    )

    log_id = Column(INTEGER(10), primary_key=True)
    username = Column(String(25), nullable=False)
    balance = Column(DECIMAL(14, 2), nullable=False)
    trans_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class USERCLASSIFICATION(Base):
    __tablename__ = 'USER_CLASSIFICATIONS'

    classification_num = Column(TINYINT(3), primary_key=True, nullable=False, server_default=text("'1'"))
    classification = Column(String(20), primary_key=True, nullable=False, index=True)
    classification_ref = Column(String(50), nullable=False, server_default=text("''"))
    active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    sortorder = Column(TINYINT(3), nullable=False, server_default=text("'0'"))


class USERCOLUMN(Base):
    __tablename__ = 'USER_COLUMNS'

    col_id = Column(String(25), primary_key=True)
    col_name = Column(String(50), nullable=False)
    col_section = Column(String(45), nullable=False, server_default=text("''"))
    col_type = Column(Enum('TEXT', 'DATE', 'INT', 'DECIMAL', 'BOOLEAN', 'SHORTTEXT', 'SELECTIONBOX'), nullable=False)
    col_max_length = Column(INTEGER(10), nullable=False)
    col_default = Column(Text, nullable=False)
    col_req = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    required_field = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    col_comment = Column(String(255), nullable=False)
    col_order = Column(TINYINT(3), nullable=False)
    visible_on_checkout = Column(Enum('n', 'y'), nullable=False, index=True, server_default=text("'n'"))


class USERCONTACTLOG(Base):
    __tablename__ = 'USER_CONTACT_LOG'

    log_id = Column(INTEGER(10), primary_key=True)
    username = Column(String(25), nullable=False, index=True)
    recontact_username = Column(String(25), index=True)
    ftype_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    customer = Column(String(25), nullable=False, index=True)
    customer_name = Column(String(100), nullable=False)
    date_contact = Column(DateTime, nullable=False)
    date_token = Column(DateTime, nullable=False)
    date_recontact = Column(Date)
    status = Column(Enum('Require Recontact', 'Recontacting', 'Completed'), nullable=False, server_default=text("'Require Recontact'"))
    notes = Column(MEDIUMTEXT)


class USERCREDITACCOUNT(Base):
    __tablename__ = 'USER_CREDIT_ACCOUNTS'

    usrcr_id = Column(INTEGER(10), primary_key=True)
    username = Column(String(25), nullable=False, index=True, server_default=text("''"))
    usrcr_type = Column(Enum('account', 'referral'), nullable=False, index=True, server_default=text("'account'"))
    currency_code = Column(String(3), nullable=False, index=True, server_default=text("''"))
    credit_amount = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class USERCREDITACCOUNTLOG(Base):
    __tablename__ = 'USER_CREDIT_ACCOUNT_LOGS'

    crlog_id = Column(INTEGER(10), primary_key=True)
    username = Column(String(25), nullable=False, index=True, server_default=text("''"))
    usrcr_type = Column(Enum('account', 'referral'), nullable=False, index=True, server_default=text("'account'"))
    currency_code = Column(String(3), nullable=False, index=True, server_default=text("''"))
    crlog_opt = Column(Enum('adjust', 'assign', 'info'), nullable=False, server_default=text("'adjust'"))
    crlog_type = Column(String(20), nullable=False, index=True, server_default=text("''"))
    crlog_ref = Column(String(25), nullable=False, index=True, server_default=text("''"))
    crlog_desc = Column(String(50), nullable=False, server_default=text("''"))
    credit_amount = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class USERREMINDERDAY(Base):
    __tablename__ = 'USER_REMINDER_DAYS'

    reminder_id = Column(INTEGER(10), primary_key=True)
    username = Column(String(25), nullable=False, index=True, server_default=text("''"))
    reminder_date = Column(Date, nullable=False, index=True, server_default=text("'0000-00-00'"))
    reminder_title = Column(String(50), nullable=False, server_default=text("''"))
    reminder_gender = Column(Enum('', 'M', 'F', 'U'), nullable=False, server_default=text("''"))
    reminder_18plus = Column(Enum('', 'n', 'y'), nullable=False, server_default=text("''"))
    next_reminder_date = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    last_reminder_sent = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))


class USERSTORECARD(Base):
    __tablename__ = 'USER_STORECARDS'

    cc_id = Column(INTEGER(10), primary_key=True)
    username = Column(String(25), nullable=False, index=True)
    card_id = Column(INTEGER(10), nullable=False, index=True)
    card_holder = Column(String(50), nullable=False)
    card_number = Column(String(30), nullable=False)
    card_exp_month = Column(TINYINT(3), nullable=False)
    card_exp_year = Column(INTEGER(10), nullable=False)
    card_client_id = Column(String(20), nullable=False, index=True)
    realtime_notes = Column(String(50), nullable=False)
    card_authorisation = Column(String(100), nullable=False)
    payment_notes = Column(MEDIUMTEXT, nullable=False)
    fraud_score = Column(DECIMAL(4, 2), nullable=False, server_default=text("'0.00'"))
    card_temporary = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    card_inuse = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class USERTERM(Base):
    __tablename__ = 'USER_TERMS'

    terms_id = Column(INTEGER(10), primary_key=True)
    terms_code = Column(String(25), nullable=False, index=True, server_default=text("''"))
    terms_name = Column(String(50), nullable=False, server_default=text("''"))
    terms_day = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    terms_type = Column(Enum('Net', 'Prepaid', 'COD'), nullable=False, server_default=text("'Net'"))
    terms_dayopt = Column(Enum('days', 'dom'), nullable=False, server_default=text("'days'"))
    terms_eom = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    sortorder = Column(TINYINT(3), nullable=False, server_default=text("'0'"))


class USERTRANSACTION(Base):
    __tablename__ = 'USER_TRANSACTIONS'

    trans_id = Column(INTEGER(10), primary_key=True)
    trans_type = Column(Enum('T', 'O', 'P', 'R', 'W', 'C'), nullable=False, index=True, server_default=text("'T'"), comment='T=Other; O=Orders; P=Payments; R=RMA/Refunds; W=Withdraws; C=Credit;')
    username = Column(String(25), nullable=False, index=True)
    date_trans = Column(Date, nullable=False, index=True, server_default=text("'0000-00-00'"))
    record_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"), comment='rma_id, payment_id, withdraw_id')
    order_id = Column(String(15), nullable=False, index=True)
    amount = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    credit = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    memo = Column(String(255), nullable=False, server_default=text("''"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class VACATIONDAY(Base):
    __tablename__ = 'VACATION_DAYS'

    vacation_group_id = Column(INTEGER(10), primary_key=True, nullable=False)
    vacation_date = Column(Date, primary_key=True, nullable=False, index=True)
    vacation_name = Column(String(50), nullable=False, server_default=text("''"))


class VACATIONGROUP(Base):
    __tablename__ = 'VACATION_GROUPS'

    vacation_group_id = Column(INTEGER(10), primary_key=True)
    vacation_group_name = Column(String(50), nullable=False, server_default=text("''"))


class VOUCHERCOUNTER(Base):
    __tablename__ = 'VOUCHERCOUNTER'

    counter = Column(INTEGER(10), primary_key=True)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class VOUCHER(Base):
    __tablename__ = 'VOUCHERS'

    voucher_id = Column(INTEGER(10), primary_key=True)
    vprogram_id = Column(INTEGER(10), nullable=False, index=True)
    voucher_title = Column(String(50), nullable=False, server_default=text("''"))
    username = Column(String(25), nullable=False, index=True)
    email = Column(String(50), nullable=False, index=True)
    order_id = Column(String(15), nullable=False, index=True)
    voucher_code = Column(String(25), nullable=False, index=True, server_default=text("''"))
    voucher_secret = Column(String(25), nullable=False, server_default=text("''"))
    voucher_balance = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    voucher_type = Column(Enum('reward', 'gift', 'thirdparty'), nullable=False, server_default=text("'reward'"))
    voucher_once = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    voucher_active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    date_notified = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_created = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class VOUCHERCREDIT(Base):
    __tablename__ = 'VOUCHER_CREDITS'

    vcredit_id = Column(INTEGER(10), primary_key=True)
    voucher_id = Column(INTEGER(10), nullable=False, index=True)
    vprogram_id = Column(INTEGER(10), nullable=False, index=True)
    order_id = Column(String(15), nullable=False, index=True)
    counter = Column(TINYINT(4), nullable=False, server_default=text("'0'"))
    vcredit_amount = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    vcredit_spent = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    vcredit_onhold = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    vcredit_expiry = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_added = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class VOUCHERCREDITSPENT(Base):
    __tablename__ = 'VOUCHER_CREDIT_SPENT'

    vspent_id = Column(INTEGER(10), primary_key=True)
    vcredit_id = Column(INTEGER(10), nullable=False, index=True)
    voucher_id = Column(INTEGER(10), nullable=False, index=True)
    order_id = Column(String(15), nullable=False, index=True)
    payment_id = Column(INTEGER(10), nullable=False, index=True)
    vspent_amount = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    vspent_once = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    date_spent = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))


class VOUCHERMESSAGE(Base):
    __tablename__ = 'VOUCHER_MESSAGES'

    voucher_id = Column(INTEGER(10), primary_key=True)
    vprogram_id = Column(INTEGER(10), nullable=False, index=True)
    order_id = Column(String(15), nullable=False, index=True)
    recipient_email = Column(String(50), nullable=False, index=True, server_default=text("''"))
    recipient_name = Column(String(100), nullable=False, server_default=text("''"))
    sender_email = Column(String(50), nullable=False, server_default=text("''"))
    sender_name = Column(String(100), nullable=False, server_default=text("''"))
    vmsg_message = Column(String(1000), nullable=False, server_default=text("''"))
    vmsg_approved = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    date_required = Column(DateTime, nullable=False, index=True, server_default=text("'0000-00-00 00:00:00'"))
    date_sent = Column(DateTime, nullable=False, index=True, server_default=text("'0000-00-00 00:00:00'"))
    date_error = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_submitted = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_submit_error = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_redeemed = Column(DateTime, nullable=False, index=True, server_default=text("'0000-00-00 00:00:00'"))
    date_created = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class VOUCHERPROGRAM(Base):
    __tablename__ = 'VOUCHER_PROGRAMS'

    vprogram_id = Column(INTEGER(10), primary_key=True)
    vprogram_type = Column(Enum('reward', 'gift', 'thirdparty'), nullable=False, index=True, server_default=text("'reward'"))
    vprogram_title = Column(String(50), nullable=False, server_default=text("''"))
    vprogram_subtitle = Column(String(100), nullable=False, server_default=text("''"))
    vprogram_submit_email = Column(String(50), nullable=False, server_default=text("''"))
    vprogram_description = Column(String(3000), nullable=False, server_default=text("''"))
    vprogram_template = Column(String(25), nullable=False, server_default=text("''"))
    vprogram_pdf_template = Column(String(25), nullable=False, server_default=text("''"))
    vprogram_pdf_size = Column(String(25), nullable=False, server_default=text("''"))
    vprogram_multiplier = Column(INTEGER(10), nullable=False, server_default=text("'1'"))
    vprogram_inc_shipping = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    vprogram_prefix = Column(String(15), nullable=False, server_default=text("''"))
    vprogram_use_start = Column(DateTime, nullable=False, index=True, server_default=text("'0000-00-00 00:00:00'"))
    vprogram_use_end = Column(DateTime, nullable=False, index=True, server_default=text("'0000-00-00 00:00:00'"))
    vprogram_use_days = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    vprogram_earn_start = Column(DateTime, nullable=False, index=True, server_default=text("'0000-00-00 00:00:00'"))
    vprogram_earn_end = Column(DateTime, nullable=False, index=True, server_default=text("'0000-00-00 00:00:00'"))
    vprogram_use_min = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    vprogram_use_max = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    vprogram_use_mod = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    vprogram_use_once = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    vprogram_apply_all = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    vprogram_visible = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    vprogram_active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    vprogram_sortby = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    date_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class VOUCHERPROGRAMGROUP(Base):
    __tablename__ = 'VOUCHER_PROGRAM_GROUPS'

    vprogram_id = Column(INTEGER(10), primary_key=True, nullable=False, server_default=text("'0'"))
    group_id = Column(INTEGER(11), primary_key=True, nullable=False, index=True, server_default=text("'0'"))


class VOUCHERPROGRAMREWARD(Base):
    __tablename__ = 'VOUCHER_PROGRAM_REWARDS'

    vreward_id = Column(INTEGER(10), primary_key=True)
    vprogram_id = Column(INTEGER(10), nullable=False, index=True)
    spent_min = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    earn_per_dollar = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    earn_amount = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    earn_max = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    vreward_earn_start = Column(DateTime, nullable=False, index=True, server_default=text("'0000-00-00 00:00:00'"))
    vreward_earn_end = Column(DateTime, nullable=False, index=True, server_default=text("'0000-00-00 00:00:00'"))


class WAREHOUSELOCATION(Base):
    __tablename__ = 'WAREHOUSELOCATIONS'

    location_id = Column(String(15), primary_key=True)
    location_ref = Column(String(20), nullable=False, server_default=text("''"))
    warehouse_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    type = Column(Enum('normal', 'virtual', 'special'), nullable=False, server_default=text("'normal'"))
    default = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class WAREHOUSELOG(Base):
    __tablename__ = 'WAREHOUSELOG'

    log_id = Column(INTEGER(10), primary_key=True)
    username = Column(String(25), nullable=False, index=True)
    type = Column(String(15), nullable=False, index=True)
    ref = Column(String(25), nullable=False, index=True)
    qty = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    SKU = Column(String(25), nullable=False, index=True)
    location_id = Column(String(50), nullable=False, index=True, server_default=text("''"))
    extra = Column(String(50), nullable=False, server_default=text("''"))
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class WAREHOUSE(Base):
    __tablename__ = 'WAREHOUSES'

    warehouse_id = Column(INTEGER(10), primary_key=True)
    is_dropshipper = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    primary = Column(Enum('n', 'y'), server_default=text("'n'"))
    active = Column(Enum('n', 'y'), server_default=text("'n'"))
    show_qty = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    warehouse_ref = Column(String(25), nullable=False, server_default=text("''"))
    name = Column(String(30))
    street1 = Column(String(50))
    street2 = Column(String(50))
    city = Column(String(50))
    state = Column(String(50))
    zip = Column(String(15))
    country = Column(String(50))
    contact = Column(String(25))
    phone = Column(String(30))
    latitude = Column(DECIMAL(14, 10))
    longitude = Column(DECIMAL(14, 10))
    pickup_instructions = Column(Text)
    operation_hours = Column(Text)
    operation_hours_timezone = Column(String(50))
    note = Column(String(255))
    wh_misc1 = Column(String(255), nullable=False, server_default=text("''"))
    wh_misc2 = Column(String(255), nullable=False, server_default=text("''"))
    wh_misc3 = Column(String(255), nullable=False, server_default=text("''"))
    wh_misc4 = Column(String(255), nullable=False, server_default=text("''"))
    wh_misc5 = Column(String(255), nullable=False, server_default=text("''"))
    wh_misc6 = Column(String(255), nullable=False, server_default=text("''"))
    wh_misc7 = Column(String(255), nullable=False, server_default=text("''"))
    wh_misc8 = Column(String(255), nullable=False, server_default=text("''"))
    wh_misc9 = Column(String(255), nullable=False, server_default=text("''"))
    wh_misc10 = Column(String(255), nullable=False, server_default=text("''"))
    whnotify_active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    whnotify_export_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    whnotify_schedule_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    whnotify_email = Column(String(255), nullable=False, server_default=text("''"))
    whnotify_email_template = Column(String(50), nullable=False, server_default=text("''"))
    whnotify_pdf_template = Column(String(50), nullable=False, server_default=text("''"))
    whemail_uploaded_active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    whemail_uploaded_days = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    whemail_uploaded_template = Column(String(50), nullable=False, server_default=text("''"))
    whemail_uploaded_export_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    whemail_uploaded_schedule_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    whemail_unsold_active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    whemail_unsold_days = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    whemail_unsold_template = Column(String(50), nullable=False, server_default=text("''"))
    whemail_unsold_export_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    whemail_unsold_schedule_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    wh_type = Column(Enum('w', 'o'), nullable=False, server_default=text("'w'"), comment='w = Warehouse, o = Outlet')
    group_id = Column(INTEGER(11))


class WAREHOUSECOLUMN(Base):
    __tablename__ = 'WAREHOUSE_COLUMNS'

    col_id = Column(String(25), primary_key=True)
    col_name = Column(String(50), nullable=False)
    col_type = Column(Enum('TEXT', 'DATE', 'INT', 'DECIMAL', 'BOOLEAN', 'SHORTTEXT', 'SELECTIONBOX'), nullable=False, server_default=text("'TEXT'"))
    col_max_length = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    col_display = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    col_thumb = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    col_filter = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    col_options = Column(MEDIUMTEXT, nullable=False)
    col_comment = Column(String(255), nullable=False)
    col_description = Column(String(100), nullable=False)
    col_order = Column(TINYINT(3), nullable=False)


class WAREHOUSEEXPORT(Base):
    __tablename__ = 'WAREHOUSE_EXPORTS'

    wh_export_id = Column(INTEGER(10), primary_key=True)
    exbatch_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    warehouse_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    wh_export_email = Column(String(255), nullable=False, server_default=text("''"))
    batch_module = Column(String(55), nullable=False, server_default=text("''"))
    date_sent = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_success = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))


class WAREHOUSEEXPORTORDERLINE(Base):
    __tablename__ = 'WAREHOUSE_EXPORT_ORDERLINES'

    wh_export_ln_ids = Column(INTEGER(10), primary_key=True)
    wh_export_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    exbatch_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    order_id = Column(String(15), nullable=False, index=True)
    counter = Column(TINYINT(4), nullable=False, server_default=text("'0'"))
    SKU = Column(String(25), index=True)
    quantity = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    warehouse_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    iskitting = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class WAREHOUSEZONE(Base):
    __tablename__ = 'WAREHOUSE_ZONES'

    warehouse_id = Column(INTEGER(10), primary_key=True, nullable=False, server_default=text("'0'"))
    zone_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True, server_default=text("'0'"))
    priority = Column(INTEGER(10), nullable=False)


class WEBHOOKEVENT(Base):
    __tablename__ = 'WEBHOOK_EVENTS'

    webhook_id = Column(INTEGER(11), primary_key=True)
    webhook_url = Column(String(255), nullable=False, server_default=text("''"))
    webhook_type = Column(String(128), nullable=False, server_default=text("''"), comment='Type of callback')
    webhook_typeid = Column(String(128), nullable=False, server_default=text("''"))
    webhook_changetype = Column(String(128), nullable=False, server_default=text("''"), comment='Change you are sending via the webhook')
    webhook_data = Column(String(128), nullable=False, server_default=text("''"), comment='Text describing change')
    webhook_response = Column(String(1024), nullable=False, server_default=text("''"))
    date_placed = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    date_submitted = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))


class WISHLIST(Base):
    __tablename__ = 'WISHLISTS'

    wishlist_id = Column(INTEGER(10), primary_key=True)
    username = Column(String(25), nullable=False, index=True, server_default=text("''"))
    wishlist_name = Column(String(50), nullable=False, server_default=text("''"))
    wishlist_private = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    wishlist_active = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    notify_purchase = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    reminder_inc_days = Column(TINYINT(4), nullable=False, server_default=text("'0'"))
    date_reminder = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_expiry = Column(Date, nullable=False, server_default=text("'0000-00-00'"))
    date_created = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class WISHLISTITEM(Base):
    __tablename__ = 'WISHLIST_ITEMS'

    wishlistitems_id = Column(INTEGER(10), primary_key=True)
    wishlist_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))
    SKU = Column(String(25), nullable=False, index=True, server_default=text("''"))
    purchased_username = Column(String(25), nullable=False, server_default=text("''"))
    purchased_first_name = Column(String(50), nullable=False, server_default=text("''"))
    purchased_last_name = Column(String(50), nullable=False, server_default=text("''"))
    purchased_email = Column(String(50), nullable=False, server_default=text("''"))
    wishlistitems_priority = Column(TINYINT(4), nullable=False, server_default=text("'0'"))
    date_purchased = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_added = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))


class ZONE(Base):
    __tablename__ = 'ZONES'

    zone_record_id = Column(INTEGER(10), primary_key=True)
    zone_id = Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    start_zip = Column(String(10), nullable=False, index=True, server_default=text("''"))
    end_zip = Column(String(10), index=True, server_default=text("''"))
    city = Column(String(50), nullable=False, index=True, server_default=text("''"))
    state = Column(CHAR(5), nullable=False, index=True, server_default=text("''"))
    country = Column(CHAR(2), nullable=False, index=True, server_default=text("''"))
    zone_ignore = Column(Enum('n', 'y'), nullable=False, index=True, server_default=text("'n'"))


class ZONESINFO(Base):
    __tablename__ = 'ZONES_INFO'

    zone_id = Column(INTEGER(10), primary_key=True)
    zone_name = Column(String(50), nullable=False, server_default=text("''"))
    zone_code = Column(String(25), nullable=False, server_default=text("''"))
    courier_id = Column(INTEGER(10), nullable=False, index=True, server_default=text("'0'"))


class ZONESLOCATION(Base):
    __tablename__ = 'ZONES_LOCATIONS'
    __table_args__ = (
        Index('totallocality', 'zip', 'locality', 'state', 'country', unique=True),
    )

    location_id = Column(INTEGER(10), primary_key=True)
    locality = Column(String(50), nullable=False, index=True, server_default=text("''"))
    zip = Column(String(10), nullable=False, server_default=text("''"))
    state = Column(String(80), nullable=False, index=True, server_default=text("''"))
    country = Column(CHAR(2), nullable=False, index=True)


class ZONESSET(Base):
    __tablename__ = 'ZONES_SETS'

    zone_id = Column(INTEGER(10), primary_key=True, nullable=False)
    set_id = Column(INTEGER(10), primary_key=True, nullable=False, index=True)


class ZONESSETINFO(Base):
    __tablename__ = 'ZONES_SET_INFO'

    set_id = Column(INTEGER(10), primary_key=True)
    set_name = Column(String(50), nullable=False)
    orderby = Column(INTEGER(10), nullable=False)


class ZONEAAEPORT(Base):
    __tablename__ = 'ZONE_AAE_PORTS'

    port_id = Column(INTEGER(10), primary_key=True)
    postcode = Column(String(10), nullable=False, index=True)
    suburb = Column(String(50), nullable=False)
    state = Column(String(3), nullable=False)
    zone = Column(String(3), nullable=False)
    port1 = Column(String(3), nullable=False)
    port2 = Column(String(3), nullable=False)
    iscountry = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))


class ZONEALLIEDPORT(Base):
    __tablename__ = 'ZONE_ALLIED_PORTS'

    port_id = Column(INTEGER(10), primary_key=True)
    postcode = Column(String(10), nullable=False, index=True)
    headport = Column(String(25), nullable=False)
    suburb = Column(String(50), nullable=False, index=True)
    state = Column(String(50), nullable=False)


class ZONEAUSTRALIANNATIONAL(Base):
    __tablename__ = 'ZONE_AUSTRALIAN_NATIONAL'

    zone_id = Column(INTEGER(10), primary_key=True)
    postcode = Column(String(10), nullable=False, index=True)
    suburb = Column(String(50), nullable=False)
    anc_zone = Column(String(10), nullable=False)


class ZONEHUNTEREXPRES(Base):
    __tablename__ = 'ZONE_HUNTEREXPRESS'

    code = Column(INTEGER(10), primary_key=True)
    abrev = Column(String(15), nullable=False)
    suburb = Column(String(20), nullable=False, index=True)
    postcode = Column(String(10), nullable=False, index=True)
    state = Column(String(10), nullable=False)
    zone_code = Column(String(25), nullable=False, index=True)
    zone_port = Column(String(10), nullable=False)
    zone_name = Column(String(25), nullable=False, index=True)
    country = Column(CHAR(2), nullable=False, index=True, server_default=text("'AU'"))


class ZONEPARCELDIRECTGROUPCODE(Base):
    __tablename__ = 'ZONE_PARCELDIRECTGROUP_CODES'

    code_id = Column(INTEGER(10), primary_key=True)
    postcode = Column(String(10), nullable=False, index=True)
    locality = Column(String(50), nullable=False, index=True, server_default=text("''"))
    sort_code = Column(String(10), nullable=False, index=True)


class Config(Base):
    __tablename__ = 'config'

    section = Column(String(10), primary_key=True, nullable=False, server_default=text("'main'"))
    name = Column(String(50), primary_key=True, nullable=False, server_default=text("''"))
    value = Column(MEDIUMTEXT, nullable=False)


class ConfigDescription(Base):
    __tablename__ = 'config_description'

    name = Column(String(50), primary_key=True)
    title = Column(String(100), nullable=False, server_default=text("''"))
    description = Column(String(255), nullable=False)
    hidden = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    readonly = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    neto_admin_only = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    system_cfg = Column(Enum('n', 'y'), nullable=False)
    data_type = Column(String(25), nullable=False)
    display_fn = Column(String(25), nullable=False)
    edit_fn = Column(String(25), nullable=False)
    update_fn = Column(String(25), nullable=False)


class Counter(Base):
    __tablename__ = 'counters'

    counter = Column(INTEGER(11), nullable=False)
    type = Column(String(32), primary_key=True)
    timestamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class InvExpenseAllocationMethod(Base):
    __tablename__ = 'inv_expense_allocation_method'

    id = Column(INTEGER(11), primary_key=True)
    name = Column(String(50), nullable=False)
    type = Column(String(50), nullable=False, unique=True)


class InvFinancialAccount(Base):
    __tablename__ = 'inv_financial_account'

    id = Column(INTEGER(11), primary_key=True)
    name = Column(String(50), nullable=False)
    type = Column(Enum('EXPENSE'), nullable=False, index=True)


class InvPurchaseOrder(Base):
    __tablename__ = 'inv_purchase_order'

    id = Column(INTEGER(11), primary_key=True)
    order_no = Column(String(15), nullable=False, index=True)
    parent_order_id = Column(INTEGER(11))
    supplier_id = Column(INTEGER(11), nullable=False, index=True)
    supplier_code = Column(String(25), nullable=False, index=True)
    administrator_id = Column(INTEGER(11), nullable=False, index=True)
    warehouse_id = Column(INTEGER(11), nullable=False, index=True)
    status_id = Column(INTEGER(11), nullable=False, index=True)
    bill_street1 = Column(String(50), nullable=False)
    bill_street2 = Column(String(50), nullable=False)
    bill_city = Column(String(50), nullable=False)
    bill_state = Column(String(50), nullable=False)
    bill_zip = Column(String(15), nullable=False)
    bill_country = Column(String(50), nullable=False)
    ship_street1 = Column(String(50), nullable=False)
    ship_street2 = Column(String(50), nullable=False)
    ship_city = Column(String(50), nullable=False)
    ship_state = Column(String(50), nullable=False)
    ship_zip = Column(String(15), nullable=False)
    ship_country = Column(String(50), nullable=False)
    currency = Column(String(3))
    exchange_rate = Column(DECIMAL(18, 8), nullable=False, server_default=text("'1.00000000'"))
    notes = Column(MEDIUMTEXT)
    date_created = Column(TIMESTAMP, nullable=False, index=True, server_default=text("CURRENT_TIMESTAMP"))
    date_updated = Column(TIMESTAMP)
    date_placed = Column(TIMESTAMP)
    date_required = Column(TIMESTAMP)
    date_exported = Column(TIMESTAMP)
    exported = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    date_due = Column(Date)
    date_invoiced = Column(Date)
    date_received = Column(TIMESTAMP, comment='date purchase order was received')
    subtotal = Column(DECIMAL(12, 2), nullable=False)
    tax = Column(DECIMAL(12, 2), nullable=False)
    grand_total = Column(DECIMAL(12, 2), nullable=False)
    reference = Column(String(25))
    last_order_line_counter = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    scanner_used = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    tax_inclusive = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    total_weight = Column(DECIMAL(12, 4))
    total_cubic_volume = Column(DECIMAL(17, 9))
    received_subtotal = Column(DECIMAL(12, 2))
    received_tax = Column(DECIMAL(12, 2))
    received_grand_total = Column(DECIMAL(12, 2))
    expense_subtotal = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    expense_tax = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    expense_grand_total = Column(DECIMAL(12, 2), nullable=False, server_default=text("'0.00'"))
    complete_subtotal = Column(DECIMAL(12, 2))
    complete_tax = Column(DECIMAL(12, 2))
    complete_grand_total = Column(DECIMAL(12, 2))


class InvPurchaseOrderExpense(Base):
    __tablename__ = 'inv_purchase_order_expense'
    __table_args__ = (
        Index('purchase_order_id', 'purchase_order_id', 'expense_allocation_method_id'),
    )

    id = Column(INTEGER(11), primary_key=True)
    purchase_order_id = Column(INTEGER(11), nullable=False)
    expense_allocation_method_id = Column(INTEGER(11), nullable=False)
    financial_account_id = Column(INTEGER(11))
    supplier_code = Column(String(25))
    description = Column(String(50), nullable=False)
    amount = Column(DECIMAL(20, 8), nullable=False)
    tax_code_id = Column(INTEGER(11), nullable=False)
    tax_rate = Column(DECIMAL(6, 4), nullable=False)
    tax_inclusive = Column(TINYINT(1), nullable=False)
    date_created = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    date_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    exported = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    date_deleted = Column(TIMESTAMP)
    exchange_rate = Column(DECIMAL(18, 8))
    currency = Column(String(3), index=True)
    date_invoiced = Column(Date, index=True)
    date_due = Column(Date, index=True)
    reference = Column(String(25))


class InvPurchaseOrderLine(Base):
    __tablename__ = 'inv_purchase_order_line'

    id = Column(INTEGER(11), primary_key=True)
    purchase_order_id = Column(INTEGER(11), nullable=False, index=True)
    inventory_id = Column(INTEGER(11), nullable=False, index=True)
    counter = Column(INTEGER(11), nullable=False)
    supplier_sku = Column(String(32))
    unit_weight = Column(DECIMAL(10, 4))
    unit_cubic_volume = Column(DECIMAL(15, 9))
    quantity = Column(INTEGER(11), nullable=False)
    unit_price = Column(DECIMAL(20, 8), nullable=False, server_default=text("'0.00000000'"))
    tax_rate = Column(DECIMAL(10, 8), nullable=False, server_default=text("'0.00000000'"))
    notax = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    delivered_quantity = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    ordered_quantity = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    on_hand_quantity = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    receive_quantity = Column(INTEGER(11))
    sub_total = Column(DECIMAL(20, 8))
    receive_unit_price = Column(DECIMAL(20, 8))
    receive_tax_rate = Column(DECIMAL(10, 8))
    receive_sub_total = Column(DECIMAL(20, 8))
    expense_unit_price = Column(DECIMAL(20, 8), nullable=False, server_default=text("'0.00000000'"))
    expense_tax = Column(DECIMAL(20, 8), nullable=False, server_default=text("'0.00000000'"))
    expense_sub_total = Column(DECIMAL(20, 8), nullable=False, server_default=text("'0.00000000'"))


class InvPurchaseOrderStatu(Base):
    __tablename__ = 'inv_purchase_order_status'

    id = Column(INTEGER(11), primary_key=True)
    order_status_name = Column(String(50), nullable=False, index=True)
    order_status_type = Column(String(10), nullable=False, unique=True)
    notification = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    template = Column(String(20), nullable=False)
    orderby = Column(TINYINT(3), nullable=False, server_default=text("'0'"))
    workflow_state = Column(TINYINT(3), nullable=False, server_default=text("'0'"))


class InvPurchaseReceipt(Base):
    __tablename__ = 'inv_purchase_receipt'

    id = Column(INTEGER(11), primary_key=True)
    delivery_number = Column(INTEGER(11))
    delivered_by = Column(INTEGER(11), nullable=False)
    date_created = Column(TIMESTAMP, nullable=False, index=True, server_default=text("CURRENT_TIMESTAMP"))
    order_id = Column(INTEGER(11), index=True)


class InvPurchaseReceiptLine(Base):
    __tablename__ = 'inv_purchase_receipt_line'

    id = Column(INTEGER(11), primary_key=True)
    purchase_receipt_id = Column(INTEGER(11), nullable=False, index=True)
    line_number = Column(INTEGER(11))
    quantity = Column(INTEGER(11), nullable=False)
    order_line_id = Column(INTEGER(11), index=True)
    from_on_hand_qty = Column(INTEGER(11), nullable=False)


class InvStockAdjustment(Base):
    __tablename__ = 'inv_stock_adjustment'

    id = Column(INTEGER(11), primary_key=True)
    type = Column(String(32), nullable=False, index=True)
    adjust_no = Column(String(15))
    reason_id = Column(INTEGER(11), nullable=False, index=True)
    subject_id = Column(INTEGER(11))
    warehouse_id = Column(INTEGER(11))
    notes = Column(MEDIUMTEXT)
    date_created = Column(TIMESTAMP, nullable=False, index=True, server_default=text("CURRENT_TIMESTAMP"))
    created_by = Column(INTEGER(11), nullable=False)
    num_lines = Column(INTEGER(11), nullable=False)
    total_onhand_adjust_qty = Column(INTEGER(11), nullable=False)
    total_onhand_adjust_value = Column(DECIMAL(12, 4), nullable=False, server_default=text("'0.0000'"))
    date_exported = Column(TIMESTAMP)


class InvStockAdjustmentLine(Base):
    __tablename__ = 'inv_stock_adjustment_line'

    id = Column(INTEGER(11), primary_key=True)
    type = Column(String(32), nullable=False, index=True)
    stock_adjustment_id = Column(INTEGER(11), nullable=False, index=True)
    inventory_id = Column(INTEGER(11), nullable=False, index=True)
    line_id = Column(INTEGER(11))
    date_created = Column(TIMESTAMP, nullable=False, index=True, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    description = Column(Text)
    warehouse_id = Column(INTEGER(11), nullable=False, index=True)
    delta_on_hand_qty = Column(INTEGER(11))
    delta_incoming_qty = Column(INTEGER(11))
    delta_committed_qty = Column(INTEGER(11))
    delta_preorder_qty = Column(INTEGER(11))
    delta_discarded_qty = Column(INTEGER(11))
    unit_price = Column(DECIMAL(12, 4), nullable=False, server_default=text("'0.0000'"))
    from_on_hand_qty = Column(INTEGER(11), nullable=False)
    to_on_hand_qty = Column(INTEGER(11), nullable=False)
    from_avg_cost_price = Column(DECIMAL(14, 4), nullable=False, server_default=text("'0.0000'"))
    to_avg_cost_price = Column(DECIMAL(14, 4), nullable=False, server_default=text("'0.0000'"))
    line_number = Column(INTEGER(11))


class InvStockAdjustmentReason(Base):
    __tablename__ = 'inv_stock_adjustment_reason'

    id = Column(INTEGER(11), primary_key=True)
    reason_text = Column(String(255), nullable=False)
    description = Column(Text)
    sort_order = Column(TINYINT(3), nullable=False, server_default=text("'0'"))
    user_visible = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    exportable = Column(TINYINT(1), nullable=False, server_default=text("'0'"))


class InvStocktake(Base):
    __tablename__ = 'inv_stocktake'

    id = Column(INTEGER(11), primary_key=True)
    stocktake_no = Column(String(15), nullable=False, unique=True)
    name = Column(String(255), nullable=False)
    warehouse_id = Column(INTEGER(11), nullable=False, index=True)
    date_due = Column(TIMESTAMP, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    created_by = Column(INTEGER(11), nullable=False)
    date_created = Column(TIMESTAMP, nullable=False, index=True, server_default=text("'0000-00-00 00:00:00'"))
    updated_by = Column(INTEGER(11), nullable=False)
    date_updated = Column(TIMESTAMP, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    started_by = Column(INTEGER(11))
    cancelled_by = Column(INTEGER(11))
    date_started = Column(TIMESTAMP)
    date_cancelled = Column(TIMESTAMP)
    date_processed = Column(TIMESTAMP, index=True)
    processed_by = Column(INTEGER(11))
    status_id = Column(INTEGER(11), nullable=False, index=True)


class InvStocktakeLine(Base):
    __tablename__ = 'inv_stocktake_line'

    id = Column(INTEGER(11), primary_key=True)
    stocktake_id = Column(INTEGER(11), nullable=False, index=True)
    inventory_id = Column(INTEGER(11), nullable=False, index=True)
    date_added = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    expected_qty = Column(INTEGER(11))
    counted_qty = Column(INTEGER(11))
    delta_qty = Column(INTEGER(11))
    unit_price = Column(DECIMAL(12, 4))


class InvStocktakeStatu(Base):
    __tablename__ = 'inv_stocktake_status'

    id = Column(INTEGER(11), primary_key=True)
    order_status_name = Column(String(50), nullable=False, index=True)
    order_status_type = Column(String(10), nullable=False, index=True)
    notification = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    template = Column(String(20), nullable=False)
    orderby = Column(TINYINT(3), nullable=False, server_default=text("'0'"))
    workflow_state = Column(TINYINT(3), nullable=False, server_default=text("'0'"))


class InvTaxCode(Base):
    __tablename__ = 'inv_tax_code'

    id = Column(INTEGER(11), primary_key=True)
    name = Column(String(50), nullable=False)
    default_tax_rate = Column(DECIMAL(6, 4), nullable=False)
    tax_rate = Column(DECIMAL(6, 4), nullable=False)
    default_has_tax = Column(TINYINT(1), nullable=False, server_default=text("'0'"))


class Phinxlog(Base):
    __tablename__ = 'phinxlog'

    version = Column(BIGINT(20), primary_key=True)
    migration_name = Column(String(100))
    start_time = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    end_time = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    breakpoint = Column(TINYINT(1), nullable=False, server_default=text("'0'"))


class PhinxlogSchema(Base):
    __tablename__ = 'phinxlog_schema'

    version = Column(BIGINT(20), primary_key=True)
    migration_name = Column(String(100))
    start_time = Column(TIMESTAMP)
    end_time = Column(TIMESTAMP)
    breakpoint = Column(TINYINT(1), nullable=False, server_default=text("'0'"))


class Session(Base):
    __tablename__ = 'session'

    module = Column(String(10), nullable=False, server_default=text("'main'"))
    id = Column(String(32), primary_key=True)
    username = Column(String(20))
    contents = Column(LONGTEXT, nullable=False)
    timestamp = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    ts = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class ADWADSIMAGE(Base):
    __tablename__ = 'ADW_ADS_IMAGES'

    ad_image_id = Column(INTEGER(11), primary_key=True)
    ad_id = Column(INTEGER(10), nullable=False, unique=True)
    image_id = Column(INTEGER(10), nullable=False, index=True)
    full_image_id = Column(ForeignKey('IMAGES.image_id', ondelete='CASCADE'), index=True)

    full_image = relationship('IMAGE')


class CONTENTIMAGE(Base):
    __tablename__ = 'CONTENT_IMAGES'
    __table_args__ = (
        Index('content_id', 'content_id', 'image_counter', unique=True),
    )

    content_image_id = Column(INTEGER(11), primary_key=True)
    content_id = Column(INTEGER(10), nullable=False)
    image_id = Column(INTEGER(10), nullable=False, index=True)
    image_counter = Column(INTEGER(10), nullable=False)
    full_image_id = Column(ForeignKey('IMAGES.image_id', ondelete='CASCADE'), index=True)

    full_image = relationship('IMAGE')


class INVENTORYIMAGE(Base):
    __tablename__ = 'INVENTORY_IMAGES'
    __table_args__ = (
        Index('inventory_counting', 'inventory_id', 'image_counter', unique=True),
    )

    inventory_image_id = Column(INTEGER(10), primary_key=True)
    inventory_id = Column(ForeignKey('INVENTORY.inventory_id', ondelete='CASCADE'), nullable=False)
    image_counter = Column(INTEGER(10), nullable=False)
    image_id = Column(INTEGER(10), nullable=False, index=True)
    full_image_id = Column(ForeignKey('IMAGES.image_id', ondelete='SET NULL'), index=True)
    medium_image_id = Column(ForeignKey('IMAGES.image_id', ondelete='SET NULL'), index=True)
    thumb_image_id = Column(ForeignKey('IMAGES.image_id', ondelete='SET NULL'), index=True)

    full_image = relationship('IMAGE', primaryjoin='INVENTORYIMAGE.full_image_id == IMAGE.image_id')
    inventory = relationship('INVENTORY')
    medium_image = relationship('IMAGE', primaryjoin='INVENTORYIMAGE.medium_image_id == IMAGE.image_id')
    thumb_image = relationship('IMAGE', primaryjoin='INVENTORYIMAGE.thumb_image_id == IMAGE.image_id')


class ITEMSPECIFICSIMAGE(Base):
    __tablename__ = 'ITEM_SPECIFICS_IMAGES'

    itemspec_image_id = Column(INTEGER(11), primary_key=True)
    itemspecval_id = Column(INTEGER(10), nullable=False, unique=True)
    image_id = Column(INTEGER(10), nullable=False, index=True)
    full_image_id = Column(ForeignKey('IMAGES.image_id', ondelete='CASCADE'), index=True)

    full_image = relationship('IMAGE')


class LOGOIMAGE(Base):
    __tablename__ = 'LOGO_IMAGES'

    logo_image_id = Column(INTEGER(11), primary_key=True)
    logo_type = Column(String(50), nullable=False, unique=True)
    image_id = Column(INTEGER(10), nullable=False, index=True)
    full_image_id = Column(ForeignKey('IMAGES.image_id', ondelete='CASCADE'), index=True)

    full_image = relationship('IMAGE')


class REGISTERSHIFT(Base):
    __tablename__ = 'REGISTER_SHIFTS'
    __table_args__ = (
        Index('register_id_active', 'register_id', 'active', unique=True),
    )

    shift_id = Column(INTEGER(10), primary_key=True)
    register_id = Column(ForeignKey('REGISTERS.register_id'), nullable=False, index=True)
    start_date = Column(DateTime, nullable=False)
    end_date = Column(DateTime)
    active = Column(Enum('n', 'y'))

    register = relationship('REGISTER')


class REGISTERSHIFTPAYMENT(Base):
    __tablename__ = 'REGISTER_SHIFT_PAYMENTS'
    __table_args__ = (
        Index('shift_id_payment_type_id', 'shift_id', 'pay_type_id', unique=True),
    )

    shift_payment_id = Column(INTEGER(10), primary_key=True)
    shift_id = Column(INTEGER(10), nullable=False)
    pay_type_id = Column(ForeignKey('PAYTYPES.id'), nullable=False, index=True)
    amount_counted = Column(DECIMAL(12, 2), server_default=text("'0.00'"))
    amount_expected = Column(DECIMAL(12, 2), server_default=text("'0.00'"))
    amount_expected_sales = Column(DECIMAL(12, 2), server_default=text("'0.00'"))
    amount_expected_refunds = Column(DECIMAL(12, 2), server_default=text("'0.00'"))

    pay_type = relationship('PAYTYPE')


class STORELOCATION(Base):
    __tablename__ = 'STORELOCATIONS'

    stloc_id = Column(INTEGER(10), primary_key=True)
    username = Column(String(25), nullable=False, index=True, server_default=text("''"))
    stloc_name = Column(String(100), nullable=False, index=True, server_default=text("''"))
    stloc_street1 = Column(String(100), nullable=False, server_default=text("''"))
    stloc_street2 = Column(String(100), nullable=False, server_default=text("''"))
    stloc_city = Column(String(50), nullable=False, server_default=text("''"))
    stloc_state = Column(String(50), nullable=False, server_default=text("''"))
    stloc_zip = Column(String(15), nullable=False, server_default=text("''"))
    stloc_country = Column(String(50), nullable=False, server_default=text("''"))
    stloc_phone = Column(String(30), nullable=False, server_default=text("''"))
    stloc_fax = Column(String(30), nullable=False, server_default=text("''"))
    stloc_email = Column(String(50), nullable=False, server_default=text("''"))
    description = Column(Text, nullable=False)
    misc1 = Column(String(50), nullable=False, server_default=text("''"))
    misc2 = Column(String(50), nullable=False, server_default=text("''"))
    misc3 = Column(String(50), nullable=False, server_default=text("''"))
    misc4 = Column(String(1000), nullable=False, server_default=text("''"))
    misc5 = Column(String(1000), nullable=False, server_default=text("''"))
    stloc_lat = Column(DECIMAL(14, 10), nullable=False, server_default=text("'0.0000000000'"))
    stloc_lng = Column(DECIMAL(14, 10), nullable=False, server_default=text("'0.0000000000'"))
    active = Column(Enum('n', 'y'), nullable=False, server_default=text("'y'"))
    approved = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    auto_url_update = Column(Enum('n', 'y'), nullable=False, server_default=text("'n'"))
    date_created = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    date_updated = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    warehouse_id = Column(ForeignKey('WAREHOUSES.warehouse_id', ondelete='SET NULL'), index=True)

    warehouse = relationship('WAREHOUSE')


class STORELOCATIONIMAGE(Base):
    __tablename__ = 'STORELOCATION_IMAGES'
    __table_args__ = (
        Index('stloc_counting_idx', 'stloc_id', 'image_counter', unique=True),
    )

    stloc_image_id = Column(INTEGER(11), primary_key=True)
    stloc_id = Column(INTEGER(10), nullable=False, index=True)
    image_id = Column(INTEGER(10), nullable=False)
    image_counter = Column(INTEGER(10), nullable=False)
    full_image_id = Column(ForeignKey('IMAGES.image_id', ondelete='SET NULL'), index=True)
    thumb_image_id = Column(ForeignKey('IMAGES.image_id', ondelete='SET NULL'), index=True)

    full_image = relationship('IMAGE', primaryjoin='STORELOCATIONIMAGE.full_image_id == IMAGE.image_id')
    thumb_image = relationship('IMAGE', primaryjoin='STORELOCATIONIMAGE.thumb_image_id == IMAGE.image_id')


class USERIMAGE(Base):
    __tablename__ = 'USER_IMAGES'

    user_image_id = Column(INTEGER(11), primary_key=True)
    user_id = Column(INTEGER(10), nullable=False, unique=True)
    image_id = Column(INTEGER(10), nullable=False, index=True)
    full_image_id = Column(ForeignKey('IMAGES.image_id', ondelete='CASCADE'), index=True)

    full_image = relationship('IMAGE')


class REGISTERSHIFTORDERPAYMENT(Base):
    __tablename__ = 'REGISTER_SHIFT_ORDERPAYMENTS'
    __table_args__ = (
        Index('shift_id', 'shift_id', 'payment_id', unique=True),
    )

    shift_orderpayment_id = Column(INTEGER(10), primary_key=True)
    shift_id = Column(ForeignKey('REGISTER_SHIFTS.shift_id', ondelete='CASCADE'), nullable=False)
    payment_id = Column(ForeignKey('ORDERPAYMENTS.payment_id', ondelete='CASCADE'), nullable=False, index=True)

    payment = relationship('ORDERPAYMENT')
    shift = relationship('REGISTERSHIFT')


class REGISTERSHIFTORDER(Base):
    __tablename__ = 'REGISTER_SHIFT_ORDERS'
    __table_args__ = (
        Index('order_id', 'order_id', 'shift_id', unique=True),
    )

    shift_order_id = Column(INTEGER(10), primary_key=True)
    shift_id = Column(ForeignKey('REGISTER_SHIFTS.shift_id'), nullable=False, index=True)
    order_id = Column(ForeignKey('ORDERS.order_id', ondelete='CASCADE'), nullable=False)

    order = relationship('ORDER')
    shift = relationship('REGISTERSHIFT')


class REGISTERSHIFTRMA(Base):
    __tablename__ = 'REGISTER_SHIFT_RMA'
    __table_args__ = (
        Index('rma_id', 'rma_id', 'shift_id', unique=True),
    )

    shift_rma_id = Column(INTEGER(10), primary_key=True)
    shift_id = Column(ForeignKey('REGISTER_SHIFTS.shift_id'), nullable=False, index=True)
    rma_id = Column(ForeignKey('RMA.rma_id', ondelete='CASCADE'), nullable=False)

    rma = relationship('RMA')
    shift = relationship('REGISTERSHIFT')
