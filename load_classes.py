from inspect import getmembers, isclass
from os import listdir



# Get contents of library folder and create a list containing all available classes from the respective modules.
available_record_types = []
for item in listdir("./library"):
    if item[-3:] == ".py":
        module = __import__("library." + item[:-3], fromlist=[''])
        for member in getmembers(module):
            if isclass(member[1]):
                available_record_types.append(member)

leo = available_record_types[0][1]()
print(available_record_types[0])
leo.do_woof()