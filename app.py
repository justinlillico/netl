from flask import Flask, render_template, request, redirect, send_file, make_response
from flask_socketio import SocketIO, emit
import utils
import pandas as pd
from queue import Queue
import logging
from threading import Thread
from time import sleep
import zipfile
from io import BytesIO
from datetime import datetime
from flask_sqlalchemy import SQLAlchemy
import json
import os
import argparse


from sqlalchemy import Column, Integer, String, Integer, DateTime, create_engine





# Initialise the flask app.
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///./data/netl_meta.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

meta_db = SQLAlchemy(app)

class Jobs(meta_db.Model):
    __tablename__ = "Jobs"
    id = Column(Integer, primary_key=True)
    source = Column(String)
    record_type = Column(String)
    state = Column(Integer)
    client = Column(String)
    requirements = Column(String)
    working_frames = Column(String)
    status = Column(String)
    last_updated = Column(DateTime, default=datetime.now())
    case_number = Column(String)

meta_db.Jobs = Jobs

# Create the meta table if it does not exist.
if not os.path.exists("./data/netl_meta.db"):
    engine = create_engine(app.config['SQLALCHEMY_DATABASE_URI'])
    Jobs.metadata.create_all(engine)

socketio = SocketIO(app)
session = {}

# A custom log handler that fires Socket IO events for send logging in loading pages.
class WindowLogger(logging.StreamHandler):

    def __init__(self, socketio):
        logging.StreamHandler.__init__(self)
        self.socketio = socketio

    def emit(self, record):
        self.socketio.emit("live_log", self.format(record))

# Setup the logger.
logger = logging.getLogger("logger")
logger.setLevel(logging.INFO)
wl = WindowLogger(socketio)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
wl.setFormatter(formatter)
logger.addHandler(wl)


def load_existing(job_number):
    return Jobs.query.filter_by(id=job_number).first()


# Primary endpoint.
@app.route('/')
def index():
    # Reset the whole thing if it has been initialised.
    session.pop("instance", None)
    return render_template("index.html", jobs_table=utils.get_jobs_html())

# Where we select a source. This is a py file in the library folder.
@app.route('/select_source', methods=['GET', 'POST'])
def select_source():
    # Reset the whole thing if it has been initialised.
    session.pop("instance", None)
    return render_template("select_source.html", sources=utils.get_sources())

# Where we select a RecordType from the source. This is a class in the py file.
@app.route('/record_types', methods=['GET', 'POST'])
def record_types():
    # Reset the whole thing if it has been initialised.
    session.pop("instance", None)
    if request.method == "POST":
        session["source"] = request.form.get('sources')
        session['available_datatypes'] = utils.get_classes_from_source(session["source"])
        return render_template("record_types.html", record_types=session['available_datatypes'])

# Here the user provides the requirements of the class.
@app.route('/info_hub', methods=['GET', 'POST'])
def info_hub():
    if request.method == "POST":
        record_type = request.form.get('record_type')
        source_class = session['available_datatypes'][record_type]
        session['working_class'] = source_class
        return render_template("info_hub.html", 
                               title="Requirements:", 
                               requirements=source_class.requirements, 
                               uploads=source_class.uploads, 
                               instructions=source_class.step_instructions["Extract"])

    # Check to see if we should load from a previous state.
    job_number = request.args.get("job_number")
    if job_number is not None:
        # TODO This should probably be a function of record_type and not in the front end.
        # TODO This could at least be factored out as it is repeated in the next function.
        result = load_existing(job_number)
        session["source"] = result.source
        session['available_datatypes'] = utils.get_classes_from_source(result.source)
        session['working_class'] = session['available_datatypes'][result.record_type]
        session['instance'] = session['working_class'](meta_db, socketio, {"sqlite_path" : "./sqlite"}, job_number=job_number)
        session['instance'].source = session['source']
        # session['instance'].case_number = session['case_number']
        session['instance'].requirements = json.loads(result.requirements)
        session['instance'].status = json.loads(result.status)
        session['instance'].set_state(result.state)

        #TODO add to this the loading of data from the sqllite db associated with the job number.
        from sqlite3 import connect

        for working_frame in json.loads(result.working_frames):
            conn = connect(os.path.join(".", "sqlite", str(result.id) + ".db"))
            try:
                session['instance'].working_dataframes[working_frame] = pd.read_sql_query("select * from working_frame_" + working_frame + ";", conn)
            except pd.io.sql.DatabaseError:
                session['instance'].working_dataframes[working_frame] = pd.read_sql_query("select * from working_frame_" + working_frame.replace(" ", "_") + ";", conn)
            



    step = session['instance'].get_step()
    if step == "Complete":
        return redirect("/complete")
    output = ""
    
    for key in session['instance'].working_dataframes.keys():
        output += "<h1>" + key.replace("working_frame_", "").title() + "</h1><br>" 
        
        output += session['instance'].working_dataframes[key].head(100).to_html(classes="frame")

    return render_template("info_hub.html", 
                           title=step, 
                           table=output, 
                           instructions=session['instance'].step_instructions[step], 
                           status=session['instance'].status, 
                           job_number=str(session['instance'].job_number))

# This is a dynamic page that will execute any of the primary methods in the classes and display output as it runs.
@app.route('/progress', methods=['GET', 'POST'])
def progress():
    if 'instance' not in session:
        session['instance'] = session['working_class'](meta_db, socketio, {"sqlite_path" : "./sqlite"})
        session['instance'].source = session['source']
        session['instance'].case_number = request.form['case_number']
        session['instance'].requirements = dict(request.form.items())
        session['instance'].client = request.form['client']
        session['instance'].uploads = { key:pd.read_csv(value, dtype=str).fillna("") for (key, value) in request.files.items() if value}

    # Make a new thread to run this thing.
    if 'thread' not in session or not session['thread'].is_alive():
        session['thread'] = Thread(target=session['instance'].execute)
        session['thread'].start()
    
    return render_template("progress.html")

@app.route('/complete', methods=['GET', 'POST'])
def complete():
    if len(session['instance'].downloads) > 0:
        for download in session['instance'].downloads.keys():
            new = BytesIO()
            new.write(session['instance'].downloads[download].to_csv().encode())
            new.seek(0)
            session['instance'].downloads[download] = new

    return render_template("complete.html", downloads=session['instance'].downloads.keys())

@app.route('/download/<key>', methods=['GET', 'POST'])
def download(key):
    return send_file(
        session['instance'].downloads[key],
        as_attachment=True,
        attachment_filename= key + '.csv',
        mimetype='text/csv',
        cache_timeout=-1
    )

    
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--port", help="The port to launch the server on.")

    args = parser.parse_args()

    if not args.port:
        args.port = 5000

    socketio.run(app, debug=True, port=args.port)
