from inspect import getmembers, isfunction, isclass
from os import listdir
import os
import sqlite3
import pandas as pd
from json import loads



def get_functions(module):
    functions = []
    for item in getmembers(module):
        if isfunction(item[1]):
            functions.append(item)

    return functions

def get_classes(module):
    classes = []
    for item in getmembers(module):
        if isclass(item[1]):
            classes.append(item)

    return classes

def get_sources():
    sources = []
    for item in listdir("./library"):
        if item[-3:] == ".py" and item != "__init__.py":
            sources.append(item[:-3])
    return sources

def get_classes_from_source(source):
    return_dict = {}
    module = __import__("library." + source, fromlist=[''])
    for member in getmembers(module):
        if isclass(member[1]) and member[0] != "RecordType" and hasattr(member[1], 'record_type_class') and member[1].record_type_class:
            return_dict[member[1].name] = member[1]
    return return_dict

def get_jobs_html():
    conn = sqlite3.connect(os.path.join(".", "data", "netl_meta.db"))

    try:
        jobs = pd.read_sql("SELECT * FROM Jobs ORDER BY id DESC", conn)
    except pd.io.sql.DatabaseError:
        # If the table hasn't been created yet, we don't need to return anything.
        return ""
    
    jobs["id"] = jobs["id"].apply(lambda x: '<a href="/info_hub?job_number=' + str(x) + '">' + str(x) + "</a>")
    states = {0 : "Extract", 1 : "Transform", 2 : "Load", 3 : "Complete"}
    jobs["state"] = jobs["state"].apply(lambda x: states.get(x, "Invalid"))

    def pretty_dict(x):
        return "<b>" + x.replace("{", "").replace("}", "").replace('"', "").replace(",", "<br><b>").replace(":", "</b>:")

    jobs["status"] = jobs["status"].apply(pretty_dict)

    jobs = jobs.drop(columns=["requirements"])

    
    return jobs.to_html(index=False, escape=False)





                    