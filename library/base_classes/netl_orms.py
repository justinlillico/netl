
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Integer

Base = declarative_base()


class Jobs(Base):
    __tablename__ = "Jobs"
    id = Column(Integer, primary_key=True)
    source = Column(String)
    record_type = Column(String)
    state = Column(Integer)
    client = Column(String)
    requirements = Column(String)