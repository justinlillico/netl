from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine, update
import os
from datetime import datetime

def get_next_job_number(meta_db):
    record = meta_db.session.query(meta_db.Jobs).order_by(meta_db.Jobs.id.desc()).first()
    if record is None:
        return 0
    return record.id + 1

def store_state(meta_db, job_number, client, source, record_type, state, json_reqs, working_frames, status, case_number, last_updated=datetime.now()):
    result = meta_db.session.query(meta_db.Jobs).filter(meta_db.Jobs.id == job_number).first()
    if result is not None:
        result.record_type = record_type
        result.state = state
        result.source = source
        result.requirements = json_reqs
        result.working_frames = working_frames
        result.status = status
        result.client = client
        result.last_updated = last_updated
        result.case_number = case_number
    else:
        j = meta_db.Jobs(id=job_number, client=client, record_type=record_type, state=state, source=source, requirements=json_reqs, working_frames=working_frames, status=status, case_number=case_number)
        meta_db.session.add(j)
    meta_db.session.commit()
