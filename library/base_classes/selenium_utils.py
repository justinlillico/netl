from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from webdriver_manager.chrome import ChromeDriverManager

class SelenimumHelper(object):

    def __init__(self):
        chromeOptions = webdriver.ChromeOptions()
        chromeOptions.add_experimental_option('useAutomationExtension', False)
        self.driver = webdriver.Chrome(ChromeDriverManager().install(), chrome_options=chromeOptions)

    def navigate_to(self, url):
        self.driver.get(url)

    def enter_text_by_id(self, _id, text, enter=False):
        elem = self.driver.find_element_by_id(_id)
        self.__enter_text(elem, text, enter)      

    def enter_text_by_name(self, _name, text, enter=False):
        elem = self.driver.find_element_by_name(_name)
        self.__enter_text(elem, text, enter) 

    def __enter_text(self, elem, text, enter):
        elem.clear()
        elem.send_keys(text)
        if enter:
            elem.send_keys(Keys.RETURN)

    def select_from_dropdown_by_name(self, name, item):
        elem = Select(self.driver.find_element_by_name(name))
        elem.select_by_visible_text(item)

    def toggle_checkbox_by_name(self, name):
        self.driver.find_element_by_name(name).click()

    def click_by_name(self, _name):
        self.__click(self.driver.find_element_by_name(_name))

    def click_by_id(self, _id):
        self.__click(self.driver.find_element_by_id(_id))

    def __click(self, elem):
        elem.click()


class NetoSite(SelenimumHelper):

    def __init__(self, url, username, password):
        super().__init__()
        self.url = url
        self.username = username
        self.password = password

    def login(self):
        self.navigate_to(self.url + "_cpanel")
        try:
            self.enter_text_by_id("username", self.username)
            self.enter_text_by_id("password", self.password, enter=True)
            self.toggle_checkbox_by_name("take_over_session")
            self.enter_text_by_id("username", self.username)
            self.enter_text_by_id("password", self.password, enter=True)
        except Exception as e:
            print(e)

    def save_and_continue(self):
        self.driver.execute_script("savechanges();")
        alert_obj = self.driver.switch_to.alert
        alert_obj.accept()