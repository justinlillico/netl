from abc import ABC, abstractmethod
import pandas as pd
import json
import sqlite3
import os
import requests
from flask_socketio import SocketIO, emit
from time import sleep
import logging
from . import sqlite_utils
from netoapi.customer import CustomerApi
from netoapi.product import ProductApi
from netoapi.content import ContentApi
from netools.database import DatabaseType, get_engine_for
from netoapi.order import OrderApi
from SQLORM.NetoORM import WISHLIST, WISHLISTITEM
from sqlalchemy.orm import Session, sessionmaker
from sqlalchemy.types import VARCHAR
from sqlite3 import InterfaceError
from datetime import datetime

# TODO Need to make passwords stored in db encrypted or something.

class State():
    EXTRACT = 0
    TRANSFORM = 1
    LOAD = 2
    COMPLETE = 3

class RecordType(ABC):

    name = "NotDefined"
    requirements = {}
    uploads = {}
    downloads = {}
    status = {}
    state = State.EXTRACT
    logger = logging.getLogger("logger")
    step_instructions = {

        "Extract" : "NotDefined",
        "Transform" : "NotDefined",
        "Load" : "NotDefined"

    }

    single_method_class = False
    record_type_class = True

    client = "" # A field for storing this records client.
    case_number = ""
    source = "" # The source which this record type came from.

    

    """
    This class will need settings passed into it. These can be stored in the flask config.
    """
    def __init__(self, meta_db, socket_io, settings, job_number=None):
        self.socket_io = socket_io
        self.settings = settings
        self.requirements_supplied = False
        self.working_dataframes = {}
        if not os.path.exists(settings["sqlite_path"]):
            os.makedirs(settings["sqlite_path"])

        # A helper for formatting transforms.
        self.table_formats = {
            "content_ftp_image" : ["content_name", "image_url"],
            "content" : ["id", "content_id", "content_name", "content_type", "short_description1", "description1", "search_keywords", "author", "external_id", "external_parent_id", "seo_meta_description", "seo_meta_keywords", "seo_page_heading", "seo_page_title", "content_url", "parent_id", "parent_content_id", "level", "sort_order", "active"],
            "products" : ["SKU", "InventoryID", "ParentSKU", "AccountingCode", "Virtual", "Brand", "Name", "Model", "SortOrder1", "SortOrder2", "RRP", "DefaultPrice", "PromotionPrice", "PromotionStartDate", "PromotionExpiryDate", "DateArrival", "CostPrice", "UnitOfMeasure", "BaseUnitOfMeasure", "BaseUnitPerQuantity", "BuyUnitQuantity", "SellUnitQuantity", "PreOrderQuantity", "PickPriority", "PickZone", "RestrictedToUserGroup", "Approved", "ApprovedForPOS", "ApprovedForMobileStore", "IsActive", "Active", "Visible", "TaxCategory", "TaxFreeItem", "TaxInclusive", "AuGstExempt", "NzGstExempt", "SearchKeywords", "ShortDescription", "Description", "TermsAndConditions", "Features", "Specifications", "Warranty", "ArtistOrAuthor", "Format", "ModelNumber", "Subtitle", "AvailabilityDescription", "ImageURL", "Images", "BrochureURL", "ProductURL", "UPC", "UPC1", "UPC2", "UPC3", "Type", "Subtype", "NumbersOfLabelsToPrint", "ReferenceNumber", "InternalNotes", "BarcodeHeight", "IsInventoried", "IsBought", "IsSold", "ExpenseAccount", "PurchaseTaxCode", "CostOfSalesAccount", "IncomeAccount", "AssetAccount", "ItemHeight", "ItemLength", "ItemWidth", "ShippingHeight", "ShippingLength", "ShippingWidth", "ShippingWeight", "CubicWeight", "HandlingTime", "SupplierItemCode", "SplitForWarehousePicking", "eBayDescription", "PrimarySupplier", "DisplayTemplate", "EditableKitBundle", "RequiresPackaging", "ItemURL", "SEOPageTitle", "SEOMetaKeywords", "SEOPageHeading", "SEOMetaDescription", "SEOCanonicalURL", "AutomaticURL", "IsAsset", "WhenToRepeatOnStandingOrders", "SerialTracking", "Group", "ShippingCategory", "Job", "Misc01", "Misc02", "Misc03", "Misc04", "Misc05", "Misc06", "Misc07", "Misc08", "Misc09", "Misc10", "Misc11", "Misc12", "Misc13", "Misc14", "Misc15", "Misc16", "Misc17", "Misc18", "Misc19", "Misc20", "Misc21", "Misc22", "Misc23", "Misc24", "Misc25", "Misc26", "Misc27", "Misc28", "Misc29", "Misc30", "Misc31", "Misc32", "Misc33", "Misc34", "Misc35", "Misc36", "Misc37", "Misc38", "Misc39", "Misc40", "Misc41", "Misc42", "Misc43", "Misc44", "Misc45", "Misc46", "Misc47", "Misc48", "Misc49", "Misc50", "Misc51", "Misc52", "MonthlySpendRequirement", "FreeGifts", "CrossSellProducts", "UpsellProducts", "KitComponents", "PriceGroups", "Categories", "ItemSpecifics", "eBayItems", "eBayProductIDs", "WarehouseQuantity", "SalesChannels", "WarehouseLocations", "Misc2", "Misc3", "external_id", "external_sku"],
            "WISHLISTS" : ["id", "username", "wishlist_name", "wishlist_private"],
            "WISHLIST_ITEMS" : ["wishlist_id", "SKU"],
            "customers" : ["Username", "Type", "EmailAddress", "SecondaryEmailAddress", "NewsletterSubscriber", "ParentUsername", "ApprovalUsername", "ReferralUsername", "ReferralCommission", "Gender", "DateOfBirth", "IdentificationType", "IdentificationDetails", "DefaultDiscounts", "DefaultDocumentTemplate", "InternalNotes", "ABN", "WebsiteURL", "CreditLimit", "DefaultInvoiceTerms", "Classification1", "Classification2", "SalesChannel", "Active", "OnCreditHold", "UserGroup", "AccountManager", "DefaultOrderType", "BillingAddress", "ShippingAddress"],
            "orders" : ["OrderID", "PurchaseOrderNumber", "OrderType", "OnHoldType", "UserGroup", "DocumentTemplate", "DatePlacedUTC", "DatePlaced", "DateRequired", "DateInvoiced", "DateDue", "Username", "Email", "BillFirstName", "BillLastName", "BillCompany", "BillStreet1", "BillStreet2", "BillCity", "BillState", "BillPostCode", "BillContactPhone", "BillCountry", "ShipFirstName", "ShipLastName", "ShipCompany", "ShipStreet1", "ShipStreet2", "ShipCity", "ShipState", "ShipPostCode", "ShipContactPhone", "ShipCountry", "EnableAddressValidation", "Operator", "OperatorDateUpdated", "SalesPerson", "CustomerRef1", "CustomerRef2", "CustomerRef3", "CustomerRef4", "CustomerRef5", "CustomerRef6", "CustomerRef7", "CustomerRef8", "CustomerRef9", "CustomerRef10", "SalesChannel", "ShipInstructions", "InternalOrderNotes", "StickyNoteTitle", "StickyNote", "StickyNotes", "OrderStatus", "OrderApproval", "PaymentMethod", "PaymentTerms", "TaxInclusive", "TaxFreeShipping", "BPAYCRN", "ShippingMethod", "ShippingCost", "SignatureRequired", "CurrencyCode", "OrderLine", "OrderRounding"]
        }

        # Connect to the meta db.
        self.meta_db = meta_db

        # Get a job number.
        if job_number is None:
            self.job_number = sqlite_utils.get_next_job_number(meta_db)
        else:
            self.job_number = job_number



    @abstractmethod
    def extractor(self):
        pass

    def transformer(self):
        pass

    def loader(self):
        pass    

    def get_blank_transform_table(self, _type):
        return pd.DataFrame(columns=self.table_formats[_type])

    def format_transform_table(self, _type, table):
        col_names = table.columns.to_list()

        keep = [col for col in col_names if col in self.table_formats[_type]]

        t = table[keep] 
        
        return t

    def get_step(self):
        if self.state == 0:
            return "Extract"
        if self.state == 1:
            return "Transform"
        if self.state == 2:
            return "Load"
        if self.state == 3:
            return "Complete"

    def store_dataframes(self):
        conn = sqlite3.connect(os.path.join(self.settings["sqlite_path"], str(self.job_number) + ".db"))
        for key in self.working_dataframes.keys():
            self.logger.info("Storing " + key + " in sql lite database " + str(self.job_number) + ".db")
            
                       
            # We want to throw an error if the user has not cast everything into the correct type.
            try:
                self.working_dataframes[key].to_sql("working_frame_" + key.replace(" ", "_"), conn, if_exists="replace", index=False)
            except InterfaceError as e:
                self.logger.error("There is an incompatible type in dataframe " + key + ". Ensure this record type is using json.dumps to store lists and dicts in cells. Exception: " + str(e))
                exit()



        json_reqs = json.dumps(self.requirements)


        sqlite_utils.store_state(self.meta_db, self.job_number, self.client, self.source, self.name, self.state, json_reqs, 
                                 json.dumps(list(self.working_dataframes.keys())), json.dumps(self.status), self.case_number)

    def set_state(self, state):
        self.state = state

    def next_state(self):
        self.state += 1

    def clear_feedbacks(self):
        self.downloads = {}
        self.status = {}

    def setup_logging(self):
        # Setup logging on a case to case basis.
        fh = logging.FileHandler(f'./logs/Job#{str(self.job_number)}_{self.case_number}.log')
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setFormatter(formatter)
        self.logger.addHandler(fh)

    def execute(self):
        sleep(1) # Not elegant, but gives the client time to load.
    
        
        if self.state == State.EXTRACT:
            # Reset the state of the downloads dict.
            self.clear_feedbacks()

            # Call to create the log file handler here after the case has been applied to the recordtype.
            self.setup_logging()
            
            # Log job number and step.
            self.logger.info("Extraction for job " + str(self.job_number) + " began.")
            self.extractor()
        elif self.state == State.TRANSFORM:
            # Log job number and step.
            self.logger.info("Transform for job " + str(self.job_number) + " began.")
            self.transformer()
        elif self.state == State.LOAD:
            # Log job number and step.
            self.logger.info("Load for job " + str(self.job_number) + " began.")
            self.loader()

        self.logger.info(self.get_step() + " complete.")
        self.socket_io.emit('work_complete')

        if self.single_method_class:
            self.state = 3
        else:
            self.next_state()

        self.store_dataframes()

    
    """
    This function will get fleshed out the more we require.
    """
    def validate_requirements_satisfied(self):
        if self.requirements_supplied:
            True

        # Check all the keys contain values.
        for key in self.requirements.keys():
            if not self.requirements[key]:
                
                raise Exception("RecordType requirements have not been satisfied")
        
        self.requirements_supplied = True
        return True


    """
    Handy function will convert all floats which are whole numbers in a json into integers.
    This is useful for id's etc. that are interpretted is floats incorrectly.
    """
    def convert_floats_to_ints(self, json):
        
        if isinstance(json, list):
            json = [self.convert_floats_to_ints(item) for item in json]
            return json

        elif isinstance(json, dict):
            json = {k : self.convert_floats_to_ints(v) for k, v in json.items()}
            return json

        if isinstance(json, float) and json.is_integer():
            return int(json)
            
        return json
            

            



    """
    This function will flatten a json response for a pandas frame as well as parse all lists into comma separated strings.
    """
    def json_to_frame(self, json_file):
        df = pd.json_normalize(json_file)

        # Fix all lists into dumps for json to be able to load and convert all floats that are whole numbers to ints.
        df = df.applymap(lambda x: json.dumps(self.convert_floats_to_ints(x)) if isinstance(x, list) else x)

        # This will cast all whole number floats to strings to drop the .0
        df = df.applymap(lambda x: str(int(x)) if isinstance(x, float) and x.is_integer() else x)
        
        df = df.fillna("").astype(str)

        
        
        return df

    """
    Will iterate through a frame returning each row as a json object of key value pairs. This is works even if there is a json stored in a cell as a string.
    """
    def iter_json(self, frame):
        cols = list(frame.columns)
        for index, row in frame.iterrows():
            op = {}
            for col in cols:
                try:
                    op[col] = json.loads(row[col])
                except:
                    op[col] = row[col]
            yield op
        

            
        

    def default_wishlist_loader(self, wishlist_table, wishlist_item_table, customer_nnumber, db_username, db_password):
        engine = get_engine_for(DatabaseType.LIVE_MYSQL, customer_nnumber, username=db_username, password=db_password)
        Session = sessionmaker(bind=engine)
        session = Session()

        wishlist_id_to_pk = {}

        for index, row in wishlist_table.iterrows():
            self.logger.info("Inserting wishlist " + str(index+1))
            current = WISHLIST(username=row["username"], wishlist_name=row["wishlist_name"], wishlist_active="y")
            session.add(current)
            session.flush()
            wishlist_id_to_pk[row["id"]] = current.wishlist_id


        wishlist_items = []
        for index, row in wishlist_item_table.iterrows():
            self.logger.info("Inserting wishlist item " + str(index+1))
            wishlist_items.append(WISHLISTITEM(wishlist_id=wishlist_id_to_pk[row["wishlist_id"]], SKU=row["SKU"]))
        
        session.add_all(wishlist_items)
        session.commit()

    def api_success(self, response):
        ack = response["Ack"]
        try:
            if ack == "Success":
                return True
            if ack == "Warning":
                self.logger.error(response["Messages"]["Warning"]["Message"])
                return False
            if ack == "Error":
                self.logger.error(response["Messages"]["Error"]["Message"])
                return False
        except:
            self.logger.error(response)

    def remove_non_alphanumeric(self, string):
        # Need to remove non alphanumeric characters.

        # 23/03/2020 There are apparently exceptions to this rule. '.' and '-' are allowed.
        exceptions = [".", "-", "_"]
        return_string = ""
        for char in string:
            if char.isalnum() or char in exceptions:
                return_string += char
        return return_string

    
    def default_product_category_loader(self, product_df, category_df, api_key, url):

        ## Check for errors.

        # Category external ID check.
        if not category_df["external_id"].all():
            self.logger.error("Not all external ID's in the categories table are populated. This is required for default_product_category_loader")
            exit()

        self.logger.info("Loading categories and retreiving IDs.")
        api = ContentApi(url, api_key)

        has_parents = []

        # original_id : new_id
        inserted = {}

        self.logger.info("Loading parent categories.")

        for index, row in category_df.iterrows():
            if str(row["external_parent_id"]) != '0' and str(row["external_parent_id"]) != "":
                _dict = row.to_dict()
                _dict["index"] = index
                has_parents.append(_dict)
                continue
            self.logger.info("Adding category " + row["content_name"])
            r = api.add_content(
                {
                    "ContentName":row["content_name"],
                    "ContentType":row["content_type"],
                    "ContentURL":row["content_url"],
                    "Description1" : row.get("description1", ""),
                    "ShortDescription1" : row.get("short_description1", ""),
                    "AutomaticURL":False,
                    "Active":True,
                    "SEOMetaDescription": row.get("seo_meta_description", ""),
                    "SEOMetaKeywords":row.get("seo_meta_keywords", ""),
                    "SEOPageHeading":row.get("seo_page_heading", ""),
                    "SEOPageTitle":row.get("seo_page_title", "")

                }
            )
            if r['Ack'] == 'Error':
                # TODO get message for this and make sure this is actually the case.
                self.logger.info("Category exists in Neto. Retrieving ID.")
                r = api.get_content({"ContentName" : row["content_name"]})
                try:
                    r = r["Content"][0]["ContentID"]
                except KeyError as e:
                    self.logger.error(str(r))
                inserted[row["external_id"]] = r
                category_df.at[index, "content_id"] = r
            else:
                inserted[row["external_id"]] = r["Content"]["ContentID"]
                category_df.at[index, "content_id"] = r["Content"]["ContentID"]
        
        # While the inserted count is less than the number of rows in the original frame.
        while len(has_parents) > 0:
            to_remove = []
            for i, child in enumerate(has_parents):    
                if child["external_parent_id"] in inserted.keys():
                    self.logger.info("Adding category " + child["content_name"])
                    r = api.add_content(
                        {
                            "ContentName":child["content_name"],
                            "ContentType":"Product Category",
                            "ContentURL":child["content_url"],
                            "Description1" : child.get("description1", ""),
                            "ShortDescription1" : child.get("short_description1", ""),
                            "ParentContentID": int(inserted[child["external_parent_id"]]),
                            "AutomaticURL":False,
                            "Active":True,
                            "SEOMetaDescription": row.get("seo_meta_description", ""),
                            "SEOMetaKeywords":row.get("seo_meta_keywords", ""),
                            "SEOPageHeading":row.get("seo_page_heading", ""),
                            "SEOPageTitle":row.get("seo_page_title", "")
                        }
                    )
                    if r['Ack'] == 'Error':
                        # TODO get message for this and make sure this is actually the case.                        
                        self.logger.info("Category exists in Neto. Retrieving ID.")
                        r = api.get_content({"ContentName" : child["content_name"]})["Content"][0]["ContentID"]
                        inserted[child["external_id"]] = r
                        category_df.at[child["index"], "content_id"] = r
                    else:
                        inserted[child["external_id"]] = r["Content"]["ContentID"]
                        category_df.at[child["index"], "content_id"] = r["Content"]["ContentID"]

                    to_remove.append(i)
            
            to_remove.sort(reverse = True)
            for n in to_remove:
                del has_parents[n]

        # Cast all category stuff into ints to be sure
        inserted = { int(k):int(v) for k,v in inserted.items() }

        def deserialise_request(d):
            for key in d.keys():
                # There is the case where a string containing digits, the letter e and at least two more can be passed as an SKU.
                # Json loads will interpret this as a float which we do not want. Scientific notation. We just want it returned as a string.
                try:
                    new = json.loads(d[key])
                    if isinstance(new, float):
                        continue
                    d[key] = new
                except:
                    pass
            return d

        def convert_bool(x):
            if isinstance(x, bool):
                return x
            if isinstance(x, str):
                if x.lower() == "true":
                    return True
                elif x.lower() == "false":
                    return False
                elif not x:
                    return x
                # Covers string 0's and 1's
                else:
                    return bool(int(x))
            if isinstance(x, int) or isinstance(x, float):
                if str(int(x)) == "1":
                    return True
                elif str(int(x)) == "0":
                    return False
                
            raise ValueError("'" + str(x) + "' is an invalid value in a boolean field.")

            

        # Return a list of items in common between two lists.
        def common(a, b):
            c = [value for value in a if value in b] 
            return c

        # Ensure that bool fields are bools.
        
        cols = ["Virtual", "Approved", "ApprovedForPOS", "Active", "Visible", "TaxFreeItem"]
        
        present_cols = common(cols, list(product_df.columns))
        product_df[present_cols] = product_df[present_cols].applymap(convert_bool)

        # Split parent products and children.
        parents = product_df[product_df["ParentSKU"] == ""].fillna("").reset_index()
        children = product_df[product_df["ParentSKU"] != ""].fillna("").reset_index()

        # Insert products based on new ID's.
        api = ProductApi(url, api_key)


        parent_sku_to_id = {}
        cols = parents.columns.to_list()

        for index, row in parents.iterrows():
            self.logger.info("Uploading parent " + row["Name"] + ". Record " + str(index + 1) + " of " + str(len(parents.index)))
            r = deserialise_request({key : row[key] for key in cols})


            # Categories
            categories = r["Categories"]
            r["Categories"] = {}
            r["Categories"]["Category"] = []
            for cat in categories:
                r["Categories"]["Category"].append({"CategoryID" : inserted[int(cat)]})

            while True:
                try:
                    response = api.add_product(r)
                    break
                except Exception:
                    pass

            if not self.api_success(response):
                self.logger.error(response)
                continue

            parent_sku_to_id[row["SKU"]] = response['Item']['InventoryID']

        for index, row in children.iterrows():
            self.logger.info("Uploading child " + row["Name"] + ". Record " + str(index + 1) + " of " + str(len(children.index)))
            r = deserialise_request({key : row[key] for key in cols})

            # Categories
            categories = r["Categories"]
            r["Categories"] = {}
            r["Categories"]["Category"] = []
            for cat in categories:
                r["Categories"]["Category"].append({"CategoryID" : inserted[int(cat)]})

        
            # Get the response and check it was okay.
            response = api.add_product(r)
            if not self.api_success(response):
                self.logger.error(response)
                continue

    # Check to see if a content images table was supplied and if so, upload the images via ftp.
    # Pass it a content_ftp_image formatted image.
    def default_category_image_loader(self, content_ftp_image, ftp_neto_site, ftp_username, password, neto_api_key):
        # Example: username = 'justin.lillico@neto.com.au@therapystore.neto.com.au', password = 'xxxxxx'.

        from io import BytesIO
        import paramiko

        mime_to_extension = {
            "image/gif" : "gif",
            "image/jpeg" : "jpg",
            "image/png" : "png",
            "image/tiff" : "tiff",
            "image/vnd.wap.wbmp" : "wbmp",
            "image/x-icon" : "ico",
            "image/x-jng" : "jng",
            "image/x-ms-bmp" : "bmp",
            "image/svg+xml" : "svg",
            "image/webp" : "webp"
        }
        

        transport = paramiko.Transport(('sftp.neto.net.au', 1022))

        username = ftp_username + "@" + ftp_neto_site
        transport.connect(username=username, password=password)

        client = paramiko.SFTPClient.from_transport(transport)
           
        base_dir = '/httpdocs/assets/webshop/cms/'

        api = ContentApi(ftp_neto_site, neto_api_key)

        for index, row in content_ftp_image.iterrows():
            self.logger.info("Uploading category image " + row["content_name"] + "(" + str(index + 1) + " of " + str(len(content_ftp_image)) + ")")

            url = row["image_url"]
            if not url:
                self.logger.info("No image link provided. Skipping.")
                continue

            # Neto has a naming convention for its category folders where the last two digits of the category
            # id are the folder name for the images. So lets use the provided name to get the id's.

            r = api.get_content({"ContentName" : row["content_name"]})
            if len(r['Content']) > 0:
                content_id = r["Content"][0]["ContentID"]
            else:
                self.logger.info("No category found for " + row["content_name"] + ". Skipping.")
                continue

            this_dir = base_dir + content_id[-2:]

            try:
                client.chdir(this_dir)  # Test if remote_path exists
            except IOError:
                client.mkdir(this_dir)  # Create remote_path

            client.chdir(this_dir)
            r = requests.get(url)
            ext = mime_to_extension.get(r.headers['Content-Type'], False)
            
            if not ext:
                self.logger.error("Unknown image type: " + r.headers['Content-Type'] + ". Skipping.")
                continue

            image = BytesIO(r.content)
            image.seek(0)
            f = client.file(content_id + "." + ext, "wb")
            f.write(image.getbuffer())


    def default_customer_loader(self, url, api_key, customer_table):

        api = CustomerApi(url, api_key)

        errors = pd.DataFrame({"Username" : ""})

        length = str(len(customer_table))

        for index, row_json in enumerate(self.iter_json(customer_table)):
            self.logger.info("Uploading customer " +str(index + 1) + " of " + length)

            # Remove date of birth if blank as API does not like it.
            if "DateOfBirth" in row_json and not row_json["DateOfBirth"]:
                del row_json["DateOfBirth"]
                
            r = api.add_customer(row_json)
            # This will print an error if one happened.
            try:
                if r["Ack"] == "Error":
                    self.logger.error(r["Messages"]["Error"]["Message"])
                    r["Username"] = row_json["Username"]
                    errors = errors.append(r, ignore_index=True)
            except:
                self.logger.error(r)

        if len(errors) > 0:
            self.downloads["errors"] = errors

    def default_order_loader(self, url, api_key, order_table, at_a_time=250):
        
        api = OrderApi(url, api_key)
        length = len(order_table)
        counter = 0
        orders = []

        for index, row_json in enumerate(self.iter_json(order_table)):            
            
            orders.append(row_json)
            
            if counter == at_a_time - 1 or index == length - 1:
                self.logger.info("Uploading orders: " + str(index - counter + 1) + " through to " + str(min((index + 1), length)) + " of a total " + str(length) + " orders.")
                r = api.add_order(orders)

                # This will print an error if one happened.
                try:
                    if r["Ack"] == "Error":
                        for error in r["Messages"]["Error"]:
                            self.logger.error(error["Message"])
                except:
                    self.logger.error(r)
                counter = 0
                orders.clear()
            
            counter += 1
            

    def default_product_quantity_updater(self, url, api_key, quantity_table, at_a_time=250):
        api = ProductApi(url, api_key)
        
        counter = 0
        updates = []
        length = len(quantity_table)

        for index, row_json in enumerate(self.iter_json(quantity_table)):
            
            updates.append(row_json)

            if counter == at_a_time - 1 or index == length - 1:
                self.logger.info("Uploading product quantities: " + str(index - counter + 1) + " through to " + str(min((index + 1), length)) + " of a total " + str(length) + " orders.")
                r = api.update_products(updates)
                print(r)

                # This will print an error if one happened.
                try:
                    if r["Ack"] == "Error":
                        for error in r["Messages"]["Error"]:
                            self.logger.error(error["Message"])
                except:
                    self.logger.error(r)
                counter = 0
                updates.clear()
            
            counter += 1





            
