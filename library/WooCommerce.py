# # If the status is trashed, set it to inactive and not visible.

from woocommerce import API
from library.base_classes.record_type import RecordType
from json import loads, dumps
from netoapi.content import ContentApi


class WooCommerceProducts(RecordType):
    name = "Woo Commerce Products/Categories"
    record_type_class = True

    requirements = {
        "URL" : "",
        "Consumer Key" : "",
        "Consumer Secret" : "",
        "Neto API Key" : "",
        "Neto customer URL" : "",
        "API Version" : ["wc/v3", "Legacy"]
    }

    step_instructions = {

        "Extract" : "NotDefined",
        "Transform" : "NotDefined",
        "Load" : "Be sure to set 'Misc2' and 'Misc3' in neto to 'external_id' and 'external_label' respectively."

    }

    instructions = """This tool extracts the products from a WooCommerce site and transforms them for use in Neto.
    
    
    DEVELOPMENT NOTES:
    This currently does not upload category images.
    It will need to rename the images column when downloading categories to something the loader accepts.
    The loader will then need to take from the user their FTP username and password and use this information
    to concatenate to it @<the clients site>.

    The loader will iterate through the images for each category and download into memory the image. Then it 
    must connect to the clients FTP using the above and rename the file to the ID of the category in the Neto 
    DB. Then it must be saved to '/httpdocs/assets/webshop/cms/<last two digits of the content ID>'. Alt images 
    are renamed the same but with an appended '-1'.

    That should be all there is to this.

    LEGACY API
    Working with this API is a nightmare. I am currently working on building this out. There does not seem to be a useful endpoint for
    the category images.
    
    
    """

    def extractor(self):
        wcapi = API(
            url= self.requirements["URL"], # "https://www.thetherapystore.com.au/",
            consumer_key = self.requirements["Consumer Key"], # "ck_d0026fe1f269be1fee179a4d1a54d3d144a9f6d8",
            consumer_secret= self.requirements["Consumer Secret"], #"cs_5910fe81c7ccfee881fd6b6ab89861c0b2a5bb62",
            wp_api = self.requirements["API Version"] != "Legacy",
            version= self.requirements["API Version"] if self.requirements["API Version"] != "Legacy" else "v3",
            timeout=10
        )
        self.total_pages = "X-WP-TotalPages" if self.requirements["API Version"] != "Legacy" else "X-WC-TotalPages"
        self.total = "X-WP-Total" if self.requirements["API Version"] != "Legacy" else "X-WC-Total"
        self.get_products(wcapi)
        self.get_categories(wcapi)

    
    def get_categories(self, wcapi):

        cats = []
        retrieved = []

        for index, row in self.working_dataframes["products"].iterrows():
            cat_json = loads(row["categories"])
            for cat in cat_json:
                if cat["id"] not in retrieved:
                    response = wcapi.get("products/categories/" + str(cat["id"])).json()
                    retrieved.append(cat["id"])
                    cats.append(response)

        
        self.working_dataframes["categories"] = self.json_to_frame(cats)
        self.status["Categories"] = self.working_dataframes["categories"].shape[0]


        
    def get_products(self, wcapi):
        response = wcapi.get("products")

        total_pages = int(response.headers[self.total_pages])

        self.logger.info("Getting " + str(response.headers[self.total]) + " parent products.")

        response = []

        for i in range(1, total_pages+1):

            self.logger.info("Page " + str(i) + " of " + str(total_pages))
            # Two different behaviours depending on if legacy or not.
            if self.requirements["API Version"] == "Legacy":
                response +=wcapi.get("products?&page="+str(i)).json()["products"]
                if i == 1:
                    break
            else:
                response += wcapi.get("products?&page="+str(i)).json()

        self.logger.info("Done. Converting to a dataframe.")

        self.working_dataframes["products"] = self.json_to_frame(response)

        variations = []

        response = []

        self.logger.info("Getting variation products.")

        length = str(self.working_dataframes["products"].shape[0])

        for index, row in self.working_dataframes["products"].iterrows():

            # The variations are located in the product information. We do things differently in this case.
            if self.requirements["API Version"] == "Legacy":
                v = loads(row["variations"])
                for variation in v:
                    variation["parent_id"] = row["id"]
                
                variations += v
                continue

            ep = "products/" + str(row["id"]) + "/variations"



            response = wcapi.get(ep)
            total_pages = int(response.headers[self.total_pages])

            self.logger.info(str(index + 1) + " of " + length + " parent products parsed...")

            response = []

            for i in range(1, total_pages+1):
                self.logger.info("Parent product " + str(index + 1) + ": variation: " + str(i))
                response += wcapi.get("products/" + str(row["id"]) + "/variations?&page="+str(i)).json()

            # Add the parent id to each result.
            for variation in response:
                variation["parent_id"] = row["id"]

            variations += response

        self.working_dataframes["variations"] = self.json_to_frame(variations)

        self.status["Parent Products"] = self.working_dataframes["products"].shape[0]
        self.status["Variation Products"] = self.working_dataframes["variations"].shape[0]


        self.logger.info("Task complete.")

    def transform_categories(self):
        # Category work.
        self.logger.info("Adding urls to categories.")
        self.working_dataframes["categories"]["content_url"] = ""
        self.working_dataframes["categories"]["content_url"] = self.working_dataframes["categories"].apply(lambda x: "product-cat/" + x["slug"] + "/", axis=1)
        self.working_dataframes["categories"]["content_type"] = "Product Category"

        self.working_dataframes["categories"] = self.working_dataframes["categories"].rename(columns={
            "name" : "content_name",
            "description" : "description1",
            "parent" : "external_parent_id",
            "id" : "external_id"
        })

        self.working_dataframes["categories"] = self.format_transform_table("content", self.working_dataframes["categories"])

    def transform_products(self):
        self.logger.info("Transforming products.")
        self.logger.info("Adding missing SKUs.")

        # Ensure no nan's
        self.working_dataframes["products"] = self.working_dataframes["products"].fillna("")
        self.working_dataframes["variations"] = self.working_dataframes["variations"].fillna("")
        
        # Replace blank SKUs
        self.working_dataframes["products"]["sku"] = self.working_dataframes["products"].apply(lambda x: x["sku"] if x["sku"] else "P" + str(x["id"]), axis=1)
        self.working_dataframes["variations"]["sku"] = self.working_dataframes["variations"].apply(lambda x: x["sku"] if x["sku"] else "P" + str(x["id"]), axis=1)

        # Transform permalink to match product_url format.
        self.working_dataframes["products"]["permalink"] = self.working_dataframes["products"]["permalink"].apply(lambda x: "product/" + x.split("/product/")[-1])
        self.working_dataframes["variations"]["permalink"] = self.working_dataframes["variations"]["permalink"].apply(lambda x: "product/" + x.split("/product/")[-1])

        # Apply parent SKUs to variation products.
        external_id_to_sku = {}
        for index, row in self.working_dataframes["products"].iterrows():
            external_id_to_sku[str(row["id"])] = row["sku"]
        
        self.working_dataframes["variations"]["parent_sku"] = ""
        self.working_dataframes["variations"]["parent_sku"] = self.working_dataframes["variations"].apply(lambda x: external_id_to_sku[str(x["parent_id"])], axis=1)

        # Create accounting code field.
        self.working_dataframes["variations"]["accounting_code"] = self.working_dataframes["variations"]["sku"]
        self.working_dataframes["products"]["accounting_code"] = self.working_dataframes["products"]["sku"]

        # Convert catalog_visibility to active, visible and approved.
        self.working_dataframes["products"]["status"] = self.working_dataframes["products"]["status"].apply(lambda x: "True" if x == "publish" else "False")
        self.working_dataframes["products"]["Visible"] = ""
        self.working_dataframes["products"]["Visible"] = self.working_dataframes["products"]["status"]
        self.working_dataframes["products"]["Approved"] = ""
        self.working_dataframes["products"]["Approved"] = self.working_dataframes["products"]["status"]
        self.working_dataframes["products"]["Active"] = ""
        self.working_dataframes["products"]["Active"] = self.working_dataframes["products"]["status"]
        
        self.working_dataframes["variations"]["purchasable"] = self.working_dataframes["variations"]["purchasable"].apply(lambda x: "True" if str(x) == "1" else "False")
        self.working_dataframes["variations"]["Approved"] = ""
        self.working_dataframes["variations"]["Approved"] = self.working_dataframes["variations"]["purchasable"]
        self.working_dataframes["variations"]["Active"] = ""
        self.working_dataframes["variations"]["Active"] = self.working_dataframes["variations"]["purchasable"]

        # Add them to default warehouse.
        self.working_dataframes["variations"]["WarehouseQuantity"] = ""
        self.working_dataframes["products"]["WarehouseQuantity"] = ""

        self.working_dataframes["variations"]["WarehouseQuantity"] = self.working_dataframes["variations"].apply(lambda x: dumps([{"WarehouseID" : 1, "Quantity" : x["stock_quantity"]}]) if x["stock_quantity"] else "", axis=1)
        self.working_dataframes["products"]["WarehouseQuantity"] = self.working_dataframes["products"].apply(lambda x: dumps([{"WarehouseID" : 1, "Quantity" : x["stock_quantity"]}]) if x["stock_quantity"] else "", axis=1)

        # Format categories

        def process_categories(x):
            if not x:
                return x
            x = loads(x)
            output = []
            for item in x:
                output.append(item["id"])
            
            return dumps(output)

        self.working_dataframes["products"]["categories"] = self.working_dataframes["products"]["categories"].apply(process_categories)

        # Apply parent categories, names and descriptions to variant products.
        self.working_dataframes["variations"]["categories"] = ""
        external_id_to_category_id = {}
        external_id_to_description = {}
        external_id_to_name = {}

        for index, row in self.working_dataframes["products"].iterrows():
            external_id_to_category_id[row["id"]] = row["categories"]
            external_id_to_description[row["id"]] = row["short_description"]
            external_id_to_name[row["id"]] = row["name"]

        self.working_dataframes["variations"]["Name"] = ""

        for index, row in self.working_dataframes["variations"].iterrows():
            self.working_dataframes["variations"].loc[index, "categories"] = external_id_to_category_id[row["parent_id"]]
            self.working_dataframes["variations"].loc[index, "Name"] = external_id_to_name[row["parent_id"]]
            
            # If no description, copy the parent.
            if row["description"] == "":
                self.working_dataframes["variations"].loc[index, "description"] = external_id_to_description[row["parent_id"]]

        # Setup specifics
        def process_attributes(x):
            if not x:
                return x

            x = loads(x)
            output = { "ItemSpecific" : []}
            for item in x:
                output["ItemSpecific"].append({
                    "Name" : item["name"],
                    "Value" : item["option"]
                })

            # If the specifics don't exist, these cannot be imported.
            if len(output["ItemSpecific"]) == 0:
                return ""

            return dumps(output)

        self.working_dataframes["variations"]["attributes"] = self.working_dataframes["variations"]["attributes"].apply(process_attributes)

        # Create rejected variations frame and remove them from the primary frame.
        self.working_dataframes["rejected_variations"] = self.working_dataframes["variations"][self.working_dataframes["variations"]["attributes"] == ""].fillna("")
        self.working_dataframes["variations"] = self.working_dataframes["variations"][self.working_dataframes["variations"]["attributes"] != ""].fillna("")

        def process_parent_images(x):
            if x["images"] == "":
                return x["images"]
            d = loads(x["images"])
            l = []
            over_count = 0
            for item in d:
                pos = str(item["position"])
                name = ""
                if pos == "0":
                    name = "Main"
                else:
                    name = "Alt " + pos
                if int(pos) > 9: # Too many images for default.
                    over_count += 1
                l.append({
                    "Name" : name,
                    "URL" : item["src"]
                })
            if over_count > 0:
                if "Over Default Images" not in self.status:
                    self.status["Over Default Images"] = ""
                self.status["Over Default Images"] += x["sku"] + " : " + str(over_count) + ", "
            return dumps({"Image" : l })

        self.working_dataframes["products"]["images"] = self.working_dataframes["products"].apply(process_parent_images, axis=1)

        # Output the number of over images.
        if "Over Default Images" not in self.status:
            self.status["Over Default Images"] = self.status["Over Default Images"][:-2] + ". Please increase the image count in neto to allow the maxiumum in this list."

        self.working_dataframes["variations"]["image.src"] = self.working_dataframes["variations"]["image.src"].apply(lambda x: dumps({"Image" : [{"Name" : "Main", "URL" : x}]}))

        # Add external label to variants and parents.
        self.working_dataframes["variations"]["Misc3"] = "variant"
        self.working_dataframes["products"]["Misc3"] = "parent"

        # Rename some fields.
        self.working_dataframes["products"] = self.working_dataframes["products"].rename(columns={
            "sku" : "SKU",
            "menu_order" : "SortOrder1",
            "id" : "Misc2",
            "permalink" : "ItemURL",
            "price" : "DefaultPrice",
            "weight" : "ShippingWeight",
            "dimensions.length" : "ShippingLength",
            "dimensions.height" : "ShippingHeight",
            "dimensions.width" : "ShippingWidth",
            "categories" : "Categories",
            "images" : "Images",
            "short_description" : "Description",
            "description" : "description_main",
            "name" : "Name"

        })

        self.working_dataframes["variations"] = self.working_dataframes["variations"].rename(columns={
            "sku" : "SKU",
            "parent_sku" : "ParentSKU",
            "menu_order" : "SortOrder1",
            "id" : "Misc2",
            "permalink" : "ProductURL",
            "price" : "DefaultPrice",
            "weight" : "ShippingWeight",
            "dimensions.length" : "ShippingLength",
            "dimensions.height" : "ShippingHeight",
            "dimensions.width" : "ShippingWidth",
            "attributes" : "ItemSpecifics",
            "categories" : "Categories",
            "image.src" : "Images",
            "description" : "Description"

        })


        # Merge frames into one.
        self.working_dataframes["products_and_variations"] = self.format_transform_table("products", self.working_dataframes["products"]).append(self.format_transform_table("products", self.working_dataframes["variations"])).fillna("")


    def transformer(self):
        self.transform_categories()
        self.transform_products()

        
    def loader(self):
        self.default_product_category_loader(self.working_dataframes["products_and_variations"], 
                                             self.working_dataframes["categories"],
                                             self.requirements["Neto API Key"],
                                             self.requirements["Neto customer URL"]
                                             )

"""
Need to document this sql...

It gets the products into the correct format for this.

-- PRODUCTS

SELECT 

p.id as 'DOKO ID',
p.sku as 'SKU',
p.inventory_id as 'InventoryID',
p.parent_sku as 'ParentSKU',
p.accounting_code as 'AccountingCode',
p.quantity as 'WarehouseQuantity',
p.virtual as 'Virtual',
p.brand as 'Brand',
p.name as 'Name',
p.model as 'Model',
p.sort_order1 as 'SortOrder1',
p.rrp as 'RRP',
p.default_price as 'DefaultPrice',
p.promotion_price as 'PromotionPrice',
p.promotion_start_date as 'PromotionStartDate',
p.promotion_expiry_date as 'PromotionExpiryDate',
p.cost_price as 'CostPrice',
p.approved as 'Approved', 
p.approved_for_pos as 'ApprovedForPOS',
p.active as 'Active', 
p.visible as 'Visible',
p.tax_free_item as 'TaxFreeItem',
p.search_keywords as 'SearchKeywords',
p.short_description as 'ShortDescription',
p.description as 'Description',
p.terms_and_conditions as 'TermsAndConditions',
p.features as 'Features',
p.specifications as 'Specifications',
p.warranty as 'Warranty',
p.artist_or_author as 'ArtistOrAuthor',
p.sub_title as 'Subtitle',
p.availability_description as 'AvailabilityDescription',
p.product_url as 'ProductURL',
p.upc as 'UPC',
p.item_height as 'ItemHeight',
p.item_length as 'ItemLength',
p.item_width as 'ItemWidth',
p.shipping_height as 'ShippingHeight',
p.shipping_width as 'ShippingWidth',
p.shipping_length as 'ShippingLength',
p.supplier_item_code as 'SupplierItemCode',
p.seo_page_title as 'SEOPageTitle',
p.seo_meta_keywords as 'SEOMetaKeywords',
p.seo_page_heading as 'SEOPageHeading',
p.seo_meta_description as 'SEOMetaDescription',
p.external_id


FROM planetnails_wp.product p;

-- CONTENT RELATIONSHIPS.

SELECT
  p.id,
  c.external_id
FROM product p
  INNER JOIN wp_term_relationships tr ON p.external_id = tr.object_id
  INNER JOIN wp_term_taxonomy tt ON tr.term_taxonomy_id = tt.term_taxonomy_id
  INNER JOIN wp_terms tp ON tt.term_id = tp.term_id AND tt.taxonomy = 'product_cat'
  INNER JOIN content c ON tp.term_id = c.external_id
WHERE IFNULL(c.external_id, 0) <> 0;

"""

class WooCommerceProductsCategoriesNoApi(WooCommerceProducts):
    name = "Woo Commerce Products & Categories No Api"
    record_type_class = True

    requirements = {
        "Neto API Key" : "",
        "Neto customer URL" : "",
    }

    uploads = {
        "Content File" : "",
        "Product File" : "",
        "Product Images" : "",
        "Product Specifics" : "",
        "Product/Content Relationships" : ""
    }

    instructions = """
    
    This tool is designed to take a DOKO transformation and load it into Neto via the API.

    """

    def extractor(self):
        self.working_dataframes["categories"] = self.uploads["Content File"]
        self.working_dataframes["products"] = self.uploads["Product File"]
        self.working_dataframes["product_specifics"] = self.uploads["Product Specifics"]
        self.working_dataframes["product_images"] = self.uploads["Product Images"]
        self.working_dataframes["content_relationships"] = self.uploads["Product/Content Relationships"]

    def transformer(self):
        self.logger.info("Renaming fields.")
        # Rename ID to external id for categories
        self.working_dataframes["categories"] = self.working_dataframes["categories"].rename({"id" : "external_id"})

        self.logger.info("Setting empty SKUs, accounting codes and names to use external id.")
        # For any products without SKU, accounting code and name, use external id.
        self.working_dataframes["products"]["SKU"] = self.working_dataframes["products"].apply(lambda x: x["SKU"] if x["SKU"] else x["external_id"], axis=1)
        self.working_dataframes["products"]["AccountingCode"] = self.working_dataframes["products"].apply(lambda x: x["AccountingCode"] if x["AccountingCode"] else x["external_id"], axis=1)
        self.working_dataframes["products"]["Name"] = self.working_dataframes["products"].apply(lambda x: x["Name"] if x["Name"] else x["external_id"], axis=1)

        self.logger.info("Setting 'trashed' items to inactive.")
        # When importing from db using DOKO scripts, it imports 'trashed' items as well. We will import these, but set their active status to false.
        self.working_dataframes["products"]["Active"] = self.working_dataframes["products"].apply(lambda x: 0 if "__trashed" in x["ProductURL"] else x["Active"], axis=1)

        self.logger.info("Mapping images, specifics and categories to products.")

        # Insert images, specifics and categories to cell.
        self.working_dataframes["products"]["Images"] = ""
        self.working_dataframes["products"]["ItemSpecifics"] = ""
        self.working_dataframes["products"]["Categories"] = ""

        # Warehouses.
        self.working_dataframes["products"]["WarehouseQuantity"] = self.working_dataframes["products"]["WarehouseQuantity"].apply(lambda x: dumps([{"WarehouseID" : 1, "Quantity" : x}]))


        l = str(len(self.working_dataframes["products"]))
        for index, row in self.working_dataframes["products"].iterrows():
            doko_id = row["DOKO ID"]
            filtered_images = self.working_dataframes["product_images"][self.working_dataframes["product_images"]["product_id"] == doko_id]
            filtered_specifics = self.working_dataframes["product_specifics"][self.working_dataframes["product_specifics"]["product_id"] == doko_id]
            filtered_categories = self.working_dataframes["content_relationships"][self.working_dataframes["content_relationships"]["id"] == doko_id]

            image = []
            item_specific = []
            categories = []

            for index2, row2 in filtered_images.iterrows():
                image.append({
                    "Name" : filtered_images.loc[index2, "name"],
                    "URL" : filtered_images.loc[index2, "url"]
                })

            for index2, row2 in filtered_specifics.iterrows():
                item_specific.append({
                    "Name": filtered_specifics.loc[index2, "specific_name"],
                    "Value": filtered_specifics.loc[index2, "specific_value"]
                })

            for index2, row2 in filtered_categories.iterrows():
                categories.append(int(filtered_categories.loc[index2, "external_id"]))
            
            self.working_dataframes["products"].loc[index, "Images"] = dumps({ "Image" : image })
            self.working_dataframes["products"].loc[index, "ItemSpecifics"] = dumps({ "ItemSpecific" : item_specific })
            self.working_dataframes["products"].loc[index, "Categories"] = dumps(categories)

            # Drop what we do not need.
            self.working_dataframes["products"] = self.format_transform_table("products", self.working_dataframes["products"])



    def loader(self):
        self.default_product_category_loader(self.working_dataframes["products"], self.working_dataframes["categories"], self.requirements["Neto API Key"], self.requirements["Neto customer URL"])



"""
Get doko customers

SELECT 

id,
username as 'Username',
email_address as 'EmailAddress',
secondary_email_address as 'SecondaryEmailAddress',
gender as 'Gender',
date_of_birth as 'DateOfBirth',
internal_notes as 'InternalNotes',
abn as 'ABN',
website_url as 'WebsiteURL',
active as 'Active',
user_group as 'UserGroup',
bill_first_name as 'BillFirstName',
bill_last_name as 'BillLastName',
bill_company as 'BillCompany',
bill_street_line1 as 'BillStreetLine1',
bill_street_line2 as 'BillStreetLine2',
bill_city as 'BillCity',
bill_state as 'BillState',
bill_post_code as 'BillPostCode',
bill_country as 'BillCountry',
bill_phone as 'BillPhone',
bill_fax as 'BillFax'

 FROM customer;
 

 Get doko shipping data
SELECT 

customer_id,
ship_title as 'ShipTitle',
ship_first_name as 'ShipFirstName',
ship_last_name as 'ShipLastName',
ship_company as 'ShipCompany',
ship_street_line1 as 'ShipStreetLine1',
ship_street_line2 as 'ShipStreetLine2',
ship_city as 'ShipCity',
ship_state as 'ShipState',
ship_post_code as 'ShipPostCode',
ship_phone as 'ShipPhone',
ship_fax as 'ShipFax'


 FROM customer_shipping_address;
"""


class WooCommerceCustomersNoApi(WooCommerceProducts):
    name = "Woo Commerce Customers No Api"
    record_type_class = True

    requirements = {
        "Neto API Key" : "",
        "Neto customer URL" : "",
    }

    uploads = {
        "Customers File" : "",
        "Customers Address File" : ""
    }

    instructions = """
    
    This tool will manually transform DOKO customers and upload them to Neto.

    """

    def extractor(self):
        self.working_dataframes["customers"] = self.uploads["Customers File"]
        self.working_dataframes["customers_shipping"] = self.uploads["Customers Address File"]

    def transformer(self):
        # Process shipping data.

        bill_cols = [item for item in list(self.working_dataframes["customers"].columns) if item[0:4] == "Bill"]

        def billing_address(x):
            json = {}

            for col in bill_cols:
                json[col] = x[col]

            return dumps(json)

        self.working_dataframes["customers"]["BillingAddress"] = self.working_dataframes["customers"].apply(billing_address, axis=1)

        self.working_dataframes["customers"]["ShippingAddress"] = ""

        shipping_cols = [item for item in list(self.working_dataframes["customers_shipping"].columns) if item[0:4] == "Ship"]

        for index, row in self.working_dataframes["customers"].iterrows():
            id = row["id"]

            # NOTE: It seems there is only the ability to add one shipping address via API so this is only going to upload the first.
            shipping_address = self.working_dataframes["customers_shipping"][self.working_dataframes["customers_shipping"]["customer_id"] == id].reset_index()

            json = {item : shipping_address.loc[0, item] for item in shipping_cols}

            self.working_dataframes["customers"].loc[index, "ShippingAddress"] = dumps(json)

    def loader(self):
        self.default_customer_loader(self.requirements["Neto customer URL"], self.requirements["Neto API Key"], self.working_dataframes["customers"])

"""
-- Get the orders.
SELECT
 
p.id as 'OrderID',
p.post_date as 'DatePlaced',
u.user_login as 'Username',
max( CASE WHEN pm.meta_key = '_wcj_invoicing_invoice_date' and p.ID = pm.post_id THEN FROM_UNIXTIME(pm.meta_value) END ) as 'DateInvoiced',
max( CASE WHEN pm.meta_key = '_billing_email' and p.ID = pm.post_id THEN pm.meta_value END ) as 'Email',
max( CASE WHEN pm.meta_key = '_billing_first_name' and p.ID = pm.post_id THEN pm.meta_value END ) as 'BillFirstName',
max( CASE WHEN pm.meta_key = '_billing_last_name' and p.ID = pm.post_id THEN pm.meta_value END ) as 'BillLastName',
max( CASE WHEN pm.meta_key = '_billing_address_1' and p.ID = pm.post_id THEN pm.meta_value END ) as 'BillStreet1',
max( CASE WHEN pm.meta_key = '_billing_address_2' and p.ID = pm.post_id THEN pm.meta_value END ) as 'BillStreet2',
max( CASE WHEN pm.meta_key = '_billing_city' and p.ID = pm.post_id THEN pm.meta_value END ) as 'BillCity',
max( CASE WHEN pm.meta_key = '_billing_state' and p.ID = pm.post_id THEN pm.meta_value END ) as 'BillState',
max( CASE WHEN pm.meta_key = '_billing_postcode' and p.ID = pm.post_id THEN pm.meta_value END ) as 'BillPostCode',
max( CASE WHEN pm.meta_key = '_shipping_first_name' and p.ID = pm.post_id THEN pm.meta_value END ) as 'ShipFirstName',
max( CASE WHEN pm.meta_key = '_shipping_last_name' and p.ID = pm.post_id THEN pm.meta_value END ) as 'ShipLastName',
max( CASE WHEN pm.meta_key = '_shipping_address_1' and p.ID = pm.post_id THEN pm.meta_value END ) as 'ShipStreet1',
max( CASE WHEN pm.meta_key = '_shipping_address_2' and p.ID = pm.post_id THEN pm.meta_value END ) as 'ShipStreet2',
max( CASE WHEN pm.meta_key = '_shipping_city' and p.ID = pm.post_id THEN pm.meta_value END ) as 'ShipCity',
max( CASE WHEN pm.meta_key = '_shipping_state' and p.ID = pm.post_id THEN pm.meta_value END ) as 'ShipState',
max( CASE WHEN pm.meta_key = '_shipping_postcode' and p.ID = pm.post_id THEN pm.meta_value END ) as 'ShipPostCode',
max( CASE WHEN pm.meta_key = '_order_total' and p.ID = pm.post_id THEN pm.meta_value END ) as order_total,
max( CASE WHEN pm.meta_key = '_order_tax' and p.ID = pm.post_id THEN pm.meta_value END ) as order_tax,
max( CASE WHEN pm.meta_key = '_paid_date' and p.ID = pm.post_id THEN pm.meta_value END ) as paid_date,
( select group_concat( order_item_name separator '|' ) from wp_woocommerce_order_items where order_id = p.ID ) as order_items
FROM planetnails_wp.wp_posts p
INNER JOIN planetnails_wp.wp_postmeta pm ON p.ID=pm.post_id
INNER JOIN planetnails_wp.wp_wc_order_stats os ON os.order_id=p.id
INNER JOIN planetnails_wp.wp_wc_customer_lookup cl ON cl.customer_id=os.customer_id
INNER JOIN planetnails_wp.wp_users u ON u.id=cl.user_id
where p.post_type='shop_order'
group by
p.ID;

-- Get the order lines.

"""