from library.base_classes.record_type import RecordType
import requests
from netoapi.content import ContentApi
import json
from netools.database import DatabaseType, get_engine_for, get_ansi_connection
import pandas as pd
from SQLORM.NetoORM import VOUCHER, VOUCHERCREDIT
from sqlalchemy.orm import Session, sessionmaker


class BigCommerceContentParentUpdate(RecordType):
    # name = "Big Commerce Content Parent Update"

    requirements = {

        "Neto API Key" : "",
        "Neto customer URL" : "",
        "ParentID" : "",
        "ID's (comma separated)" : "",
        "Content Type" : ""

    }

    def extractor(self):
        pass

    def transformer(self):
        pass

    def loader(self):
        payloads = []

        api = ContentApi(self.requirements["Neto customer URL"], self.requirements["Neto API Key"])

        for item in self.requirements["ID's (comma separated)"].split(","):

            payload = {
                "ContentID" : item,
                "ParentContentID" : self.requirements["ParentID"],
                "ContentType" : self.requirements["Content Type"]
            }

            api.update_content(payload)


class BigCommerceAddVoucher(RecordType):
    name = "Add Voucher"
    record_type_class = True
    instructions = "This module will take a csv in the format 'order_id, sku, senderemail, sendername, recipientname, recipientemail, giftmessage, datesendvoucher' and add them all to neto."
    single_method_class = True

    requirements = {

        "Neto API Key" : "",
        "Neto customer URL" : ""

    }

    uploads = {
        "working_file" : ""
    }

    def execute():
        pass


    def transformer(self):
        pass

    def loader(self):

        api = VoucherApi(self.requirements["Neto customer URL"], self.requirements["Neto API Key"])
        
        headers = list(self.uploads["working_file"].columns)
        payload = {}

        self.uploads["working_file"] = self.uploads["working_file"].fillna("")

        for index, row in self.uploads["working_file"].iterrows():
            for h in headers:
                payload[h] = row[h]

            print(payload)
            
            print(api.add_voucher(payload))


class NetoGetApiLog(RecordType):
    name = "Get API Log"
    record_type_class = True
    instructions = "This will get an API log and give you a csv."
    single_method_class = True

    api_map = {

        'paypal' : "processor_module='paypal_express' OR processor_module='paypal'",
        'stripe' : "processor_module='stripe'",
        'eway' : "processor_module='eway_rapid' OR processor_module='eway_securepanel'",
        'braintree' : "processor_module='braintree'",
        'zip' : "processor_module='zipmoney_v2'",
        'afterpay' : "processor_module='afterpay_v1'"

    }

    requirements = {

        "N Number" : 0,
        "API type" : api_map.keys(),
        "DB Username" : "",
        "DB Password" : ""

    }

    step_instructions = {

        "Extract" : "Give us your data for this.",
        "Transform" : "Not much will happen here, you'll just go to load",
        "Load" : "And next will be download..."

    }

    

    conn = None

    def extractor(self):
        self.logger.info("Connecting to:" + self.requirements["N Number"] + "_main")
        self.conn = get_engine_for(DatabaseType.LIVE_MYSQL, self.requirements["N Number"] + "_main", username=self.requirements["DB Username"], password=self.requirements["DB Password"])
        self.logger.info("Success!")
        # Get the data into a pandas dataframe.
        self.logger.info("Running Query...")
        self.working_dataframes["main"] = pd.read_sql("SELECT * FROM INTEGRATION_API_LOG WHERE " + self.api_map[self.requirements["API type"]], self.conn).fillna("")
        self.logger.info("Loading dataframe.")
        self.downloads[self.requirements["N Number"]] = self.working_dataframes["main"]
        self.logger.info("Process complete!")

    def transformer(self):
        pass

    def loader(self):
        pass
        



class NetoGetCustomers(RecordType):
    name = "Get Content"
    record_type_class = True
    instructions = "This will get a table of content to look at."

    requirements = {

        "Neto API Key" : "",
        "Neto customer URL" : "",
        "Orders to get" : ""

    }

    def extractor(self):
        self.logger.info("Querying neto...")
        api = ContentApi(self.requirements["Neto customer URL"], self.requirements["Neto API Key"])
        responses = []
        for item in self.requirements["Orders to get"].split(","):
            res = api.get_content({"ContentID" : item, "OutputSelector" : "ContentID"})
            if(len(res) > 0):
                responses.append(res)
        print(responses)
        self.working_dataframes["response"] = self.json_to_frame(responses)

    def transformer(self):
        pass

    def loader(self):
        pass  

class NetoImportVouchers(RecordType):
    name = "Import Vouchers"
    record_type_class = True
    instructions = "This will take a csv file containing information about vouchers to add into Neto and add them."

    requirements = {

        "Database Username" : "",
        "Database password" : "",
        "Customer db name" : ""

    }

    uploads = {
        "Datafile" : ""
    }

    step_instructions = {

        "Extract" : """
        Provide a datafile to work with.<br>The data should be of the format: <br>
        <table style="width:100%" border="1"><tbody><tr><th>DateCreated</th><th>VoucherType</th><th>VoucherStatus(reward, gift ,thirdparty)</th><th>InitialValue</th><th>RedeemedValue</th><th>RemainingValue</th><th>Username</th><th>VoucherTitle</th><th>OrderID</th><th>Username</th><th>Email</th></tbody></table><br>""",
        "Transform" : "NotDefined",
        "Load" : "NotDefined"

    }

    def extractor(self):
        self.logger.info("Extracting table...")
        self.working_dataframes["vouchers"] = self.uploads["Datafile"].fillna("")

    def transformer(self):
        self.logger.info("Removing '$' from numeric columns.")

        # Just in case some goose left spaces in the column headers (which seems to happen...)
        self.working_dataframes["vouchers"] = self.working_dataframes["vouchers"].rename(columns=lambda x: x.strip())

        self.working_dataframes["vouchers"]['RemainingValue'] = self.working_dataframes["vouchers"]['RemainingValue'].apply(lambda x: x.replace("$", "") if isinstance(x, str) else x)
        self.working_dataframes["vouchers"]['InitialValue'] = self.working_dataframes["vouchers"]['InitialValue'].apply(lambda x: x.replace("$", "") if isinstance(x, str) else x)
        self.working_dataframes["vouchers"]['RedeemedValue'] = self.working_dataframes["vouchers"]['RedeemedValue'].apply(lambda x: x.replace("$", "") if isinstance(x, str) else x)
        self.logger.info("Transforming dates to match Neto.")
        self.working_dataframes["vouchers"]['DateCreated'] = pd.to_datetime(self.working_dataframes["vouchers"]['DateCreated'], dayfirst=True)

        # Add a unique voucher id column
        engine = get_engine_for(DatabaseType.LIVE_MYSQL, self.requirements["Customer db name"], username=self.requirements["Database Username"], password=self.requirements["Database password"])  
        conn = get_ansi_connection(engine)
        used_codes = list(pd.read_sql("SELECT voucher_code FROM VOUCHERS;", conn)["voucher_code"].values)
        new_codes = []
        needed_code_count = len(self.working_dataframes["vouchers"])
        counter = 0
        while len(new_codes) != needed_code_count:
            str_counter = str(counter)
            code = "V" + str_counter.zfill(13)
            
            if code not in used_codes:
                new_codes.append(code)
            
            counter += 1
              
        self.working_dataframes["vouchers"]["VoucherCode"] = ""
        self.working_dataframes["vouchers"]["VoucherCode"] = self.working_dataframes["vouchers"]["VoucherCode"].apply(lambda x: new_codes.pop(0))

    def loader(self):
        self.logger.info("Connecting to database...")
        engine = get_engine_for(DatabaseType.LIVE_MYSQL, self.requirements["Customer db name"], username=self.requirements["Database Username"], password=self.requirements["Database password"])  
        conn = get_ansi_connection(engine)
        Session = sessionmaker(bind=engine)
        session = Session()

        self.logger.info("Executing insertions...")
        parent_rows = []
        for index, row in self.working_dataframes["vouchers"].iterrows():
            self.logger.info("Inserting record " + str(index+1))
            parent_rows.append(VOUCHER(voucher_title=row["VoucherTitle"], email=row["Email"], date_created=row["DateCreated"], voucher_balance=row["RemainingValue"], vprogram_id=1, order_id=row["OrderID"], username=row["Username"]))
        
        session.add_all(parent_rows)
        session.flush() # This will synchronise the primary key.
        child_rows = []
        for index, row in self.working_dataframes["vouchers"].iterrows():
            child_rows.append(VOUCHERCREDIT(vprogram_id=1, order_id=row["OrderID"], voucher_id=parent_rows[index].voucher_id, vcredit_amount=row["InitialValue"], vcredit_spent=row["RedeemedValue"], date_added=row["DateCreated"]))

        session.add_all(child_rows)
        session.commit()
        


