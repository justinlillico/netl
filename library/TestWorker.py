from library.base_classes.record_type import RecordType
from time import sleep
import logging
import pandas as pd


class TestWorkerClass(RecordType):
    name = "Test Worker"
    instructions = "Has some dummy workers in it to test."
    record_type_class = True
    step_instructions = {

        "Extract" : "This is going to do some dummy work.",
        "Transform" : "This is going to go Optimum Prime on the data's ass.",
        "Load" : "Lock and..."

    }

    def extractor(self):
        self.logger.info("Creating massive dataframe.")
        dfObj = pd.DataFrame(columns=['User_ID'])
        for i in range(10000):
            if i % 1000 == 0:
                self.logger.info("Now " + str(i) + " in.")
            dfObj = dfObj.append({"User_ID" : i}, ignore_index=True)

        self.logger.info("Adding 1 to every id.")
        for index, row in dfObj.iterrows():
            dfObj.loc[index, "User_ID"] += 1

        self.working_dataframes["Leo"] = dfObj
        self.status["Records"] = 10000
        self.status["Some other data"] = "Wowzer"
        
    def transformer(self):
        self.logger.info("Adding 1 to every id.")
        for index, row in self.working_dataframes["Leo"].iterrows():
            self.working_dataframes["Leo"].loc[index, "User_ID"] += 1

    def loader(self):
        self.logger.info("Adding 1 to every id.")
        for index, row in self.working_dataframes["Leo"].iterrows():
            self.working_dataframes["Leo"].loc[index, "User_ID"] += 1
