# # If the status is trashed, set it to inactive and not visible.

from library.base_classes.record_type import RecordType
from json import loads, dumps
from netoapi.content import ContentApi
from netoapi.product import ProductApi
from netoapi.order import OrderApi
from netoapi.warehouse import WarehouseApi
import pandas as pd
from math import ceil
import numpy as np
import requests
import re
import os


class ShopifyBaseObject(RecordType):
    record_type_class = False
    
    def string_json_object(self, _json):

        if isinstance(_json, list):
            for i in range(len(_json)):
                _json[i] = self.string_json_object(_json[i])
            return _json
        
        if isinstance(_json, dict):
            for k in _json.keys():
                _json[k] = self.string_json_object(_json[k])
            return _json

        _json = str(_json)

        return _json

    
    """
    Will get all pages of an endpoint. Works on non paginated (2019-07) by default. Pass pagenated as true to work in a pagenated way.
    """
    def get_all_json_entries(self, endpoint, url, api_key, password, paginated=False, limit=250, custom_filters={}, log_output=True):
        
        # Get all the customers from the shopify database.
        data = []
        regex = r"(?:\<)(.*?)(?:\>)"

        custom_filters_string = ""

        # Add custom filters.
        if custom_filters:
            for key in custom_filters.keys():
                custom_filters_string += "&" + key + "=" + custom_filters[key]

        while True:
            try:
                count_url = url + "/" + endpoint + "/count.json?" + custom_filters_string if custom_filters_string else url + "/" + endpoint + "/count.json"  
                r = requests.get(count_url, auth=(api_key, password))
                total_to_get = int(r.json()["count"])
                break
            except Exception as e:
                print(e)
                self.logger.error(str(r.text))
                self.logger.info("Retrying...")


        if log_output:
            self.logger.info("Extracting " + str(total_to_get) + " " + endpoint + ".")
        
        if paginated:
            link = url + "/" + endpoint + ".json?limit=" + str(limit)
        else:
            page = 1

        
        while len(data) < total_to_get:

            # Get a new json of information.
            if paginated:
                response = requests.get(link + custom_filters_string, auth=(api_key, password))
                
                # This will cover any issues shopify may have and log the error.
                try:
                    if "errors" in response.json():
                        self.logger.error(response["errors"])
                        self.logger.info("Retrying...")
                        continue # Restart the loop if it errored.
                except Exception as e:
                    self.logger.error(str(response.text))
                    self.logger.info("Retrying...")
                    continue

                if "Link" in response.headers:
                    custom_filters_string = "" # Once we have a link, we have all the filters in the link so this must be nothing.
                    link = re.findall(regex, response.headers['Link'])[-1] # The link to next is the last one.
                response = response.json()

            else:
                response = requests.get(url + "/" + endpoint + ".json?limit=" + str(limit) + "&page=" + str(page) + custom_filters_string, auth=(api_key, password))
                
                try:
                    response = response.json()
                    if "errors" in response:
                        self.logger.error(response["errors"])
                        self.logger.info("Retrying...")
                        continue # Restart the loop if it errored.
                except Exception as e:
                    self.logger.error(str(response.text))
                    self.logger.info("Retrying...")
                    continue

                page += 1


            # Add it to our output.
            try:
                data += response[endpoint]
            except KeyError as e:
                self.logger.error("Response returned: " + str(response) + ". " + str(e))
            
            # Log.
            if log_output:
                self.logger.info(str(len(data)) + " " + endpoint + " extracted.")

        self.status["Total " + endpoint] = len(data)
                
        return data

class ShopifyProducts(ShopifyBaseObject):
    name = "Shopify Products"
    record_type_class = True
    step_instructions = {
        "Extract" : "This will get the products and categories from shopify and put them into neto. Please ensure an 'external_id' and 'external_sku' custom field exists for products before continuing." ,
        "Transform" : "",
        "Load" : ""
    }

    requirements = {
        "Shopify API URL" : "",
        "Shopify API Key" : "",
        "Shopify API Password" : "",
        "Neto API Key" : "",
        "Neto customer URL" : "",
        "FTP Username" : "",
        "FTP Password" : ""
    }

    def extractor(self):
   

        self.logger.info("Fetching all products from Shopify.")
        products = self.get_all_json_entries("products", self.requirements["Shopify API URL"] + "/2019-07", self.requirements["Shopify API Key"], self.requirements["Shopify API Password"], paginated=True)

        self.logger.info("Fetching all collections from Shopify.")
        # Get collections information that exists to add later to what we find on the products.
        smart_collection_data = requests.get(self.requirements["Shopify API URL"] + "/2019-07/smart_collections.json", 
                                       auth=(self.requirements["Shopify API Key"],self.requirements["Shopify API Password"])).json()["smart_collections"]

        
        # Get the custom collections and add those as well.
        custom_collection_data = requests.get(self.requirements["Shopify API URL"] + "/2019-07/custom_collections.json", 
                                       auth=(self.requirements["Shopify API Key"],self.requirements["Shopify API Password"])).json()["custom_collections"]

        if len(custom_collection_data) > 0:
            self.working_dataframes["custom_collections"] = self.json_to_frame(custom_collection_data)
        
        if len(smart_collection_data) > 0:
            self.working_dataframes["smart_collections"] = self.json_to_frame(smart_collection_data)

        self.working_dataframes["products"] = self.json_to_frame(products).fillna("")

        # Get rid of the pesky none's
        self.working_dataframes["products"] = self.working_dataframes["products"].applymap(lambda x: "" if x == "None" else x)

        

        

    def collections_to_categories(self):

        # Create categories table.
        self.working_dataframes["categories"] = self.get_blank_transform_table("content")
        self.working_dataframes["products"]["categories"] = dumps([])

        # Create images table.
        self.working_dataframes["content_images"] = self.get_blank_transform_table("content_ftp_image")

        collections = []
        # Iterate both collections.
        for key in self.working_dataframes.keys():
            if "_collections" in key:
                collections.append(key)

        self.logger.info("Processing collections into categories and mapping products to them.")

        for collection in collections:
            length = str(len(self.working_dataframes[collection]))
            self.logger.info("Working on " + collection)
            for index, row in self.working_dataframes[collection].iterrows():

                self.logger.info("Processing " + row["title"] + " : " + row["id"] + ". Record " + str(index + 1) + " of " + length)

                # Give the products the id to their collections.
                response = self.get_all_json_entries(
                    "products", 
                    self.requirements["Shopify API URL"] + "/2019-07", 
                    self.requirements["Shopify API Key"], 
                    self.requirements["Shopify API Password"], 
                    paginated=True, 
                    custom_filters={"collection_id" : str(row["id"]), "fields" : "id"},
                    log_output=False
                )



                for product in response:
                    location = self.working_dataframes["products"][self.working_dataframes["products"]["id"] == str(product["id"])].index.tolist()[0]
                    l = loads(self.working_dataframes["products"].loc[location, "categories"])
                    l.append(str(row["id"]))
                    self.working_dataframes["products"].loc[location, "categories"] = dumps(l)

                # Just in case some category names are over 100 chars, they may become duplicates. In this case, we will ensure it is unique
                unique_cat_name = []
                def uniquify_cat_name(cat):
                    nonlocal unique_cat_name
                    cat = cat[:100]
                    counter = 1
                    while cat in unique_cat_name:
                        counter_string = str(counter)
                        cat = cat[:-len(counter_string)] + counter_string
                        counter += 1
                    unique_cat_name.append(cat)
                    return cat

                new_cat_name = uniquify_cat_name(row["title"])

                # Convert the collection data to category data.
                cat_row = {

                    "content_name" : new_cat_name, 
                    "content_type" : "Product Category", 
                    "description1" : row["body_html"], 
                    "external_id" : row["id"],
                    "content_url" : row["handle"]

                }

                self.working_dataframes["categories"] = self.working_dataframes["categories"].append(cat_row, ignore_index=True).fillna("")

                # Add this categories images to the images to insert via SFTP table.
                ftp_row = {
                    "content_name" : new_cat_name,
                    "image_url" : row["image.src"]
                }

                self.working_dataframes["content_images"] = self.working_dataframes["content_images"].append(ftp_row, ignore_index=True).fillna("")



  
    def transformer(self):
        # Setup products and variations to match the correct format.

        # So, every product has a parent in shopify. If there is only one product, it has a single variant.
        # We iterate through all the returned data making a single product or extract each variant out in a child/parent relationship.

        self.collections_to_categories()

        self.logger.info("Begining work on products.")
        
        self.working_dataframes["products_and_children"] = self.get_blank_transform_table("products")

        used_skus = []
        used_urls = []

        auth = (self.requirements["Shopify API Key"], self.requirements["Shopify API Password"])

        def get_valid_url(url):
            counter = 1
            new_url = url
            while new_url in used_urls:
                new_url = url + str(counter)
                counter += 1
            used_urls.append(new_url)
            return new_url


        def add_sales_channel(scope, op):
            if scope == "web":
                op["SalesChannels"] = dumps({
                    "SalesChannel" : [{
                        "SalesChannelID" : 9,
                        "IsApproved" : "True"
                    }]
                })
            return op



        def get_valid_sku(variant):
            nonlocal used_skus

            # Remove non alphanumerics and truncate to 15 characters.
            sku = self.remove_non_alphanumeric(str(variant["sku"]))[:15]

            # If there is no SKU, use the ID.
            if not sku:
                sku = str(variant["id"])[:15]
 
            counter = 1

            # While it has been used, append a number to the end.
            while sku.lower() in used_skus:
                l = len(str(counter)) + 1 # Get the length of the counter
                sku = self.remove_non_alphanumeric(str(variant["sku"]))[:15 - l] + "_" + str(counter) # truncate the sku to 15 minus the length of the counter plus the underscore.
                counter += 1 

            used_skus.append(sku.lower())
            return sku

        def get_image_name(index):
            if str(index) == "1":
                return "Main"
            else:
                return "Alt " + str(int(index) - 1)

        def get_specifics(variant, options_json):
            counter = 1
            specs = []

            while "option" + str(counter) in variant and variant["option" + str(counter)] != "None":
                for item in options_json:
                    if str(item["position"]) == str(counter) and variant["option" + str(counter)] in item["values"]:
                        # Ignore default.
                        if item["name"] == "Title" and len(item["values"]) == 1 and item["values"][0] == "Default Title":
                            continue

                        specs.append({
                            "Name" : item["name"],
                            "Value" : variant["option" + str(counter)],
                            "SortOrder" : counter
                        }) 
                counter += 1
            
            return specs

        def get_cost(inventory_id):
            r = None # Needs to be here to avoid killing the application if an exception is thrown.
            # This while loop will prevent any timeouts killing the entire application.
            while True:
                try:
                    r = requests.get(self.requirements["Shopify API URL"] + "/2019-07/inventory_items/" + str(inventory_id)  + ".json", auth=auth)
                    r = r.json()
                    break
                except Exception:
                    self.logger.error(str(r))
                    self.logger.info("Retrying...")
                
            return r['inventory_item']['cost']

        # Highest image count.
        self.status["Highest Image Count"] = 0
        

        for index, row in self.working_dataframes["products"].iterrows():
            self.logger.info("Transforming row " + str(index + 1) + " of " + str(len(self.working_dataframes["products"])))
            # Load the variants and the options jsons.
            variant_json = loads(row["variants"])
            options_json = loads(row["options"])
            
            # If there are multiple variants, create a parent.
            if len(variant_json) == 1:

                # SKU's are limited to 15 characters so we chop it to 
                sku = get_valid_sku(variant_json[0])
                
                # A dictionary to build the entry in the output.
                op = {
                    "AccountingCode" : row["id"], # This is important. We are using the parent ID as an identifier for single variant products so we can reference them against the parent and not the child. This is easier to trace back in shopify
                    "external_id" : variant_json[0]["id"], # Here, we will use the variant ID.
                    "SKU" : sku,
                    "Name" : row["title"],
                    "ItemURL" : "product/" + get_valid_url(row["handle"]),
                    "Description" : row["body_html"],
                    "SearchKeywords" : row["tags"],
                    "Brand" : row["vendor"],
                    "ShippingWeight" : str(float(variant_json[0]["grams"])/1000.0),
                    "DefaultPrice" : variant_json[0]["price"],
                    "WarehouseQuantity" : dumps([{"WarehouseID" : 1, "Quantity" : int(variant_json[0]["inventory_quantity"])}]),
                    "Images" : dumps({"Image" : [{"Name" : get_image_name(image["position"]), "URL" : image["src"]} for i, image in enumerate(loads(row["images"]))]}),
                    "Active" : "True" if row["published_at"] else "False",
                    "external_sku" : variant_json[0]["sku"],
                    "ItemSpecifics" : dumps({ "ItemSpecific" : get_specifics(variant_json[0], options_json)}),
                    "Categories" : row["categories"],
                    "CostPrice" : get_cost(variant_json[0]["inventory_item_id"])
                }

                # Inefficient, yes. But this aint no race.
                image_count = len(loads(op["Images"])["Image"])

                if image_count > int(self.status["Highest Image Count"]):
                    self.status["Highest Image Count"] = str(image_count)

                # If this is only for web, add in salechannel information.
                # Work out sales channel data.
                op = add_sales_channel(row["published_scope"], op)
                

                self.working_dataframes["products_and_children"] = self.working_dataframes["products_and_children"].append(op, ignore_index=True).fillna("")
            else:

                # Create parent.

                op = {
                    "AccountingCode" : row["id"],
                    "external_id" : row["id"],
                    "SKU" : "PT" + row["id"],
                    "Name" : row["title"],
                    "ItemURL" : "product/" + get_valid_url(row["handle"]),
                    "Description" : row["body_html"],
                    "SearchKeywords" : row["tags"],
                    "Brand" : row["vendor"],
                    "Images" : dumps({"Image" : [{"Name" : get_image_name(image["position"]), "URL" : image["src"]} for image in loads(row["images"])]}),
                    "Active" : "True" if row["published_at"] else "False",
                    "Categories" : row["categories"]
                }

                # If this is only for web, add in salechannel information.
                # Work out sales channel data.
                op = add_sales_channel(row["published_scope"], op)

                self.working_dataframes["products_and_children"] = self.working_dataframes["products_and_children"].append(op, ignore_index=True).fillna("")



                # Iterate over the variants...
                for variant in variant_json:

                    # This will map the image id to its source.
                    id_to_src = {}
                    for image in loads(row["images"]):
                        id_to_src[image["id"]] = image["src"]

                    # SKU's are limited to 15 characters so we chop it to 
                    sku = get_valid_sku(variant)

                    op = {
                        "AccountingCode" : variant["id"],
                        "SKU" : sku,
                        "Name" : row["title"],
                        "ItemURL" : "product/" + get_valid_url(sku),
                        "Description" : row["body_html"],
                        "SearchKeywords" : row["tags"],
                        "Brand" : row["vendor"],
                        "ShippingWeight" : str(float(variant["grams"])/1000.0),
                        "DefaultPrice" : variant["price"],
                        "WarehouseQuantity" : dumps([{"WarehouseID" : 1, "Quantity" : int(variant["inventory_quantity"])}]),
                        "ParentSKU" : "PT" + row["id"],
                        "Images" : dumps({"Image" : [{"Name" : "Main", "URL" : id_to_src.get(variant["image_id"], "")}]}),
                        "ItemSpecifics" : dumps({ "ItemSpecific" : get_specifics(variant, options_json)}),
                        "Active" : "True" if row["published_at"] else "False",
                        "external_sku" : variant["sku"],
                        "external_id" : variant["id"],
                        "Categories" : row["categories"],
                        "CostPrice" : get_cost(variant["inventory_item_id"])
                    }

                    # If this is only for web, add in salechannel information.
                    # Work out sales channel data.
                    op = add_sales_channel(row["published_scope"], op)

                    self.working_dataframes["products_and_children"] = self.working_dataframes["products_and_children"].append(op, ignore_index=True).fillna("")
                
        self.working_dataframes["products_and_children"] = self.working_dataframes["products_and_children"].reset_index()

        
    def loader(self):

        self.default_product_category_loader(self.working_dataframes["products_and_children"], 
                                             self.working_dataframes["categories"],
                                             self.requirements["Neto API Key"],
                                             self.requirements["Neto customer URL"]
                                             )

        self.default_category_image_loader(self.working_dataframes["content_images"], self.requirements["Neto customer URL"], self.requirements["FTP Username"], 
                                           self.requirements["FTP Password"], self.requirements["Neto API Key"])


class ShopifyUpdateStockQuantity(ShopifyBaseObject):
    name = "Shopify Product Quantity Updater"
    record_type_class = True
    step_instructions = {
        "Extract" : "",
        "Transform" : "",
        "Load" : ""
    }

    requirements = {
        "Previous Job Number" : ""
    }

    def extractor(self):
        self.logger.info("Loading previous job product table.")
        
        # TODO This is all quite hacky. These tools need to be located elsewhere so they can be used throughout the application.
        # This is copy pasted from the app.
        from sqlite3 import connect
        conn = connect(os.path.join(".", "sqlite", str(self.requirements["Previous Job Number"]) + ".db"))
        self.working_dataframes["products"] = pd.read_sql_query("select * from working_frame_products_and_children;", conn)

        def load_existing(job_number):
            return self.meta_db.Jobs.query.filter_by(id=job_number).first()

        self.requirements = loads(load_existing(self.requirements["Previous Job Number"]).requirements)
            


    def transformer(self):
        
        # Firstly, get all the parents to use for getting the qty for all their variants.
        parents = self.working_dataframes["products"][self.working_dataframes["products"]["ParentSKU"] == ""].reset_index()

        # Clear WarehouseQuantity field.
        self.working_dataframes["products"]["WarehouseQuantity"] = ""

        # Get updated quantity data from shopify 250 at a time.
        url = self.requirements["Shopify API URL"] + "/2019-07/products/"
        url_suffix = "/variants.json"
        auth = (self.requirements["Shopify API Key"], self.requirements["Shopify API Password"])

        l = str(len(parents))

        self.working_dataframes["UnknownVariants"] = pd.DataFrame()
    
        for index, row in parents.iterrows():
            self.logger.info("Processing quantities for all variants per parent. Parent " + str(index + 1) + " of " + l)

            # Have to ensure we do not excede the request limit of the api
            while True:
                r = requests.get(url + row["AccountingCode"] + url_suffix, auth=auth).json()
                if "errors" not in r:
                    break
                else:
                    if r["errors"] != "Exceeded 2 calls per second for api client. Reduce request rates to resume uninterrupted service.":
                        self.logger.error(r)

            
            for variant in r["variants"]:
                filtered = self.working_dataframes["products"][self.working_dataframes["products"]["external_id"] == str(variant["id"])]

                if len(filtered) == 0:
                    self.working_dataframes["UnknownVariants"] = self.working_dataframes["UnknownVariants"].append({"ID" : str(variant["id"]), "Title" : str(variant["title"])}, ignore_index=True)
                    continue

                self.working_dataframes["products"].loc[filtered.iloc[0].name, "WarehouseQuantity"] = dumps(
                    {
                        "WarehouseID" : 1,
                        "Quantity" : variant["inventory_quantity"],
                        "Action" : "Set"
                    }
                )

        # self.logger.info("Dropping redundant data.")
        self.working_dataframes["products"] = self.working_dataframes["products"][["SKU", "WarehouseQuantity"]]

    def loader(self):
        self.default_product_quantity_updater(self.requirements["Neto customer URL"], self.requirements["Neto API Key"], self.working_dataframes["products"])


class ShopifyCustomers(ShopifyBaseObject):
    name = "Shopify Customers"
    record_type_class = True

    requirements = {
        "Shopify API URL" : "",
        "Shopify API Key" : "",
        "Shopify API Password" : "",
        "Neto API Key" : "",
        "Neto customer URL" : "",
        "Customer N Number" : ""
    }

    def extractor(self):

        self.logger.info("Querying shopify for customers.")
        data = self.get_all_json_entries("customers", self.requirements["Shopify API URL"] + "/2019-07", self.requirements["Shopify API Key"], self.requirements["Shopify API Password"])
        self.working_dataframes["Customers"] = self.json_to_frame(data).fillna("").astype(str)

        
    def transformer(self):


        self.working_dataframes["Customers"] = self.working_dataframes["Customers"].rename(columns={

            "id" : "Username",
            "state" : "Active",
            "email" : "EmailAddress",
            "note" : "InternalNotes"
            

        })

        def create_shipping_json(x):
            return dumps({
                    "ShipFirstName" : x["default_address.first_name"],
                    "ShipLastName" : x["default_address.last_name"],
                    "ShipCompany" : x["default_address.company"],
                    "ShipStreetLine1" : x["default_address.address1"],
                    "ShipStreetLine2" : x["default_address.address2"],
                    "ShipCity" : x["default_address.city"],
                    "ShipState" : x["default_address.province_code"],
                    "ShipCountry" : x["default_address.country_code"],
                    "ShipPostCode" : x["default_address.zip"],
                    "ShipPhone" : x["default_address.phone"],
                })

        def create_billing_json(x):
            return dumps({
                    "BillFirstName" : x["first_name"],
                    "BillLastName" : x["last_name"],
                    "BillPhone" : x["phone"],
                })

        # Apply shipping and billing information in correct format.
        self.working_dataframes["Customers"]["ShippingAddress"] = self.working_dataframes["Customers"].apply(create_shipping_json, axis=1)
        self.working_dataframes["Customers"]["BillingAddress"] = self.working_dataframes["Customers"].apply(create_billing_json, axis=1)

        # Active state of customer.
        self.working_dataframes["Customers"]["Active"] = self.working_dataframes["Customers"]["Active"].apply(lambda x: "True" if x == "enabled" else "False")

        # As we cannot update date registered via api, lets produce a csv containing the sql to do an update via db.
        self.downloads["CustomerCreatedUpdates"] = pd.DataFrame()

        self.downloads["CustomerCreatedUpdates"]["Statement"] = self.working_dataframes["Customers"].apply(lambda x: "UPDATE " + self.requirements["Customer N Number"] + "_main.USERS SET registration_date='" + x["created_at"] + "' WHERE username=" + x["Username"] + ";", axis=1)


        self.working_dataframes["Customers"] = self.format_transform_table("customers", self.working_dataframes["Customers"])

    def loader(self):
        self.default_customer_loader(self.requirements["Neto customer URL"], self.requirements["Neto API Key"], self.working_dataframes["Customers"])


class ShopifyOrders(ShopifyBaseObject):
    name = "Shopify Orders"
    record_type_class = True

    requirements = {
        "Shopify API URL" : "",
        "Shopify API Key" : "",
        "Shopify API Password" : "",
        "Neto API Key" : "",
        "Neto customer URL" : "",
        "Client N number" : ""
    }


    def extractor(self):
        self.logger.info("Querying shopify for orders.")
        data = self.get_all_json_entries("orders", self.requirements["Shopify API URL"] + "/2019-07", 
                                         self.requirements["Shopify API Key"], self.requirements["Shopify API Password"])


        self.working_dataframes["Orders"] = self.json_to_frame(data)


    def transformer(self):

        api = ProductApi(self.requirements["Neto customer URL"], self.requirements["Neto API Key"])

        # Debug
        # self.working_dataframes["Orders"] = self.working_dataframes["Orders"].head(5)

        # Create a dataframe for generating SQL to run afterwards.
        sql_name = str(self.job_number) + " SQL to run"
        self.downloads[sql_name] = pd.DataFrame(columns=["Statements"])

        # Copy old frame for reference data.
        self.working_dataframes["OriginalOrders"] = self.working_dataframes["Orders"].copy(deep=True)

        ext_id_to_neto_sku = {}

        query_size = 200
        q = []
        n = []
        # Populate ext_ids this pre-emptively.

        l = len(self.working_dataframes["Orders"])

        for index, row in self.working_dataframes["Orders"].iterrows():
            
            
            j = loads(row["line_items"])
            for item in j:
                q.append(str(item["variant_id"]))
                n.append(str(item["title"]))  
            
            if (index % query_size == 0 and index != 0) or index == l - 1:
                self.logger.info("Getting all variant SKUs for record from " + str(index + 1 - query_size) + " to " + str(index + 1) + " of " + str(l))
                r = api.get_product({"AccountingCode" : q, "OutputSelector" : ["SKU", "AccountingCode"]})
                for item in r["Item"]:
                    ext_id_to_neto_sku[str(item["AccountingCode"])] = item["SKU"]
                r = api.get_product({"Name" : n, "OutputSelector" : ["SKU", "external_id", "Name"]})
                for item in r["Item"]:
                    if str(item["external_id"]) in q and str(item["external_id"]) not in ext_id_to_neto_sku:
                        ext_id_to_neto_sku[str(item["external_id"])] = item["SKU"]
                q.clear()
                n.clear()

        # Create a frame for storing any products that do not exist in Neto for one reason or another for the user to see.
        self.working_dataframes["Products not in Neto"] = pd.DataFrame(columns = {"ID", "Title", "Error"})

        mapped_invalid_sku = False

        def format_line_items(x):

            # A dictionary mapping the external_id to the neto SKU.
            nonlocal ext_id_to_neto_sku, mapped_invalid_sku

            self.logger.info(str(x.name) + " of " + str(l) + " processed.") 

            # Get the line items json and load it.
            line_items = loads(x["line_items"])

            new_line_items = []

            # Iterate through each line item.
            for line_item in line_items:

                # Get the variant ID.
                variant_id = str(line_item["variant_id"])

                # Notes.
                item_notes = ""

                # Default sku
                sku = "PLACEHOLDER_SKU"

                # If there is none for this, we need to generate something to use as an SKU.
                if variant_id == "null" or variant_id == "None" or not variant_id:
                    self.logger.info("Product " + str(variant_id) + " does not have an sku. Applying notes for mapping to Neto.")
                    self.working_dataframes["Products not in Neto"] = self.working_dataframes["Products not in Neto"].append({
                        "ID" : variant_id,
                        "Title" : line_item["title"],
                        "Error" : "No variant ID."
                    }, ignore_index=True)
                    item_notes = "Shopify product " + line_item["title"] + "."
                else:
                    # If there is something valid, we have to go and get the proper sku based on name and ID from neto.
                    if str(variant_id) not in ext_id_to_neto_sku:

                        # Get the response from Neto based on name.
                        r = api.get_product({"AccountingCode" : variant_id, "OutputSelector" : ["SKU"]})

                        # There is also the case the accounting code does not match the id as variant and parent ids are different.
                        # In this case, check for the product by name and then get the one with the same external ID is its there.
                        if "Item" in r and len(r["Item"]) == 0:
                            r = api.get_product({"Name" : line_item["title"], "OutputSelector" : ["external_id"]})

                            if "Item" in r and len(r["Item"]) > 0:
                                r["Item"] = [item for item in r["Item"] if str(item["external_id"]) == str(variant_id)]



                        # If there was a response, iterate through all results and add them to the ext_id_to_neto_sku dict.
                        if "Item" in r and len(r["Item"]) == 1 and "SKU" in r["Item"][0]:
                            ext_id_to_neto_sku[str(variant_id)] = str(r["Item"][0]["SKU"])
                            sku = ext_id_to_neto_sku[variant_id]
                        else:
                            self.logger.info("Neto did not return an SKU for variant ID " + str(variant_id) + ". Mapping to default product and creating notes.")
                            self.working_dataframes["Products not in Neto"] = self.working_dataframes["Products not in Neto"].append({
                                "ID" : variant_id,
                                "Title" : line_item["title"],
                                "Error" : "Not found in Neto."
                            }, ignore_index=True)
                            item_notes = "Shopify product " + line_item["title"] + "."

                    else:
                        # If the varient id is in the ext_id_to_neto_sku dict, use it to set sku.
                        sku = ext_id_to_neto_sku[str(variant_id)]

                # If SKU is placeholder at this point and we have not yet, we need to make something to map it to in Neto.
                if sku == "PLACEHOLDER_SKU" and not mapped_invalid_sku:
                    
                    # If it already exists, we are going to ignore the error. As long as its there, we are good.
                    api.add_product({
                        "SKU" : sku,
                        "Name" : sku,
                        "Description" : "Default sku for shopify migration where there is no valid product on an orderline. Job number " + str(self.job_number)
                    })

                    mapped_invalid_sku = True

                new_line_items.append({
                    "SKU" : sku,
                    "ItemNotes" : item_notes,
                    "ItemDescription" : line_item["title"],
                    "ItemSerialNumber" : "",
                    "Dropshipper":"",
                    "WarehouseName":"",
                    "WarehouseReference":"",
                    "TaxFree": not line_item["taxable"], # Invert this logic.
                    "WarehouseID": 1, # Use the default.
                    "Quantity": line_item["quantity"],
                    "UnitPrice": line_item["price"],
                    "UnitCost":"",
                    "ShippingWeight":"",
                    "DiscountPercent":"",
                    "DiscountAmount": line_item["total_discount"],
                    "Cubic":"",
                    "ExternalSystemIdentifier": str(line_item["id"]),
                    "ExternalOrderReference": x["id"],
                    "ExternalOrderLineReference": str(line_item["id"]),
                    "QuantityShipped" : str(int(line_item["quantity"]) - int(line_item["fulfillable_quantity"]))
                })

            return dumps(new_line_items)


        self.logger.info("Obtaining correct SKU's from Neto based on name and external_id")
        self.working_dataframes["Orders"]["line_items"] = self.working_dataframes["Orders"].apply(format_line_items, axis=1)
        self.status["Non-existant SKUs"] = str(len(self.working_dataframes["Products not in Neto"]))

        # Transform shipping cost.
        def tranform_shipping_cost(x):
            x = loads(x)

            total = 0
            if len(x) > 0:
                for item in x:
                    total += float(item["price"])
                return str(total)

            return ""

        self.working_dataframes["Orders"]["shipping_lines"] = self.working_dataframes["Orders"]["shipping_lines"].apply(tranform_shipping_cost)



        self.working_dataframes["Orders"] = self.working_dataframes["Orders"].rename(columns={

            "id" : "OrderID",
            "created_at" : "DatePlaced",
            "closed_at" : "DateInvoiced",
            "email" : "Email",
            "customer.id" : "Username",
            "customer.note" : "ShipInstructions",
            "billing_address.first_name" : "BillFirstName",
            "billing_address.last_name" : "BillLastName",
            "billing_address.company" : "BillCompany",
            "billing_address.address1" : "BillStreet1",
            "billing_address.address2" : "BillStreet2",
            "billing_address.city" : "BillCity",
            "billing_address.province_code" : "BillState",
            "billing_address.zip" : "BillPostCode",
            "billing_address.phone" : "BillContactPhone",
            "billing_address.country_code" : "BillCountry",
            "shipping_address.first_name" : "ShipFirstName",
            "shipping_address.last_name" : "ShipLastName",
            "shipping_address.company" : "ShipCompany",
            "shipping_address.address1" : "ShipStreet1",
            "shipping_address.address2" : "ShipStreet2",
            "shipping_address.city" : "ShipCity",
            "shipping_address.province_code" : "ShipState",
            "shipping_address.zip" : "ShipPostCode",
            "shipping_address.phone" : "ShipContactPhone",
            "shipping_address.country_code" : "ShipCountry",
            "line_items" : "OrderLine",
            "shipping_lines" : "ShippingCost"




        })

        self.working_dataframes["Orders"]["OrderType"] = "Sales"

        # All orders need to be dispatched.
        self.working_dataframes["Orders"]["OrderStatus"] = "Dispatched"


        # Get the default warehouse address and populate address fields which are blank.
        w_api = WarehouseApi(self.requirements["Neto customer URL"], self.requirements["Neto API Key"])
        warehouse_data = w_api.get_warehouse({"WarehouseID" : 1, "OutputSelector" : ["WarehouseStreet1", "WarehouseStreet2", "WarehouseCity", "WarehouseState", "WarehousePostcode", "WarehouseCountry"]})["Warehouse"][0]


        self.logger.info("Applying default warehouse address to orders without addresses and creating SQL to run after migration.")
        l = str(len(self.working_dataframes["Orders"]))
        for index, row in self.working_dataframes["Orders"].iterrows():

            # Add all the extra stuff the API cannot do.
            self.downloads[sql_name] = self.downloads[sql_name].append({
                    "Statements" : "UPDATE " + self.requirements["Client N number"] + "_main.ORDERS SET coupondiscount=" + str(row["total_discounts"]) + ", subtotal=" + str(row["subtotal_price"]) + ", grand_total=" + str(row["total_price"]) + ", tax=" + str(row["total_tax"]) + " WHERE order_id=" + str(row["OrderID"]) + ";"
                    }, ignore_index=True)


            if not (row["Username"] and row["BillFirstName"] and row["BillLastName"] and row["BillStreet1"] and row["BillCity"] and row["BillState"] and row["BillPostCode"] and row["BillCountry"] and 
                    row["ShipFirstName"] and row["ShipLastName"] and row["ShipStreet1"] and row["ShipCity"] and row["ShipState"] and row["ShipPostCode"] and row["ShipCountry"]):
                self.working_dataframes["Orders"].loc[index, "Username"] = "noreg"
                self.working_dataframes["Orders"].loc[index, "BillStreet1"] = warehouse_data["WarehouseStreet1"]
                self.working_dataframes["Orders"].loc[index, "BillStreet2"] = warehouse_data["WarehouseStreet2"]
                self.working_dataframes["Orders"].loc[index, "BillCity"] = warehouse_data["WarehouseCity"]
                self.working_dataframes["Orders"].loc[index, "BillState"] = warehouse_data["WarehouseState"]
                self.working_dataframes["Orders"].loc[index, "BillPostCode"] = warehouse_data["WarehousePostcode"]
                self.working_dataframes["Orders"].loc[index, "BillCountry"] = warehouse_data["WarehouseCountry"]
                self.working_dataframes["Orders"].loc[index, "BillFirstName"] = "Cash"
                self.working_dataframes["Orders"].loc[index, "BillLastName"] = "Sale"
                self.working_dataframes["Orders"].loc[index, "ShipStreet1"] = warehouse_data["WarehouseStreet1"]
                self.working_dataframes["Orders"].loc[index, "ShipStreet2"] = warehouse_data["WarehouseStreet2"]
                self.working_dataframes["Orders"].loc[index, "ShipCity"] = warehouse_data["WarehouseCity"]
                self.working_dataframes["Orders"].loc[index, "ShipState"] = warehouse_data["WarehouseState"]
                self.working_dataframes["Orders"].loc[index, "ShipPostCode"] = warehouse_data["WarehousePostcode"]
                self.working_dataframes["Orders"].loc[index, "ShipCountry"] = warehouse_data["WarehouseCountry"]
                self.working_dataframes["Orders"].loc[index, "ShipFirstName"] = "Cash"
                self.working_dataframes["Orders"].loc[index, "ShipLastName"] = "Sale"


        self.working_dataframes["Orders"] = self.format_transform_table("orders", self.working_dataframes["Orders"])


    def loader(self):
        self.default_order_loader(self.requirements["Neto customer URL"], self.requirements["Neto API Key"], self.working_dataframes["Orders"])


class ShopifyGiftVouchers(ShopifyBaseObject):
    name = "Shopify Vouchers"
    record_type_class = True

    requirements = {
        "Shopify API URL" : "",
        "Shopify API Key" : "",
        "Shopify API Password" : "",
        "Database Username" : "",
        "Database password" : "",
        "Customer db name" : ""
    }


    def extractor(self):
        self.logger.info("Querying shopify for vouchers.")
        data = self.get_all_json_entries("gift_cards", self.requirements["Shopify API URL"] + "/2019-07", 
                                         self.requirements["Shopify API Key"], self.requirements["Shopify API Password"])