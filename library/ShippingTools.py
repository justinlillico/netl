from library.base_classes.record_type import RecordType
from io import StringIO
import pandas as pd
import os
from datetime import date

file_locations = os.path.join(os.path.dirname(__file__), "ShippingTools")


class EParcelRates(RecordType):
    name = "eParcel Rates"
    instructions = "Provide customer data and this will return the data for the clients site."
    single_method_class = True
    requirements = {
        "Client Postcode" : ""
    }

    def extractor(self):
        # Load definitions.
        area_definitions = pd.read_csv(os.path.join(file_locations, "eparcel", "area_definitions.csv"), dtype=object)
        zone_definitions = pd.read_csv(os.path.join(file_locations, "eparcel", "zone_definitions.csv"), dtype=object)
        area_definitions.fillna("", inplace=True)
        zone_definitions.fillna("", inplace=True)

        # In the area definitions table, find the origin postcode int the 'Postcodes' field and extract the coresponding 'Area' field.
        origin_zone = self.get_origin_area(area_definitions, self.requirements["Client Postcode"])

        new_rows = []

        ## For each row
        for index, row in zone_definitions.iterrows():
            
            ## Get the word from 'destination' and go back to the area defintions table.
            area_key = row["Destination"]
            
            ## Make a temporary dataframe and filter 'Area' to equal 'destination'.
            filtered_area = area_definitions[area_definitions["Zone Name"] == area_key]
            
            ## For each row in temp table.
            for index2, row2 in filtered_area.iterrows():
                
                ### Insert a new row into the output containing this rows Country, Courier, From Post Code and To Post Code.

                ### Take the contents of column x and and set that as the output['Zone Code'] and output['Zone Name'].
                new_row = {
                    "Country" : row2["Country"],
                    "Courier" : row2["Courier"],
                    "From Post Code" : row2["From Post Code"],
                    "To Post Code" : row2["To Post Code"],
                    "Zone Code" : row[origin_zone],
                    "Zone Name" : row[origin_zone]

                }

                new_rows.append(new_row)


        # Produce output file.
        output = pd.DataFrame(new_rows)
        self.downloads[self.requirements["Client Postcode"] + "_output"] = output
        

    def get_origin_area(self, df, origin_postcode):

        origin_postcode = int(origin_postcode)

        for index, row in df.iterrows():
            lower = int(row["From Post Code"])
            upper = int(row["To Post Code"])

            if origin_postcode >= lower and origin_postcode <= upper:
                return row["Zone Code"]

        return ""


class StartrackConverter(RecordType):
    name = "Startrack Converter"
    instructions = "This module will convert a rate file and return road express and fpp rates files."
    single_method_class = True
    step_instructions = {

        "Extract" : "Specify a rates file to perform the convertions on."

    }

    uploads = {

        "rates_file" : pd.DataFrame()

    }

    def extractor(self):
        couriername =f"Startrack {date.today()}*"

        raw_rates_file = self.uploads["rates_file"]
        raw_rates_file.fillna("", inplace=True)

        # Figure out the origin zone. Get the most common origin zone under EXP in service code.
        origin_zone = raw_rates_file[raw_rates_file["Service Code"] == "EXP"]["Origin Zone"].value_counts().index.to_list()[0]


        # Filtering.
        rates_file = raw_rates_file[raw_rates_file["Origin Zone"] == origin_zone]
        rates_file = rates_file[(rates_file["Service Code"] == "LOC") | (rates_file["Service Code"] == "EXP")] # 

        # Change the zonecode LA1 to the origin zone code that is passed as an arg.
        rates_file["Destination Zone"] = rates_file["Destination Zone"].replace("LA1", origin_zone)

        # Output.
        mapping = {
            "Zone Code"	: "Destination Zone",
            "Zone Name"	: "Destination Zone",
            "Minimum Charge" : "Specific Min Charge",
            "1st Parcel" : "Basic Charge",
            "Per Kg" : "Break Rate"
        }

        # Create new dataframe.
        road_express_rates = pd.DataFrame(columns=["Courier Name"])

        # Loop through and apply our map.
        for key in mapping.keys():
            road_express_rates[key] = rates_file[mapping[key]]

        # Add static value column.
        road_express_rates["Courier Name"] = couriername

        self.logger.info("Created express rates file.")
        # Output.
        self.downloads["road_express_rates"] = road_express_rates

        # start with creating your ZONE FILE.
        zones_file = pd.read_csv(os.path.join(file_locations, "startrack", "LOCATION.csv"), dtype=object)
        zones_file = zones_file.fillna("")

        # Filtering
        zones_file = zones_file[zones_file["Servicing City"].str[0] != "Z"]

        #get all zone codes from rates file above.
        rates_zones_code = list(road_express_rates["Zone Code"].values)

        zones_file["zone"] = zones_file.apply(lambda x: x["Onforwarding Zone"] if not x["Direct Zone"] else x["Direct Zone"], axis=1)
        
        zones_file["zone"] = zones_file.apply(lambda x: x["zone"] if x["zone"] in rates_zones_code else x["Onforwarding Zone"], axis=1)

        # Removes zone codes from the zone file that dont exist in your rates file.
        zones_file = zones_file[zones_file.zone.isin(rates_zones_code)]

        # zone maps.
        zonesmap = {
            "Zone Code"	: "zone",
            "Zone Name"	: "zone",
            "City / Suburb":"Suburb Name",
            "From Post Code":"Postcode",
            "To Post Code":"Postcode"
        }

        # Create new dataframe.
        zonesdf = pd.DataFrame(columns=["Courier"])

        # Loop through and apply our map.
        for key in zonesmap.keys():
            zonesdf[key] = zones_file[zonesmap[key]]

        # Add static value column.
        zonesdf["Courier"] = couriername
        zonesdf["Country"] = "AU"
        
        self.logger.info("Created zones file.")
        # ratesdf.
        self.downloads["Zones"] = zonesdf

        ### BEGIN FPP WORK###
        
        # Filter to only keep FPP.
        raw_rates_file = raw_rates_file[raw_rates_file["Service Code"] == "FPP"]
        raw_rates_file = raw_rates_file[raw_rates_file["Destination Zone"] != "ZNA"]

        # Filter out places we are not shipping from.
        raw_rates_file = raw_rates_file[(raw_rates_file["Origin Zone"] == origin_zone) | (raw_rates_file["Origin Zone"] == "***")]

        # Copy the *** row and replace the origin zone with all the zone codes from the rates file.
        for field in rates_zones_code:
            if field not in list(raw_rates_file["Destination Zone"].values):
                row = raw_rates_file.loc[raw_rates_file['Origin Zone'] == "***"].copy()
                row["Origin Zone"] = field
                row["Destination Zone"] = field
                raw_rates_file = raw_rates_file.append(row, ignore_index=True)

        raw_rates_file = raw_rates_file.reset_index()

        fpp_rates_file = pd.DataFrame(columns=list(road_express_rates.columns) + ["Min", "Max"])

        min_max = [["0", "1"], ["1.001", "3"], ["3.001", "5"], ["5.001", "10"], ["10.001", "20"]]
        add_fields = [["Basic Charge"], ["Basic Charge", "Break Rate.1"], ["Basic Charge", "Break Rate.3"], ["Basic Charge", "Break Rate.5"], ["Basic Charge", "Break Rate.7"]]
        

        
        for minmax, fields in zip(min_max, add_fields):
        
            for index, row in raw_rates_file.iterrows():
                if row["Origin Zone"] == "***":
                    continue
                
                    
                new_row = {}
                new_row["Zone Code"] = row["Destination Zone"]
                new_row["Zone Name"] = row["Destination Zone"]
                new_row["Courier Name"] = couriername
                first_parcel = 0
                for num in fields:
                    first_parcel += float(row[num])
                new_row["1st Parcel"] = str(round(first_parcel, 2))
                new_row["Min"] = minmax[0]
                new_row["Max"] = minmax[1]
                fpp_rates_file = fpp_rates_file.append(new_row, ignore_index=True)

                
        self.logger.info("Created FPP file.")
        self.downloads["FPP file"] = fpp_rates_file


class DirectFreight(RecordType):
    name = "Direct Freight Zones & Rates Converter"
    step_instructions = {

        "Extract" : """
            Please provide the data below. Can be either a CSV or an XLSX file.<br>Please ensure the headers are:<br>
            <table style="width:100%">
            <tr>
                <th>Type</th>
                <th>From</th>
                <th>To</th>
                <th>Suburb</th>
                <th>Zone Name</th>
                <th>Zone Code</th>
                <th>City / Suburb</th>
                <th>Basic Charge</th>
                <th>Min Charge</th>
                <th>Rate</th>
            </tr>
            </table>
            <br>
            Order is not important.
        """

    }

    instructions = "This module will take a rates and zones file for direct freight and convert them into neto useable format."
    single_method_class = True

    requirements = {
        "Origin Zone" : "",
        "Courier Name" : "",
        "Maximum" : ""
    }
    uploads = {
        "Rates file" : "",
        "Zones file" : ""
    }

    """
    Little helper that will use the correct pandas load for the filetype provided.
    """
    def load_csv_or_excel(self, filename):
        if ".csv" in filename:
            return pd.read_csv(filename, dtype=str).fillna("")
        else:
            return pd.read_excel(filename, dtype=str).fillna("")

    def get_consignment_zones(self, rates_df, zones_df):
        # Add zone names from zones that are not in rates file.
        to_in_rates = rates_df['ToZone_CityTo_concat'].values
        to_insert = []

        for index, row in zones_df.iterrows():
            if row["Zone Name"] not in to_in_rates and row["Zone Name"] not in to_insert:
                try:
                    float(row["Zone Name"].split(" ")[-1])
                    to_insert.append(row["Zone Name"])
                except ValueError:
                    continue

        return to_insert

    def update_rates_with_zones(self, rates, inserts):
    
        new_rows = []

        for index, row in rates.iterrows():
            for insert in inserts:
                if insert.split(" ")[0] == row["To"]:
                    new_row = row.copy()
                    new_row["finalzone"] = insert
                    new_row["Basic Charge"] = round(float(new_row["Basic Charge"]) + float(insert.split(" ")[-1]), 2)
                    new_rows.append(new_row)

        rates = rates.append(pd.DataFrame(new_rows, columns=rates.columns)).reset_index()
        rates["Per Subsequent Parcel"] = rates.apply(lambda x: x["Basic Charge"] - float(x["finalzone"].split(" ")[-1]) if x["finalzone"].split(" ")[-1].isdigit() else x["Basic Charge"], axis=1)

        return rates


    def extractor(self):
        rates = self.load_csv_or_excel(self.uploads["Rates file"])
        zones = self.load_csv_or_excel(self.uploads["Zones file"])

        # Remove whitespace.
        rates = rates.applymap(lambda x: x.strip() if isinstance(x, str) else x)

        # Remove all rows that have pallet as the Type, dont have bris as the from and dont have a blank cityto.
        rates = rates[rates["Type"] != "PALLET"]
        rates = rates[rates["From"] == self.requirements["Origin Zone"]]
        rates = rates[rates["To"] != ""]

        # Concatenate ToZone with CityTo then remove all excess whitespace.
        rates["ToZone_CityTo_concat"] = rates["To"] + " " + rates["Suburb"]
        rates["ToZone_CityTo_concat"] = rates["ToZone_CityTo_concat"].apply(lambda x: " ".join(x.split()))

        # Get all the concatenated names from the previous step.
        suburb_specific_codes = rates['ToZone_CityTo_concat'].values

        # Populate the Zone name field with the concatenated data from the suburb_specific_rates if it is present in that table. Otherwise, it is zone name.
        zones["Zone Name"] = zones.apply(lambda row: row["Zone Code"] + " " + row["City / Suburb"] if row["Zone Code"] + " " + row["City / Suburb"] in suburb_specific_codes else row["Zone Name"], axis=1)

        # Copy over zone code.
        zones["Zone Code"] = zones["Zone Name"]

        # Create final zone column.
        rates["finalzone"] = rates.apply(lambda row: row["To"] if not row["Suburb"] else row["ToZone_CityTo_concat"], axis=1)

        # Get a list of zones that are not in the rates file that are consigment zones.
        to_insert = self.get_consignment_zones(rates, zones)

        # Add these to rates, adjusting the basic charge as necessary.
        rates = self.update_rates_with_zones(rates, to_insert)

        neto_map = {

            "Zone Code" : "finalzone",
            "Zone Name" : "finalzone",
            "Minimum Charge" : "Min Charge",
            "1st Parcel" : "Basic Charge",
            "Per kg" : "Rate",
            "Per Subsequent Parcel" : "Per Subsequent Parcel"

        }

        # Create output file with courier name field.
        output = pd.DataFrame(columns=["Courier Name"])

        # Loop through and apply our map.
        for key in neto_map.keys():
            output[key] = rates[neto_map[key]]

        # Populate courier name field for all rows.
        output["Courier Name"] = self.requirements["Courier Name"]
        output["Maximum"] = self.requirements["Maximum"]  

        # Output everything.
        self.downloads["Zones File"] = zones
        self.downloads["Rates File"] = output


