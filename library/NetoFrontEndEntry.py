from library.base_classes.record_type import RecordType
from library.base_classes.selenium_utils import NetoSite

class ProductDataFeedSetup(RecordType):
    name = "Product Datafeed Setup"
    record_type_class = True
    instructions = "This function will automatically configure a standard product data export feed for the site in question."
    single_method_class = True

    requirements = {

        "Neto username" : "",
        "Neto password" : "",
        "Site url" : "",
        "Export Schedule" : [
            "Daily 12am",
            "Daily 1am",
            "Daily 2am",
            "Daily 3am",
            "Daily 4am",
            "Daily 5am",
            "Daily 6am",
            "Daily 7am",
            "Daily 8am",
            "Daily 9am",
            "Weekly - Sunday 1am"
        ]

    }

    def extractor(self):
        ns = NetoSite(self.requirements["Site url"], self.requirements["Neto username"], self.requirements["Neto password"])
        ns.login()
        ns.navigate_to(ns.url + "_cpanel/dsexport/view?id=New")
        ns.select_from_dropdown_by_name("export_module", "Inventory Export")
        ns.select_from_dropdown_by_name("sub_type", "None")
        ns.select_from_dropdown_by_name("active", "Active") 
        ns.enter_text_by_name("export_name", "netl_product_data_feed") 
        ns.enter_text_by_name("export_description", "This is an automatically generated feed using netl for site " + ns.url + ".")
        ns.select_from_dropdown_by_name("export_group_id", "")
        ns.enter_text_by_name("export_filename", "product.csv")
        ns.save_and_continue()
        ns.select_from_dropdown_by_name("export_method", "Local Server (Specify folder on local server where file will reside below)")
        ns.enter_text_by_name("export_location", "netl")
        ns.select_from_dropdown_by_name("schedule_id", self.requirements["Export Schedule"])
        ns.enter_text_by_name("export_file_header", '"SKU","Parent SKU","Name","Brand","Description","Specifications","Features","Specifics","Sort Order 1","Sort Order 2","Short Description","Length (m)","Width (m)","Height (m)","Weight (kg)","Cubic","Tax Inclusive","Tax Free Product","Shipping Category","Upsell","Cross-sell","Main Image","Alt 1","Alt 2","Alt 3","Alt 4","Alt 5","Alt 6","Alt 7","Alt 8","Alt 9","Alt 10","Extra Options","Kitting","UPC","Supplier Item Code","RRP","Cost Price","Price A","Price B","Price C","Price D"\n')
        ns.enter_text_by_name("export_body", '"[@SKU@]","[@parent_sku@]","[@model@]","[@brand@]","[%format type:\'text\' noeol:\'1\' %][@description@][%end format%]","[%format type:\'text\' noeol:\'1\' %][@specifications@][%end format%]","[%format type:\'text\' noeol:\'1\' %][@features@][%end format%]","[@ebay_specifics@]","[@sortorder@]","[@sortorder2@]","[%format type:\'text\' noeol:\'1\' %][@short_description@][%end format%]","[@itm_length@]","[@itm_width@]","[@itm_height@]","[@weight@]","[@cubic@]","[@tax_inc@]","[@notax@]","[@sh_type_name@]","[@upsell_products@]","[@crosssell_products@]","[@approved_pos@]","[@image_full@]","[@thumb_full_1@]","[@thumb_full_2@]","[@thumb_full_3@]","[@thumb_full_4@]","[@thumb_full_5@]","[@thumb_full_6@]","[@thumb_full_7@]","[@thumb_full_8@]","[@thumb_full_9@]","[@thumb_full_10@]","[@extra@]","[@bundles@]","[@upc@]","[@supplier_code@]","[@retail@]","[@cost@]","[@price_1@]","[@price_3@]","[@price_4@]","[@price_5@]","[@price_6@]"\n')
        ns.save_and_continue()

