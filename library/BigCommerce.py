# This file should contain all RecordTypes for big commerce.
from library.base_classes.record_type import RecordType
import pandas as pd
import requests
import json
from netoapi.content import ContentApi

#TODO Remove logic for BigCommerce API and make a library for it. There is currently bugger all for python v3 (though this is all V2)
#TODO Have some kind of class variable determine 
#TODO Error handling... Common error type that gets raised to flask and displayed?
#TODO Serialise RecordType.

class BigCommerceCommon(RecordType):

    url_endpoint = ""

    requirements = {
        "x-auth-token": "",
        "x-auth-client": "",
        "store key" : ""
    }

    api_version = "v3" # default.

    """
    This will return all pages of an api endpoint as a dataframe.
    """
    def big_commerce_api_to_dataframe(self, endpoint):
        key = self.requirements["store key"]
        url = f"https://api.bigcommerce.com/stores/{key}/{self.api_version}/{endpoint}"
        limit = 250
        page = 1

        headers = {k: self.requirements[k] for k in ('x-auth-token', 'x-auth-client')}
        headers['accept'] = "application/json"
        headers['content-type'] = "application/json"

        output = []
        while True:

            # Query the correct page.
            response = requests.request("GET", url + "?page=" + str(page) + "&limit=" + str(limit), headers=headers)
            json_response = json.loads(response.text)

            # If this is v2, a different approach is needed.
            if self.api_version.lower() == "v2":
                output += json_response
                if len(json_response) == limit:
                    page += 1
                    continue
                else:
                    break

            # If none on the page, break.
            if json_response["meta"]["pagination"]["count"] == 0:
                break
            
            # There is something to add so add it.
            output += json_response["data"]

            # If the number of records on this page is less than the limit, this was out last page. Else, add one to the pages to check.
            if int(json_response["meta"]["pagination"]["count"]) < limit:
                break
            else:
                page += 1

        

        return self.json_to_frame(output)

    def extractor(self):
        self.working_dataframes["BigCommerceResponse"] = self.big_commerce_api_to_dataframe(self.url_endpoint)
        self.store_dataframes()

class BigCommerceWishlists(BigCommerceCommon):
    name = "Big Commerce Wishlists"
    record_type_class = True

    requirements = {
        "x-auth-token": "",
        "x-auth-client": "",
        "store key" : "",
        "Database name" : "",
        "Database Username" : "",
        "Database Password" : ""
    }

    def extractor(self):
        self.working_dataframes["Wishlists"] = self.big_commerce_api_to_dataframe("wishlists")
        self.working_dataframes["Products"] = self.big_commerce_api_to_dataframe("catalog/products")
        self.store_dataframes()

    def transformer(self):
        # Reformat wishlists to Neto standard.
        self.working_dataframes["Wishlists"]["is_public"] = self.working_dataframes["Wishlists"]["is_public"].apply(lambda x: "y" if x == "True" else "n")

        sku_to_id = {}
        for index, row in self.working_dataframes["Products"].iterrows():
            sku_to_id[row["id"]] = row["sku"]

        def process_products(x):

            items = json.loads(x)
            output = []
            for item in items:
                output.append(sku_to_id[item["product_id"]])
            return output

        self.working_dataframes["Wishlists"]["items"] = self.working_dataframes["Wishlists"]["items"].apply(process_products)

        # Create wishlist items.
        self.working_dataframes["Wishlist Items"] = self.get_blank_transform_table("WISHLIST_ITEMS")

        for index, row in self.working_dataframes["Wishlists"].iterrows():
            
            for item in row["items"]:
                self.working_dataframes["Wishlist Items"] = self.working_dataframes["Wishlist Items"].append({"wishlist_id" : row["id"], "SKU" : item}, ignore_index=True)
            
        self.working_dataframes["Wishlist Items"].reset_index()


        self.working_dataframes["Wishlists"] = self.working_dataframes["Wishlists"].rename(columns={
            "name" : "wishlist_name",
            "customer_id" : "username",
            "is_public" : "wishlist_private"
        })

        self.working_dataframes["Wishlists"] = self.format_transform_table("WISHLISTS", self.working_dataframes["Wishlists"])

    def loader(self):
        self.default_wishlist_loader(self.working_dataframes["Wishlists"], self.working_dataframes["Wishlist Items"], self.requirements["Database name"],
                                     self.requirements["Database Username"], self.requirements["Database Password"])


class BigCommerceBlogs(BigCommerceCommon):

    name = "Big Commerce Blogs"
    url_endpoint = "blog/posts"
    record_type_class = True
    api_version = "v2"

    requirements = {
        "x-auth-token": "",
        "x-auth-client": "",
        "store key" : "",
        "Content Name (on neto site)": "",
        "Neto API Key" : "",
        "Neto customer URL" : ""
    }

    def transformer(self):
        self.working_dataframes["BigCommerceResponse"]["ContentType"] = self.requirements["Content Name (on neto site)"]
        self.working_dataframes["BigCommerceResponse"] = self.working_dataframes["BigCommerceResponse"].rename(columns={"title": "ContentName", "summary": "ShortDescription1", "body" : "Description1", "url": "ContentURL", "is_published": "Active", "published_date.date": "DatePosted", "author": "Author", "meta_description": "SEOMetaDescription", "meta_keywords": "SEOMetaKeywords"})
        self.working_dataframes["BigCommerceResponse"]["ContentURL"] = self.working_dataframes["BigCommerceResponse"]["ContentURL"].apply(lambda x: x[1:] if x[0] == "/" else x)
        self.store_dataframes()

    def loader(self):

        api = ContentApi(self.requirements["Neto customer URL"], self.requirements["Neto API Key"])

        # Get a list of all the columns in the transformed table.
        keys = list(self.working_dataframes["BigCommerceResponse"].columns)

        # List for storing responses.
        responses = []
        # Iterate over the table and convert each row to dictionaries for the post.
        for index, row in self.working_dataframes["BigCommerceResponse"].iterrows():
            this_entry = {}
            for key in keys:
                this_entry[key] = row[key]
            responses.append(api.add_content(this_entry))

        # Return the response.
        return responses
          

class BigCommerceCustomerOrders(BigCommerceCommon):

    name = "Big Commerce Customer Orders"
    url_endpoint = "orders"
    record_type_class = True

    def loader(self):
        pass
    
    def transformer(self):
        pass
