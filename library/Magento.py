from library.base_classes.record_type import RecordType
from json import loads, dumps
from netoapi.content import ContentApi


"""
Need to document this sql...

It gets the products into the correct format for this.

-- PRODUCTS

SELECT 

p.id as 'DOKO ID',
p.sku as 'SKU',
p.inventory_id as 'InventoryID',
p.parent_sku as 'ParentSKU',
p.accounting_code as 'AccountingCode',
p.quantity as 'WarehouseQuantity',
p.virtual as 'Virtual',
p.brand as 'Brand',
p.name as 'Name',
p.model as 'Model',
p.sort_order1 as 'SortOrder1',
p.rrp as 'RRP',
p.default_price as 'DefaultPrice',
p.promotion_price as 'PromotionPrice',
p.promotion_start_date as 'PromotionStartDate',
p.promotion_expiry_date as 'PromotionExpiryDate',
p.cost_price as 'CostPrice',
p.approved as 'Approved', 
p.approved_for_pos as 'ApprovedForPOS',
p.active as 'Active', 
p.visible as 'Visible',
p.tax_free_item as 'TaxFreeItem',
p.search_keywords as 'SearchKeywords',
p.short_description as 'ShortDescription',
p.description as 'Description',
p.terms_and_conditions as 'TermsAndConditions',
p.features as 'Features',
p.specifications as 'Specifications',
p.warranty as 'Warranty',
p.artist_or_author as 'ArtistOrAuthor',
p.sub_title as 'Subtitle',
p.availability_description as 'AvailabilityDescription',
p.product_url as 'ItemURL',
p.upc as 'UPC',
p.item_height as 'ItemHeight',
p.item_length as 'ItemLength',
p.item_width as 'ItemWidth',
p.shipping_height as 'ShippingHeight',
p.shipping_width as 'ShippingWidth',
p.shipping_length as 'ShippingLength',
p.supplier_item_code as 'SupplierItemCode',
p.seo_page_title as 'SEOPageTitle',
p.seo_meta_keywords as 'SEOMetaKeywords',
p.seo_page_heading as 'SEOPageHeading',
p.seo_meta_description as 'SEOMetaDescription',
p.external_id


FROM product p;

-- CONTENT RELATIONSHIPS.

select p.id, ccp.category_id as 'external_id' from product p inner join catalog_category_product ccp on p.external_id=ccp.product_id;

"""

class MagentoProductsCategoriesNoApi(RecordType):
    name = "Magento Products & Categories No Api"
    record_type_class = True

    requirements = {
        "Neto API Key" : "",
        "Neto customer URL" : "",
    }

    uploads = {
        "Content File" : "",
        "Product File" : "",
        "Product Images" : "",
        "Product Specifics" : "",
        "Product/Content Relationships" : ""
    }

    instructions = """
    
    This tool is designed to take a DOKO transformation and load it into Neto via the API.

    """

    def extractor(self):
        self.working_dataframes["categories"] = self.uploads["Content File"]
        self.working_dataframes["products"] = self.uploads["Product File"]

        # If there are no specifics, use an empty table.
        if "Product Specifics" in self.uploads:
            self.working_dataframes["product_specifics"] = self.uploads["Product Specifics"]
        else:
            from pandas import DataFrame
            self.working_dataframes["product_specifics"] = DataFrame(columns=["id", "product_id", "specific_name", "specific_value"])

        self.working_dataframes["product_images"] = self.uploads["Product Images"]
        self.working_dataframes["content_relationships"] = self.uploads["Product/Content Relationships"]

    def transformer(self):
        self.logger.info("Renaming fields.")
        # Rename ID to external id for categories
        self.working_dataframes["categories"] = self.working_dataframes["categories"].rename(columns={"parent_id" : "external_parent_id"})

        self.logger.info("Setting empty SKUs, accounting codes and names to use external id.")
        # For any products without SKU, accounting code and name, use external id.
        self.working_dataframes["products"]["SKU"] = self.working_dataframes["products"].apply(lambda x: x["SKU"] if x["SKU"] else x["external_id"], axis=1)
        self.working_dataframes["products"]["AccountingCode"] = self.working_dataframes["products"].apply(lambda x: x["AccountingCode"] if x["AccountingCode"] else x["external_id"], axis=1)
        self.working_dataframes["products"]["Name"] = self.working_dataframes["products"].apply(lambda x: x["Name"] if x["Name"] else x["external_id"], axis=1)

        self.logger.info("Setting 'trashed' items to inactive.")
        # When importing from db using DOKO scripts, it imports 'trashed' items as well. We will import these, but set their active status to false.
        self.working_dataframes["products"]["Active"] = self.working_dataframes["products"].apply(lambda x: 0 if "__trashed" in x["ItemURL"] else x["Active"], axis=1)

        self.logger.info("Mapping images, specifics and categories to products.")

        # Insert images, specifics and categories to cell.
        self.working_dataframes["products"]["Images"] = ""
        self.working_dataframes["products"]["ItemSpecifics"] = ""
        self.working_dataframes["products"]["Categories"] = ""

        # Clean the ItemURL
        self.working_dataframes["products"]["ItemURL"] = self.working_dataframes["products"]["ItemURL"].apply(lambda x: x.replace("index.php/", ""))

        # Warehouses.
        self.working_dataframes["products"]["WarehouseQuantity"] = self.working_dataframes["products"]["WarehouseQuantity"].apply(lambda x: dumps([{"WarehouseID" : 1, "Quantity" : x}]))

        self.working_dataframes["products"]["SKU"] = self.working_dataframes["products"]["SKU"].apply(lambda x: x[:15])


        l = str(len(self.working_dataframes["products"]))
        for index, row in self.working_dataframes["products"].iterrows():
            doko_id = row["DOKO ID"]
            filtered_images = self.working_dataframes["product_images"][self.working_dataframes["product_images"]["product_id"] == doko_id]
            filtered_specifics = self.working_dataframes["product_specifics"][self.working_dataframes["product_specifics"]["product_id"] == doko_id]
            filtered_categories = self.working_dataframes["content_relationships"][self.working_dataframes["content_relationships"]["id"] == doko_id]

            image = []
            item_specific = []
            categories = []

            for index2, row2 in filtered_images.iterrows():
                image.append({
                    "Name" : filtered_images.loc[index2, "name"],
                    "URL" : filtered_images.loc[index2, "url"]
                })

            for index2, row2 in filtered_specifics.iterrows():
                item_specific.append({
                    "Name": filtered_specifics.loc[index2, "specific_name"],
                    "Value": filtered_specifics.loc[index2, "specific_value"]
                })

            for index2, row2 in filtered_categories.iterrows():
                categories.append(int(filtered_categories.loc[index2, "external_id"]))
            
            self.working_dataframes["products"].at[index, "Images"] = dumps({ "Image" : image })
            self.working_dataframes["products"].at[index, "ItemSpecifics"] = dumps({ "ItemSpecific" : item_specific })
            self.working_dataframes["products"].at[index, "Categories"] = dumps(categories)

            # Drop what we do not need.
            self.working_dataframes["products"] = self.format_transform_table("products", self.working_dataframes["products"])

    def loader(self):
        self.default_product_category_loader(self.working_dataframes["products"], self.working_dataframes["categories"], self.requirements["Neto API Key"], self.requirements["Neto customer URL"])



"""
Get doko customers

SELECT 

id,
username as 'Username',
email_address as 'EmailAddress',
secondary_email_address as 'SecondaryEmailAddress',
gender as 'Gender',
date_of_birth as 'DateOfBirth',
internal_notes as 'InternalNotes',
abn as 'ABN',
website_url as 'WebsiteURL',
active as 'Active',
user_group as 'UserGroup',
bill_first_name as 'BillFirstName',
bill_last_name as 'BillLastName',
bill_company as 'BillCompany',
bill_street_line1 as 'BillStreetLine1',
bill_street_line2 as 'BillStreetLine2',
bill_city as 'BillCity',
bill_state as 'BillState',
bill_post_code as 'BillPostCode',
bill_country as 'BillCountry',
bill_phone as 'BillPhone',
bill_fax as 'BillFax'

 FROM customer;
 

 Get doko shipping data
SELECT 

customer_id,
ship_title as 'ShipTitle',
ship_first_name as 'ShipFirstName',
ship_last_name as 'ShipLastName',
ship_company as 'ShipCompany',
ship_street_line1 as 'ShipStreetLine1',
ship_street_line2 as 'ShipStreetLine2',
ship_city as 'ShipCity',
ship_state as 'ShipState',
ship_post_code as 'ShipPostCode',
ship_phone as 'ShipPhone',
ship_fax as 'ShipFax'


 FROM customer_shipping_address;
"""


class MagentoCustomersNoApi(RecordType):
    name = "Magento Customers No Api"
    record_type_class = True

    requirements = {
        "Neto API Key" : "",
        "Neto customer URL" : "",
    }

    uploads = {
        "Customers File" : "",
        "Customers Address File" : ""
    }

    instructions = """
    
    This tool will manually transform DOKO customers and upload them to Neto.

    """

    def extractor(self):
        self.working_dataframes["customers"] = self.uploads["Customers File"]
        self.working_dataframes["customers_shipping"] = self.uploads["Customers Address File"]

    def transformer(self):
        # Process shipping data.

        bill_cols = [item for item in list(self.working_dataframes["customers"].columns) if item[0:4] == "Bill"]

        def billing_address(x):
            json = {}

            for col in bill_cols:
                json[col] = x[col]

            return dumps(json)

        self.working_dataframes["customers"]["BillingAddress"] = self.working_dataframes["customers"].apply(billing_address, axis=1)

        self.working_dataframes["customers"]["ShippingAddress"] = ""

        shipping_cols = [item for item in list(self.working_dataframes["customers_shipping"].columns) if item[0:4] == "Ship"]

        for index, row in self.working_dataframes["customers"].iterrows():
            id = row["id"]

            # NOTE: It seems there is only the ability to add one shipping address via API so this is only going to upload the first.
            shipping_address = self.working_dataframes["customers_shipping"][self.working_dataframes["customers_shipping"]["customer_id"] == id].reset_index()

            json = {item : shipping_address.loc[0, item] for item in shipping_cols}

            self.working_dataframes["customers"].at[index, "ShippingAddress"] = dumps(json)

    def loader(self):
        self.default_customer_loader(self.requirements["Neto customer URL"], self.requirements["Neto API Key"], self.working_dataframes["customers"])
