import petl
from netools.database import DatabaseType, get_engine_for, get_ansi_connection
from utils import parse_sql
import argparse
import os


def extract(db_from, schema):
    engine = get_engine_for(DatabaseType.MYSQL, schema)
    con = get_ansi_connection(engine)
    _dir = os.path.dirname(os.path.realpath(__file__))

    with open(os.path.join(_dir, "sql", db_from + "_extract.sql"), "r") as sql:
        sql = sql.readlines()
    
    queries = parse_sql(sql)

    for query_name in queries.keys():
        table = petl.fromdb(con, queries[query_name])
        petl.tocsv(table, './' + query_name + ".csv")

    print("Done!")




if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Extract data required for migration into csv')
    parser.add_argument('db_from', choices=['magento'], help='The previous system it is from.')
    parser.add_argument('schema', help='The schema name we are getting the data from.')

    args = parser.parse_args()

    extract(args.db_from, args.schema)
    