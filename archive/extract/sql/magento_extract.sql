-- metaname: category_entity
SELECT
*
FROM catalog_category_entity entity

-- JOINS
LEFT JOIN catalog_category_entity_varchar _varchar ON _varchar.entity_id = entity.entity_id


-- metaname: category_attribute
SELECT
*
FROM 
eav_attribute;


--- ***
--- PRODUCTS
--- ***

catalog_product_entity
catalog_product_entity_varchar
catalog_product_entity_text
cataloginventory_stock_item
catalog_product_entity_decimal
catalog_product_entity_datetime
catalog_product_entity_int
catalog_product_relation
mg_catalog_category_product