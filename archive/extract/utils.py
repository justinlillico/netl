

def parse_sql(text):
    output = {}
    query = ""
    query_name = ""
    read = False
    for line in text:
        if "-- metaname:" in line:
            query_name = line.replace("-- metaname: ", "").replace("\n", "")

        if "SELECT" in line and query_name:
            read = True

        if ";" in line and read:
            query += line
            output[query_name] = query
            query = ""
            query_name = False
            read = False
        
        if read:
            query += " " + line

    return output